#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""


from collections import OrderedDict
from struct      import *

import sx1262

from node import Node



################################################################################

class SingleTxNode(Node):
  def __init__(self, radioConfig, position=(0, 0), nodeId=None):
    super().__init__(radioConfig=radioConfig, position=position, nodeId=nodeId)

  def init(self):
    self.radioTx(payload='Test from N{}'.format(self._nodeIdSim).encode('utf-8'))

  def txDoneCallback(self):
    self.print('TxDone!')


################################################################################

class PeriodicTxNode(Node):
  def __init__(self, radioConfig, position=(0, 0), period=5.0, startOffset=0.0, nodeId=None):
    super().__init__(radioConfig=radioConfig, position=position, nodeId=nodeId)
    self.counter     = 0
    self.period      = period
    self.startOffset = startOffset

  def init(self):
    self.timerStart(timerId=0, timeout=self.startOffset)

  def timerCallback(self, timerId):
    self.print('timerCallback (count={})'.format(self.counter))
    self.counter += 1
    self.radioTx(
      payload = 'test'.encode('utf-8')
      # payload = pack('10s', b'test')
    )
    self.timerStart(
      timerId = 0,
      timeout = self.period,
    )

  def txDoneCallback(self):
    self.print('TxDone!')

################################################################################


class SingleRxNode(Node):
  def __init__(self, radioConfig, duration, position=(0, 0), nodeId=None):
    super().__init__(radioConfig=radioConfig, position=position, nodeId=nodeId)
    self.duration = duration

  def init(self):
    self.radioRx(timeout=self.duration)

  def rxDoneCallback(self, payload, crc, rssi=None, snr=None):
    self.print('RxDone, payload="{}"'.format(payload))

  def rxTimeoutCallback(self):
    self.print('RxTimeout callback')

################################################################################


class ContinuousRxNode(Node):
  def __init__(self, radioConfig, position=(0, 0), nodeId=None):
    super().__init__(radioConfig=radioConfig, position=position, nodeId=nodeId)
    self.counter = 0

  def init(self):
    self.radioRx(timeout=3.0)

  def rxDoneCallback(self, payload, crc, rssi=None, snr=None):
    # self.counter += 1
    self.print('RxDone, payload="{}"'.format(payload.decode('utf-8')))
    self.radioRx(timeout=3.0)

  def rxTimeoutCallback(self):
    self.counter += 1
    if self.counter >= 2:
      self.radioConfig.frequency = 868300000
    self.print('RxTimeout callback')
    self.radioRx(timeout=3.0)

################################################################################


if __name__ == '__main__':
  pass
