#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""


import pickle
import itertools
import os
import numpy    as np
import pandas   as pd
import networkx as nx
from copy        import copy, deepcopy
from collections import OrderedDict

import sx1262

import events

np.seterr('raise')

################################################################################
PLOT_HEIGHT = 600


################################################################################
# Statistics class
################################################################################

class Statistics(object):
  def __init__(self, sim):
    self._sim                    = sim
    self._tx                     = OrderedDict()  # txNodeId -> list(dict(ts, msg, rxNodeId, pathlossMatrix))
    self._rx                     = OrderedDict()  # rxNodeId -> list(dict(ts, msg, txNodeId))
    self._registeredNodesHost    = OrderedDict()  # registered nodes on host; roundNo -> (rcIdx -> nodeIdList)
    self._registeredNodesClients = list()         # registered nodes on client; list of tuples (ts, nodeId, rcIdx)
    self._nodeIdList             = None
    self._hostNodeId             = OrderedDict()
    self._roundBoundaries        = None
    self._energyPerLsrRoundDf    = None
    self._thinningStatus         = list()
    self._selectedPaths          = list()
    self._graphInfo              = list()

  ##############################################################################
  # Collection
  ##############################################################################

  def addSentMsg(self, msg, txNodeId, rxNodeId):
    if txNodeId not in self._tx:
      self._tx[txNodeId] = []

    self._tx[txNodeId].append({'ts': self._sim.t, 'msg': msg, 'rxNodeId': rxNodeId, 'pathlossMatrix': self._sim._pathlossMatrix})

  def addReceivedMsg(self, msg, txNodeId, rxNodeId):
    if rxNodeId not in self._rx:
      self._rx[rxNodeId] = []

    self._rx[rxNodeId].append({'ts': self._sim.t, 'msg': msg, 'txNodeId': txNodeId})

  def addRegisteredNodesHost(self, registeredNodes, roundNo):
    """
    Args:
      registeredNodes: dict: rcIdx -> list of registered nodes
      roundNo:
    """
    self._registeredNodesHost[roundNo] = deepcopy(registeredNodes)

  def addRegisteredNodeClient(self, nodeId, rcIdx):
    # get sim time to determine roundIdx in post-processing (client nodes do not
    # keep track of global roundIdx and we cannot reliably get roundIdx from
    # host in post state as host may have already updated it for the coming round)
    ts = self._sim.t
    self._registeredNodesClients.append((ts, nodeId, rcIdx))

  def addGraphInfo(self, hopDistanceMap, edges, roundIdx, rcIdx):
    self._graphInfo.append((hopDistanceMap, deepcopy(edges), deepcopy(roundIdx), rcIdx))

  def addThinningStatusHost(self, status, roundIdx, rcIdx):
    """Collect stats of thinning status (only works if full thinning is applied)
    """
    self._thinningStatus.append((status, roundIdx, rcIdx))

  def addSelectedPath(self, selectedPath, nodeId, roundIdx, rcIdx):
    """Collect selected path (only works if full thinning is applied)
    """
    self._selectedPaths.append((selectedPath, nodeId, roundIdx, rcIdx))

  def setNodeIdList(self, nodeIdList):
    self._nodeIdList = sorted(nodeIdList)

  def addHostNodeId(self, hostNodeId):
    self._hostNodeId[self._sim.t] = hostNodeId

  ##############################################################################
  # Evaluation
  ##############################################################################

  def evalRegistrationHost(self):
    """Determine for which rcIdx each node was registered in every round (based on data from host)
    """
    self.checkDataAvailable()

    regList = []
    for roundIdx, d in self._registeredNodesHost.items():
      # skip last potentially incomplete round
      if roundIdx > self.lastRoundIdx:
        continue

      for nodeId in self._nodeIdList:
        # skip host node
        if nodeId == self.hostNodeId:
          continue

        rcIdxRes = None  # default in case node is not registered with any rcIdx in current round
        for rcIdx, rc in enumerate(self.radioConfigsSim):
          if rcIdx in d and nodeId in d[rcIdx]:
            if rcIdxRes is not None:
              raise Exception('Node seems to be registered for more than 1 rcIdx!')
            rcIdxRes = rcIdx
        regList.append({
          'nodeId':   nodeId,
          'roundIdx': roundIdx,
          'rcIdx':    rcIdxRes,
        })

    return pd.DataFrame(regList, dtype=pd.Int64Dtype())

  def evalRegistrationClients(self):
    """Determine for which rcIdx each node was registered in every round (based on data from clients)
    """
    self.checkDataAvailable()

    regList = []
    for elem in self._registeredNodesClients:
      ts, nodeId, rcIdx = elem
      roundIdx          = self.ts2RoundIdx(ts)

      # skip last potentially incomplete round
      if roundIdx > self.lastRoundIdx:
        continue

      regList.append({
        'nodeId':   nodeId,
        'roundIdx': roundIdx,
        'rcIdx':    rcIdx,
      })

    return pd.DataFrame(regList, dtype=pd.Int64Dtype())

  def evalGraphInfo(self):
    """Get graph info (based on data from host)
    """
    self.checkDataAvailable()
    if not len(self._graphInfo):
      raise Exception('graphInfo data missing!')

    tmpList = []
    for elem in self._graphInfo:
      hopDistanceMap, edges, roundIdx, rcIdx = elem

      # skip last potentially incomplete round
      if roundIdx > self.lastRoundIdx:
        continue

      tmpList.append({
        'hopDistanceMap': hopDistanceMap,
        'edges':          edges,
        'roundIdx':       roundIdx,
        'rcIdx':          rcIdx,
      })

    return pd.DataFrame(tmpList)

  def evalThinningStatus(self):
    """Get thinning state (based on data from host)
    """
    self.checkDataAvailable()
    if not len(self._thinningStatus):
      raise Exception('thinningStatus data missing!')

    tmpList = []
    for elem in self._thinningStatus:
      status, roundIdx, rcIdx = elem

      # skip last potentially incomplete round
      if roundIdx > self.lastRoundIdx:
        continue

      tmpList.append({
        'status':   status,
        'roundIdx': roundIdx,
        'rcIdx':    rcIdx,
      })

    return pd.DataFrame(tmpList)

  def evalSelectedPaths(self):
    """Get selected paths (based on data from host)
    """
    self.checkDataAvailable()
    if not len(self._selectedPaths):
      raise Exception('thinningStatus data missing!')

    tmpList = []
    for elem in self._selectedPaths:
      selectedPath, nodeId, roundIdx, rcIdx = elem
      if rcIdx is None:
        rcIdx = pd.NA

      # skip last potentially incomplete round
      if roundIdx > self.lastRoundIdx:
        continue

      tmpList.append({
        'selectedPath': selectedPath,
        'nodeId':       nodeId,
        'roundIdx':     roundIdx,
        'rcIdx':        rcIdx,
      })

    return pd.DataFrame(tmpList)

  def evalReliability(self):
    """ Determine number of Tx and Rx packets as well as the reliability (Rx/Tx) for every roundIdx and every nodeId
        Should help to answer the question "How many of the assigned data slots can successfully be used to transfer data from the client nodes to the host node?"
        NOTE: data ACK and retransmissions of failed is not taken into account.
    """
    self.checkDataAvailable()

    reliabilityList = []
    nodeIds         = copy(self._nodeIdList)
    nodeIds.remove(self.hostNodeId)

    for nodeId in nodeIds:
      tsRx = [e['ts'] for e in self._rx[self.hostNodeId] if e['txNodeId'] == nodeId]          if self.hostNodeId in self._rx else []
      tsTx = [e['ts'] for e in self._tx[nodeId]          if e['rxNodeId'] == self.hostNodeId] if nodeId          in self._tx else []
      roundIdxRx = np.asarray([self.ts2RoundIdx(ts) for ts in tsRx])
      roundIdxTx = np.asarray([self.ts2RoundIdx(ts) for ts in tsTx])

      for roundIdx in range(self.lastRoundIdx + 1):
        reliabilityList.append({
            'nodeId':   nodeId,
            'roundIdx': roundIdx,
            'txPkts':   np.sum(roundIdxTx == roundIdx),
            'rxPkts':   np.sum(roundIdxRx == roundIdx),
        })

    dfOut = pd.DataFrame(reliabilityList)

    return dfOut

  def evalEnergy(self):
    """Absolute energy per round (tries to prevent using values from bootstrapping/unoptimized state)
    """
    self.checkDataAvailable()

    df       = self._energyPerLsrRoundDf
    energyDf = deepcopy(df[df.nodeId != self.hostNodeId])

    return energyDf

  def evalEnergyNormalized(self):
    """Normalized energy per node using the calculated consumption of optimal multi-hop paths.
    """
    # NOTE: The implementation of this method does not seem to be very efficient, the time required for execution seems to grow more than linearly with increasing simulation duration. Avoid execution if not required!
    self.checkDataAvailable()

    energyNormalizedList = []
    eDf                  = self._energyPerLsrRoundDf
    for roundIdx in range(self.lastRoundIdx + 1):
      # determine actually consumed energy
      eDfRound     = eDf[(eDf.roundIdx == roundIdx) & (eDf.nodeId != self.hostNodeId)]
      energyActual = eDfRound.energy.sum()  # sum over all nodeIds (except hostNodeId), rcIdx, and radioOps

      # construct graph from path loss matrix for every radio config and every path loss matrix (ts when matrix becomes active serves as index)
      # get all SetPathlossMatrix events
      spmeList = [e for e in self._sim.eq.q if type(e) == events.SetPathlossMatrix]

      g = OrderedDict()
      for rcIdx, rc in enumerate(self.radioConfigsSim):
        # determine feasible path loss
        plMax = rc.txPower - rc.sensitivity

        g[rcIdx] = OrderedDict()
        for e in spmeList:
          g[rcIdx][e.ts] = nx.DiGraph()
          for nodeIdTx in self._nodeIdList:
            for nodeIdRx in self._nodeIdList:
              pl = e.pathlossMatrix[self._sim.nodeId2nodeIdSim(nodeIdTx)][self._sim.nodeId2nodeIdSim(nodeIdRx)]
              if not np.isnan(pl) and pl <= plMax:
                g[rcIdx][e.ts].add_edge(nodeIdTx, nodeIdRx)

          # ensure all nodes are contained in graph to prevent NodeNotFound errors when searching shortest paths
          for nodeId in self._nodeIdList:
            g[rcIdx][e.ts].add_node(nodeId)

      # calculate optimal energy for all transmissions that actually happened
      rxElements = [e for e in self._rx[self.hostNodeId] if self.ts2RoundIdx(e['ts']) == roundIdx]  # list of rx packets in round
      if len(rxElements) == 0:
        print('WARNING: evalEnergyNormalized: no packet received in round {}! => cannot calculate normalized energy!'.format(roundIdx))
        energyOptimal = np.nan
      else:
        energyOptimal = 0
        for rxElem in rxElements:
          ts = rxElem['ts']
          pathEnergyList = []
          for rcIdx, rc in enumerate(self.radioConfigsSim):
            keys  = list(g[rcIdx].keys())
            tsIdx = np.searchsorted(keys, ts, side='right') - 1  # -1 since inserting is always on the right, side='right' for "<="
            tsKey = keys[tsIdx]
            try:
              spList = nx.all_shortest_paths(g[rcIdx][tsKey], rxElem['txNodeId'], self.hostNodeId)
              for sp in spList:  # keep for loop inside try; all_shortest_paths returns a generator which is only evaluate if iterated (alternative would be converting generator using list())
                rc.phyPl = rxElem['msg'].size

                numTxTotal = 0
                for i, nodeIdPath in enumerate(sp[:-1]):  # last node is always the hostNode (unconstrained node) for which we do not count Tx
                  numTxTotal += self._sim.nodes[self._sim.nodeId2nodeIdSim(nodeIdPath)].lsr.primitiveConfig.nTx
                pathEnergyList.append(rc.timeOnAir*(numTxTotal*sx1262.getTxPower(rc.txPower) + (len(sp) - 2)*sx1262.getRxPower()))  # (len(sp) - 2): we do not count Rx on initiating and hostNode
            except nx.NetworkXNoPath:
              pass  # we expect that for certain rcIdx there is no path

          if len(pathEnergyList) == 0:
            # this should not happen as we expect that there is at least one path for one rcIdx, otherwise we could not have received a stream request and registered the node
            raise Exception('No optimal min energy path found!')

          energyOptimal += np.min(pathEnergyList)

      energyNormalizedList.append({
        'roundIdx':      roundIdx,
        'energyActual':  energyActual,
        'energyOptimal': energyOptimal,
      })

    dfOut = pd.DataFrame(energyNormalizedList)
    dfOut['energyNormalized'] = dfOut.energyActual/dfOut.energyOptimal

    return dfOut

  ##############################################################################
  # Helper Functions
  ##############################################################################

  def checkDataAvailable(self):
    if not len(self._tx):
      raise Exception('tx data missing!')
    if not len(self._rx):
      raise Exception('rx data missing!')
    if not len(self._registeredNodesHost):
      raise Exception('registeredNodesHost data missing!')
    if not len(self._registeredNodesClients):
      raise Exception('registeredNodesClient data missing!')
    if self._nodeIdList is None:
      raise Exception('nodeIdList data missing!')
    if self._hostNodeId is None:
      raise Exception('hostNodeId data missing!')
    if self._roundBoundaries is None:
      raise Exception('roundBoundaries data missing!')
    if self._energyPerLsrRoundDf is None:
      raise Exception('energyPerLsrRoundDf data missing!')

  def ts2RoundIdx(self, ts):
    """Convert sim timestamp to roundIdx
    """
    return np.searchsorted(self._roundBoundaries, ts, side='right') - 1

  @property
  def hostNodeId(self):
    if len(self._hostNodeId) > 1:
      raise Exception('More than one hostNodeId set (not supported yet)!')
    return list(self._hostNodeId.values())[0]

  @property
  def lastRoundIdx(self):
    roundIndices = self._energyPerLsrRoundDf.roundIdx.unique()
    if len(roundIndices) < 2:
      raise Exception('Not enough rounds!')
    # omit last round since it may be incomplete/not terminated due to stopping sim
    return max(roundIndices) - 1

  @property
  def radioConfigsHostNode(self):
    return self._sim.nodes[self._sim.nodeId2nodeIdSim(self.hostNodeId)].lsr.radioConfigs

  @property
  def radioConfigsSim(self):
    return self._sim.radioConfigs

################################################################################


if __name__ == '__main__':
  pass
