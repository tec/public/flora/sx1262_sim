#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""

import numpy as np

from events import *

################################################################################
# Event Queue
################################################################################


class EventQueue():
  def __init__(self, sim):
    self.nextItem = 0
    self.q        = []  # event queue
    self.tsList   = []  # list of timestamps corresponding to events in self.q (this is redundant info; only useful to improve performance of addEvent(); repeated creation of list of ts is not necessary)
    self.sim      = sim

  def addEvent(self, event):
    """Insert events into the queue based on their timestamps.
       Ordering is preserved for events with the same timestamp value.
    Args:
      event: Event obj to be added to the event queue
    """
    # only search through (and therefore insert) events in the future not in the past (for better performance)
    currItem = self.nextItem - 1
    if currItem > 0 and event.ts < self.q[currItem].ts:
      raise Exception('Events can only be inserted in the future!')
    idx = np.searchsorted(self.tsList[self.nextItem:], event.ts, side='right')
    self.q.insert(self.nextItem + idx, event)
    self.tsList.insert(self.nextItem + idx, event.ts)

  def removeEvent(self, idx):
    self.q.pop(idx)
    self.tsList.pop(idx)

  def removeEvents(self, eventType, actionId):
    """Remove all events in the future which are of type eventType and belong to the action with actionId
    """
    # eventsToDel to make sure that list is not altered while iterating over it
    eventsToDel = []

    for i, event in enumerate(self.q[self.nextItem:]):
      if isinstance(event, eventType) and event.actionId == actionId:
        eventsToDel.append(self.nextItem + i)

    # delete from back to front such that IDs of found elements does not change with popping
    for e in reversed(eventsToDel):
      self.removeEvent(e)

  def removeTimeoutEvents(self, nodeIdSim, timerId):
    """Removes all TimerTimeout events in the future which correspond to a specific nodeIdSim and timerId
    """
    # eventsToDel to make sure that list is not altered while iterating over it
    eventsToDel = []

    for i, event in enumerate(self.q[self.nextItem:]):
      if isinstance(event, TimerTimeout) and self.sim.at[event.actionId].nodeIdSim == nodeIdSim:
        eventsToDel.append(self.nextItem + i)

    # delete from back to front such that IDs of found elements do not change with popping
    for e in reversed(eventsToDel):
      self.removeEvent(e)

  def removeRadioEvents(self, nodeIdSim):
    """Removes all radio related events in the future which correspond to a specific nodeIdSim
    """
    # eventsToDel to make sure that list is not altered while iterating over it
    eventsToDel = []

    for i, event in enumerate(self.q[self.nextItem:]):
      # collect list of future radio events
      if isinstance(event, RxStart)           or \
         isinstance(event, RxHeaderValidSync) or \
         isinstance(event, RxDone)            or \
         isinstance(event, RxTimeout)         or \
         isinstance(event, TxStart)           or \
         isinstance(event, TxDone):
        if self.sim.at[event.actionId].nodeIdSim == nodeIdSim:
          eventsToDel.append(self.nextItem + i)

    # delete from back to front such that IDs of found elements do not change with popping
    for e in reversed(eventsToDel):
      self.removeEvent(e)

  def getNextEvent(self):
    if self.nextItem <= len(self.q) - 1:
      self.nextItem += 1
      return self.q[self.nextItem - 1]
    else:
      return None

  def isTimerActive(self, nodeIdSim, timerId):
    for i, event in enumerate(self.q[self.nextItem:]):
      if isinstance(event, TimerTimeout)                    and \
         self.sim.at[event.actionId].nodeIdSim == nodeIdSim and \
         self.sim.at[event.actionId].timerId   == timerId:
        return True

    return False

  def __len__(self):
    return len(self.q) - self.nextItem

################################################################################


if __name__ == '__main__':
  pass
