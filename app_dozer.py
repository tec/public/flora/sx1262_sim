#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""

import numpy as np
from copy import copy, deepcopy
from collections import OrderedDict

from sx1262 import LoraConfig, FskConfig
import sx1262

from sim import Sim
from node import *
from glossy import *
from dozer import *
import scenario_flocklab
import scenarios

################################################################################
# General Config
################################################################################
config = OrderedDict()

config['scenario'] = 'lm'
config['hostNodeId'] = 1
config['txPwr'] = 8
config['seed'] = 0
config['modulations'] = [5, 10]
config['radioConfigsSlice'] = slice(1) # stop=1
config['sensitivitySrcDatasheet'] = False

################################################################################

if __name__ == '__main__':
  radioConfigs = []
  for modIdx in config['modulations']:
    rc = sx1262.getFloraConfig(modIdx=modIdx)
    # additional fields required for the simulation
    rc.frequency = 868100000  # in Hz
    rc.txPower   = config['txPwr']
    rc.sensitivitySrcDatasheet = config['sensitivitySrcDatasheet']
    radioConfigs.append(rc)

  sim = Sim(
    logFileName='sim_log.txt',
    outputDir='./output/output_dozer'
  )
  sim.setRadioConfigs(radioConfigs)
  sim.propagation = True
  sim.timeDrift = True
  sim.probabilisticRf = True
  sim.verbose = True
  sim.debug = False
  sim.setRandSeed(config['seed'])

  ##############################################################################
  # Scenarios
  ##############################################################################

  if config['scenario'][:3] != 'fl2':

    def genNode(nodeId, nodeIdList, pos):
      return DozerNode(
        position=pos,
        nodeId=nodeId,
        radioConfig=radioConfigs[config['radioConfigsSlice']],
        hostNodeId=config['hostNodeId'],
        startDelay=5 if nodeId==config['hostNodeId'] else 0,
      )
    # scenario_flocklab.addNodes(sim, genNode, pathlossMatrixScaling=1.2)

    # plot node positions
    # gnssNodes = [1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 15, 17, 20, 24, 25, 29, 30, 31, 32] # GNSS only
    # gnssNodes = [1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 15, 17, 20, 24, 25, 29, 31, 32] # GNSS only (without node30)
    # gnssNodes = [1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 15, 17, 20, 24, 25, 30, 31, 32] # GNSS only (without node29)
    gnssNodes = [1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 15, 17, 20, 24, 25, 31, 32] # GNSS only (without node29 and node30)
    scenario_flocklab.addNodes(
      sim,
      genNode,
      pathlossMatrixScaling=1.2,
      subset=gnssNodes,
    )

  else: # scenario != 'fl2'

    def genNode(nodeId, nodeIdList, pos):
      return DozerNode(
        position=pos,
        nodeId=nodeId,
        radioConfig=radioConfigs[config['radioConfigsSlice']],
        hostNode=hostNodeId,
        startDelay=5 if nodeId==hostNodeId else 0,
      )

    edges = deepcopy(scenarios.scenarioData[scenario]['edges'])
    pathlossMatrix, nodeIdListPlm = scenarios.edgesToPathlossMatrix(
      edges,
      modIdxLongrange=config['modulations'][0],
      modIdxShortrange=config['modulations'][1],
      txPwr=config['txPwr'],
      sensitivitySrcDatasheet=config['sensitivitySrcDatasheet'],
    )
    # set initial pathloss matrix (use path loss matrix generated above to ensure that we use same path loss matrix for all protocols within the same scenario)
    sim.schedulePathlossMatrix(
      pathlossMatrix=deepcopy(pathlossMatrix)*1.0,
      nodeIdList=deepcopy(nodeIdListPlm),
    )

    scenarios.addNodes(
      sim=sim,
      scenario=scenario,
      genNode=genNode,
      nodeIdListPlm=nodeIdListPlm,
    )


  ##############################################################################
  sim.plotNodePositions()
  sim.runSimulation(until=2*60+2)
  sim.plotTransmissions(outFile='transmissions.html')
  sim.plotEnergy(outFile='energy.html')

  # sim.printEventQueue()
