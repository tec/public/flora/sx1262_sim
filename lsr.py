#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""


import os
import enum
import struct
import itertools
import pandas   as pd
import numpy    as np
import networkx as nx
from munch       import Munch
from copy        import copy, deepcopy
from transitions import Machine
from collections import OrderedDict, deque, Counter

import sx1262
import plot_energy_lsr

from message import Message
from node    import Node
from actions import *
from glossy  import Glossy
from gloria  import Gloria, GloriaMsg


################################################################################
# Config
################################################################################
## Select mask size (delay and particip masks)
# # 32bit (small scenarios with <=32 nodes)
# MASK_FORMAT = 'I'
# MASK_INIT_SET = 0xffffffff
# MAX_NUM_NODES = 32
# 64bit (large scenarios <=64 nodes)
MASK_FORMAT = 'Q'
MASK_INIT_SET = 0xffffffffffffffff
MAX_NUM_NODES = 64

################################################################################
# Message Definitions
################################################################################

class LsrMsgType(enum.IntEnum):
    SCHEDULE   = 0
    STREAM_REQ = 1
    DATA       = 2
    DUMMY      = 3


class LsrMsg(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = True
    self._formats          = OrderedDict([
        ('msgType', 'B'),
        ('src', 'H'),
        ('dst', 'H'),
        ('payload', 'c')
    ])
    super().__init__(msg, raw)


class LsrSchedule(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = True
    self._formats          = OrderedDict([
        ('hostNodeId', 'H'),
        ('time', 'L'),
        ('period', 'H'),
        ('dataSlotOffset', 'B'),
        ('streamAck', 'H'),
        ('delayMask', MASK_FORMAT),
        ('nContSlots', 'B'),
        ('nDataSlots', 'B'),
        ('nParticipUpdates', 'B'),
        ('raw', 'c'),
    ])
    super().__init__(msg, raw)
    # TODO: add data ACK to message


class LsrScheduleSlots(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = True
    self._formats          = OrderedDict([
        ('slots', 'H'),
    ])
    super().__init__(msg, raw)


class LsrScheduleParticipUpdate(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = False
    self._formats          = OrderedDict([
        ('nodeIdTx', 'H'),
        ('participMask', MASK_FORMAT),
    ])  # participMask: bit position = participNode
    super().__init__(msg, raw)


class LsrHopDistanceMeasurement(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = False
    self._formats          = OrderedDict([
        ('rcIdx', 'B'),
        ('hopDistance', 'B'),
    ])
    super().__init__(msg, raw)


class LsrStreamReq(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = False
    self._formats          = OrderedDict([
        ('nodeId', 'H'),
        ('changeExistingStream', 'B'),
        ('msgSize', 'B'),
        ('nMsgs', 'B'),
        ('period', 'H') # period = IPI in LWB paper
    ])
    super().__init__(msg, raw)


class LsrDataMsg(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = True
    self._formats          = OrderedDict([('nHopDistanceMeasurements', 'B'), ('raw', 'c')])
    super().__init__(msg, raw)


class ApplicationMsg(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = False
    self._formats          = OrderedDict([('seqNum', 'B'), ('text', '15s')])  # 16 Bytes total size
    super().__init__(msg, raw)


################################################################################
# LSR State Machine
################################################################################

lsrStates = [
  'Init',
  'Bootstrap',
  'Schedule',
  'Contention',
  'Data',
  'Post',
]

lsrTransitions = [
    { 'trigger': 'fsmStart',          'source': 'Init',       'dest': 'Schedule',   'after': ['printState', 'triggerProcessing', 'resetCurrRcIdx'] },  # only useful for host
    { 'trigger': 'fsmBootstrap',      'source': 'Init',       'dest': 'Bootstrap',  'after': ['printState', 'triggerProcessing'] },
    { 'trigger': 'fsmBootstrap',      'source': 'Bootstrap',  'dest': 'Bootstrap',  'after': ['printState', 'triggerProcessing'] },
    { 'trigger': 'fsmBootstrap',      'source': 'Schedule',   'dest': 'Bootstrap',  'after': ['printState', 'updateLastReceivedScheduleRcIdxStats', 'triggerProcessing'] },
    { 'trigger': 'fsmBootstrap',      'source': 'Contention', 'dest': 'Bootstrap',  'after': ['printState', 'triggerProcessing'] },
    { 'trigger': 'fsmBootstrap',      'source': 'Data',       'dest': 'Bootstrap',  'after': ['printState', 'triggerProcessing'] },
    { 'trigger': 'fsmBootstrapDone',  'source': 'Bootstrap',  'dest': 'Schedule',   'after': ['printState'] },
    { 'trigger': 'fsmSkipRound',      'source': 'Schedule',   'dest': 'Post',       'after': ['printState', 'resetCurrRcIdx', 'updateLastReceivedScheduleRcIdxStats', 'triggerProcessing'] },
    { 'trigger': 'fsmScheduleDone',   'source': 'Bootstrap',  'dest': 'Contention', 'after': ['printState', 'resetCurrRcIdx', 'updateLastReceivedScheduleRcIdxStats'] },
    { 'trigger': 'fsmSchedule',       'source': 'Schedule',   'dest': 'Schedule',   'after': ['printState', 'advanceSlot'] },
    { 'trigger': 'fsmScheduleDone',   'source': 'Schedule',   'dest': 'Contention', 'after': ['printState', 'resetCurrRcIdx', 'updateLastReceivedScheduleRcIdxStats', 'advanceSlot'] },  # NOTE: ordering of "after" functions is important!
    { 'trigger': 'fsmContention',     'source': 'Contention', 'dest': 'Contention', 'after': ['printState', 'advanceSlot'] },
    { 'trigger': 'fsmContentionDone', 'source': 'Contention', 'dest': 'Data',       'after': ['printState', 'resetCurrRcIdx', 'advanceSlot'] },  # NOTE: ordering of "after" functions is important!
    { 'trigger': 'fsmSkipRound',      'source': 'Contention', 'dest': 'Post',       'after': ['printState', 'resetCurrRcIdx', 'triggerProcessing'] },
    { 'trigger': 'fsmDataDone',       'source': 'Data',       'dest': 'Post',       'after': ['printState', 'resetCurrRcIdx'] },
    { 'trigger': 'fsmPostDone',       'source': 'Post',       'dest': 'Schedule',   'after': ['printState'] },
    { 'trigger': 'fsmStop',           'source': 'Schedule',   'dest': 'Init',       'after': ['printState', 'resetCurrRcIdx'] },
    { 'trigger': 'fsmStop',           'source': 'Contention', 'dest': 'Init',       'after': ['printState', 'resetCurrRcIdx'] },
    { 'trigger': 'fsmStop',           'source': 'Data',       'dest': 'Init',       'after': ['printState', 'resetCurrRcIdx'] },
]

################################################################################
# Thinning State Machine
################################################################################


class ThinningState(enum.IntEnum):
  Invalid               = 0
  CollectingHopDistance = 1
  MeasuringDelay        = 2
  MeasuringHopDistance  = 3
  Converged             = 4

################################################################################
# LSR
################################################################################


class Lsr():
  def __init__( self,
                timerId,
                primitiveClass,
                radioConfigs,
                primitiveTimerId,
                node,
                nTx = 3,
                numHops = 4,
                slotLength = 1.0,
                safetyMargin = 50e-3,
                period = 10,
                streamPeriod = 10,
                queueSize = MAX_NUM_NODES,
                minNodeId = 1,
                maxNodeId = 1+MAX_NUM_NODES-1,
                thinningHopDistance = True,
                thinningConnectivityGraph = True,
                statsDepth = 3,
                selRcIdxPercentile = 40,
                deregisteringThreshold = None,
                numMissedSchedulesToBootstrap = 3,
                useBackoffForBootstrap = True,
                skipBootstrapLimit = 15,
                maxPayloadSize = 96,
                useDelayedSchedule = True,
                useBps = True,
                bpsRoundThreshold = None,
              ):
    self.primitiveConfig = Munch()

    # sanitize args
    if type(numHops) != list:
      numHops = [numHops]*len(radioConfigs)
    if type(slotLength) != list:
      slotLength = [slotLength]*len(radioConfigs)

    # initialize & set config
    # host & client
    self.timerId = timerId
    self.node    = node
    self.primitiveConfig.primitiveClass = primitiveClass
    self.radioConfigs = deepcopy(radioConfigs)
    self.isHost       = None
    self.lastRcIdx    = len(self.radioConfigs) - 1
    self.primitiveConfig.timerId = primitiveTimerId
    self.primitiveConfig.nTx     = nTx                          # number of retransmissions by each node within a flood
    self.primitiveConfig.numHops = numHops                      # number of hops (corresponds to the max radius of the network), for each radioConfig
    self.floodLength  = None                                    # seconds
    self.slotLength   = slotLength                              # seconds, for each radioConfig
    self.safetyMargin = safetyMargin                            # seconds
    self.period       = period                                  # seconds
    self.streamPeriod = streamPeriod                            # seconds
    self.queueSize    = queueSize                               # size of the Rx and Tx queue
    self.currDataSlotIdx    = 0
    self.minNodeId          = minNodeId                         # min/max nodeId (relevant for mapping of bits in the delay and participation masks to nodes)
    self.maxNodeId          = maxNodeId
    self.currRcIdx          = 0                                 # state for iterating through all radioConfigs in each lwb phase
    self.useDelayedSchedule = useDelayedSchedule                # switch to choose approach to get measurements for not active modulation (only has en effect if thinningConnectivityGraph==True)
                                                                #   False: do not delay schedule, measure hop distance for not active modulation by data transmissions from host to nodes, potentially additional dummy pkts need to be added if host has no demand for sending msgs
                                                                #   True:  delay schedule and use it to measure hop distance for not active modulation, do not use data by data transmissions from host to nodes (this requires to extract the delayMask from the schedule msg during the flood in order to decide whether the schedule should be delayed or not)
    self.statsDepth         = statsDepth                        # depth of the history which is used for the active modulation and thinning

    # host only
    self.thinningHopDistance       = thinningHopDistance        # Enable/disable thinning based on hop distance measurements
    self.thinningConnectivityGraph = thinningConnectivityGraph  # Enable/disable thinning based on connectivity measurements obtained by delaying retransmissions
    self.noDataReceivedThreshold           = deregisteringThreshold if deregisteringThreshold is not None else 3*period  # threshold for deregistering nodes on host (time delta for which host does not receive data), default: 3*period
    self.noSlotAssignmentReceivedThreshold = deregisteringThreshold if deregisteringThreshold is not None else 3*period  # threshold for deregistering node (time delta for which host does not receive data), default: 3*period
    self.preRegisterNodes          = OrderedDict()              # nodes to register before the start of the protocol execution
    self.useBps                    = useBps                     # use best path selection: switch to choose path selection approach (only has en effect if thinningConnectivityGraph==True)
                                                                #   True: use exhaustive search in all path selection combinations and randomly select one of the combos with min cost
                                                                #   False: randomly choose a path for each node (not optimal)
    self.bpsRoundThreshold = bpsRoundThreshold                  # update of set of best paths is triggered if bpsCounter exceeds this threshold; if None, selection is never updated because of the counter (but of course still if paths are missing); only relevant if useBps==True
    self.delayStatsPercentile = 30                              # Percentile (in percent) for delay stats; smaller than 50 means more than 50% of 1s are required to consider the connection to exist
    for rcIdx, rc in enumerate(self.radioConfigs):
      self.preRegisterNodes[rcIdx] = []

    # client only
    self.numMissedSchedulesToBootstrap = numMissedSchedulesToBootstrap
    self.useBackoffForBootstrap        = useBackoffForBootstrap  # switch to turn on backoff for receiving schedule when bootstrapping
    self.skipBootstrapLimit            = skipBootstrapLimit      # limit on number of periods in which no attempt is made to receive the schedule when bootstrapping (NOTE: this is not a hard limit, the value is simply used for determining a random number)
    self.selRcIdxPercentile            = selRcIdxPercentile      # percentile for selecting best modulation
    self.preActivatedRcIdx             = None                    # registration to activate before the start of the protocol execution
    self.preActivatedDataFirstRound    = None                    # switch to enable/disable data packet generation before first round

    # determine max payload size for LSR packets
    emptyLsrMsg           = LsrMsg(msg={'msgType': 0, 'src': 0, 'dst': 0, 'payload': []}).size
    emptyGloriaMsg        = GloriaMsg(msg={'slot': 0, 'payload': []}).size
    payloadSizeUpperBound = self.radioConfigs[0]._phyPlRange[1] - emptyGloriaMsg - emptyLsrMsg
    if maxPayloadSize > payloadSizeUpperBound:
      raise Exception('maxPayloadSize={} is larger than payloadSizeUpperBound={}!'.format(maxPayloadSize, payloadSizeUpperBound))
    self.maxPayloadSize = maxPayloadSize

    # initialize flooding primitive
    self.primitive = self.primitiveConfig.primitiveClass(timerId=self.primitiveConfig.timerId, node=self.node)
    self.primitive.registerFloodCallback(self.floodCallback)

    # internal state
    self.sm = Machine(model=self, states=lsrStates, transitions=lsrTransitions, initial='Init')

    # DEBUGGING
    self.dataSeqNum = 0

  def _resetStart(self):
    # state to reset at every (re)-start
    self.tsGlobal      = None
    self.tsLocal       = None
    self.rxQueue       = []
    self.txQueue       = []
    self._postCallback = None
    self.to_Init()

    if self.isHost:
      # host
      self.hostNodeId = self.node.nodeId
      if (self.node._sim) and (self.node._sim.stats is not None):
        self.node._sim.stats.addHostNodeId(self.hostNodeId)
      self.tsLastDataReceived = OrderedDict()    # one entry for each registered node
    else:
      # client
      self.hostNodeId                     = None
      self.lastReceivedScheduleRcIdxStats = deque([], maxlen=self.statsDepth)
      self.registeredStreamRcIdx          = None   # rcIdx of registered stream, None otherwise
      self.contentionSlotCounter          = 0      # counter for exponential backoff in contention slot (only one counter is needed as only contention of last received schedule radioConfig is used)
      self.backoffUseContention           = None   # state per round to remember decision of isNextContentionSlotUsed
      self.isSendingContention            = False  # state to determine after contention flood whether node was sending during last slot
      self.isIdleContention               = False  # state to determine after contention flood whether node was idle during last slot
      self.roundsSinceLastRegCounter      = None   # counter to count rounds since node was the last time registered
      self.roundsSinceLastScheduleCounter = None   # counter to count rounds since we received last schedule for rcIdx==0
      self.failedBootstrapCounter         = 0      # counter to count number of attempts to bootstrap (for exponential backoff)
      self.tsLastScheduleReceived         = None   # timestamp when last schedule was received
      self.tsLastSlotAssignmentReceived   = None   # timestamp when last data slot assignment was received

      # TODO: implement support for multiple streams, currently only a single stream per node is supported
      # self.activeStreams = []              # list of stream IDs of active and acked streams

    # state per radioConfig
    # host & client
    self.lastSyncPointLt = OrderedDict()
    self.lastSyncPointNt = OrderedDict()
    self.floodLength     = OrderedDict()
    self.floodLengthCont = OrderedDict()
    # host
    if self.isHost:
      self.registeredNodes    = OrderedDict()
      self.streamRequestQueue = OrderedDict()    # holds all stream requests of a round
      self.roundNo            = 0
      self.streamToAck        = OrderedDict()
      self._thinningStatus    = OrderedDict()
    # client
    # (nothing)

    # init for all rcIdx
    for rcIdx, rc in enumerate(self.radioConfigs):
      # host & client
      self.lastSyncPointLt[rcIdx] = None
      self.lastSyncPointNt[rcIdx] = None
      # host
      if self.isHost:
        self.registeredNodes[rcIdx]    = []
        self.streamRequestQueue[rcIdx] = deque([])
        self.streamToAck[rcIdx]        = None
        self._thinningStatus[rcIdx]    = ThinningState.Invalid
      # client
      # (nothing)

      # determine floodDuration (schedule and data)
      if self.isHost:
        print('= flood duration rcIdx=={}) ='.format(rcIdx))
      rcTmp       = copy(rc)
      rcTmp.phyPl = self.maxPayloadSize
      floodLength = self.primitive.getFloodDuration(
        nTx=self.primitiveConfig.nTx,
        numHops=self.primitiveConfig.numHops[rcIdx],
        radioConfig=rcTmp,
      )
      if floodLength > (self.slotLength[rcIdx] - self.safetyMargin):
        raise Exception('floodLength of radioConfig \'{}\' (idx={}) in combination with nTxSending={} and numHops={} leads to a floodLength ({}s) which is together with the safetyMargin={}s longer than the slotLength={}s'.format(rc, rcIdx, self.primitiveConfig.nTx, self.primitiveConfig.numHops[rcIdx], floodLength, self.safetyMargin, self.slotLength[rcIdx]))
      self.floodLength[rcIdx] = floodLength
      if self.isHost:
        print('Flood duration (schedule and data): {:0.3f}s'.format(self.floodLength[rcIdx]))

      # determine contention floodDuration
      rcTmp = copy(rc)
      # TODO: get size of stream request
      rcTmp.phyPl = LsrMsg(msg={
        'msgType': LsrMsgType.STREAM_REQ,
        'src': 0,
        'dst': 0,
        'payload': LsrStreamReq(msg={'nodeId': 0, 'changeExistingStream': 0, 'msgSize': 0, 'nMsgs': 0, 'period': 0}).raw,
      }).size
      floodLengthCont = self.primitive.getFloodDuration(
        nTx=self.primitiveConfig.nTx,
        numHops=self.primitiveConfig.numHops[rcIdx],
        radioConfig=rcTmp,
      )
      if floodLengthCont > self.slotLength[rcIdx] - self.safetyMargin:
        raise Exception('floodLength of radioConfig \'{}\' (idx={}) in combination with nTxSending={} and numHops={} leads to a floodLength ({}s) which is together with the safetyMargin={}s longer than the slotLength={}s'.format(rc, rcIdx, self.primitiveConfig.nTx, self.primitiveConfig.numHops[rcIdx], floodLength, self.safetyMargin, self.slotLength[rcIdx]))
      self.floodLengthCont[rcIdx] = floodLengthCont
      if self.isHost:
        print('Flood duration (contention): {:0.3f}s'.format(self.floodLengthCont[rcIdx]))

    self._resetThinning()
    self._resetRound()

  def _resetRound(self):
    # state to reset before every round
    # TODO: cleanup what is initialized here and in _reset_start()
    self.backoffUseContention = None
    self.lastSchedule         = OrderedDict()
    self.lastSlots            = OrderedDict()
    for rcIdx, rc in enumerate(self.radioConfigs):
      self.lastSchedule[rcIdx] = None
      self.lastSlots[rcIdx]    = None
      if not self.isHost:
        self.hopDistanceMeasurementClient[rcIdx] = None

  def _print(self, text):
    self.node.print('[LSR] {}'.format(text))

  ##############################################################################
  # Interface
  ##############################################################################

  def start(self, isHost, postCallback=None, woEventGen=False):
    """ Start LSR
    isHost:       bool to indicate whether this instance is used on the host or a source node
    postCallback: callback function which is called at the end of an LSR round
    woEventGen:   if True, no simulation events will be generated (NOTE: useful if LSR is used without simulation)
    """
    # self._initState() # TODO: currently LSR can only be started once, implement and use _reset() to reset state
    self.isHost = isHost
    self._resetStart()  # NOTE: resets _postCallback
    self._postCallback = postCallback

    # pre-register nodes
    if self.isHost:
      # pre-register nodes on host
      for rcIdx in self.preRegisterNodes.keys():
        for nodeId in self.preRegisterNodes[rcIdx]:
          self.registerNode(nodeId=nodeId, rcIdx=rcIdx, generateAck=False)
    else:
      # activate registration on node
      if self.preActivatedRcIdx is not None:
        self.registeredStreamRcIdx = self.preActivatedRcIdx
        self.lastReceivedScheduleRcIdxStats.extend([self.preActivatedRcIdx]*self.statsDepth)
        self.contentionSlotCounter = 0  # reset counter for exponential backoff
        self.tsLastSlotAssignmentReceived = self.node.getTs()  # initialize here to make sure that node detects condition when slot assignment is never received
        if self.preActivatedDataFirstRound:
          # generate data packet in first round
          self.node.postCallback()

    if woEventGen:
      return

    # use timer action to trigger callback which includes timestamp
    if self.isHost:
      self.fsmStart()
    else:
      self.fsmBootstrap()

  def stop(self):
    if self.primitive.running:
      self.primitive.stop()

    # Stop ongoing radio and timer actions
    self.node.timerStop(timerId=self.timerId)
    self.node.radioReset()

  def sendMsg(self, msg):
    """Puts a message into the outgoing queue
    Args:
      msg: LsrMessage object to be sent
    Returns:
      True if successful, False otherwise
    """
    if len(self.txQueue) < self.queueSize:
      self.txQueue.append(msg)
      return True
    else:
      self._print('WARNING: txQueue is full!')
      return False

  @property
  def rxQueuePktsWaiting(self):
    return len(self.rxQueue)

  def receiveMsg(self):
    """Returns the oldest received message in the input queue
    Returns:
      LsrMessage object which has been received & nodeId of sending node
    """
    if len(self.rxQueue) == 0:
      return None, None
    return self.rxQueue.pop(0)

  @property
  def txQueuePktsWaiting(self):
    return len(self.txQueue)

  def getTime(self, localTime=None):
    if self.tsGlobal is None:
      return None
    if localTime is None:
      localTime = self.node.getTs()
    return self.tsGlobal + (localTime - self.tsLocal)

  def registerNode(self, nodeId, rcIdx, changeExistingStream=False, generateAck=True):
    """Register a node on the host
    Args:
      nodeId:               ID of the node to be registered
      rcIdx:                rcIdx where node wants to be registered
      changeExistingStream: bool to indicate by the node that a stream which is already registered for the same rcIdx is requested to be changed (useful to prevent thinning state reset)
      generateAck:          bool to enable generation of streamAck in following schedule in case the registration is successful (setting this to false is useful for pre-registering nodes)
    """
    if nodeId == self.node.nodeId:
      self._print('WARNING: Node {} NOT registered! Host node cannot be registered!'.format(nodeId))
      return False
    if changeExistingStream:
      # there should be no need to deregister node
      # to be on the safe side, we make sure node is actually already registered for rxIdx
      if nodeId not in self.registeredNodes[rcIdx]:
        # this can happen in very rare cases where node requests change of data rate of existing stream while registration times out on host
        self._print('WARNING: Registration of node {} requested with changeExistingStream=True but node not already registered!'.format(nodeId))
        changeExistingStream = False
    if not changeExistingStream:
      # deregister node from all rcIdx (resets thinning state)
      for rcIdxIter, rc in [(rcIdxIter, rc) for (rcIdxIter, rc) in enumerate(self.radioConfigs)]:
        if nodeId in self.registeredNodes[rcIdxIter]:
          self._print('Deregistered node {} from rcIdx={} as it requested stream for rcIdx={}'.format(nodeId, rcIdxIter, rcIdx))
          self.deregisterNode(nodeId, rcIdxIter)

    # check if there is enough time available for a data slot
    timeRemaining = self.period
    for rcIdxIter, regList in self.registeredNodes.items():
      timeRemaining -= (2 + len(regList)) * self.slotLength[rcIdxIter] # +2: 1 slot for schedule, 1 slot for contention
      if rcIdxIter == 0:
        timeRemaining -= 1 * self.slotLength[rcIdxIter] # 1 slot reserved for potential tx from host (only in Mod0)
    if timeRemaining >= self.slotLength[rcIdx]:
      self.registeredNodes[rcIdx]     = sorted(list(set(self.registeredNodes[rcIdx]).union(set([nodeId]))))
      self.tsLastDataReceived[nodeId] = self.node.getTs()  # init with timestamp of time of registration such that node is removed if no data is received for too long time
      self.initThinningForNode(nodeId, rcIdx)
      if generateAck:
        self.streamToAck[rcIdx]     = nodeId
      self.participNewNode[rcIdx] = True
      self._print('[registerNode] Node {} registered for rcIdx={}!'.format(nodeId, rcIdx))
      return True
    else:
      self._print('Node {} NOT registered! No more LSR data slots available!'.format(nodeId))
      self.streamToAck[rcIdx] = None
      return False

  def deregisterNode(self, nodeId, rcIdx):
    if nodeId not in self.registeredNodes[rcIdx]:
      raise Exception('Node {} is not registered, cannot deregister it!'.format(nodeId))
    self.registeredNodes[rcIdx].remove(nodeId)
    del self.tsLastDataReceived[nodeId]
    self.deinitThinningForNode(nodeId, rcIdx)
    self._print('[deregisterNode] [rcIdx={}] Node {} deregistered!'.format(rcIdx, nodeId))

################################################################################

  def _updateFsm(self, calledFromFloodCallback=False):
    if self.state == 'Bootstrap':
      if self.primitive.running:
        # no schedule received otherwise a floodCallback would have been triggered and primitive would no longer be running
        self.primitive.stop()
        self.failedBootstrapCounter += 1  # reset
        self.triggerProcessing()
      elif calledFromFloodCallback:
        scheduleReceived = self.processScheduleFlood()
        if scheduleReceived:
          if self.currRcIdx == 0:
            self.failedBootstrapCounter = 0  # reset
          # transition to next slot
          if self.currRcIdx == self.lastRcIdx:
            self.fsmScheduleDone()
          else:
            self.currRcIdx += 1
            self.fsmBootstrapDone()
          # figure out if it is still possible to participate in the current round
          try:
            self.getNextSlotStart()  # fails if tStart is in the past
          except:
            # reception of schedule was too late, there is not enough time to advance to next schedule slot
            self._print('WARNING: Skipping round since schedule received too late to participate in current round!')
            self.fsmSkipRound()
          else:
            self.advanceSlot()  # intentionally called manually here instead of in "after" of transition
        else:  # no schedule received
          if self.currRcIdx == 0:
            self.failedBootstrapCounter = 0  # reset
          self.fsmBootstrap()
      else:  # primitive not running & no floodCallback
        self._print('Bootstrapping...')
        self.roundsSinceLastScheduleCounter = None  # reset
        self.deregistrationDetectedOnClient()

        # backoff for schedule reception
        randInt   = np.random.randint(low=0, high=min(self.skipBootstrapLimit, 2**self.failedBootstrapCounter))
        randRange = 0.2
        duration  = np.random.uniform(self.period*(1. - randRange), self.period*(1. + randRange))  # random duration, otherwise we could systematically miss schedule flood since we are listening for more than one LSR slot duration
        self._print('bootstrap duration: {}'.format(duration))
        if self.useBackoffForBootstrap and randInt != 0:
          self.node.timerStart(timerId=self.timerId, timeout=duration)
        else:
          # try (again) to receive schedule
          self.startListeningFlood(syncFlood=True, abortRetransmitFunc=self.abortRetransmitFuncSchedule, duration=duration)

    elif self.state == 'Schedule':
      if self.primitive.running or calledFromFloodCallback:
        if self.primitive.running:
          self.primitive.stop()
        if not self.isHost:
          self.processScheduleFlood()
        # handle case where no schedule with rcIdx==0 is received
        if self.currRcIdx == 0 and not self.isHost:
          if self.lastSchedule[0] is None:
            if (self.roundsSinceLastScheduleCounter is None) or (self.roundsSinceLastScheduleCounter >= self.numMissedSchedulesToBootstrap):
              self._print('No schedule received for {} times! -> bootstrapping'.format(self.roundsSinceLastScheduleCounter))
              self.fsmBootstrap()
            else:
              self._print('No schedule received for {} times! -> skipping round'.format(self.roundsSinceLastScheduleCounter))
              self.fsmSkipRound()
            return
        # transition to next slot/state
        if self.currRcIdx == self.lastRcIdx:
          # at this point, we received for sure at least schedule for rcIdx==0
          self.fsmScheduleDone()
        else:
          self.currRcIdx += 1
          self.fsmSchedule()
      else:  # primitive not running
        if self.isHost:
          self.sendScheduleFlood()
        else:  # not host
          self.startListeningFlood(syncFlood=True, abortRetransmitFunc=self.abortRetransmitFuncSchedule)

    elif self.state == 'Contention':
      wasSending               = self.isSendingContention if (not self.isHost) else False
      wasIdle                  = self.isIdleContention    if (not self.isHost) else False
      self.isSendingContention = False
      self.isIdleContention    = False

      if self.primitive.running or calledFromFloodCallback or wasIdle:
        if self.primitive.running:
          self.primitive.stop()

        if self.isHost:
          self.processContentionFlood()
        else:  # client
          if (self.currRcIdx == self.getBestRcIdx()) and (not wasSending):
            # only process contention if client node was listening (not when sending)
            self.checkContentionFloodReceived()

        if self.currRcIdx == self.lastRcIdx:
          self.fsmContentionDone()
          self.advanceToNextNonEmptyDataSlot(atLeastOneSlot=False)
        else:
          self.currRcIdx += 1
          self.fsmContention()
      else:  # primitive not running
        duration = self.floodLengthCont[self.currRcIdx] + self.safetyMargin

        if self.isHost:
          self.startListeningFlood(nTx=0, duration=duration)
        else:  # client
          if self.currRcIdx == self.getBestRcIdx():
            # if we use a contention slot it would be this one
            if self.isNextContentionSlotUsed():
              if self.isRcChangeDesired():
                self._print('Trying to change radioConfig from rcIdx={} to rcIdx={}; contentionSlotCounter={}'.format(self.registeredStreamRcIdx, self.getBestRcIdx(), self.contentionSlotCounter))
              self.sendContentionFlood()
            else:
              # do not send streamReq due to exponential backoff or because no stream is required
              if self.primitiveConfig.numHops[self.currRcIdx] > 1:
                self.startListeningFlood(duration=duration)
              else:  # single-hop case
                self.isIdleContention = True
                self.triggerProcessing()
            # update status of backoff
            if (self.registeredStreamRcIdx is None) or self.isRcChangeDesired():
              self.contentionSlotCounter += 1
              # self._print('contentionSlotCounter={}'.format(self.contentionSlotCounter)) # DEBUG
          else:  # not last modulation
            if self.primitiveConfig.numHops[self.currRcIdx] > 1:
              self.startListeningFlood(duration=duration)
            else:  # single-hop case
              self.isIdleContention = True
              self.triggerProcessing()

    elif self.state == 'Data':
      slots = self.lastSlots[self.currRcIdx]
      if self.primitive.running or calledFromFloodCallback:
        if self.primitive.running:
          self.primitive.stop()
        nodeIdTx = slots[self.currDataSlotIdx]
        if nodeIdTx != self.node.nodeId:
          self.processDataFlood(nodeIdTx)
        self.advanceToNextNonEmptyDataSlot(atLeastOneSlot=True)
        self.node.timerStart(timerId=self.timerId, timeout=self.getNextSlotStart(), isRelative=False)
      else:  # primitive not running
        # NOTE: Comment the following condition if all nodes should always participate in data slots
        if (not self.isHost) and (self.registeredStreamRcIdx is None):
          # Do not participate in data slots if no stream has been registered yet (without stream, host does not know that node exists and host therefore cannot optimize node)
          self.advanceToNextNonEmptyDataSlot(atLeastOneSlot=True)
          self.node.timerStart(timerId=self.timerId, timeout=self.getNextSlotStart(), isRelative=False)
        else:
          # Participate in data floods
          if slots[self.currDataSlotIdx] == int(self.node.nodeId):
            self.sendData()
          else:
            # determine and set if retransmission should be delayed
            if self.isHost:  # ignore participation and delay mask
              self.startListeningFlood(nTx=0)  # prevent host from retransmitting data message
              # self.startListeningFlood()
            else:  # client node (check participation and delay mask)
              if self.getBitFromMask(self.participMask[self.currRcIdx], nodeId=slots[self.currDataSlotIdx]) and self.primitiveConfig.numHops[self.currRcIdx] > 1:
                delayedTx = self.getBitFromMask(self.lastSchedule[self.currRcIdx]['delayMask'], self.node.nodeId)
                self.startListeningFlood(delayedTx=delayedTx)
              else:  # no particip or single-hop case
                self.advanceToNextNonEmptyDataSlot(atLeastOneSlot=True)
                self.node.timerStart(timerId=self.timerId, timeout=self.getNextSlotStart(), isRelative=False)

    elif self.state == 'Post':
      if self.isHost:
        self.updateRegisteredNodes()
        self.updateThinning()
        self.updateDelayMask()

      # execute application POST method
      if self._postCallback is not None:
        self._postCallback()

      # increase round counter
      if self.isHost:
        self.roundNo += 1
        for rcIdx, rc in enumerate(self.radioConfigs):
          self.bpsCounter[rcIdx] += 1
      else:  # client
        if self.roundsSinceLastRegCounter is not None:
          self.roundsSinceLastRegCounter += 1
        # stats about registered nodes on client
        if (self.node._sim) and (self.node._sim.stats is not None):
          self.node._sim.stats.addRegisteredNodeClient(self.node.nodeId, self.registeredStreamRcIdx)

      # determine start of next round (before updating state and without getNextSlotStart() due to round boundary!)
      numRounds = self.roundsSinceLastScheduleCounter if ((not self.isHost) and (self.roundsSinceLastScheduleCounter is not None)) else 0
      timeout   = self.lastSyncPointLt[0] + self.period*(numRounds + 1)
      timeout  -= (self.safetyMargin if not(self.isHost) else 0.0)
      # assertion
      if timeout < self.node.getTs():
        raise Exception('Timeout is in the past! ({} < {})'.format(timeout, self.node.getTs()))
      # advance state
      self.fsmPostDone()
      # cleanup
      self._resetRound()
      # schedule wakeup
      self.node.timerStart(timerId=self.timerId, timeout=timeout, isRelative=False)

  ##############################################################################
  # Callbacks
  ##############################################################################

  def rxDoneCallback(self, payload, crc, rssi=None, snr=None):
    self.primitive.rxDoneCallback(payload, crc, rssi, snr)

  def txDoneCallback(self):
    self.primitive.txDoneCallback()

  def rxTimeoutCallback(self):
    self.primitive.rxTimeoutCallback()

  def timerCallback(self, timerId):
    if timerId == self.primitive.timerId:
      self.primitive.timerCallback(timerId=timerId)
    elif timerId == self.timerId:
      # self._print('timerCallback: {}: primitive.running={}'.format(self.state, self.primitive.running)) # DEBUG
      self._updateFsm()

  def floodCallback(self):
    self.node.timerStop(timerId=self.timerId)
    self._updateFsm(calledFromFloodCallback=True)

  ##############################################################################
  # Helper Functions
  ##############################################################################

  def startListeningFlood(self, nTx=None, duration=None, syncFlood=False, delayedTx=0, abortRetransmitFunc=None):
    if nTx is None:
      nTx = self.primitiveConfig.nTx
    if self.primitiveConfig.numHops[self.currRcIdx] == 1:
      nTx = 0
    duration = self.floodLength[self.currRcIdx] + self.safetyMargin if duration is None else duration
    self.primitive.setDelayedTx(numSlots=delayedTx)
    self.node.radioConfig = self.radioConfigs[self.currRcIdx]
    ret = self.primitive.start(
      initiator=False,
      payload=None,
      nTx=nTx,
      syncFlood=syncFlood,
      abortRetransmitFunc=abortRetransmitFunc,
    )
    self.node.timerStart(timerId=self.timerId, timeout=duration)
    return ret

  def sendScheduleFlood(self, woEventGen=False):
    # store current time
    ts = self.node.getTs()
    self.lastSyncPointLt[self.currRcIdx] = ts
    self.lastSyncPointNt[self.currRcIdx] = ts  # same since we are the host
    self._print('[rcIdx={}] sendScheduleFlood: lastSyncPointLt: {}'.format(self.currRcIdx, self.lastSyncPointLt[self.currRcIdx]))
    self._print('[rcIdx={}] registeredNodes: {}'.format(self.currRcIdx, self.registeredNodes[self.currRcIdx]))

    slots = copy(self.registeredNodes[self.currRcIdx])
    # insert slots for transmissions (host -> constrained nodes)
    # NOTE: normal app messages are always sent with modulation 0
    if (len(self.txQueue) and self.currRcIdx == 0) or self.isDummyMsgRequired(self.currRcIdx):
      slots.insert(0, self.node.nodeId)
    scheduleSlots = LsrScheduleSlots(msg={
      'slots': slots,
    })
    self.lastSlots[self.currRcIdx] = slots

    # add participUpdates to message; respect max payload length and only add as many participUpdates as fit in the message
    if len(self.participUpdateQueue[self.currRcIdx]):
      # get size of empty schedule pkt
      emptyScheduleSize  = LsrSchedule(msg={'hostNodeId': 0, 'time': 0, 'period': 0, 'dataSlotOffset': 0, 'streamAck': 0, 'delayMask': 0, 'nContSlots': 0, 'nDataSlots': 0, 'nParticipUpdates': 0, 'raw': b''}).size  # dummy schedule to determine size
      participUpdateSize = LsrScheduleParticipUpdate(msg={'nodeIdTx': 0, 'participMask': 0}).size

      # add participUpdates which fit into the schedule packet
      remainingSpace                  = self.maxPayloadSize - emptyScheduleSize - scheduleSlots.size
      numAvailableParticipUpdateSlots = int(np.floor(remainingSpace / participUpdateSize))
      numParticipUpdatesMsg           = min(len(self.participUpdateQueue[self.currRcIdx]), numAvailableParticipUpdateSlots)
      participUpdateMsgBytes          = b''.join([upd.raw for upd in self.participUpdateQueue[self.currRcIdx][:numParticipUpdatesMsg]])
      self._print('[rcIdx={}] sending particip updates for nodeIdTx: {}'.format(self.currRcIdx, [upd['nodeIdTx'] for upd in self.participUpdateQueue[self.currRcIdx][:numParticipUpdatesMsg]]))

      # delete updates which fit into schedule, remaining participUpdates will be kept in the queue and will be sent in next schedule
      del self.participUpdateQueue[self.currRcIdx][:numParticipUpdatesMsg]
    else:
      numParticipUpdatesMsg  = 0
      participUpdateMsgBytes = b''

    # calc cumulative slots for dataSlotOffset
    if woEventGen:
      dataSlotOffset = 255 # FIXME: replace hardcoded value by actual size (calcsize of LsrSchedule._format['dataSlotOffset'])
    else:
      dataSlotOffset = 0
      for rcIdx in range(self.currRcIdx):
        dataSlotOffset += self.lastSchedule[rcIdx]['nDataSlots']

    # prepare msg
    schedule = LsrSchedule(msg={
      'hostNodeId':       self.node.nodeId,
      'time':             type(self).ts2us(ts) % 2**32,  # NOTE: 32bit needs to be consistent with size of 'time' field ('L') in LsrSchedule!
      'period':           self.period,
      'dataSlotOffset':   dataSlotOffset,
      'streamAck':        self.streamToAck[self.currRcIdx] if self.streamToAck[self.currRcIdx] is not None else 0,
      'delayMask':        self.delayMask[self.currRcIdx],
      'nContSlots':       1,
      'nDataSlots':       len(slots),
      'nParticipUpdates': numParticipUpdatesMsg,
      'raw':              scheduleSlots.raw + participUpdateMsgBytes,
    })
    self.lastSchedule[self.currRcIdx] = schedule
    self.streamToAck[self.currRcIdx]  = None

    # # DEBUG
    # self._print('nDataSlots: {}'.format(len(slots)))
    # self._print('nParticipUpdates: {}'.format(numParticipUpdatesMsg))

    if woEventGen:
      return

    scheduleMsg = LsrMsg(msg={
      'msgType': int(LsrMsgType.SCHEDULE),
      'src':     self.node.nodeId,
      'dst':     0,
      'payload': schedule.raw,
    })

    # Send schedule
    self.node.radioConfig = self.radioConfigs[self.currRcIdx]
    self.primitive.start(
      initiator = True,
      payload   = scheduleMsg.raw,
      nTx       = self.primitiveConfig.nTx,
      syncFlood = True,
    )

    # Setup timer for stopping primitive flood
    self.node.timerStart(timerId=self.timerId, timeout=self.floodLength[self.currRcIdx])

  def processScheduleFlood(self):
    if self.isHost:
      raise Exception('Should only be called on clients')

    scheduleReceived = False
    if self.primitive.getRxCnt():
      # self._print('getRxIndex: {}'.format(self.primitive.getRxIndex()))
      lsrMsg = LsrMsg(raw=self.primitive.getPayload())
      # self._print('lsrMsg: {}'.format(lsrMsg))
      if lsrMsg['msgType'] == int(LsrMsgType.SCHEDULE):
        # self._print('Schedule received!')
        scheduleReceived            = True
        self.tsLastScheduleReceived = self.node.getTs()
        self.hostNodeId             = lsrMsg['src']

        # process schedule
        schedule = LsrSchedule(raw=lsrMsg['payload'])
        self._print('[rcIdx={}] schedule: {}'.format(self.currRcIdx, schedule))
        self.lastSchedule[self.currRcIdx] = schedule
        self.hostNodeId                   = schedule['hostNodeId']

        if self.primitive.isTRefUpdated():
          self.lastSyncPointLt[self.currRcIdx] = self.primitive.getTRef()
          # self._print('lastSyncPointLt: {}'.format(self.lastSyncPointLt[self.currRcIdx]))
          # self._print('current Ts: {}'.format(self.node.getTs()))
          self.lastSyncPointNt[self.currRcIdx] = type(self).us2ts(schedule['time'])

        # collect hop distance measurement on client to send piggy-backed with data
        if self.useDelayedSchedule:
          # collect hop distance no matter whether delayed or not
          if (not self.isHost):
            self.hopDistanceMeasurementClient[self.currRcIdx] = self.primitive.getRxIndex() + 1
        else:
          # collect hop distance only if no delay is applied, otherwise (dummy) transmissions from host is used
          if (not self.isHost) and (schedule['delayMask'] == 0):
            self.hopDistanceMeasurementClient[self.currRcIdx] = self.primitive.getRxIndex() + 1

        # split raw bytes
        slotSize            = LsrScheduleSlots(msg={'slots': [0]}).size
        offset              = schedule['nDataSlots']*slotSize
        slotsBytes          = schedule['raw'][:offset]
        participUpdateBytes = schedule['raw'][offset:]

        # process slots info
        if schedule['nDataSlots']:
          slotsMsg                       = LsrScheduleSlots(raw=slotsBytes)
          self.lastSlots[self.currRcIdx] = slotsMsg['slots']
        else:
          self.lastSlots[self.currRcIdx] = []
        self._print('[rcIdx={}] slots: {}'.format(self.currRcIdx, self.lastSlots[self.currRcIdx]))

        # process participation updates
        participUpdateSize = LsrScheduleParticipUpdate(msg={'nodeIdTx': 0, 'participMask': 0}).size
        for i in range(schedule['nParticipUpdates']):
          participUpdate = LsrScheduleParticipUpdate(raw=participUpdateBytes[i*participUpdateSize:(i+1)*participUpdateSize])
          bitVal         = self.getBitFromMask(participUpdate['participMask'], self.node.nodeId)
          self.participMask[self.currRcIdx] = self.writeBitInMask(self.participMask[self.currRcIdx], participUpdate['nodeIdTx'], bitVal)

          # # DEBUG
          # self._print('Updated participMask: nodeIdTx={}, particip={}'.format(participUpdate['nodeIdTx'], bitVal))

        # reset roundsSinceLastRegCounter
        if self.node.nodeId in self.lastSlots[self.currRcIdx]:
          self.roundsSinceLastRegCounter = 0

        # update state based on content of schedule msg
        if self.node.nodeId == schedule['streamAck']:
          # node received stream acknowledgement
          self._print('Stream registered for rcIdx={}, previous rcIdx={}'.format(self.currRcIdx, self.registeredStreamRcIdx))
          self.registeredStreamRcIdx = self.currRcIdx
          self.contentionSlotCounter = 0  # reset counter for exponential backoff
          self.tsLastSlotAssignmentReceived = self.node.getTs()  # initialize here to make sure that node detects condition when slot assignment is never received

        if self.node.nodeId in self.lastSlots[self.currRcIdx]:
          # node received a data slot assignment
          self.tsLastSlotAssignmentReceived = self.node.getTs()

        if (self.registeredStreamRcIdx is not None) and (self.registeredStreamRcIdx == self.currRcIdx) and (not self.isRcChangeDesired()):
          # check for stream deregistration
          timeSinceLastSchedule     = self.node.getTs() - self.tsLastScheduleReceived  # time since last reception of schedule of active rcIdx
          timeSinceLastSlotAssigned = self.node.getTs() - self.tsLastSlotAssignmentReceived
          if (timeSinceLastSlotAssigned > timeSinceLastSchedule) or (timeSinceLastSlotAssigned > self.noSlotAssignmentReceivedThreshold):  # this assumes that for a registered stream the schedule always contains a data slot (i.e. period of stream == period of LSR protocol); change if this no longer the case
            self._print('Deregistration of stream detected (timeSinceLastSlotAssigned became too large)')
            self.deregistrationDetectedOnClient()

    # update roundsSinceLastScheduleCounter
    if (self.currRcIdx == 0):
      if scheduleReceived:
        self.roundsSinceLastScheduleCounter = 0
      else:  # no schedule received
        if self.roundsSinceLastScheduleCounter is not None:
          self.roundsSinceLastScheduleCounter += 1

    return scheduleReceived

  def sendContentionFlood(self):
    self._print('sendContentionFlood')
    self.isSendingContention = True

    # prepare msg
    contention = LsrStreamReq(msg=OrderedDict([
      ('nodeId', self.node.nodeId),
      ('changeExistingStream', 0),  # currently changing data rate of an existing stream is not used
      ('msgSize', 30),
      ('nMsgs', 1),
      ('period', self.streamPeriod),
    ]))
    contMsg = LsrMsg(msg=OrderedDict([
      ('msgType', int(LsrMsgType.STREAM_REQ)),
      ('src', self.node.nodeId),
      ('dst', self.hostNodeId),
      ('payload', contention.raw),
    ]))

    # Send contention
    self.node.radioConfig = self.radioConfigs[self.currRcIdx]
    self.primitive.start(
      initiator = True,
      payload   = contMsg.raw,
      nTx       = self.primitiveConfig.nTx,
      syncFlood = False,
    )

    # Setup timer for stopping primitive flood
    self.node.timerStart(timerId=self.timerId, timeout=self.floodLengthCont[self.currRcIdx])

  def processContentionFlood(self):
    if self.primitive.getRxCnt():
      # self._print('getRxIndex: {}'.format(self.primitive.getRxIndex()))
      lsrMsg = LsrMsg(raw=self.primitive.getPayload())
      # self._print('lsrMsg: {}'.format(lsrMsg))
      if lsrMsg['msgType'] == int(LsrMsgType.STREAM_REQ):
        self._print('Stream Request received!')
        lsrStreamReq = LsrStreamReq(raw=lsrMsg['payload'])
        self._print('lsrStreamReq: {}'.format(lsrStreamReq))
        self.streamRequestQueue[self.currRcIdx].append(lsrStreamReq)

  def checkContentionFloodReceived(self):
    """Reset the contention counter if no contention received in listening flood
    """
    if self.primitive.getRxCnt():
      lsrMsg = LsrMsg(raw=self.primitive.getPayload())
      if lsrMsg['msgType'] == int(LsrMsgType.STREAM_REQ):
        # we received a stream request -> do not reset contention counter
        return
    # reset contention counter to speed up contention in case contention slot was not used
    # self._print('reset contentionSlotCounter') # DEBUG
    self.contentionSlotCounter = 0

  def sendData(self):
    self._print('sendData')

    if len(self.txQueue) or self.isDummyMsgRequired(self.currRcIdx):
      if len(self.txQueue):
        self._print('Data in txQueue available -> sending 1 element')
        appMsg = self.txQueue.pop(0)
        if not isinstance(appMsg, ApplicationMsg):
          raise Exception('Element in txQueue is not of type ApplicationMsg!')
        if (self.node._sim) and (self.node._sim.stats is not None):
          self.node._sim.stats.addSentMsg(appMsg, txNodeId=self.node.nodeId, rxNodeId=(0 if self.isHost else self.hostNodeId))  # add msg to statistics

        # add piggy-back hop distance measurements of rcConfigs with more link budget than currently used (clients only)
        hdmList = []
        if not self.isHost and self.currRcIdx != 0:
          for rcIdx in range(self.currRcIdx):
            if self.hopDistanceMeasurementClient[rcIdx] is not None:
              hdmList.append(LsrHopDistanceMeasurement(msg={'rcIdx': rcIdx, 'hopDistance': self.hopDistanceMeasurementClient[rcIdx]}))
            else:
              print('WARNING: no hop distance measurement collected!')  # this can happen in case the data/dummy flood from the host is not received

        lsrDataMsg = LsrDataMsg(msg={
          'nHopDistanceMeasurements': len(hdmList),
          'raw':                      b''.join([hdm.raw for hdm in hdmList]) + appMsg.raw,
        })

        if lsrDataMsg.size > self.maxPayloadSize:
          raise Exception('Size of Data message ({}) exceeds self.maxPayloadSize={}!'.format(lsrDataMsg.size, self.maxPayloadSize))

        lsrMsg = LsrMsg(msg=OrderedDict([
          ('msgType', int(LsrMsgType.DATA)),
          ('src', self.node.nodeId),
          ('dst', 0 if self.isHost else self.hostNodeId),
          ('payload', lsrDataMsg.raw),
        ]))

      elif self.isDummyMsgRequired(self.currRcIdx):
        self._print('Dummy msg required -> sending dummy msg')
        lsrMsg = LsrMsg(msg=OrderedDict([
          ('msgType', int(LsrMsgType.DUMMY)),
          ('src', self.node.nodeId),
          ('dst', 0 if self.isHost else self.hostNodeId),
          ('payload', b''),
        ]))
      else:
        raise Exception('This should never happen. One of the condition must be true!')

      # send data flood
      self.node.radioConfig = self.radioConfigs[self.currRcIdx]
      self.primitive.start(
        initiator = True,
        payload   = lsrMsg.raw,
        nTx       = self.primitiveConfig.nTx,
        syncFlood = False,
      )

      # Setup timer for stopping primitive flood
      self.node.timerStart(timerId=self.timerId, timeout=self.floodLength[self.currRcIdx])

    else:  # no msg in txQueue
      self._print('No element in txQueue and isDummyMsgRequired is False -> skipping data slot')
      self.advanceToNextNonEmptyDataSlot(atLeastOneSlot=True)
      self.node.timerStart(timerId=self.timerId, timeout=self.getNextSlotStart(), isRelative=False)

  def processDataFlood(self, nodeId):
    """
    Args:
      nodeId: ID of node who initiated the flood
    """
    if self.primitive.getRxCnt() == 0:
      if self.isHost:
        self._print('No data from node {} received!'.format(nodeId))
    else:
      # self._print('getRxIndex: {}'.format(self.primitive.getRxIndex()))
      lsrMsg = LsrMsg(raw=self.primitive.getPayload())
      # self._print('lsrMsg: {}'.format(lsrMsg))

      if lsrMsg['msgType'] == int(LsrMsgType.DATA):
        lsrDataMsg = LsrDataMsg(raw=lsrMsg['payload'])
        hdmSize    = LsrHopDistanceMeasurement(msg={'rcIdx': 0, 'hopDistance': 0}).size
        dataRaw    = lsrDataMsg['raw']

        if lsrMsg['dst'] == self.node.nodeId or lsrMsg['dst'] == 0:
          self._print('Data from node {} received!'.format(lsrMsg['src']))
          appMsg = ApplicationMsg(raw=dataRaw[lsrDataMsg['nHopDistanceMeasurements']*hdmSize:])
          if len(self.rxQueue) < self.queueSize:
            self.rxQueue.append((appMsg, nodeId))
          else:
            self._print('WARNING: rxQueue is full! Received msg discarded!')

          if self.isHost:
            self.tsLastDataReceived[nodeId] = self.node.getTs()
            # collect thinning stats
            self.hopDistanceMeasurementsHost[self.currRcIdx][nodeId].append(self.primitive.getRxIndex() + 1)
            # collect thinning stats from piggy-back info of modulations with larger link budget (i.e. lower rcIdx)
            for i in range(lsrDataMsg['nHopDistanceMeasurements']):
              hdm = LsrHopDistanceMeasurement(raw=dataRaw[i*hdmSize:(i+1)*(hdmSize)])
              self.hopDistanceMeasurementsHost[hdm['rcIdx']][nodeId].append(hdm['hopDistance'])

        if (not self.useDelayedSchedule) and (not self.isHost and lsrMsg['src'] == self.hostNodeId):
          # collect hop distance measurement on client to send piggy-backed with data  (only for the case that delay is applied)
          if nodeId == self.hostNodeId and self.lastSchedule[self.currRcIdx]['delayMask'] != 0:
            self.hopDistanceMeasurementClient[self.currRcIdx] = self.primitive.getRxIndex() + 1

      elif (not self.useDelayedSchedule) and (lsrMsg['msgType'] == int(LsrMsgType.DUMMY)):
        # only non-host nodes should receive dummy msgs
        if self.isHost:
          raise Exception('Host node received a dummy message!')
        # collect hop distance measurement on client to send piggy-backed with data (only for the case that delay is applied)
        if not self.isHost and self.lastSchedule[self.currRcIdx]['delayMask'] != 0:
          self.hopDistanceMeasurementClient[self.currRcIdx] = self.primitive.getRxIndex() + 1

      else:
        if self.isHost:
          self._print('No data from node {} received!'.format(nodeId))

  def incrementDataSlot(self):
    slots = None
    if self.lastSchedule[self.currRcIdx] is not None:
      # slot info for currRcIdx is available
      slots = self.lastSlots[self.currRcIdx]

    if (slots is None) or (self.currDataSlotIdx >= len(slots) - 1):
      # second condition: last data slot ("=") or schedule for current rcIdx does not contain any data slots (">")
      self.currDataSlotIdx = 0
      if self.currRcIdx == self.lastRcIdx:
        self.fsmDataDone()
      else:
        self.currRcIdx += 1
    else:
      self.currDataSlotIdx += 1

  def isCurrDataSlotValid(self):
    slots = self.lastSlots[self.currRcIdx]
    if (not self.isHost) and (self.registeredStreamRcIdx is not None) and (self.currRcIdx > self.registeredStreamRcIdx):
      # clients should not participate in data floods with modulation lower than stored in registeredStreamRcIdx
      return False
    if (slots is not None) and (len(slots) > 0) and (self.currDataSlotIdx < len(slots)):
      return True
    else:
      return False

  def advanceToNextNonEmptyDataSlot(self, atLeastOneSlot=False):
    """Advance state (by incrementing currDataSlotIdx and currRcIdx) to the next taken slot (or the next self.state if there is no taken slot)
    Args:
      atLeastOneSlot: True: advance at least one slot; False: if current state is a non-empty, state is not advanced
    """
    if atLeastOneSlot:
      self.incrementDataSlot()
    while self.state == 'Data' and not self.isCurrDataSlotValid():
      self.incrementDataSlot()

  def advanceSlot(self):
    if not self.primitive.running:
      if self.state == 'Bootstrap':
        self.triggerProcessing()
      else:
        self.node.timerStart(timerId=self.timerId, timeout=self.getNextSlotStart(), isRelative=False)

  def triggerProcessing(self):
    self.node.timerStart(timerId=self.timerId, timeout=0)

  def resetCurrRcIdx(self):
    self.currRcIdx = 0

  def isNextFloodRx(self):
    if self.state == 'Schedule':
      return not self.isHost
    elif self.state == 'Contention':
      if self.isHost:
        return True
      else:  # not host
        return not(self.isNextContentionSlotUsed() and self.currRcIdx == self.getBestRcIdx())
    elif self.state == 'Data':
      slots = self.lastSlots[self.currRcIdx]
      return (slots[self.currDataSlotIdx] != self.node.nodeId) if slots else True
    else:
      raise Exception('Unexpected state for isNextFloodRx()!')

  def getNextSlotStart(self):
    """Get time of start of the next slot (with respecting safety margin). Should be called after updating self.state
       NOTE: should be called AFTER advancing slot (by updating self.currRcIdx and self.currDataSlotIdx and self.state (if required)), should not be used for wait time between rounds (Post -> Schedule) as the time reference would not be defined since no schedule has been received yet...
    """
    tStart    = 0
    numRounds = self.roundsSinceLastScheduleCounter if ((not self.isHost) and (self.roundsSinceLastScheduleCounter is not None)) else 0
    tStart   += self.lastSyncPointLt[0] + self.period*numRounds

    # IDEA: support rounds without rcIdx==0 schedules by using lastSyncPointLt[currRcIdx]
    if   self.state == 'Schedule':
      tStart += (self.currRcIdx)*self.slotLength[self.currRcIdx] - self.isNextFloodRx()*self.safetyMargin
    elif self.state == 'Contention':
      tStart += (len(self.radioConfigs) + self.currRcIdx)*self.slotLength[self.currRcIdx] - self.isNextFloodRx()*self.safetyMargin
    elif self.state == 'Data':
      tStart += (2*len(self.radioConfigs) + self.lastSchedule[self.currRcIdx]['dataSlotOffset'] + self.currDataSlotIdx) * self.slotLength[self.currRcIdx] - self.isNextFloodRx()*self.safetyMargin  # 2*len(self.radioConfigs): "2x" for Schedule and Contention slots
    elif self.state == 'Post':
      tStart = self.node.getTs()
    else:
      raise Exception('Unexpected state ({}) for getNextSlotStart()!'.format(self.state))

    # assertion
    if tStart < self.node.getTs():
      raise Exception('[nodeId={}] Timeout is in the past! ({} < {})'.format(self.node.nodeId, tStart, self.node.getTs()))

    return tStart

  def isNextContentionSlotUsed(self):
    """Determines if node wants to make use of the upcoming contention slot
    """
    # reduce probability of collision by using exponential backoff
    # IDEA: set initial counter value != 0 to improve situation at startup when all nodes try to connect

    if self.backoffUseContention is None:
      randInt = np.random.randint(low=0, high=min(self.numNodes, 2**self.contentionSlotCounter))
      self.backoffUseContention = (randInt == 0)  # store decision for the case that isNextContentionSlotUsed is called multiple times in the same round
    if self.backoffUseContention:
      self._print(f'registeredStreamRcIdx: {self.registeredStreamRcIdx}')
      return (self.registeredStreamRcIdx is None) or (self.isRcChangeDesired())
    else:
      return False

  def updateRegisteredNodes(self, singleRcIdx=None):
    """Deregister and register nodes on the host.
       NOTE: Host-only method
       Args:
        singleRcIdx: if None, update registered nodes for all rcIdx; if integer: only update rcIdx of a single node
    """
    # update stats
    if (singleRcIdx is None) or (singleRcIdx == len(self.radioConfigs) - 1):
      if (self.node._sim) and (self.node._sim.stats is not None):
        self.node._sim.stats.addRegisteredNodesHost(self.registeredNodes, self.roundNo)

    rcList = range(len(self.radioConfigs)) if (singleRcIdx is None) else [singleRcIdx]
    for rcIdx in rcList:
      # remove nodes for which host did not receive data recently
      for nodeId in self.registeredNodes[rcIdx]:
        timeSinceLastDataReceived = self.node.getTs() - self.tsLastDataReceived[nodeId]
        if timeSinceLastDataReceived > self.noDataReceivedThreshold:
          self.deregisterNode(nodeId, rcIdx)

      # process stream requests
      if len(self.streamRequestQueue[rcIdx]):
        streamReq = self.streamRequestQueue[rcIdx].popleft()
        nodeId    = streamReq['nodeId']
        if streamReq['period'] != self.period:
          raise Exception('Currently, streams with a period different from the round period are not implemented/supported! ({} != {})'.format(streamReq['period'], self.period))

        # reset thinning state only if node indicates that it detected a deregistration (i.e. changeExistingStream=False)
        self.registerNode(nodeId, rcIdx, streamReq['changeExistingStream'])
      if len(self.streamRequestQueue[rcIdx]):
        raise Exception('Multiple stream requests for same rcIdx were queued! This should not happen!')

  @property
  def numNodes(self):
    return self.maxNodeId - self.minNodeId + 1

  @staticmethod
  def ts2us(ts):
    return int(ts * 1e6)

  @staticmethod
  def us2ts(us):
    return float(us) / 1e6

  def getBitFromMask(self, mask, nodeId):
    if nodeId < self.minNodeId or nodeId > self.maxNodeId:
      raise Exception('Cannot extract bit from bit mask, nodeId out of range!')
    pos = nodeId - self.minNodeId
    return (mask >> pos) & 0b1

  def setBitInMask(self, mask, nodeId):
    if nodeId < self.minNodeId or nodeId > self.maxNodeId:
      raise Exception('Cannot extract bit from bit mask, nodeId out of range!')
    pos = nodeId - self.minNodeId
    return (0b1 << pos) | mask

  def clearBitInMask(self, mask, nodeId):
    if nodeId < self.minNodeId or nodeId > self.maxNodeId:
      raise Exception('Cannot extract bit from bit mask, nodeId out of range!')
    pos = nodeId - self.minNodeId
    return ~(0b1 << pos) & mask

  def writeBitInMask(self, mask, nodeId, bitVal):
    if bitVal:
      return self.setBitInMask(mask, nodeId)
    else:
      return self.clearBitInMask(mask, nodeId)

  def printState(self):
    pass
    # self._print('state={}, currRcIdx={}'.format(self.state, self.currRcIdx))

  def getRcIdxOfLastReceivedSchedule(self):
    for rcIdx in reversed(range(len(self.radioConfigs))):
      if not self.lastSchedule[rcIdx] is None:
        return rcIdx
    return None

  def getBestRcIdx(self):
    ret = np.round(np.percentile(self.lastReceivedScheduleRcIdxStats, self.selRcIdxPercentile)).astype(int)

    # override: use higher modulation (lower rcIdx) if stream request did not lead to a registered stream many times
    # situation: node receives schedule in bestRcIdx but cannot successfully send stream request
    if self.roundsSinceLastRegCounter is not None and self.roundsSinceLastRegCounter > self.numNodes:
      ret = max(ret - int(self.roundsSinceLastRegCounter/self.numNodes), 0)

    return ret

  def updateLastReceivedScheduleRcIdxStats(self):
    if not self.isHost:
      lastRcIdx = self.getRcIdxOfLastReceivedSchedule()
      if lastRcIdx is not None:
        self.lastReceivedScheduleRcIdxStats.append(lastRcIdx)

  def isRcChangeDesired(self):
    # check if change of radioConfig would make sense
    # NOTE: does not return True if self.registeredStreamRcIdx == None
    # NOTE: do not set self.registeredStreamRcIdx=None as this would circumvent the contention backoff mechanism and prevent normal participation! -> we introduced isRcChangeDesired() to detect case where change is desired
    return (len(self.lastReceivedScheduleRcIdxStats) == self.lastReceivedScheduleRcIdxStats.maxlen) and (self.registeredStreamRcIdx is not None) and (self.getBestRcIdx() != self.registeredStreamRcIdx)

  @staticmethod
  def plotEnergyPerRound(sim, radioConfigs, outFile, primitiveMsgClass=GloriaMsg):
    """Plots energy consumed in each round for each node
    """

    def isBoundaryMsg(payload):
      primitiveMsg = primitiveMsgClass(raw=payload)
      if primitiveMsg['slot'] != 0:
        return False
      lsrMsg = LsrMsg(raw=primitiveMsg['payload'])
      return lsrMsg['msgType'] == int(LsrMsgType.SCHEDULE)

    eplrp = plot_energy_lsr.EnergyPerLsrRoundPlot()
    eplrp.loadFromSim(sim, radioConfigs, isBoundaryMsg)
    eplrp.generatePlot(os.path.join(sim._outputDir, outFile))

  @staticmethod
  def isNoScheduleMsg(payload):
    if len(payload) == 0:
      return True
    lsrMsg = LsrMsg(raw=payload)
    return lsrMsg['msgType'] != int(LsrMsgType.SCHEDULE)

  def abortRetransmitFuncSchedule(self, payload):
    abort = self.isNoScheduleMsg(payload)

    if self.useDelayedSchedule:
      self.extractScheduleAndSetPrimitiveDelay(payload)

    return abort

  def preRegisterNode(self, nodeId, rcIdx):
    self.preRegisterNodes[rcIdx].append(nodeId)

  def preActivateRegistration(self, rcIdx, dataFirstRound=True):
    self.preActivatedRcIdx = rcIdx
    self.preActivatedDataFirstRound = dataFirstRound

##############################################################################
# Thinning
##############################################################################

  def _resetThinning(self):
    # state per radioConfig
    # host & client
    self.delayMask = OrderedDict()                            # bit position: txNode
    if self.isHost:
        # host
        self.thinningHistory                 = OrderedDict()
        self.currNotDelayedNode              = OrderedDict()
        self.currDelayedLayer                = OrderedDict()
        self.hopDistanceMeasurementsHost     = OrderedDict()  # any hop distance measurements collected during the round (w/ and w/o delayed retransmissions)
        self.hopDistanceStats                = OrderedDict()  # stats based on retransmissions w/o delay (estimate of true hop distance)
        self.participMasks                   = OrderedDict()  # key: txNode; bit position: participNode (all nodes with a bit set to 1 in participMasks[txNode] particip in the flood initiated by txNode)
        self.participUpdateQueue             = OrderedDict()
        self.participMasksLast               = OrderedDict()
        self.participNewNode                 = OrderedDict()
        self.cg                              = OrderedDict()  # connectivity graph
        self.notDelayedNodeQueue             = OrderedDict()  # state for measuring using delayed retransmissions
        self._delayStats                     = OrderedDict()  # stats for measurements based on delayed retransmissions
        self.bpsCounter                      = OrderedDict()  # counts rounds since last update of "best path selection", for periodic triggering of update
        self.selectedShortestPath            = OrderedDict()  # store selected shortest path to ensure same path is used once selected; to prevent too many participUpdates
        for nodeId in [nodeId for nodeId in range(self.minNodeId, self.maxNodeId + 1) if nodeId != self.hostNodeId]:
          self.selectedShortestPath[nodeId]  = None
    else:
        # client
        self.participMask                 = OrderedDict()     # bit position: txNode; (self.node.nodeId participates in all slots which correspond to a bit which is set to 1 in participMask)
        self.hopDistanceMeasurementClient = OrderedDict()     # hop distance measurement for rcIdx > currRcIdx

    # init for all rcIdx
    for rcIdx, rc in enumerate(self.radioConfigs):
      # host & client
      self.delayMask[rcIdx] = 0
      if self.isHost:
        # host
        self.thinningHistory[rcIdx]                 = []
        self.currNotDelayedNode[rcIdx]              = None
        self.currDelayedLayer[rcIdx]                = None
        self.hopDistanceMeasurementsHost[rcIdx]     = OrderedDict()
        self.hopDistanceStats[rcIdx]                = OrderedDict()
        self.participMasks[rcIdx]                   = {}
        self.participUpdateQueue[rcIdx]             = []
        self.participMasksLast[rcIdx]               = None
        self.participNewNode[rcIdx]                 = False
        self.cg[rcIdx]                              = nx.Graph()
        self.cg[rcIdx].add_node(self.hostNodeId)  # always add host node
        for nodeId in range(self.minNodeId, self.maxNodeId + 1):
          self.participMasks[rcIdx][nodeId] = MASK_INIT_SET
        for nodeId in self.registeredNodes[rcIdx]:
          self.initThinningForNode(nodeId, rcIdx)
        self.notDelayedNodeQueue[rcIdx] = deque([])
        self.bpsCounter[rcIdx]          = 0
        self._delayStats[rcIdx]         = dict()
        for edge in itertools.product(range(self.minNodeId, self.maxNodeId + 1), range(self.minNodeId, self.maxNodeId + 1)):
          if edge[0] == edge[1]:
            continue
          self._delayStats[rcIdx][tuple(sorted(edge))] = deque([], maxlen=self.statsDepth)

      else:
        # client
        self.participMask[rcIdx]                 = MASK_INIT_SET
        self.hopDistanceMeasurementClient[rcIdx] = None

  def initThinningForNode(self, nodeId, rcIdxArg):
    # init thinning related structures
    for rcIdx in range(rcIdxArg + 1):
      self.hopDistanceMeasurementsHost[rcIdx][nodeId] = []
      self.hopDistanceStats[rcIdx][nodeId]            = deque([], maxlen=self.statsDepth)
      self.participMasks[rcIdx][nodeId]               = MASK_INIT_SET  # all other nodes should participate when deregistered node sends (key: nodeIdTx, bit position: particip node)

      # TODO: implement alternative particip update sending (bit position: nodeIdTx; nodeId participates in floods initiated by nodes for which bit is set) -> this could potentially reduce the number of sent participUpdates
      for nodeIdTx in [nId for nId in range(self.minNodeId, self.maxNodeId + 1) if nId != nodeId]:
        self.participMasks[rcIdx][nodeIdTx] = self.setBitInMask(self.participMasks[rcIdx][nodeIdTx], nodeId)  # deregistered node should participate in all transmissions sent by other nodes
      self.cg[rcIdx].add_node(nodeId)

  def deinitThinningForNode(self, nodeId, rcIdxArg):
    # deinit thinning related structures
    for rcIdx in range(rcIdxArg + 1):
      del self.hopDistanceMeasurementsHost[rcIdx][nodeId]
      del self.hopDistanceStats[rcIdx][nodeId]
      # NOTE: resetting particip info is redundant when deregistering -> will be done when node is registered (again)
      self.selectedShortestPath[nodeId] = None

      # remove node from connectivity graph
      self.cg[rcIdx].remove_node(nodeId)

      # reset hop distance buffer (hopDistanceStats) and participMask for all nodes which are disconnected to host after the removal of the node
      for nodeIdIter in [n for n in self.cg[rcIdx].nodes() if n != nodeId]:
        if not nx.has_path(self.cg[rcIdx], self.hostNodeId, nodeIdIter):
          self._print('[deinitThinningForNode] [rcIdx={}] Reset hopDistanceStats and connectivity graph for nodeId={}'.format(rcIdx, nodeIdIter))
          self.cg[rcIdx].remove_node(nodeIdIter)  # ensure that all corresponding edges are removed as well
          self.initThinningForNode(nodeIdIter, rcIdx)

      # reset delay measurement stats for node
      for edge in itertools.product(range(self.minNodeId, self.maxNodeId + 1), (nodeId,)):
        if edge[0] == edge[1]:
          continue
        self._delayStats[rcIdx][tuple(sorted(edge))].clear()

  def updateThinning(self, singleRcIdx=None):
    """Update thinning state.
       NOTE: Host-only method
       Args:
        singleRcIdx: if None, update registered nodes for all rcIdx; if integer: only update rcIdx of a single node
    """
    # collect, store, and use thinning stats
    rcList = range(len(self.radioConfigs)) if (singleRcIdx is None) else [singleRcIdx]
    for rcIdx in rcList:
      # save snapshot of hop distance measurements, delay, and particip info
      self.thinningHistory[rcIdx].append({
        'hopDistance':    self.hopDistanceMeasurementsHost[rcIdx],
        'delayMask':      self.delayMask[rcIdx],
        'participMasks':  copy(self.participMasks[rcIdx]),
        'delayedLayer':   self.currDelayedLayer[rcIdx],
        'notDelayedNode': self.currNotDelayedNode[rcIdx],
      })

      # update hop distance stats data
      if self.delayMask[rcIdx] == 0:
        for nodeId, hpMeasurements in self.hopDistanceMeasurementsHost[rcIdx].items():
          if hpMeasurements:
            self.hopDistanceStats[rcIdx][nodeId].extend(hpMeasurements)  # hopDistanceStats uses a circular buffer, i.e. old items are automatically overwritten when appending

      # determine hop distance for all nodes for which we have collected enough stats
      if self.thinningHopDistance or self.thinningConnectivityGraph:
        hopDistanceMean, hopDistanceRounded, hopDistanceMap = self.getHopDistance(rcIdx=rcIdx)
        # DEBUG
        hopDistanceMeanPrint = {k: round(v, 2) if isinstance(v, float) else v for k, v in hopDistanceMean.items()}
        self._print('[updateThinning] [roundNo={}] [rcIdx={}] hopDistanceMean: {}'.format(self.roundNo, rcIdx, hopDistanceMeanPrint))
        self._print('[updateThinning] [roundNo={}] [rcIdx={}] hopDistanceMap: {}'.format(self.roundNo, rcIdx, hopDistanceMap))

      # update participMasks based on updated hop distance stats
      if self.thinningHopDistance:
        for nodeIdTx in self.registeredNodes[rcIdx]:
          for nodeIdParticip in self.hopDistanceMeasurementsHost[rcIdx].keys():  # all nodes which are known to potentially collect hop distance measurements will potentially participate
            if (hopDistanceMean[nodeIdParticip] >= hopDistanceMean[nodeIdTx]):
              # nodeIdParticip-node should not participate in floods initiated by nodeIdTx-node
              self.participMasks[rcIdx][nodeIdTx] = self.clearBitInMask(self.participMasks[rcIdx][nodeIdTx], nodeIdParticip)
            else:
              self.participMasks[rcIdx][nodeIdTx] = self.setBitInMask(self.participMasks[rcIdx][nodeIdTx], nodeIdParticip)

      # update connectivity graph
      if self.thinningHopDistance or self.thinningConnectivityGraph:
        if self.thinningHopDistance:
          # update based on hop distance measurements (connections with the host)
          # NOTE: update in any case (delayMask == 0b0 or delayMask != 0) since we receive piggy-back hopDistance measurements independent of delayMask
          # prepare
          oldFirstLayerNodes = list(self.cg[rcIdx].neighbors(self.hostNodeId))
          oldFirstLayerNodes = set(oldFirstLayerNodes)
          newFirstLayerNodes = set(hopDistanceMap[1]) if (1 in hopDistanceMap) else set()

          # remove superfluous edges (first layer)
          for nodeId in oldFirstLayerNodes.difference(newFirstLayerNodes):
            self.cg[rcIdx].remove_edge(self.hostNodeId, nodeId)

          # remove edges of nodes where hop distance is not consistent with connectivity graph
          # FIXME: check if the following is also necessary in case of only hop distance thinning
          for d, nodeIdList in hopDistanceMap.items():
            for nodeId in nodeIdList:
              if ( (not nx.has_path(self.cg[rcIdx], self.hostNodeId, nodeId)) or
                   (nx.has_path(self.cg[rcIdx], self.hostNodeId, nodeId) and (nx.shortest_path_length(self.cg[rcIdx], self.hostNodeId, nodeId) != d)) ):
                edgeRmList = list(self.cg[rcIdx].edges(nodeId))
                self.cg[rcIdx].remove_edges_from(edgeRmList)

          # add missing edges (first layer)
          for nodeId in newFirstLayerNodes.difference(oldFirstLayerNodes):
            self.cg[rcIdx].add_edge(self.hostNodeId, nodeId)
          # add all edges to upper layer if lower layer consists of a single node only, iteratively repeat for all layers (only in case all hop distances have been measured already!)
          nodesWithoutHopDistance = [nodeId for nodeId, hd in hopDistanceMean.items() if np.isnan(hd)]
          layersWithoutNodes      = [k for k, v in hopDistanceMap.items() if len(v) == 0]  # to prevent cases where hopDistanceMap[layerBelow] is empty
          if (len(nodesWithoutHopDistance) == 0) and (len(layersWithoutNodes) == 0):  # condition that self._thinningStatus[rcIdx] could be set to ThinningState.MeasuringDelay
            for i in range(1, max(hopDistanceMap)):
              if len(hopDistanceMap[i]) == 1:  # layer consisting of only one node
                # add edges for all nodes in the higher layer
                for nodeId in hopDistanceMap[i+1]:
                  self.cg[rcIdx].add_edge(hopDistanceMap[i][0], nodeId)
                # remove node from notDelayedNodeQueue as all higher layer nodes are now connected
                self.notDelayedNodeQueue[rcIdx] = deque([e for e in self.notDelayedNodeQueue[rcIdx] if e != hopDistanceMap[i][0]])

        if self.thinningConnectivityGraph and self.delayMask[rcIdx] != 0b0:  # NOTE: condition is only sufficient to detect round with delay measurements if cases with a layer consisting of a single node are handled by updateThinning (by adding edges in these cases); otherwise delay mask would be 0b0 in these cases
          # update based on delay measurements
          if (self.currDelayedLayer[rcIdx] + 1) in hopDistanceMap:
            for nodeIdTx in hopDistanceMap[self.currDelayedLayer[rcIdx] + 1]:
              # compare hopDistance measured with delayed nodes with hopDistance measured without delayed nodes; if no change, add connection
              if (nodeIdTx in self.hopDistanceMeasurementsHost[rcIdx]) and len(self.hopDistanceMeasurementsHost[rcIdx][nodeIdTx]) and (nodeIdTx in hopDistanceMean):
                hopDistanceMeasurement = np.mean(self.hopDistanceMeasurementsHost[rcIdx][nodeIdTx])
                # self._print('[updateThinning] [rcIdx={}] [nodeIdTx={}] hopDistanceMeasurement: {:0.3f}, hopDistanceMean: {:0.3f}'.format(rcIdx, nodeIdTx, hopDistanceMeasurement, hopDistanceMean[nodeIdTx])) # DEBUG
                currEdge = tuple(sorted((nodeIdTx, self.currNotDelayedNode[rcIdx])))  # use sorted to make notation of bi-directional edge unique; tuple is necessary to use edge as key for dict
                if type(self).hopDistanceRound(hopDistanceMeasurement) <= hopDistanceRounded[nodeIdTx]:
                  self._delayStats[rcIdx][currEdge].append(1)
                else:
                  self._delayStats[rcIdx][currEdge].append(0)

                if np.percentile(self._delayStats[rcIdx][currEdge], self.delayStatsPercentile) > 0.5:
                  self.cg[rcIdx].add_edge(*currEdge)  # NOTE: adding edge which already exists is not a problem (no error is raised and it will not be added a second time)
                else:
                  if self.cg[rcIdx].has_edge(*currEdge):
                    self.cg[rcIdx].remove_edge(*currEdge)
              else:
                self._print('[updateThinning] [roundNo={}] [rcIdx={}] [nodeIdTx={}] no measurements!'.format(self.roundNo, rcIdx, nodeIdTx))  # DEBUG

        # ensure that no node has connections to nodes of the same layer (this can happen if new edges are added)
        # IDEA: maybe it would be beneficial to keep edges between nodes of the same layer to reduce overhead when shorter path is lost again; in this case maybe GraphPlot should be adapted to only plot the shortest connectivity graph instead of the full graph
        for nodeId in self.cg[rcIdx].nodes:
          neighbors = list(self.cg[rcIdx].neighbors(nodeId))
          for n in neighbors:
            if (n in hopDistanceRounded) and (nodeId in hopDistanceRounded):
              if hopDistanceRounded[n] == hopDistanceRounded[nodeId]:
                self.cg[rcIdx].remove_edge(n, nodeId)

        # DEBUG
        self._print('[updateThinning] [roundNo={}] [rcIdx={}] cg.edges: {}'.format(self.roundNo, rcIdx, self.cg[rcIdx].edges()))
        if (self.node._sim) and (self.node._sim.stats is not None):
          self.node._sim.stats.addGraphInfo(hopDistanceMap, self.cg[rcIdx].edges(), self.roundNo, rcIdx)

      # update participMasks based on connectivity graph
      if self.thinningHopDistance or self.thinningConnectivityGraph:
        if len(self.notDelayedNodeQueue[rcIdx]) == 0:  # only update thinning based on connectivity graph when probing of a full layer has been completed (to prevent disabling nodes while probing is still ongoing)
          # determine all available shortest paths for all nodes
          allShortestPaths = OrderedDict()
          for nodeIdTx in self.registeredNodes[rcIdx]:
            if self.cg[rcIdx].has_node(nodeIdTx):
              try:
                allShortestPaths[nodeIdTx] = list(nx.all_shortest_paths(self.cg[rcIdx], nodeIdTx, self.node.nodeId))
              except nx.NetworkXNoPath:
                pass

          # select shortest paths for every node (2 approaches)
          if self.useBps:
            # exhaustive search with random selection of one of the best combinations
            if len(allShortestPaths):
              # only update set of selected paths if for at least one node there is no shortest path selected or if selected shortest path does not exist anymore
              pathMissing = False
              for nodeIdTx in allShortestPaths.keys():
                if (self.selectedShortestPath[nodeIdTx] is None) or (self.selectedShortestPath[nodeIdTx] not in allShortestPaths[nodeIdTx]):
                  pathMissing = True
              if pathMissing or (self.bpsRoundThreshold is not None and self.bpsCounter[rcIdx] >= self.bpsRoundThreshold):
                self.bpsCounter[rcIdx] = 0
                txCountStats           = []
                for combo in itertools.product(*[list(range(len(e))) for e in allShortestPaths.values()]):
                  txNodes = []
                  for pathIdx, nodeIdTx in zip(combo, allShortestPaths.keys()):
                    path     = allShortestPaths[nodeIdTx][pathIdx]
                    txNodes += path[:-1]
                  c = Counter(txNodes)
                  txCountStats.append({
                    'combo':      combo,
                    'maxCount':   np.max(list(c.values())),
                    'totalCount': np.sum(list(c.values())),
                  })

                df = pd.DataFrame(txCountStats)
                df.sort_values(by=['maxCount'], ascending=True, inplace=True)  # NOTE: totalCount is the same for all combinations as only shortest paths (not all paths) are considered
                maxCountMinValue = df.iloc[0].maxCount
                df               = df[df.maxCount == maxCountMinValue]  # only consider combinations with minimal cost
                comboSelIdx      = np.random.randint(low=0, high=len(df))
                comboSel         = df.iloc[comboSelIdx].combo
                for pathIdx, nodeIdTx in zip(comboSel, allShortestPaths.keys()):
                  self.selectedShortestPath[nodeIdTx] = allShortestPaths[nodeIdTx][pathIdx]
          else:  # useBps == False
            # random selection of one shortest path
            for nodeIdTx in allShortestPaths.keys():
              # only update path if no shortest path is selected or if selected shortest path does not exist anymore
              if (self.selectedShortestPath[nodeIdTx] is None) or (self.selectedShortestPath[nodeIdTx] not in allShortestPaths[nodeIdTx]):
                selectedPathIdx = np.random.randint(low=0, high=len(allShortestPaths[nodeIdTx]))
                self.selectedShortestPath[nodeIdTx] = allShortestPaths[nodeIdTx][selectedPathIdx]

          # update particip masks
          for nodeIdTx in allShortestPaths.keys():
            # print('shortestPath: {}: {}'.format(nodeIdTx, self.selectedShortestPath[nodeIdTx])) # DEBUG
            for nodeIdParticip in self.hopDistanceMeasurementsHost[rcIdx].keys():  # all nodes which are known to potentially collect hop distance measurements will potentially participate
              if nodeIdParticip not in self.selectedShortestPath[nodeIdTx]:
                self.participMasks[rcIdx][nodeIdTx] = self.clearBitInMask(self.participMasks[rcIdx][nodeIdTx], nodeIdParticip)
              else:  # not needed in case "random selection of one shortest path" is used
                self.participMasks[rcIdx][nodeIdTx] = self.setBitInMask(self.participMasks[rcIdx][nodeIdTx], nodeIdParticip)

      # reset hop distance measurements
      for nodeId in self.hopDistanceMeasurementsHost[rcIdx].keys():
        self.hopDistanceMeasurementsHost[rcIdx][nodeId] = []

      # determine particip updates which are then sent in the next schedule
      if self.thinningHopDistance or self.thinningConnectivityGraph:
        # TODO: once alternative particip update is implemented, use single particip update to inform new node where to particip; currently we need to send particip updates for all Tx nodes
        if self.participNewNode[rcIdx]:
          self.participUpdateQueue[rcIdx].clear()
        for nodeIdTx in range(self.minNodeId, self.maxNodeId + 1):
          # only send particip update if participMask changed (use history to detect change)
          if ( self.participMasksLast[rcIdx] is None ) or ( self.participMasks[rcIdx][nodeIdTx] != self.participMasksLast[rcIdx][nodeIdTx] ) or self.participNewNode[rcIdx]:
            self.participUpdateQueue[rcIdx].append(
              LsrScheduleParticipUpdate(msg={
                'nodeIdTx':     nodeIdTx,
                'participMask': self.participMasks[rcIdx][nodeIdTx],
              })
            )
            # # DEBUG
            # self._print('[updateThinning] changed participMask for nodeIdTx={}:'.format(nodeIdTx))
            # self._print('old mask: {}'.format(bin(self.thinningHistory[rcIdx][-1]['participMasks'][nodeId])))
            # self._print('new mask: {}'.format(bin(self.participMasks[rcIdx][nodeId])))
        self.participNewNode[rcIdx] = False

        # store latest particip masks for detection of future changes
        self.participMasksLast[rcIdx] = deepcopy(self.participMasks[rcIdx])

    # log all selected shortest paths to stats object (do it outside of for loop over rcIdx to prevent logging it multiple times)
    if (singleRcIdx is None or singleRcIdx == len(self.radioConfigs) - 1):
      if self.thinningConnectivityGraph:
        for nodeId, selectedPath in self.selectedShortestPath.items():
          rcIdxTmp = None
          for rcIdxIter, regNodes in self.registeredNodes.items():
            if nodeId in regNodes:
              rcIdxTmp = rcIdxIter
          if (self.node._sim) and (self.node._sim.stats is not None):
            self.node._sim.stats.addSelectedPath(selectedPath, nodeId, self.roundNo, rcIdxTmp)

  def updateDelayMask(self, singleRcIdx=None):
    """Update delay mask for next round.
       NOTE: Host-only method
       Args:
        singleRcIdx: if None, update registered nodes for all rcIdx; if integer: only update rcIdx of a single node
    """
    # update delay mask (prepare for next round)
    rcList = range(len(self.radioConfigs)) if (singleRcIdx is None) else [singleRcIdx]
    if not self.thinningConnectivityGraph:
      for rcIdx in rcList:
        self.delayMask[rcIdx] = 0b0
    else:  # if self.thinningConnectivityGraph==True
      # IDEA: when converged, measure delay of oldest connection in graph again (use attribute of edge=roundIdx)
      for rcIdx in rcList:
        # set default values
        delayMaskTmp                   = 0b0
        prevDelayedLayer               = self.currDelayedLayer[rcIdx]
        self.currNotDelayedNode[rcIdx] = None
        self.currDelayedLayer[rcIdx]   = None

        # obtain pre-requisites
        hopDistanceMean, hopDistanceRounded, hopDistanceMap = self.getHopDistance(rcIdx=rcIdx)
        nodesWithoutHopDistance = [nodeId for nodeId, hd in hopDistanceMean.items() if np.isnan(hd)]
        layersWithoutNodes      = [k for k, v in hopDistanceMap.items() if len(v) == 0]  # to prevent cases where hopDistanceMap[layerBelow] is empty

        self._thinningStatus[rcIdx] = None

        # determine delayMask
        if len(nodesWithoutHopDistance) or len(layersWithoutNodes):
          # if there are registered nodes without hopDistanceMean value -> measure hop distance w/o delay
          delayMaskTmp = 0b0
          self._print('[updateDelayMask] [roundNo={}] [rcIdx={}] measuring hop distance! nodesWithoutHopDistance={}, layersWithoutNodes={}'.format(self.roundNo, rcIdx, nodesWithoutHopDistance, layersWithoutNodes))
          self._thinningStatus[rcIdx] = ThinningState.CollectingHopDistance
        else:
          # try to perform measurements with delaying retransmissions
          # NOTE: at this point, we assume that hopDistanceMean does not contain any nan elements; if this is changed later, make sure to set bit in delayMask to 1 for nodes with unknown hop distance (to not influence the measurements falsify the resulting decisions)

          # notDelayedNodeQueue could contain nodes added in previous rounds, ensure that next element in queue is contained in hopDistanceMean.keys()
          while len(self.notDelayedNodeQueue[rcIdx]) and self.notDelayedNodeQueue[rcIdx][0] not in hopDistanceMean:
            print('WARNING: skipped nodeId={} in notDelayedNodeQueue with rxIdx={}'.format(self.notDelayedNodeQueue[rcIdx][0], rcIdx))
            self.notDelayedNodeQueue[rcIdx].popleft()

          # determine nodes to delay (layer below lowest unconnected node, if there is an unconnected node)
          extraHopDistanceMeasurementRound = False
          if len(self.notDelayedNodeQueue[rcIdx]) == 0:
            # get all unconnected nodes (if any)
            unconnectedNodes = {}
            for nodeId in self.cg[rcIdx].nodes():
              if not nx.has_path(self.cg[rcIdx], self.hostNodeId, nodeId):
                unconnectedNodes[nodeId] = hopDistanceRounded[nodeId]

            if len(unconnectedNodes):
              # add nodes from the layer below the unconnected node with the lowest hop distance to the notDelayedNodeQueue
              lowestUnconnectedNode = min(unconnectedNodes, key=unconnectedNodes.get)
              layerBelow            = hopDistanceRounded[lowestUnconnectedNode] - 1
              if layerBelow == 0:
                raise Exception('Trying to delay layer {}! This does not work!'.format(layerBelow))
              layerBelowNodes = hopDistanceMap[layerBelow]  # we assume that there are no nodes with hopDistance==1 and which are unconnected (with hopDistance==1 implies that they are connected to the host), they should have been added in updateThinning()
              if len(layerBelowNodes) == 0:
                raise Exception('Trying to delay layer {} with {} nodes! This does not work!'.format(layerBelow, len(layerBelowNodes)))

              if prevDelayedLayer == layerBelow:
                # prevent of measuring same layer again without updating hopDistance first -> update hopDistance first
                extraHopDistanceMeasurementRound = True
              else:
                self.notDelayedNodeQueue[rcIdx].extend(layerBelowNodes)

          if len(self.notDelayedNodeQueue[rcIdx]):
            # perform delay measurement
            self._print('[updateDelayMask] [roundNo={}] [rcIdx={}] measuring delay! notDelayedNodeQueue={}'.format(self.roundNo, rcIdx, list(self.notDelayedNodeQueue[rcIdx])))
            self._thinningStatus[rcIdx]    = ThinningState.MeasuringDelay
            self.currNotDelayedNode[rcIdx] = self.notDelayedNodeQueue[rcIdx].popleft()
            self.currDelayedLayer[rcIdx]   = hopDistanceRounded[self.currNotDelayedNode[rcIdx]]
            for nodeId in hopDistanceMap[self.currDelayedLayer[rcIdx]]:
              # skip currNotDelayedNode
              if nodeId == self.currNotDelayedNode[rcIdx]:
                continue
              # delay all other nodes from the currDelayedLayer
              delayMaskTmp = self.setBitInMask(delayMaskTmp, nodeId)
            if delayMaskTmp == 0:
              raise Exception('Performing delay measurements without delaying any node! This should not happen!')  # cases where a layer consists of a single node should be handled (updateThinning should add edges in this case)

          else:
            # no delay measurement
            delayMaskTmp = 0b0
            if extraHopDistanceMeasurementRound:
              self._print('[updateDelayMask] [roundNo={}] [rcIdx={}] measuring hop distance before trying to measure delay of same layer again! notDelayedNodeQueue={}'.format(self.roundNo, rcIdx, self.notDelayedNodeQueue[rcIdx]))
              self._thinningStatus[rcIdx] = ThinningState.MeasuringHopDistance
            else:
              self._print('[updateDelayMask] [roundNo={}] [rcIdx={}] thinning converged: hop distance and connectivity of all participating nodes ({}) is known!'.format(self.roundNo, rcIdx, len(list(self.hopDistanceStats[rcIdx].keys()))))
              self._thinningStatus[rcIdx] = ThinningState.Converged
              # IDEA: pick random node (it is unclear which node / edges requires and updated measurement most urgently)

        if (self.node._sim) and (self.node._sim.stats is not None):
          self.node._sim.stats.addThinningStatusHost(self._thinningStatus[rcIdx], self.roundNo, rcIdx)

        # DEBUG
        self._print('[updateDelayMask] [roundNo={}] [rcIdx={}] currNotDelayedNode: {}'.format(self.roundNo, rcIdx, self.currNotDelayedNode[rcIdx]))
        self._print('[updateDelayMask] [roundNo={}] [rcIdx={}] currDelayedLayer: {}'.format(self.roundNo, rcIdx, self.currDelayedLayer[rcIdx]))
        self._print('[updateDelayMask] [roundNo={}] [rcIdx={}] delayMask: {}'.format(self.roundNo, rcIdx, bin(delayMaskTmp)))

        self.delayMask[rcIdx] = delayMaskTmp

  def getHopDistance(self, rcIdx):
    hopDistanceMean    = {}
    hopDistanceRounded = {self.hostNodeId: 0}

    for nodeId in self.hopDistanceStats[rcIdx].keys():
      if len(self.hopDistanceStats[rcIdx][nodeId]) == self.hopDistanceStats[rcIdx][nodeId].maxlen:  # require the stats buffer to be filled
        hopDistanceMean[nodeId]    = np.mean(self.hopDistanceStats[rcIdx][nodeId])
        # hopDistanceRounded[nodeId] = type(self).hopDistanceRound(hopDistanceMean[nodeId])
        hopDistanceRounded[nodeId] = np.min(self.hopDistanceStats[rcIdx][nodeId])
      else:
        hopDistanceMean[nodeId] = np.nan

    # hopDistanceMap: remap dict: hop distance -> nodeId
    hopDistanceMap = OrderedDict([(0, [self.hostNodeId])])
    if np.sum(np.invert(np.isnan(list(hopDistanceMean.values())))) > 0:
      for hopDistance in range(1, np.max(list(hopDistanceRounded.values())) + 1):
        hopDistanceMap[hopDistance] = [nodeId for nodeId, hd in hopDistanceRounded.items() if hd == hopDistance]

    return hopDistanceMean, hopDistanceRounded, hopDistanceMap

  @staticmethod
  def hopDistanceRound(hopDistanceVal):
    # FIXME: remove
    # offset round: prefer rounding down since shorter hop distance is more likely the true hop distance?
    # TODO: use median instead of round?
    # return np.floor(hopDistanceVal).astype(int)  # alternative rounding
    return np.round(hopDistanceVal - 0.25).astype(int)

  def isDummyMsgRequired(self, rcIdx):
    # client node is never required to send a dummy msg
    if not self.isHost:
      return False

    # never send dummy msgs if hop distance is measured with delayed schedule
    if self.useDelayedSchedule:
      return False

    if rcIdx == 0:
      # normal app messages are always sent with modulation 0
      ret = (len(self.txQueue) == 0) and (self.delayMask[rcIdx] != 0)
    else:
      ret = (self.delayMask[rcIdx] != 0)
    return ret

  def deregistrationDetectedOnClient(self):
    # TODO: maybe combine with _resetStart
    if self.isHost:
      raise Exception('This is only meant to be called on client nodes!')
    if self.preActivatedRcIdx is None:
      # reset registeredStreamRcIdx -> this will make trigger node to send streamReq and indicate its deregistration in that streamReq
      self.registeredStreamRcIdx          = None
      # reset modulation (rcIdx) stats
      self.lastReceivedScheduleRcIdxStats = deque([], maxlen=self.statsDepth)
    # reset counter
    self.roundsSinceLastRegCounter      = None
    # reset thinning for node
    self._resetThinning()

  def extractScheduleAndSetPrimitiveDelay(self, payload):
    """Extract schedule from payload and apply delay to flooding primitive according to delayMask in schedule.
    (Useful, when schedule should be used to measure hop distance with delayed retransmission.)
    """
    lsrMsg = LsrMsg(raw=payload)
    if lsrMsg['msgType'] == int(LsrMsgType.SCHEDULE):
      schedule  = LsrSchedule(raw=lsrMsg['payload'])
      delayedTx = self.getBitFromMask(schedule['delayMask'], self.node.nodeId)
      if delayedTx:
        self.primitive.setDelayedTx()

################################################################################


class LsrNode(Node):
  def __init__( self,
                radioConfigs,
                hostNodeId,
                nodeId,
                position = (0, 0),
                nTx = 3,
                numHops = 4,
                slotLength = 1.0,
                period = 40,
                streamPeriod = 40,
                startDelay = 0,
                minNodeId = 1,
                maxNodeId = 1+MAX_NUM_NODES-1,
                thinningHopDistance = True,
                thinningConnectivityGraph = True,
                statsDepth = 3,
                selRcIdxPercentile = 40,
                deregisteringThreshold = None,
                useBps=True,
                bpsRoundThreshold = None
              ):
    super().__init__(radioConfig=None, position=position, nodeId=nodeId)

    self.lsr = Lsr(
      timerId=1,
      primitiveClass=Gloria,
      radioConfigs=deepcopy(radioConfigs),
      primitiveTimerId=0,
      period=period,
      streamPeriod=streamPeriod,
      node=self,
      nTx=nTx,
      safetyMargin=20e-3,
      numHops=numHops,
      slotLength=slotLength,
      minNodeId=minNodeId,
      maxNodeId=maxNodeId,
      thinningHopDistance=thinningHopDistance,
      thinningConnectivityGraph=thinningConnectivityGraph,
      statsDepth=statsDepth,
      selRcIdxPercentile=selRcIdxPercentile,
      deregisteringThreshold=deregisteringThreshold,
      useBackoffForBootstrap=True,
      skipBootstrapLimit=15,
      useDelayedSchedule=True,
      useBps=useBps,
      bpsRoundThreshold=bpsRoundThreshold,
    )

    self.hostNodeId = hostNodeId
    self.startDelay = startDelay
    self.timerId    = 2
    self.dataSeqNum = 0
    dummyAppMsg     = ApplicationMsg({'seqNum': 0, 'text': ''})
    self.dataSeqNumLimit = 2**8*struct.calcsize(dummyAppMsg._formats['seqNum'])  # for modulo wrapping around dataSeqNum which is added to sent data message

  def postCallback(self):
    # if self.lsr.registeredStreamRcIdx is not None or self.lsr.isHost: # generate msgs only if stream registered
    # if True: # generate app msgs no matter whether stream is registered
    if not self.lsr.isHost:  # generate app msgs always if not host
      appMsg = ApplicationMsg(msg={
        'seqNum': copy(self.dataSeqNum % self.dataSeqNumLimit),
        'text':   b'testtesttesttes',
      })
      self.lsr.sendMsg(appMsg)           # add msg to out queue
      self.dataSeqNum += 1

    self.print('rxQueuePktsWaiting: {}'.format(self.lsr.rxQueuePktsWaiting))

    while self.lsr.rxQueuePktsWaiting > 0:
      appMsg, nodeId = self.lsr.receiveMsg()
      if (self._sim) and (self._sim.stats is not None):
        self._sim.stats.addReceivedMsg(appMsg, txNodeId=nodeId, rxNodeId=self.nodeId)
      self.print('appMsg from node {}: {}'.format(nodeId, appMsg))

  def init(self):
    # delay start of lsr
    self.timerStart(timerId=self.timerId, timeout=self.startDelay)

  def timerCallback(self, timerId):
    self.lsr.timerCallback(timerId)

    if timerId == self.timerId:
      # start lsr
      self.lsr.start(
        isHost=(self.nodeId == self.hostNodeId),
        postCallback=self.postCallback,
      )

  def rxDoneCallback(self, payload, crc, rssi=None, snr=None):
    self.lsr.rxDoneCallback(payload, crc, rssi, snr)

  def txDoneCallback(self):
    self.lsr.txDoneCallback()

  def rxTimeoutCallback(self):
    self.lsr.rxTimeoutCallback()

################################################################################


if __name__ == '__main__':
  pass
