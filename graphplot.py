#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""


import os
import itertools
import numpy             as np
import matplotlib.pyplot as plt
import networkx          as nx
from collections import OrderedDict
from copy        import deepcopy


class GraphPlot(object):
  def __init__(self):
    pass

  @staticmethod
  def generatePlot(distanceMap, edgeList, filePaths=None, nodeColor='#FFC847', layerLine=True):
    """Generate plot from distance map and connectivity info
    Args:
      distanceMap:
      edgeList:
      filePaths:   list of file paths for saving output graph (reason for list: support saving graph in different formats)
      distanceMap: dict which maps distance to node IDs (assumptions: OrderedDict with increasing distance as keys; entry for distance=0 consists of list only containing the hostNodeId)
      nodeColor:
      layerLine:
    """
    if len(distanceMap[0]) != 1:
      raise Exception('distanceMap for distance=0 should only contain a single node, the host node!')
    hostNodeId = distanceMap[0][0]

    def getHopDistance(nodeId):
      for d, nodeIdList in distanceMap.items():
        if nodeId in nodeIdList:
          return d
      return None

    g = nx.Graph()
    if isinstance(edgeList, dict):
      for edge, label in edgeList.items():
        g.add_edge(edge[0], edge[1], label=label)
    else:
      g.add_edges_from(edgeList)

    # add unconnected nodes
    for d, nodeIdList in distanceMap.items():
      for nodeId in nodeIdList:
        if nodeId not in g:
          g.add_node(nodeId)

    # construct extended graph (where unconnected nodes are connected in order to place them on the correct level)
    gTmp             = deepcopy(g)
    distanceMapTmp   = deepcopy(distanceMap)
    unconnectedNodes = [nodeId for nodeId in g.nodes if not nx.has_path(g, nodeId, hostNodeId)]

    for nodeId in unconnectedNodes:
      dist = getHopDistance(nodeId)
      if dist is None:
        print('WARNING: Node {} has no distance information and is therefore omitted in graphPlot (filePaths={})!'.format(nodeId, os.path.basename(filePaths)))
      elif dist == 0:
        raise Exception('There should only be a single root node!')
      else:
        # iteratively add dummy nodes (if necessary) for layers without nodes
        distLow = dist-1
        while len(distanceMapTmp[distLow]) == 0:
          distLow -= 1
        if distLow < 0:
          raise Exception('No node found to attach node with nodeId {}'.format(nodeId))
        for i in range(distLow, dist-1):
          dummyNodeId = 'd{}'.format(i+1)
          distanceMapTmp[i+1].append(dummyNodeId)
          gTmp.add_edge(distanceMapTmp[i][-1], dummyNodeId)
        # add current node
        gTmp.add_edge(distanceMapTmp[dist-1][-1], nodeId)

    # ensure layer-wise plotting by setting ordering of nodes in graph object (not ensured by 'dot' plotting program)
    gTmpTmp        = nx.Graph()
    nodeIdsOrdered = itertools.chain.from_iterable(distanceMapTmp.values())
    gTmpTmp.add_nodes_from(nodeIdsOrdered)
    gTmpTmp.add_edges_from(gTmp.edges)

    # # DEBUG
    # print('g.nodes: {}'.format(g.nodes))
    # print('g.edges: {}'.format(g.edges))
    # print('gTmp.nodes: {}'.format(gTmp.nodes))
    # print('gTmp.edges: {}'.format(gTmp.edges))
    # print('gTmpTmp.nodes: {}'.format(gTmpTmp.nodes))
    # print('gTmpTmp.edges: {}'.format(gTmpTmp.edges))

    # use pygraphviz dot program to determine node positions
    positions = nx.drawing.nx_agraph.graphviz_layout(gTmpTmp, prog="dot")
    # invert graph
    positions = {k: (v[0], -v[1]) for k, v in positions.items()}

    # draw graph
    if (filePaths is not None) and len(filePaths):
      plt.ioff()  # turn off interactive plotting (pop-up window) since it is closed immediately anyway
    else:
      plt.close('all')  # close previous plots automatically only in when using plotting interactively (e.g. when debugging)
    fig, ax = plt.subplots(figsize=(5.25, 3))
    if isinstance(edgeList, dict):
      width = [(3 if g.edges[u, v]['label'] >= 1 else 1) for u, v in g.edges]
    else:
      width = 1
    nx.draw(
      g,
      positions,
      with_labels=True,
      node_color=nodeColor,
      node_size=400,
      linewidths=1.,  # width of line around nodes
      width=width,    # widths of edges
    )
    # if isinstance(edgeList, dict):
    #   nx.draw_networkx_edge_labels(
    #     g,
    #     positions,
    #     edge_labels={e: g.edges[e]['label'] for e in g.edges},
    #     font_color='black',
    #   )
    ax = plt.gca()  # get the current axis
    ax.set_xlim( (ax.get_xlim()[0] - 10, ax.get_xlim()[1] + 10) )  # NOTE: required border depends on figsize
    ax.set_ylim( (ax.get_ylim()[0] - 10, ax.get_ylim()[1] + 10) )
    if layerLine:
      yVals = [v[1] for k, v in positions.items()]
      yLines = np.arange(min(yVals), max(yVals)+1, 72)  # 72 is the default distance between nodes; TODO: figure out this value programmatically instead of hard-coding it
      for yLine in yLines:
        ax.axhline(y=yLine, color="grey", alpha=0.4, linestyle=":", zorder=-1)
    if len(ax.collections):
      ax.collections[0].set_edgecolor("#000000")  # ax.collections[0] is a PathCollection object governing the nodes
    if (filePaths is not None) and len(filePaths):
      for filePath in filePaths:
        plt.savefig(filePath, bbox_inches="tight")
      plt.close(fig)
      plt.ion()  # turn on interactive plotting again


################################################################################

if __name__ == '__main__':

  # # connectivity graph complete
  # distanceMap = OrderedDict([(0, [1]), (1, [9, 5, 8, 6, 3]), (2, [2, 7, 4])])
  # edges = [(1, 9), (1, 5), (1, 8), (1, 3), (1, 6), (9, 7), (5, 7), (6, 7), (2, 3), (3, 4)]

  # # connectivity graph incomplete
  # distanceMap = OrderedDict([(0, [1]), (1, [9, 5, 8, 6, 3]), (2, [2, 4])])
  # edges = [(1, 9), (1, 5), (1, 8), (1, 3)]

  # # layer without node
  # distanceMap = OrderedDict([(0, [1]), (1, []), (2, [5])])
  # edges = []

  # # debug
  # distanceMap = OrderedDict([(0, [1]), (1, [3, 4]), (2, [5, 6])])
  # edges = [(1, 3), (1, 4), (4, 5), (4, 6)]

  # debug
  distanceMap = OrderedDict([(0, [1]), (1, [3, 4]), (2, [5, 6])])
  edges = {(1, 3): 1, (1, 4): 2, (4, 5): 0, (4, 6): 1, (3, 5): 1, (3, 6): 1}

  gp = GraphPlot()
  gp.generatePlot(distanceMap, edges)
