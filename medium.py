#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""


import unittest

################################################################################


class MediumAction():
  """Represents one radio action (Rx or Tx) on the medium.
  """
  def __init__(self, isTx, actionId, overlap=None):
    self.isTx     = isTx
    self.actionId = actionId

    if overlap is None:
      self._overlap = set()
    else:
      self._overlap = overlap

  def __repr__(self):
    return 'isTx={}, actionId={}, overlap={}'.format(self.isTx, self.actionId, self.overlap)

  @property
  def overlap(self):
    return list(self._overlap)

  def extendOverlap(self, actionIdList):
    self._overlap = self._overlap.union(set(actionIdList))

################################################################################


class Medium():
  """Holds state about which RF transmissions are currently ongoing
  """
  def __init__(self):
    self._d = dict()

  def __contains__(self, key):
    return key in self._d

  def __iter__(self):
    return iter(self._d)

  def items(self):
    return self._d.items()

  def addAction(self, nodeIdSim, isTx, actionId):
    """Add a radio action (Rx or Tx) to the medium.
       NOTE: medium does not check whether actionId actually corresponds to a Rx or Tx action! This should be done before adding it.
    """
    if nodeIdSim in self._d:
      raise Exception('Node {} can only perform one Rx/Tx action at a time!'.format(nodeIdSim))
    self._d[nodeIdSim] = MediumAction(isTx, actionId)
    if isTx:
      # add actionId to all currently receiving actions
      for k in self._d.keys():
        if not self._d[k].isTx:
          self._d[k].extendOverlap([actionId])
    else:  # Rx action
      # add all nodes currently sending as overlap to receiving MediumAction obj
      self._d[nodeIdSim].extendOverlap(self.getAllTxActionIds())

  def removeAction(self, nodeIdSim):
    return self._d.pop(nodeIdSim)

  def getAllTxActionIds(self):
    return [op.actionId for nodeIdSim, op in self._d.items() if op.isTx]

################################################################################


class TestMedium(unittest.TestCase):
  def test1(self):
    m = Medium()
    m.addAction(nodeIdSim=0, isTx=True,  actionId=100)
    m.addAction(nodeIdSim=1, isTx=False, actionId=102)
    m.addAction(nodeIdSim=2, isTx=True,  actionId=105)
    m.addAction(nodeIdSim=3, isTx=True,  actionId=107)
    m.removeAction(nodeIdSim=3)
    ret = m.removeAction(nodeIdSim=1)
    self.assertEqual(sorted(ret.overlap), [100, 105, 107])

################################################################################


if __name__ == '__main__':
  unittest.main()
