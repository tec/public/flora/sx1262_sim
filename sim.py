#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""


import os
import sys
import pickle
import itertools
import networkx          as nx
import numpy             as np
import matplotlib.pyplot as plt
from collections         import OrderedDict
from copy                import copy, deepcopy
from bokeh.plotting      import figure, show, save, output_file
from bokeh.models        import ColumnDataSource, Range1d, LabelSet, Label
from bokeh.models.glyphs import ImageURL
from bokeh.io            import export_png
from bokeh.models.graphs import from_networkx

from sx1262 import LoraConfig, FskConfig
import sx1262

from event_queue        import *
from actions            import *
from medium             import Medium
from node               import Node
from plot_transmissions import TransmissionPlot
from plot_energy        import AccumulatedEnergyPlot
from plot_energy_lsr    import EnergyPerLsrRoundPlot
from sim_statistics     import Statistics

################################################################################
# SIMULATION class
################################################################################


class Sim():
  # class global constants
  MAX_FREQUENCY_DEVIATION = 1000  # Max freq deviation which still allows successful reception (in Hz)
  FLOCKLAB_FLOORMAP_PATH  = os.path.join(os.getcwd(), 'flocklab/flocklab_floormap.png')

  def __init__(self, logFileName=None, outputDir='./output', statusIncrement=60, collectStats=True):

    # Configuration
    self.propagation        = False      # simulate propagation of RF signals
    self.timeDrift          = False      # enables simulation of per node time drift (with slope and offset)
    self.probabilisticRf    = False      # enables simulation of random packet loss due to bad receive conditions
    self.flocklabBackground = False      # add flocklab floormap background to node position plot
    self.verbose            = True       # print INFO output
    self.debug              = False      # print DEBUG output
    self.antennaGain        = 0          # in dB
    self.radioConfigs       = None       # List of available radio configurations
    self._outputDir         = outputDir  # Set and prepare output directory
    os.makedirs(self._outputDir, exist_ok=True)

    # State
    self.eq                 = EventQueue(self)  # event queue
    self.at                 = dict()            # action table
    self.medium             = Medium()          # medium: holds state about which transmissions are currently ongoing
    self.logFileName        = logFileName       # Filename, std.out is used if logFile is None
    self._logFile           = None              # actual file handle (or std.out handle)
    self.logEventProcessing = False             # Log event processing info

    self.nodes              = dict()
    self.dataSinkNodeIdSim  = None
    self._pathlossMatrix    = None
    self.modulationMatrix   = None
    self.weightMatrix       = None
    self._driftDict         = dict()
    self._offsetDict        = dict()

    self.t                  = 0  # simulation time
    self.tStop              = 1000  # simulation end time
    self.statusIncrement    = statusIncrement
    self.statusNext         = self.statusIncrement

    # list of considered modulations (ordered by increasing sensitivity dBm value!)
    self.modulationList = list((
      'lora12',
      # 'lora11',
      # 'lora10',
      # 'lora9',
      # 'lora7',
      # 'lora8',
      # 'lora6',
      'lora5',
      'fsk100000',
    ))

    # open log file
    if self.logFileName is None:
      print('== Logging to stdout ==')
      self._logFile = sys.stdout
    else:
      print('== Logging to file \'{}\' =='.format(self.logFileName))
      self._logFile = open(os.path.join(self._outputDir, self.logFileName), 'w')

    # statistics
    if collectStats:
      self.stats = Statistics(sim=self)
    else:
      self.stats = None

  def setRadioConfigs(self, radioConfigs):
    self.radioConfigs = deepcopy(radioConfigs)

  @staticmethod
  def setRandSeed(randSeed):
    """Set seed for random number generator (needs to be set before calling any other method in order to show effect!)
    """
    np.random.seed(randSeed)

  def printInfo(self, text, time=False):
    print('[{:>11.6f}] {}'.format(self.t, text), file=self._logFile)
    # if time:
    #   print('[{:>10.6f}] {}'.format(self.t, text), file=self._logFile)
    # else:
    #   print('{:>12} {}'.format('[INFO]', text), file=self._logFile)

  def printDebug(self, text):
    if self.debug:
      print('{:>12} {}'.format('[DEBUG]', text), file=self._logFile)

  def setDataSink(self, nodeIdSim=None, nodeId=None):
    assert (nodeIdSim is not None) or (nodeId is not None)
    if nodeIdSim is not None:
      self.dataSinkNodeIdSim = nodeIdSim
    else:
      for node in self.nodes.values():
        if node.nodeId == nodeId:
          self.dataSinkNodeIdSim = node._nodeIdSim

  def nodeIdSim2nodeId(self, nodeIdSim):
    return self.nodes[nodeIdSim].nodeId

  def nodeId2nodeIdSim(self, nodeId):
    for node in self.nodes.values():
      if node.nodeId == nodeId:
        return node._nodeIdSim

  def getNodeIdList(self):
    return [self.nodeIdSim2nodeId(nodeIdSim) for nodeIdSim in self.nodes.keys()]

  def calcOptimalAvgPower(self, txPower, dataPayload, dataPeriod):
    """Performs all additional calculations which are necessary to determine the average power consumption in the optimal case
    Assumptions:
      * pathlossMatrix
      * modulationMatrix
    Args:
      txPower:     transmission power (constant for all nodes) [dBm]
      dataPayload: payload size [Bytes] (limited by payload size of packet)
      dataPeriod:  period of data generation [s]
    """

    # calculate everything for a single packet transmitted
    self.determineMinModulation(txPower=txPower)
    self.determineMinWeights(payloadLen=dataPayload, txPower=txPower)
    self.generateGraphFromWeights(txPower=txPower)
    self.determineShortestPathCost(txPower=txPower)
    # self.determineMinMaxCost(txPower=txPower)

    # WARNING if there are nodes which are not connected to data sink
    notConnected = [nodeIdSim for nodeIdSim, e in self.shortestPathCumulativeWeight.items() if e is None]
    if len(notConnected) > 0:
      print('WARNING: There are nodes not connected to the data sink: {}'.format(list(notConnected)))

    # ASSERT that optimal scheme is feasible (total active time of each node is below 100%)
    for nodeIdSim in range(len(self.nodes)):
      assert self.nodeActiveTime[nodeIdSim]/dataPeriod <= 1.

    # plot of power consumption of each node if every node in network sends 1 packet (shortestPath)
    x = [str(self.nodeIdSim2nodeId(e)) for e in self.nodeEnergyConsumption.keys()]  # needs to be string to let bokeh know that x is categorical
    y = np.asarray(list(self.nodeEnergyConsumption.values()))/dataPeriod
    p = figure(x_range=x, plot_height=350, plot_width=700, title="Power consumption per node",)
    p.vbar(x=x, top=y, width=0.9)
    p.xaxis.axis_label      = 'Node'
    p.yaxis.axis_label      = 'Avg Power (W)'
    p.xgrid.grid_line_color = None
    p.y_range.start         = 0
    output_file(os.path.join(self._outputDir, "power_per_node.html"))
    save(p)
    export_png(p, filename=os.path.join(self._outputDir, "power_per_node.png"))

    print('=== shortestPath (nodeIdList) ===')
    for nodeIdSim, spath in self.shortestPath.items():
      print('{}: {}'.format(self.nodeIdSim2nodeId(nodeIdSim), [self.nodeIdSim2nodeId(e) for e in spath]))

    # calculate average total power of network
    totalEnergySinglePkt = np.sum([e for e in self.shortestPathCumulativeWeight.values() if e is not None])
    avgPower = totalEnergySinglePkt / dataPeriod
    return avgPower

  def addNode(self, node):
    """
    NOTE: node.nodeId is user defined for nice representation. Internally nodes get a sequential node ID assigned.
    """
    # add ref to sim (for debug printing)
    node.setSim(self)

    # assign internal ID and add new node to list
    nodeIdSim = len(self.nodes)
    node.setNodeIdSim(nodeIdSim)
    self.nodes[nodeIdSim] = node

    # add time correction values
    if self.timeDrift:
      # FIXME
      maxPpm    = 40     # in ppm
      maxOffset = 100.0  # in s
      self._driftDict[nodeIdSim]  = np.random.uniform(-maxPpm / 1e6, maxPpm / 1e6)
      self._offsetDict[nodeIdSim] = np.random.uniform(0, maxOffset)

  def calcPathlossmatrixFromPositions(self):
    """Calculate the path loss matrix (path loss between any two nodes)
    """
    numNodes        = len(self.nodes)
    nodeIdList      = [n.nodeId for n in self.nodes.values()]
    pathlossMatrix  = np.ones((numNodes, numNodes)) * 1e3  # initialize with very bad link

    # iterate through all possible combinations of 2 nodes
    for txNodeIdSim in range(numNodes):
      for rxNodeIdSim in range(numNodes):
        if txNodeIdSim == rxNodeIdSim:
          pathlossMatrix[txNodeIdSim, rxNodeIdSim] = 0.
        else:
          distance = np.sqrt( (self.nodes[txNodeIdSim].x - self.nodes[rxNodeIdSim].x)**2 + (self.nodes[txNodeIdSim].y - self.nodes[rxNodeIdSim].y)**2 )
          if distance > 0.:
            f    = 868e6
            lambdaSymb = 3e8 / f
            h_B  = 18                                                                                                              # Height of base station antenna. Unit: meter (m)
            h_M  = 18                                                                                                              # Height of mobile station antenna. Unit: meter (m)
            C_h  = 0.8 + (1.1 * np.log10(f) - 0.7) * h_M - 1.56 * np.log10(f)                                                      # Antenna height correction factor for small/medium city
            L_U  = 69.55 + 26.16 * np.log10(f) - 13.82 * np.log10(h_B) - C_h + (44.9 - 6.55 * np.log10(h_B)) * np.log10(distance)  # path loss urban
            L_SU = L_U - 2 * (np.log10(f / 28))**2 - 5.4                                                                           # path loss suburban
            # # path loss based on Hata model (urban)
            # pathlossMatrix[txNodeIdSim, rxNodeIdSim] = L_U
            #
            # path loss based on Hata model (suburban)
            pathlossMatrix[txNodeIdSim, rxNodeIdSim] = L_SU

            # # path loss in dB based on Friis Transmission Formula
            # pathlossMatrix[txNodeIdSim, rxNodeIdSim] = 20*np.log10( (4*np.pi*distance)/lambdaSymb ) # path loss in dB based on Friis Transmission Formula
          else:
            pathlossMatrix[txNodeIdSim, rxNodeIdSim] = 0

    print('=== pathlossMatrix ===')
    print(pathlossMatrix)

    self.schedulePathlossMatrix(
      pathlossMatrix = pathlossMatrix,
      nodeIdList     = nodeIdList,
    )

  def addNodesFromLinktest(self, linktestFile, scaling=1.0):
    """Add nodes and path loss matrix from info obtained from a linktest
    Args:
      linktestFile: path to the linktest pickle file which contains nodeList and path loss matrix
      scaling:
    """
    import pickle

    with open(linktestFile, 'rb' ) as f:
        d = pickle.load(f)

    for nodeId in d['nodeList']:
      n = Node(nodeId=nodeId, radioConfig=None)
      self.addNode(n)
    self._pathlossMatrix = d['pathlossMatrix'] * scaling

  def determineMinModulation(self, txPower=14, antennaGain=0):
    """For every link, determine the modulation with shortest on-air-time which still reliably works
    Assumption:
      * path loss matrix is available
    Args:
      txPower:     transmit power [dBm]
      antennaGain: antenna gain (at each side tx/rx) [dBm]
    """
    numNodes              = len(self.nodes)
    self.modulationMatrix = np.empty((numNodes, numNodes), dtype=object)
    # iterate through all link combinations
    for txNodeId in range(numNodes):
      for rxNodeId in range(numNodes):
        if txNodeId == rxNodeId:
          self.modulationMatrix[(txNodeId, rxNodeId)] = None
        else:
          # determine min modulation
          rxPower         = txPower + 2*antennaGain - self._pathlossMatrix[(txNodeId, rxNodeId)]
          sensitivityList = [sx1262.getSensitivity('lora' if 'lora' in m else 'fsk', int(m.replace('lora', '').replace('fsk', '')), srcDatasheet=False) for m in self.modulationList]
          offset          = 1  # offset to tune reliability
          modIdx          = np.searchsorted(sensitivityList, rxPower - offset) - 1

          if modIdx < 0:
            self.modulationMatrix[(txNodeId, rxNodeId)] = None
          else:
            self.modulationMatrix[(txNodeId, rxNodeId)] = self.modulationList[modIdx]
    print('=== modulationMatrix ===')
    print(self.modulationMatrix)

  def determineMinWeights(self, payloadLen, txPower=14):
    """Calculates the weights (energy and time-on-air) for every link between 2 nodes.
    Assumption:
      * modulationMatrix is available
    Args:
      payloadLen: length of the payload to be sent over the link (in bytes). Must be smaller than max payload length.
      txPower:
    """
    # initialize weightMatrix
    numNodes = len(self.nodes)
    self.weightMatrix = np.ones((numNodes, numNodes)) * np.nan  # cost of a single transmission with the given payload in energy consumed
    self.toaMatrix    = np.ones((numNodes, numNodes)) * np.nan  # time-on-air values for each (directed) edge

    # initialize modulation config objects
    fskconfig = FskConfig()
    fskconfig.bitrate        = 50000
    fskconfig.nPreambleBits  = 16
    fskconfig.nSyncwordBytes = 2
    fskconfig.nLengthBytes   = 1
    fskconfig.nAddressBytes  = 1
    fskconfig.phyPl          = 0
    fskconfig.nCrcBytes      = 1

    loraconfig = LoraConfig()
    loraconfig.bw            = 125000
    loraconfig.sf            = 7
    loraconfig.phyPl         = 0
    loraconfig.cr            = 1
    loraconfig.ih            = False
    loraconfig.lowDataRate   = False
    loraconfig.crc           = True
    loraconfig.nPreambleSyms = 8

    # iterate through all link combinations
    for txNodeId in range(numNodes):
      for rxNodeId in range(numNodes):
        weight = None
        toa    = None
        if txNodeId == rxNodeId:
          weight = np.nan
          toa    = np.nan
        else:
          mod = self.modulationMatrix[(txNodeId, rxNodeId)]
          if mod is None:
              weight = np.nan
              toa    = np.nan
          else:
            if 'fsk' in mod:
              fskconfig.phyPl   = payloadLen
              fskconfig.bitrate = int(mod.replace('fsk', ''))
              toa               = fskconfig.timeOnAir
              weight            = toa * (sx1262.getRxPower() + sx1262.getTxPower(txPower))
            elif 'lora' in mod:
              loraconfig.phyPl = payloadLen
              loraconfig.sf    = int(mod.replace('lora', ''))
              toa              = loraconfig.timeOnAir
              weight           = toa * (sx1262.getRxPower() + sx1262.getTxPower(txPower))
            else:
              raise Exception('Unknown modulation!')
        self.weightMatrix[(txNodeId, rxNodeId)] = weight
        self.toaMatrix[(txNodeId, rxNodeId)] = toa
    print('=== weightMatrix ===')
    print(self.weightMatrix)

  def generateGraphFromWeights(self, txPower):
    """Generate networkx graph from weightMatrix
    Assumptions:
     * weightMatrix is available
    """
    # construct bi-directional graph with cost of single transmission as edge weights
    self.g   = nx.DiGraph()
    numNodes = len(self.nodes)
    for txNodeId in range(numNodes):
      for rxNodeId in range(numNodes):
        if txNodeId == rxNodeId:
          continue
        if np.isnan(self.weightMatrix[(txNodeId, rxNodeId)]):
          continue
        self.g.add_edge(txNodeId, rxNodeId, weight=self.weightMatrix[(txNodeId, rxNodeId)])

    # plot the obtained graph
    plot  = figure(title="Networkx Integration Demonstration", x_range=(-1.1, 1.1), y_range=(-1.1, 1.1))
    graph = from_networkx(self.g, nx.fruchterman_reingold_layout(self.g), scale=2, center=(0, 0))
    plot.renderers.append(graph)
    output_file(os.path.join(self._outputDir, "optimal_weight_graph.html"))
    save(plot)
    # show(plot)

  def determineShortestPathCost(self, txPower):
    """Determine shortest path and corresponding cost (corresponding cumulative edge weight). In addition, calculate cost of each node.
    Assumptions:
     * network graph derived from weightMatrix
    """

    # initialization
    self.shortestPath                 = OrderedDict()
    self.shortestPathCumulativeWeight = OrderedDict()
    self.nodeActiveTime               = OrderedDict()
    self.nodeRxTime                   = OrderedDict()
    self.nodeTxTime                   = OrderedDict()
    self.nodeEnergyConsumption        = OrderedDict()
    for nodeIdSim in range(len(self.nodes)):
        self.nodeActiveTime[nodeIdSim]        = 0
        self.nodeRxTime[nodeIdSim]            = 0
        self.nodeTxTime[nodeIdSim]            = 0
        self.nodeEnergyConsumption[nodeIdSim] = 0

    # calculate
    for nodeIdSim in range(len(self.nodes)):
      if nodeIdSim == self.dataSinkNodeIdSim:
        continue
      if nodeIdSim not in list(self.g.nodes):
        # there is no multi-hop connection from requested node to the data sink
        self.shortestPath[nodeIdSim]                 = None
        self.shortestPathCumulativeWeight[nodeIdSim] = None
        continue

      # determine shortest path
      spath = nx.shortest_path(self.g, source=nodeIdSim, target=self.dataSinkNodeIdSim, weight='weight')
      assert len(spath) >= 2
      # determine cumulative weight of shortest path
      cumWeight = 0
      for i in range(len(spath)-1):
        cumWeight += self.weightMatrix[(spath[i], spath[i+1])]
      self.shortestPath[nodeIdSim]                 = spath
      self.shortestPathCumulativeWeight[nodeIdSim] = cumWeight

      # determine active time for all nodes if all the nodes send exactly 1 message (both end-points need to be active for every edge of the shortest path)
      for i in range(len(spath)-1):
        txNodeId = spath[i]
        rxNodeId = spath[i+1]
        txTime   = self.toaMatrix[(txNodeId, rxNodeId)]
        rxTime   = self.toaMatrix[(txNodeId, rxNodeId)]
        self.nodeActiveTime[txNodeId] += txTime
        self.nodeActiveTime[rxNodeId] += rxTime
        self.nodeTxTime[txNodeId]     += txTime
        self.nodeRxTime[rxNodeId]     += rxTime
        self.nodeEnergyConsumption[txNodeId] += txTime * sx1262.getTxPower(txPower)
        self.nodeEnergyConsumption[rxNodeId] += rxTime * sx1262.getRxPower()

    print('=== Shortest Paths ===')
    print(self.shortestPath)
    print('=== Cumulative Weights of Shortest Paths ===')
    print(self.shortestPathCumulativeWeight)
    print('=== nodeActiveTime ===')
    print(self.nodeActiveTime)
    print('=== nodeTxTime ===')
    print(self.nodeTxTime)
    print('=== nodeRxTime ===')
    print(self.nodeRxTime)

    # # matplotlib variant
    # pos = nx.fruchterman_reingold_layout(g)
    # nx.draw_networkx_labels(g, pos)
    # nx.draw_networkx_nodes(g, pos, node_size=500)
    # nx.draw_networkx_edges(g,pos)
    # nx.draw(g)

  def determineMinMaxCost(self, txPower):
    """Exhaustive search over all combinations of all paths from all nodes to find the combination with min power consumption of the node with max power consumption.
    Assumptions:
     * network graph derived from weightMatrix
    """

    # initialization
    self.pathList              = OrderedDict()
    self.nodeActiveTime        = OrderedDict()
    self.nodeRxTime            = OrderedDict()
    self.nodeTxTime            = OrderedDict()
    self.nodeEnergyConsumption = OrderedDict()
    for nodeIdSim in range(len(self.nodes)):
        self.nodeActiveTime[nodeIdSim]        = 0
        self.nodeRxTime[nodeIdSim]            = 0
        self.nodeTxTime[nodeIdSim]            = 0
        self.nodeEnergyConsumption[nodeIdSim] = 0

    # determine all possible paths for all nodes
    for nodeIdSim in range(len(self.nodes)):
      if nodeIdSim == self.dataSinkNodeIdSim:
        continue
      if nodeIdSim not in list(self.g.nodes):
        # there is no multi-hop connection from requested node to the data sink
        self.pathList[nodeIdSim] = None
        continue
      # determine list of paths
      self.pathList[nodeIdSim] = list(nx.all_simple_paths(self.g, source=nodeIdSim, target=self.dataSinkNodeIdSim, cutoff=5))

    # iterate over all combinations of paths of all nodes.
    for pathCombinationList in itertools.product(self.pathList.values()):
      print(pathCombinationList)
      # determine power energy per node
      # determine active time for all nodes if all the nodes send exactly 1 message (both end-points need to be active for every edge of the shortest path)
      for i, path in enumerate(pathCombinationList):
        # FIXME: ignore destination node
        txNodeId = path[i]
        rxNodeId = path[i+1]
        txTime   = self.toaMatrix[(txNodeId, rxNodeId)]
        rxTime   = self.toaMatrix[(txNodeId, rxNodeId)]
        self.nodeActiveTime[txNodeId] += txTime
        self.nodeActiveTime[rxNodeId] += rxTime
        self.nodeTxTime[txNodeId]     += txTime
        self.nodeRxTime[rxNodeId]     += rxTime
        self.nodeEnergyConsumption[txNodeId] += txTime * sx1262.getTxPower(txPower)
        self.nodeEnergyConsumption[rxNodeId] += rxTime * sx1262.getRxPower()

    print('=== Shortest Paths ===')
    print(self.shortestPath)
    print('=== Cumulative Weights of Shortest Paths ===')
    print(self.shortestPathCumulativeWeight)
    print('=== nodeActiveTime ===')
    print(self.nodeActiveTime)
    print('=== nodeTxTime ===')
    print(self.nodeTxTime)
    print('=== nodeRxTime ===')
    print(self.nodeRxTime)

    # # matplotlib variant
    # pos = nx.fruchterman_reingold_layout(g)
    # nx.draw_networkx_labels(g, pos)
    # nx.draw_networkx_nodes(g, pos, node_size=500)
    # nx.draw_networkx_edges(g,pos)
    # nx.draw(g)

  def plotNodePositions(self):
    """Plots the geometric positions of the wireless nodes.
    """
    output_file(os.path.join(self._outputDir, "node_positions.html"), title="Node Positions")

    xCoords = [node.x for nodeIdSim, node in self.nodes.items()]
    yCoords = [node.y for nodeIdSim, node in self.nodes.items()]
    nodeIdSimList = [nodeIdSim   for nodeIdSim, node in self.nodes.items()]
    nodeIdList    = [node.nodeId for nodeIdSim, node in self.nodes.items()]

    source = ColumnDataSource(data=dict(xCoords=xCoords,
                                        yCoords=yCoords,
                                        nodeIdList=nodeIdList
                                        ))

    plot = figure(
      title='',
      match_aspect=True,
      sizing_mode='stretch_both',
    )

    # add node locations
    plot.scatter(
      x='xCoords',
      y='yCoords',
      size=15,  # for label "aside"
      # size=25, # for label "on circle"
      source=source,
      level='overlay'
    )
    plot.xaxis[0].axis_label = 'x-coordinate [m]'
    plot.yaxis[0].axis_label = 'y-coordinate [m]'

    # add labels
    labels = LabelSet(
      x='xCoords',
      y='yCoords',
      text='nodeIdList',
      source=source,
      render_mode='canvas',
      level='overlay',
      # ## on circle
      # x_offset=0,
      # y_offset=-5,
      # text_font_size='14pt',
      # text_font_style='bold',
      # text_color='white',
      # text_baseline='center',
      # text_align='center',
      # aside
      x_offset=10,
      y_offset=-25,
      background_fill_color='orange',
      border_line_color='orange',
      # background_fill_color='#ffe270',
      # border_line_color='#ffe270',
      border_line_width=5,
      text_baseline='bottom',  # top works, but border at the bottom is too large
      text_align='left',
      text_font_size='14pt',
      text_font_style='bold',
    )
    plot.add_layout(labels)

    # add flocklab background
    if self.flocklabBackground:
      imageSource = ColumnDataSource(dict(
        imagePath=[type(self).FLOCKLAB_FLOORMAP_PATH],
        imageX=[0],
        imageY=[0],
        imageW=[776],
        imageH=[800],
      ))
      imageGlyph = ImageURL(
        url="imagePath",
        x="imageX",
        y="imageY",
        w="imageW",
        h="imageH",
        anchor="top_left",
      )
      plot.add_glyph(imageSource, imageGlyph)

    save(plot)
    # show(plot)

  def processNextEvent(self):
    """Process next event in event queue
    Returns:
      True if simulation time exceeded the stop time of the simulation, False otherwise.
    """
    # get next event from the event queue & get corresponding action and nodeIdSim
    event = self.eq.getNextEvent()
    if event.actionId is not None:
      action    = self.at[event.actionId]
      nodeIdSim = action.nodeIdSim
    else:
      action    = None
      nodeIdSim = None

    # update simTime
    self.t = event.ts

    # print status
    if self.t >= self.statusNext:
      print('t={}s'.format(self.statusNext))
      self.statusNext += self.statusIncrement

    if self.t > self.tStop:
      # simulation time exceed
      return True

    if self.logEventProcessing:
      self.printInfo('Processing {} for N{}'.format(type(event).__name__, nodeIdSim), time=True)

    # handle the events
    if isinstance(event, TimerTimeout):
      self.nodes[nodeIdSim].timerCallback(action.timerId)
    elif isinstance(event, RxStart):
      self.medium.addAction(nodeIdSim=nodeIdSim, isTx=False, actionId=event.actionId)
    elif isinstance(event, RxDone):
      # call RxDone callback function
      txAction = self.at[event.txActionId]
      self.nodes[nodeIdSim].rxDoneCallback(
        payload=txAction.payload,
        crc=True,
        rssi=None,
        snr=None
      )
    elif isinstance(event, RxTimeout):
      if nodeIdSim in self.medium:
        self.medium.removeAction(nodeIdSim)
        self.nodes[nodeIdSim].rxTimeoutCallback()
      else:
        raise Exception('ERROR: This condition should never be reached since RxTimeout event should have been deleted during processing of Tx/RxDone event!')
    elif isinstance(event, TxStart):
      self.medium.addAction(nodeIdSim=nodeIdSim, isTx=True, actionId=event.actionId)
    elif isinstance(event, TxDone):
      # sending node
      # remove medium state
      self.medium.removeAction(nodeIdSim)

      # Call callback function
      self.nodes[nodeIdSim].txDoneCallback()

      # receiving node
      # go through all currently ongoing Rx actions and handle successful receptions caused by the current TxDone event
      # generates & adds newActions internally
      self.potentiallyFinishRadioRxActions(event)
    elif isinstance(event, SetPathlossMatrix):
      self._setPathlossMatrix(event.pathlossMatrix, event.nodeIdList)

    # simulation time not exceed
    return False

  def potentiallyFinishRadioRxActions(self, txDoneEvent):
    """Go through all currently ongoing Rx actions and handle successful receptions caused by the provided TxDone event
    """
    currRadioTxActionId   = txDoneEvent.actionId
    currRadioTxAction     = self.at[currRadioTxActionId]
    mediumEntriesToRemove = []  # entries cannot be deleted inside loop since this would change dict size while iterating (not allowed in python)

    # iterate through nodes currently receiving (rxNodes)
    for nId, rxMa in [(nId, ma) for (nId, ma) in self.medium.items() if not ma.isTx]:
      overlapList = deepcopy(rxMa.overlap)
      # self.printDebug('Node {} overlapList: {}'.format(nId, overlapList))
      assert currRadioTxAction.nodeIdSim in [self.at[aId].nodeIdSim for aId in overlapList]
      rxAction  = self.at[rxMa.actionId]
      rxSuccess = True

      # skip reception if there is no finite path loss
      if np.isnan(self._pathlossMatrix[currRadioTxAction.nodeIdSim, rxAction.nodeIdSim]):
        continue  # no reception

      # calc received power level of currRadioTxAction at rxAction location (used for propagation overlap checks)
      currRadioTxActionRxPowerLevel = currRadioTxAction.radioConfig.txPower + self.antennaGain - self._pathlossMatrix[currRadioTxAction.nodeIdSim, rxAction.nodeIdSim] + self.antennaGain

      # check whether the rxNode had NO chance to receive the full transmission of the currently tx-ing node (i.e. no overlap in time)
      # check timing matches
      if (currRadioTxAction.startTime < rxAction.startTime) or \
         (currRadioTxAction.endTime   > rxAction.endTime):
        self.printDebug('timing does not match!')
        continue  # no reception

      # check if frequency matches
      if np.abs(currRadioTxAction.radioConfig.frequency - rxAction.radioConfig.frequency) > type(self).MAX_FREQUENCY_DEVIATION:
        self.printDebug('frequency does not match!')
        continue  # no reception

      # check if bandwidth matches
      if currRadioTxAction.radioConfig.bw != rxAction.radioConfig.bw:
        self.printDebug('bandwidth does not match!')
        continue  # no reception

      # check if propagation is good enough (rxPower above sensitivity)
      slack = currRadioTxActionRxPowerLevel - rxAction.radioConfig.sensitivity
      if self.propagation:
        if slack < 0:
          self.printDebug('received signal power is below sensitivity!')
          continue  # no reception
        if self.probabilisticRf:
          # let the rx fail with a probability based on the rxPowerLevel slack
          # print('==!== slack: {}'.format(slack))
          # rand = np.random.uniform(-10, 10)
          rand = np.random.normal(loc=0.0, scale=4.0)
          if slack < rand:
            continue  # no reception

      # check if there were other RadioTxActions colliding with the currRadioTxAction (using the overlapList)
      # remove current Tx action from overlapList
      while currRadioTxActionId in overlapList:
        overlapList.remove(currRadioTxActionId)

      # iterate through all remaining overlapping txActions to check for collision
      for overlapActionId in overlapList:
        currTxRc = currRadioTxAction.radioConfig
        txAction = self.at[overlapActionId]
        txRc     = txAction.radioConfig

        # check overlap in time
        currTxTl    = currRadioTxAction.startTime
        currTxTh    = currRadioTxAction.endTime
        txTl        = txAction.startTime
        txTh        = txAction.endTime
        timeOverlap = type(self).doRangesOverlap((currTxTl, currTxTh), (txTl, txTh))

        # check overlap in frequency
        currTxFl = currTxRc.frequency - currTxRc.bw/2
        currTxFh = currTxRc.frequency + currTxRc.bw/2
        txFl     = txRc.frequency     - txRc.bw/2
        txFh     = txRc.frequency     + txRc.bw/2
        freqOverlap = type(self).doRangesOverlap((currTxFl, currTxFh), (txFl, txFh))

        # check propagation overlap
        propagationOverlap = None
        if self.propagation:
          # consider capture effect
          txActionRxPowerPowerLevel = txAction.radioConfig.txPower + self.antennaGain - self._pathlossMatrix[txAction.nodeIdSim, rxAction.nodeIdSim] + self.antennaGain
          if self.probabilisticRf:
            # we assume power capture can still happen
            slack = currRadioTxActionRxPowerLevel - txActionRxPowerPowerLevel
            # print('===== slack: {}'.format(slack))
            # rand = np.random.uniform(-4, 4) # this leads to probability of successful Rx of 50% if both rx signals have the same level -> this is most likely not realistic, if both have same level there are more corrupted packets, maybe use exponential decrease for the CDF
            # rand = np.random.normal(loc=3, scale=3) # too pessimistic?
            rand = np.random.normal(loc=0.0, scale=3.0)
            if slack < rand:
              propagationOverlap = True  # i.e. Rx fails due to overlapping Tx
            else:
              propagationOverlap = False  # i.e. Rx does not fail due to overlapping Tx

          else:  # not self.probabilisticRf
            if txActionRxPowerPowerLevel < currRadioTxActionRxPowerLevel - 3:
              # power Capture happens (currRadioTxAction signal is 3 dB or stronger than signal from txAction)
              propagationOverlap = False  # i.e. Rx does not fail due to overlapping Tx
            else:
              propagationOverlap = True  # i.e. Rx fails due to overlapping Tx

        self.printDebug('timeOverlap={}, freqOverlap={}, propagationOverlap={}'.format(timeOverlap, freqOverlap, propagationOverlap))

        if timeOverlap and freqOverlap and propagationOverlap:
          rxSuccess = False

      # rxNode successfully received the message from the node currently finishing its transmission (TxDone)
      if rxSuccess:
        # prepare removal of medium state
        mediumEntriesToRemove.append(rxAction.nodeIdSim)

        # set actual end time of rxAction
        rxAction.endTime = self.t

        # remove corresponding RxTimeout event from event queue
        self.eq.removeEvents(RxTimeout, rxMa.actionId)

        # Add RxDone event to event queue
        self.eq.addEvent(
          RxDone( actionId=rxMa.actionId,
                  txActionId=txDoneEvent.actionId,
                  ts=txDoneEvent.ts)
        )
      else:
        # if there was no successful reception leave everything as is -> maybe another TxDone will be successful or eventually rxTimeout will happen
        pass

    # delete self.medium entries
    for i in mediumEntriesToRemove:
      self.medium.removeAction(i)

  @staticmethod
  def doRangesOverlap(r1, r2):
    assert len(r1) == 2
    assert len(r2) == 2
    l1, h1 = r1
    l2, h2 = r2
    assert l1 <= h1
    assert l2 <= h2

    # return (((l1>=l2 and l1<=h2) or (h1>=l2 and h1<=h2)) or \
    #         ((l2>=l1 and l2<=h1) or (h2>=l1 and h2<=h1)))
    return (h1 >= l2 and h2 >= l1)

  def simTime2nodeTime(self, nodeIdSim, ts=None):
    if ts is None:
      ts = self.t
    if self.timeDrift:
      return (1.0 + self._driftDict[nodeIdSim]) * ts + self._offsetDict[nodeIdSim]
    else:
      return ts

  def applyTimeDriftToTimeDiff(self, timeDiff, nodeIdSim):
    if self.timeDrift:
      return (1.0 + self._driftDict[nodeIdSim]) * timeDiff
    else:
      return timeDiff

  def addActions(self, actionList, nodeIdSim):
    """Adds actions to the action table and generates and adds corresponding events to the event queue
    """
    # don't execute if there is no actionList
    if type(actionList) != list:
      return

    # don't execute if actionList is empty
    if len(actionList) == 0:
      return

    # sanity checks
    assert nodeIdSim in self.nodes.keys()

    # process all actions
    for action in actionList:
      currAction           = deepcopy(action)
      currAction.nodeIdSim = nodeIdSim
      eventList            = []

      # determine next actionId (used later to add action to action table)
      actionId = len(self.at)

      # generate events
      if isinstance(currAction, TimerStartAction):
        if self.eq.isTimerActive(currAction.nodeIdSim, currAction.timerId):
          raise Exception('Tried to start timer (nodeIdSim={}, timerId={}) again. Each timer can be started only once (before it expires or is stopped)!'.format(currAction.nodeIdSim, currAction.timerId))
        eventList += [
          TimerTimeout(
            actionId=actionId,
            ts=self.t + self.applyTimeDriftToTimeDiff(currAction.timeout, nodeIdSim)
          )
        ]

        # FIXME: Remove debug code
        # self.printInfo('===== adding TimerTimeout (timerId={timerId}) event at LT={LT}, ST={ST}, timeout_orig={timeout_orig}, timeout_conv={timeout_conv}, ST_sched={ST_sched} for node {node}'.format(
        #   timerId=currAction.timerId,
        #   LT=self.simTime2nodeTime(nodeIdSim),
        #   ST=self.t,
        #   ST_sched=self.t + self.applyTimeDriftToTimeDiff(currAction.timeout, nodeIdSim ),
        #   timeout_orig=currAction.timeout,
        #   timeout_conv=self.applyTimeDriftToTimeDiff(currAction.timeout, nodeIdSim ),
        #   node=self.nodeIdSim2nodeId(nodeIdSim),
        # ))
      elif isinstance(currAction, TimerStopAction):
        self.eq.removeTimeoutEvents(nodeIdSim=currAction.nodeIdSim, timerId=currAction.timerId)
      elif isinstance(currAction, RadioRxAction):
        if not type(self).isRadioConfigOk(self.nodes[currAction.nodeIdSim].radioConfig):
          raise Exception('Error: Each node needs to have a valid radioConfig when triggering a RadioRxAction!')

        currAction.radioConfig = deepcopy(self.nodes[currAction.nodeIdSim].radioConfig)
        startTime              = self.t
        endTime                = self.t + currAction.timeout
        currAction.startTime   = startTime
        currAction.endTime     = endTime

        eventList += [
          RxStart(  actionId=actionId, ts=startTime),
          RxTimeout(actionId=actionId, ts=endTime),
        ]
      elif isinstance(currAction, RadioTxAction):
        if not type(self).isRadioConfigOk(self.nodes[currAction.nodeIdSim].radioConfig):
          raise Exception('Error: Each node needs to have a valid radioConfig when triggering a RadioTxAction!')
        if type(currAction.payload) is not bytes:
          raise Exception('ERROR: Payload needs to be of type "bytes"!')

        currAction.radioConfig       = deepcopy(self.nodes[currAction.nodeIdSim].radioConfig)
        currAction.radioConfig.phyPl = len(currAction.payload)
        toa                  = currAction.radioConfig.timeOnAir
        startTime            = self.t
        endTime              = self.t + toa
        currAction.startTime = startTime
        currAction.endTime   = endTime

        eventList += [
          TxStart(actionId=actionId, ts=startTime),
          TxDone( actionId=actionId, ts=endTime)
        ]
      elif isinstance(currAction, RadioResetAction):
        # add end time to ongoing transmissions/receptions and remove them from medium
        # FIXME: check if this is sufficient to finish ongoing Rx/Tx
        if currAction.nodeIdSim in self.medium:
          ma = self.medium.removeAction(currAction.nodeIdSim)
          self.at[ma.actionId].endTime = self.t

        # remove all future radio related events of this node
        self.eq.removeRadioEvents(nodeIdSim=currAction.nodeIdSim)

      # add action to action table
      self.at[actionId] = currAction

      # add events to the event queue
      for event in eventList:
        self.eq.addEvent(event)

  @staticmethod
  def isRadioConfigOk(rc):
    """Check if radio config contains all necessary infos for sim and does not contain unwanted info.
    Returns:
      True if radioConfig is ok.
    """
    if rc is None:
      return False

    if not hasattr(rc, 'frequency'):
      return False
    else:
      if rc.frequency is None:
        return False

    if not hasattr(rc, 'txPower'):
      return False
    else:
      if rc.txPower is None:
        return False
      else:
        if rc.txPower not in sx1262.getConfigTxPowerLevels():
          return False

    if rc.phyPl is not None:
      return False

    return True

  def printEventQueue(self):
    print('EventQueue:')
    print('{:>10} {:>4} {:>16}'.format('Time', 'Node', 'Event'))
    for event in self.eq.q:
      print('{:>10.6f} {:>4} {:>16}'.format(event.ts, '{}'.format(self.nodeIdSim2nodeId(self.at[event.actionId].nodeIdSim)), type(event).__name__), file=self._logFile)

  def runSimulation(self, until=None):
    if self.stats is not None:
      self.stats.setNodeIdList(sorted([n.nodeId for n in self.nodes.values()]))

    if until is not None:
      self.tStop = until
    print('=== SIMULATION STARTED ===')
    self.printInfo('=== SIMULATION STARTED ===', time=True)

    # initialize all nodes
    for nodeIdSim, node in self.nodes.items():
      self.printInfo('Initializing nodeIdSim={} (nodeId={})'.format(nodeIdSim, node.nodeId))
      node.init()

    # sanity check for path loss matrix (path loss matrix should not yet have been set & first event in event queue should set the path loss matrix)
    if (self._pathlossMatrix is not None) or (type(self.eq.q[0]) != SetPathlossMatrix):
      raise Exception('ERROR: path loss matrix must be initialized at the beginning of the simulation (use schedulePathlossMatrix(..., ts=0))!')

    # process event queue
    while len(self.eq):
      stop = self.processNextEvent()
      if stop:
        break

    print('=== SIMULATION ENDED ===')
    self.printInfo('=== SIMULATION ENDED ===', time=True)

    # close log file
    if self.logFileName is not None:
      self._logFile.close()

  def plotTransmissions(self, outFile):
    """Plots all RadioTxAction and RadioRxAction elements for all nodes
    """
    tp = TransmissionPlot(self)
    tp.generatePlot(os.path.join(self._outputDir, outFile))

  def plotEnergy(self, outFile):
    """Plots accumulated energy consumption over time for each node
    """
    ep = AccumulatedEnergyPlot()
    ep.loadFromSim(self)
    ep.generatePlot(os.path.join(self._outputDir, outFile))

  def _setPathlossMatrix(self, pathlossMatrix, nodeIdList):
    """Set the currently used path loss matrix. Also useful to initialize the path loss matrix for the simulation.
       NOTE: The provided path loss matrix needs to cover all nodes in the simulation but may also contain additional (unused) nodes.
    """
    simNodes = set([n.nodeId for n in self.nodes.values()])
    plmNodes = set(nodeIdList)
    if len(simNodes.difference(plmNodes)):
      raise Exception('Path loss matrix must cover all nodes of the simulation! ({} != {})'.format(sorted([n.nodeId for n in self.nodes.values()]), sorted(nodeIdList)))
    self._pathlossMatrix = np.full(shape=(len(nodeIdList), len(nodeIdList)), fill_value=np.nan)
    nodesIterList        = sorted(simNodes.intersection(plmNodes))
    for nodeIdTx in nodesIterList:
      for nodeIdRx in nodesIterList:
        simTxIdx = self.nodeId2nodeIdSim(nodeIdTx)
        simRxIdx = self.nodeId2nodeIdSim(nodeIdRx)
        plmTxIdx = nodeIdList.index(nodeIdTx)
        plmRxIdx = nodeIdList.index(nodeIdRx)
        self._pathlossMatrix[simTxIdx][simRxIdx] = pathlossMatrix[plmTxIdx][plmRxIdx]

  def schedulePathlossMatrix(self, pathlossMatrix, nodeIdList, ts=0):
    """Schedule updating the path loss matrix at a specific point in the simulation time.
    Args:
      ts:             timestamp in simulation time when the path loss matrix shall become active
      pathlossMatrix: path loss matrix which shall become active
      nodeIdList:     list of node IDs which corresponds to the rows/cols of the path loss matrix
    """
    self.eq.addEvent(
      SetPathlossMatrix(
        ts=ts,
        pathlossMatrix=deepcopy(pathlossMatrix),
        nodeIdList=deepcopy(nodeIdList),
      )
    )

################################################################################


if __name__ == '__main__':
  s = Sim()
