#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""


import struct
import unittest
from collections import OrderedDict
from copy        import copy

################################################################################
# MESSAGE class
################################################################################


class Message(object):
  def __init__(self, msg=None, raw=None):
    self.endianChar = '<' # select endianness ('<': little-endian, '>' bit-endian)

    # Fields which need to be defined in child classes:
    # self._hasVariableField = None
    # self._formats          = None

    # Parent method which needs to be called in child class
    # super().__init__(msg, raw)

    # variable-sized field is stored as a list of elements with the format specified for the corresponding field, it can be set by providing a bytes obj

    # check if _formats is OrderedDict
    if not type(self._formats) == OrderedDict:
      raise Exception('_formats field of Message must be OrderedDict!')

    # check if _hasVariableField is boolean
    if not isinstance(self._hasVariableField, (int, float)):
      raise Exception('_hasVariableField field of Message must be boolean!')

    # initialize _values dict
    self._values = copy(self._formats)
    for k, v in self._values.items():
      self._values[k] = None

    # set initial data from constructor
    if msg is not None and raw is not None:
      raise Exception('You MUST NOT set both, \'msg\' and \'raw\' at the same time!')
    if msg is not None:
      if type(msg) == dict or type(msg) == OrderedDict:
        if sorted(msg.keys()) != sorted(self._values.keys()):
          raise Exception('msg initialization must contain all keys exactly once!')
        for k, v in msg.items():
          self[k] = v  # uses __setitem__ function!
      elif type(msg) == list:
        if len(list) != len(self._values):
          raise Exception('Provided list does not contain expected number of elements!')
        else:
          for i, k in enumerate(list(self._values.keys())):
            self[k] = msg[i]  # uses __setitem__ function!
    elif raw is not None:
      self.raw = raw  # uses setter function!
    else:
      raise Exception('You MUST set exactly one of \'msg\' and \'raw\'!')

  def __getitem__(self, key):
    return self._values[key]

  def __setitem__(self, key, val):
    lastKey = next(reversed(self._values))
    if self._hasVariableField and key == lastKey:
      if type(val) == list:
        self._values[key] = val
      elif type(val) == bytes:
        self._values[key] = self._rawToList(val)
      else:
        raise Exception('Unknown value type ({}) for last element ({})!'.format(type(val), lastKey))
    else:
      self._values[key] = val

  def __repr__(self):
    return repr(self._values)

  @property
  def raw(self):
    if self._hasVariableField:
      formatFixed = self.endianChar + ''.join(list(self._formats.values())[:-1])  # explicitly define endianness to disable auto-padding
      lastElem = self._values[next(reversed(self._values))]
      raw = struct.pack(formatFixed, *(list(self._values.values())[:-1])) + self._listToRaw(lastElem)
    else:
      raw = struct.pack(self.endianChar + ''.join(self._formats.values()), *list(self._values.values()))  # explicitly define endianness to disable auto-padding
    return raw

  @raw.setter
  def raw(self, raw):
    if len(raw) == 0 and not(self._hasVariableField and len(self._formats) == 1):  # raw can only be empty if msg only has single field with variable size
      raise Exception('raw must NOT be empty if message has fixed-size fields!')
    if not( type(raw) == bytes or (type(raw) == list and ( (len(raw) > 0 and type(raw[0]) == bytes) or (len(raw) == 0) )) ):
      raise Exception('raw must be of type \'bytes\' or a list of \'bytes\'!')
    if type(raw) == list:
      raw = b''.join(raw)
    if self._hasVariableField:
      formatFixed = self.endianChar + ''.join(list(self._formats.values())[:-1])  # explicitly define endianness to disable auto-padding
      sizeFixed   = struct.calcsize(formatFixed)
      l           = list(struct.unpack(formatFixed, raw[:sizeFixed]))
      l.append(self._rawToList(raw[sizeFixed:]))
    else:
      l = struct.unpack(self.endianChar + ''.join(self._formats.values()), raw)  # explicitly define endianness to disable auto-padding
    for i, k in enumerate(self._values.keys()):
      self._values[k] = l[i]

  @property
  def size(self):
    if self._hasVariableField:
      if self._values is None:
        raise Exception('ERROR: Cannot determine size of message since content has not been set (yet)!')
      lastKey     = next(reversed(self._formats))
      formatFixed = self.endianChar + ''.join(list(self._formats.values())[:-1])  # explicitly define endianness to disable auto-padding
      return struct.calcsize(formatFixed) + struct.calcsize(self._formats[lastKey])*len(self._values[lastKey])
    else:
      return struct.calcsize(self.endianChar + ''.join(self._formats.values()))  # explicitly define endianness to disable auto-padding

  def _listToRaw(self, dataList):
    """Function to convert variable field list to raw
    """
    lastKey    = next(reversed(self._values))
    elemFormat = self.endianChar + self._formats[lastKey]  # explicitly define endianness to disable auto-padding
    return b''.join([struct.pack(elemFormat, elem) for elem in dataList])

  def _rawToList(self, raw):
    """Function to convert variable field raw to list
    """
    lastKey    = next(reversed(self._values))
    elemFormat = self.endianChar + self._formats[lastKey]  # explicitly define endianness to disable auto-padding
    elemSize   = struct.calcsize(elemFormat)
    numElems   = int(len(raw)/elemSize)
    return [struct.unpack_from(elemFormat, raw, i*elemSize)[0] for i in range(numElems)]

################################################################################


class TestMessage(unittest.TestCase):
  def test1(self):
    class TestMsg(Message):
      def __init__(self, msg=None, raw=None):
        self._hasVariableField = True
        self._formats          = OrderedDict([('slot', 'B'), ('rand', 'B'), ('payload', 'c')])
        super().__init__(msg, raw)

    tm1 = TestMsg(msg={
      'slot':    8,
      'rand':    2,
      'payload': b'test',
    })
    tm1['rand'] = 5

    self.assertEqual(tm1['slot'], 8)
    self.assertEqual(tm1['rand'], 5)
    self.assertEqual(b''.join(tm1['payload']), b'test')
    self.assertEqual(tm1.size, 6)
    self.assertEqual(tm1.raw, b'\x08\x05test')

    print(tm1)
    print(tm1.raw)

  def test2(self):
    class TestMsg(Message):
      def __init__(self, msg=None, raw=None):
        self._hasVariableField = False
        self._formats          = OrderedDict([('slot', 'B'), ('rand', 'B')])
        super().__init__(msg, raw)

    tm2 = TestMsg(raw=b'\x08\x05')
    self.assertEqual(tm2.raw, b'\x08\x05')

    print(tm2)
    print(tm2.raw)

  def test3(self):
    class TestMsg(Message):
      def __init__(self, msg=None, raw=None):
        self._hasVariableField = True
        self._formats          = OrderedDict([('slot', 'B'), ('rand', 'B'), ('payload', 'c')])
        super().__init__(msg, raw)

    tm3 = TestMsg(raw=b'\x08\x05test')
    self.assertEqual(tm3.raw, b'\x08\x05test')

    print(tm3)
    print(tm3.raw)

  def test4(self):
    class TestMsg(Message):
      def __init__(self, msg=None, raw=None):
        self._hasVariableField = True
        self._formats          = OrderedDict([('slot', 'B'), ('rand', 'B'), ('payload', 'c')])
        super().__init__(msg, raw)

    tm4 = TestMsg(msg={
      'slot':    8,
      'rand':    2,
      'payload': [b'test'],  # element of list is not a single-byte char -> should not work
    })

    def dummy():  # necessary since assertRaises requires function handle not function call (with braces), potential args (not needed here) could be passed as separate args to assertRaises
      return tm4.raw

    self.assertRaises(struct.error, dummy)

  def test5(self):
    class TestMsg(Message):
      def __init__(self, msg=None, raw=None):
        self._hasVariableField = False
        self._formats          = OrderedDict([('slot', 'H'), ('rand', 'I')])
        super().__init__(msg, raw)

    tm5 = TestMsg(msg={
      'slot': 0,
      'rand': 0,
    })

    self.assertEqual(tm5.size, 6)

  def test6(self):
    """Msg with fixed-sized field(s) should raise error if raw is set to []
    """
    class TestMsg(Message):
      def __init__(self, msg=None, raw=None):
        self._hasVariableField = True
        self._formats          = OrderedDict([('slot', 'H'), ('rand', 'I')])
        super().__init__(msg, raw)

    def dummy():  # necessary since assertRaises requires function handle not function call (with braces), potential args (not needed here) could be passed as separate args to assertRaises
      TestMsg(raw=[])

    self.assertRaises(Exception, dummy)

  def test7(self):
    """Msg with only variable sized fields should not raise error if raw is set to []
    """
    class TestMsg(Message):
      def __init__(self, msg=None, raw=None):
        self._hasVariableField = True
        self._formats          = OrderedDict([('rand', 'I')])
        super().__init__(msg, raw)

    try:
      TestMsg(raw=[])
    except:
      self.fail('exception unexpectedly raised!')

################################################################################


if __name__ == '__main__':
  unittest.main()

  # class TestMsg(Message):
  #   def __init__(self, msg=None, raw=None):
  #     self._hasVariableField = True
  #     self._formats = OrderedDict([('rand', 'I')])
  #     super().__init__(msg, raw)
  #
  # TestMsg(raw=[])
