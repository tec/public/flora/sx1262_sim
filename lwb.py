#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""


import enum
import struct
import numpy as np
from collections import OrderedDict
from munch       import Munch
from transitions import Machine
from copy        import copy, deepcopy

import sx1262

from message import Message
from node    import Node
from actions import *
from glossy  import Glossy
from gloria  import Gloria


################################################################################
# Message Definitions
################################################################################

class LwbMsgType(enum.IntEnum):
    SCHEDULE   = 0
    STREAM_REQ = 1
    DATA       = 2

class LwbMsg(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = True
    self._formats          = OrderedDict([('msgType', 'B'), ('src', 'H'), ('dst', 'H'), ('payload', 'c')])
    super().__init__(msg, raw)

class LwbSchedule(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = True
    self._formats          = OrderedDict([('hostNodeId', 'H'), ('time', 'L'), ('period', 'H'), ('nContSlots', 'B'), ('nDataSlots', 'B'), ('slots', 'H')])
    super().__init__(msg, raw)
    # TODO: add S-ACK to message
    # TODO: add D-ACK to message

class LwbStreamReq(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = False
    self._formats          = OrderedDict([('nodeId', 'H'), ('streamId', 'B'), ('msgSize', 'B'), ('nMsgs', 'B'), ('period', 'H')])
    super().__init__(msg, raw)

# class LwbStreamAck(Message):
#   def __init__(self, msg=None, raw=None):
#     self._hasVariableField = False
#     self._formats = ('nodeId', 'H'), ('streamId', 'B')
#     super().__init__(msg, raw)

class ApplicationMsg(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = False
    self._formats          = OrderedDict([('seqNum', 'B'), ('text', '12s')])
    super().__init__(msg, raw)

################################################################################
# LWB State Machine
################################################################################
lwbStates = [
  'Init',
  'HostSchedule',
  'HostContention',
  'ClientBootstrap',
  'ClientSchedule',
  'ClientContention',
  'Data',
  'Post',
]
lwbTransitions = [
    # host
    { 'trigger': 'hostStart',              'source': 'Init',             'dest': 'HostSchedule',     'after': ['printState'] },
    { 'trigger': 'hostScheduleSent',       'source': 'HostSchedule',     'dest': 'HostContention',   'after': ['printState'] },
    { 'trigger': 'hostContentionDone',     'source': 'HostContention',   'dest': 'Data',             'after': ['printState'] },
    { 'trigger': 'hostPostDone',           'source': 'Post',             'dest': 'HostSchedule',     'after': ['printState'] },
    { 'trigger': 'hostStop',               'source': 'HostSchedule',     'dest': 'Init',             'after': ['printState'] },
    { 'trigger': 'hostStop',               'source': 'HostContention',   'dest': 'Init',             'after': ['printState'] },
    { 'trigger': 'hostStop',               'source': 'HostData',         'dest': 'Init',             'after': ['printState'] },
    # client
    { 'trigger': 'clientStart',            'source': 'Init',             'dest': 'ClientBootstrap',  'after': ['printState'] },
    { 'trigger': 'clientScheduleReceived', 'source': 'ClientBootstrap',  'dest': 'ClientSchedule',   'after': ['printState'] },
    { 'trigger': 'clientScheduleReceived', 'source': 'ClientSchedule',   'dest': 'ClientContention', 'after': ['printState'] },
    { 'trigger': 'clientContentionDone',   'source': 'ClientContention', 'dest': 'Data',             'after': ['printState'] },
    { 'trigger': 'clientPostDone',         'source': 'Post',             'dest': 'ClientSchedule',   'after': ['printState'] },
    { 'trigger': 'clientStop',             'source': 'ClientSchedule',   'dest': 'Init',             'after': ['printState'] },
    { 'trigger': 'clientStop',             'source': 'ClientContention', 'dest': 'Init',             'after': ['printState'] },
    { 'trigger': 'clientStop',             'source': 'ClientData',       'dest': 'Init',             'after': ['printState'] },
    { 'trigger': 'clientBootstrap',        'source': 'ClientSchedule',   'dest': 'ClientBootstrap',  'after': ['printState'] },
    { 'trigger': 'clientBootstrap',        'source': 'ClientContention', 'dest': 'ClientBootstrap',  'after': ['printState'] },
    { 'trigger': 'clientBootstrap',        'source': 'ClientData',       'dest': 'ClientBootstrap',  'after': ['printState'] },
    # host & client
    { 'trigger': 'dataDone',               'source': 'Data',             'dest': 'Post',             'after': ['printState'] },
]

################################################################################
# LWB
################################################################################


class Lwb():
  def __init__(self, timerId, primitiveClass, primitiveTimerId, node, nTx=3, floodLength=0.8, slotLength=1.0, safetyMargin=0.1, period=10, queueSize=10):
    self.primitiveConfig = Munch()

    # initialize
    self.timerId                        = timerId
    self.node                           = node
    self.primitiveConfig.primitiveClass = primitiveClass
    self.primitiveConfig.timerId        = primitiveTimerId
    self.primitiveConfig.nTx            = nTx              # number of retransmissions by each node
    self.floodLength                    = floodLength      # seconds
    self.slotLength                     = slotLength       # seconds
    self.safetyMargin                   = safetyMargin     # seconds
    self.period                         = period           # seconds
    self.queueSize                      = queueSize        # size of the Rx and Tx queue
    self.registeredNodes                = []

    # initialize flooding primitive
    self.primitive = self.primitiveConfig.primitiveClass(timerId=self.primitiveConfig.timerId, node=self.node)

    # internal state
    self.sm = Machine(model=self, states=lwbStates, transitions=lwbTransitions, initial='Init')


  def _resetStart(self):
    self.tsGlobal         = None
    self.tsLocal          = None
    self.rxQueue          = []
    self.txQueue          = []
    self.isHost           = None
    self.hostNode         = None
    self.postCallback     = None
    self.to_Init()
    self.lastSyncPointLt  = None
    self.lastSyncPointNt  = None
    self.currDataSlotIdx  = 0
    self.currHostNodeId   = None
    self.streamRegistered = False  # state for client node to indicate whether a stream has successfully been registered
    # host node vars
    self.nextStreamId     = 0  # next ID for requesting ID (ensures that stream ID is unique per node)
    # self.registeredNodes = [] # do not reset to allow for pre-registering nodes!
    # TODO: implement support for multiple streams, currently only a single stream per node is supported
    # self.activeStreams   = [] # list of stream IDs of active and acked streams

  def _print(self, text):
    self.node.print('[LWB] {}'.format(text))

  ##############################################################################
  # Interface
  ##############################################################################

  def start(self, isHost, postCallback=None):
    self._resetStart()
    self.isHost = isHost
    if self.isHost:
      self.hostNodeId     = self.node.nodeId
      self.currHostNodeId = int(self.node.nodeId)
    self._postCallback = postCallback

    # use timer action to trigger callback which includes timestamp
    if self.isHost:
      self.hostStart()
      self.node.timerStart(timerId=self.timerId, timeout=0)
    else:
      self.clientStart()
      self.node.timerStart(timerId=self.timerId, timeout=0)

  def stop(self):
    if self.primitive.running:
      self.primitive.stop()

    # Stop ongoing radio and timer actions
    self.node.timerStop(timerId=self.timerId)
    self.node.radioReset()

  def sendMsg(self, msg):
    """Puts a message into the outgoing queue
    Args:
      msg: LwbMsg object to be sent
    Returns:
      True if successful, False otherwise
    """
    self.node.print('sendMsg')
    if len(self.txQueue) < self.queueSize:
      self.txQueue.append(msg)
      return True
    else:
      self._print('WARNING: txQueue is full!')
      return False

  @property
  def rxQueuePktsWaiting(self):
    return len(self.rxQueue)

  def receiveMsg(self):
    """Returns the oldest received message in the input queue
    Returns:
      LwbMsg object which has been received & nodeId of sending node
    """
    if len(self.rxQueue) == 0:
      return None, None
    return self.rxQueue.pop(0)

  @property
  def txQueuePktsWaiting(self):
    return len(self.txQueue)

  def getTime(self, localTime=None):
    if self.tsGlobal is None:
      return None
    if localTime is None:
      localTime = self.node.getTs()
    return self.tsGlobal + (localTime - self.tsLocal)

  ##############################################################################
  # Callbacks
  ##############################################################################

  def rxDoneCallback(self, payload, crc, rssi=None, snr=None):
    self.primitive.rxDoneCallback(payload, crc, rssi, snr)

  def txDoneCallback(self):
    self.primitive.txDoneCallback()

  def rxTimeoutCallback(self):
    self.primitive.rxTimeoutCallback()

  def timerCallback(self, timerId):
    if timerId == self.primitive.timerId:
      self.primitive.timerCallback(timerId=timerId)
    elif timerId == self.timerId:
      self._updateFsm()

  ##############################################################################
  # Helper Functions
  ##############################################################################

  def _updateFsm(self):
    self._print('timerCallback: {}: primitive.running={}'.format(self.state, self.primitive.running))
    # HOST: State Machine ###################################################
    if self.state == 'HostSchedule':
      if self.primitive.running:
        self.primitive.stop()
        # move to the next state
        self.hostScheduleSent()
        waitTime = max(0, self.slotLength - (self.node.getTs() - self.lastSyncPointLt) % self.slotLength - self.safetyMargin)
        self.node.timerStart(timerId=self.timerId, timeout=waitTime)
      else:  # primitive not running
        self.lastRoundStart = self.node.getTs()
        self.sendScheduleFlood()

    elif self.state == 'HostContention':
      if self.primitive.running:
        self.primitive.stop()
        self.hostContentionDone()
        self.processContentionFlood()
        timeout = max(self.node.getTs(), self.lastSyncPointLt + 2*self.slotLength - self.safetyMargin)
        self.node.timerStart(timerId=self.timerId, timeout=timeout, isRelative=False)
      else:  # primitive not running
        self.startListeningFlood(nTx=0)

    # CLIENT: State Machine #################################################
    elif self.state == 'ClientBootstrap':
      if self.primitive.running:
        self.primitive.stop()
        scheduleReceived = self.processScheduleFlood()
        if scheduleReceived:
          self.clientScheduleReceived()
          waitTime = max(0, self.period - (self.node.getTs() - self.lastSyncPointLt) - self.safetyMargin)
          self.node.timerStart(timerId=self.timerId, timeout=waitTime)
        else:
          self.node.timerStart(timerId=self.timerId, timeout=0)
      else:  # primitive not running
        self.streamRegistered = False  # reset stream state
        # try (again) to receive schedule
        # random delay, otherwise we could systematically miss schedule flood since we are listening for more than one LWB slot duration)
        self.startListeningFlood(syncFlood=True, duration=np.random.uniform(self.period*0.8, self.period*1.2))

    elif self.state == 'ClientSchedule':
      # Listening for a schedule flood
      if self.primitive.running:
        self.primitive.stop()
        scheduleReceived = self.processScheduleFlood()
        if scheduleReceived:
          self.clientScheduleReceived()
          waitTime = self.slotLength - (self.node.getTs() - self.lastSyncPointLt)
          if not self.dataSlotRequired():
            waitTime -= self.safetyMargin  # start listening earlier
          if waitTime > 0:
            self.node.timerStart(timerId=self.timerId, timeout=waitTime)
          else:  # unexpected behavior -> bootstrap
            self._print('Negative wait time! -> bootstrapping')
            self.bootstrap()
        else:  # no schedule received
          # TODO: first try to receive next schedule, only bootstrap after N misses
          self._print('No schedule received! -> bootstrapping')
          self.bootstrap()
      else:  # primitive not running
        # try to receive schedule
        self.startListeningFlood(syncFlood=True, duration=self.floodLength+self.safetyMargin)

    elif self.state == 'ClientContention':
      if self.primitive.running:
        self.primitive.stop()
        self.clientContentionDone()
        slots = self.lastSchedule['slots']
        timeout = self.lastSyncPointLt + (self.currDataSlotIdx + 2)*self.slotLength
        if slots and slots[self.currDataSlotIdx] != int(self.node.nodeId):
          timeout -= self.safetyMargin  # start earlier if listening
        timeout = max(self.node.getTs(), timeout)
        self.node.timerStart(timerId=self.timerId, timeout=timeout, isRelative=False)
      else:  # primitive not running
        if self.dataSlotRequired():
          self.sendContentionFlood()
        else:
          self.startListeningFlood()
    # HOST & CLIENT: State Machine ##########################################
    elif self.state == 'Data':
      slots = self.lastSchedule['slots']
      if self.primitive.running:
        self.primitive.stop()
        self.processDataFlood(slots[self.currDataSlotIdx])
        self.finishDataSlot()
      else:  # primitive not running
        if len(slots) == 0:
          # skip data slots (since schedule does not contain any data slots)
          self.finishDataSlot()
        # NOTE: Comment the following condition if all nodes should always participate in data slots
        elif (not self.isHost) and (not self.streamRegistered):
          # do not participate in data slots if no stream has been registered yet (without stream, host does not know that node exists and host therefore cannot optimize node)
          self.finishDataSlot()
        else:
          # Participate in data floods
          if slots[self.currDataSlotIdx] == int(self.node.nodeId):
            self.sendData()
          else:
            self.startListeningFlood()

    elif self.state == 'Post':
      # execute application POST method
      self._postCallback()
      # wait until start of next round
      timeout = self.lastSyncPointLt + self.period
      # advance state
      if self.isHost:
        self.hostPostDone()
      else:
        self.clientPostDone()
        timeout -= self.safetyMargin
      self.node.timerStart(timerId=self.timerId, timeout=max(self.node.getTs(), timeout), isRelative=False)

  def sendScheduleFlood(self):
    # store current time
    ts = self.node.getTs()
    self.lastSyncPointLt = ts
    self.lastSyncPointNt = ts  # same since we are the host
    self._print('sendScheduleFlood: lastSyncPointLt: {}'.format(self.lastSyncPointLt))
    self._print('registeredNodes: {}'.format(self.registeredNodes))

    slots = copy(self.registeredNodes)
    if len(self.txQueue):
      slots.insert(0, self.node.nodeId)

    # prepare msg
    schedule = LwbSchedule(msg={
      'hostNodeId': self.node.nodeId,
      'time':       type(self).ts2us(ts),
      'period':     self.period,
      'nContSlots': 1,
      'nDataSlots': len(slots),
      'slots':      slots,
    })
    self.lastSchedule = schedule

    scheduleMsg = LwbMsg(msg={
      'msgType': int(LwbMsgType.SCHEDULE),
      'src':     self.node.nodeId,
      'dst':     0,
      'payload': schedule.raw,
    })

    # Send schedule
    self.primitive.start(
      initiator = True,
      payload   = scheduleMsg.raw,
      nTx       = self.primitiveConfig.nTx,
      syncFlood = True,
    )

    # Setup timer for stopping primitive flood
    self.node.timerStart(timerId=self.timerId, timeout=self.floodLength)

  def sendContentionFlood(self):
    self._print('sendContentionFlood')

    # prepare msg
    contention = LwbStreamReq(msg=OrderedDict([
      ('nodeId',   self.node.nodeId),
      ('streamId', copy(self.nextStreamId)),
      ('msgSize',  30),
      ('nMsgs',    1),
      ('period',   30),
    ]))
    self.nextStreamId += 1
    contMsg = LwbMsg(msg=OrderedDict([
      ('msgType', int(LwbMsgType.STREAM_REQ)),
      ('src',     self.node.nodeId),
      ('dst',     self.currHostNodeId),
      ('payload', contention.raw),
    ]))

    # Send schedule
    self.primitive.start(
      initiator = True,
      payload   = contMsg.raw,
      nTx       = self.primitiveConfig.nTx,
      syncFlood = False,
    )

    # Setup timer for stopping primitive flood
    self.node.timerStart(timerId=self.timerId, timeout=self.floodLength)

  def startListeningFlood(self, nTx=None, duration=None, syncFlood=False):
    self._print('startListeningFlood')
    duration = self.floodLength if duration is None else duration
    ret = self.primitive.start(
      initiator = False,
      payload   = None,
      nTx       = self.primitiveConfig.nTx if nTx is None else nTx,
      syncFlood = syncFlood,
    )
    self.node.timerStart(timerId=self.timerId, timeout=duration)
    return ret

  def sendData(self):
    self._print('sendData')

    if len(self.txQueue):
      self._print('Data in txQueue available -> sending 1 element')
      appMsg = self.txQueue.pop(0)
      if not isinstance(appMsg, ApplicationMsg):
        raise Exception('Element in txQueue is not of type ApplicationMsg!')

      lwbMsg = LwbMsg(msg=OrderedDict([
        ('msgType', int(LwbMsgType.DATA)),
        ('src',     self.node.nodeId),
        ('dst',     self.currHostNodeId),
        ('payload', appMsg.raw),
      ]))

      self.primitive.start(
        initiator = True,
        payload   = lwbMsg.raw,
        nTx       = self.primitiveConfig.nTx,
        syncFlood = False,
      )

      # Setup timer for stopping primitive flood
      self.node.timerStart(timerId=self.timerId, timeout=self.floodLength)
    else:  # no msg in txQueue
      self._print('No data in txQueue available -> skipping data slot')
      self.finishDataSlot()

  def processScheduleFlood(self):
    if self.primitive.getRxCnt():
      # self._print('getRxIndex: {}'.format(self.primitive.getRxIndex()))
      lwbMsg = LwbMsg(raw=self.primitive.getPayload())
      # self._print('lwbMsg: {}'.format(lwbMsg))

      if lwbMsg['msgType'] == int(LwbMsgType.SCHEDULE):
        self._print('Schedule received!')
        lwbSchedule     = LwbSchedule(raw=lwbMsg['payload'])
        self.hostNodeId = lwbSchedule['hostNodeId']
        self._print('lwbSchedule: {}'.format(lwbSchedule))
        if self.primitive.isTRefUpdated():
          self.lastSyncPointLt = self.primitive.getTRef()
          self._print('lastSyncPointLt: {}'.format(self.lastSyncPointLt))
          self._print('current Ts: {}'.format(self.node.getTs()))
          self.lastSyncPointNt = type(self).us2ts(lwbSchedule['time'])
        self.lastSchedule = lwbSchedule
        self.currHostNodeId = lwbMsg['src']
        if self.node.nodeId in lwbSchedule['slots']:
          self._print('Stream registered!')
          self.streamRegistered = True

        return True
    return False

  def processContentionFlood(self):
    if self.primitive.getRxCnt():
      # self._print('getRxIndex: {}'.format(self.primitive.getRxIndex()))
      lwbMsg = LwbMsg(raw=self.primitive.getPayload())
      # self._print('lwbMsg: {}'.format(lwbMsg))
      if lwbMsg['msgType'] == int(LwbMsgType.STREAM_REQ):
        self._print('Stream Request received!')
        lwbStreamreq    = LwbStreamReq(raw=lwbMsg['payload'])
        lwbStreamreqMsg = lwbStreamreq  # FIXME: this is a property and is therefore not directly subscriptable
        self._print('lwbStreamreq: {}'.format(lwbStreamreqMsg))

        # check if node is not already registered
        if lwbStreamreqMsg['nodeId'] in self.registeredNodes:
          self._print('Node {} NOT registered! Node is already registered!'.format(lwbStreamreqMsg['nodeId']))
          return False

        # check if there are still data slots available
        if self.period / self.slotLength - 3 - len(self.registeredNodes) >= 1:  # -3: 1 slot for schedule, 1 slot for contention, 1 slot reserved for potential tx from host
          self._print('Node {} registered!'.format(lwbStreamreqMsg['nodeId']))
          self.registeredNodes = sorted(list(set(self.registeredNodes).union(set([lwbStreamreqMsg['nodeId']]))))
        else:
          self._print('Node {} NOT registered! No more LWB data slots available!'.format(lwbStreamreqMsg['nodeId']))
        return True
    return False

  def processDataFlood(self, nodeId):
    if self.primitive.getRxCnt():
      # self._print('getRxIndex: {}'.format(self.primitive.getRxIndex()))
      lwbMsg = LwbMsg(raw=self.primitive.getPayload())
      # self._print('lwbMsg: {}'.format(lwbMsg))

      if lwbMsg['msgType'] == int(LwbMsgType.DATA):
        self._print('Data from node {} received!'.format(lwbMsg['src']))
        appMsg = ApplicationMsg(raw=lwbMsg['payload'])
        if len(self.rxQueue) < self.queueSize:
          self.rxQueue.append((appMsg, nodeId))
          return True
        else:
          self._print('WARNING: rxQueue is full! Received msg discarded!')
          return False
    return False

  def finishDataSlot(self):
    slots = self.lastSchedule['slots']
    if self.currDataSlotIdx < len(slots) - 1:
      self.currDataSlotIdx += 1
      timeout = self.lastSyncPointLt + (self.currDataSlotIdx + 2)*self.slotLength
      if slots and slots[self.currDataSlotIdx] != self.node.nodeId:
        timeout -= self.safetyMargin  # start earlier if listening
      self.node.timerStart(timerId=self.timerId, timeout=max(self.node.getTs(), timeout), isRelative=False)
    else:
      self.dataDone()
      self.currDataSlotIdx = 0
      self.node.timerStart(timerId=self.timerId, timeout=0)

  def bootstrap(self):
    self.clientBootstrap()
    self.node.timerStart(timerId=self.timerId, timeout=0)

  def dataSlotRequired(self):
    # FIXME: reduce probability of collision at startup by exponential backoff and initial backoff counter value
    return (not self.streamRegistered)

  @staticmethod
  def ts2us(ts):
    return int(ts * 1e6)

  @staticmethod
  def us2ts(us):
    return float(us) / 1e6

  def printState(self):
    self._print('new state: {}'.format(self.state))

################################################################################


class LwbNode(Node):
  def __init__(self, radioConfig, hostNodeId, nodeId, position=(0, 0), nTx=3, startDelay=0):
    super().__init__(radioConfig=radioConfig, position=position, nodeId=nodeId)

    self.lwb = Lwb(timerId=1, primitiveClass=Gloria, primitiveTimerId=0, node=self, nTx=nTx)

    self.hostNodeId = hostNodeId
    self.startDelay = startDelay
    self.timerId    = 2
    self.dataSeqNum = 0

  def postCallback(self):
    self.print('postCallback')
    # if self.lwb.streamRegistered or self.lwb.isHost: # generate msgs only if stream registered
    if True:  # generate app msgs no matter whether stream is registered
      appMsg = ApplicationMsg(msg={
        'seqNum': copy(self.dataSeqNum),
        'text':   b'testtesttest',
      })
      self.lwb.sendMsg(appMsg)  # add msg to out queue
      self.dataSeqNum += 1

    self.print('rxQueuePktsWaiting: {}'.format(self.lwb.rxQueuePktsWaiting))
    while self.lwb.rxQueuePktsWaiting > 0:
      appMsg, nodeId = self.lwb.receiveMsg()
      self.print('appMsg from node {}: {}'.format(nodeId, appMsg))

  def init(self):
    # delay start of lwb
    self.timerStart(timerId=self.timerId, timeout=self.startDelay)

  def timerCallback(self, timerId):
    self.lwb.timerCallback(timerId)

    if timerId == self.timerId:
      # start lwb
      self.lwb.start(
        isHost       =(self.nodeId == self.hostNodeId),
        postCallback = self.postCallback,
      )

  def rxDoneCallback(self, payload, crc, rssi=None, snr=None):
    self.lwb.rxDoneCallback(payload, crc, rssi, snr)

  def txDoneCallback(self):
    self.lwb.txDoneCallback()

  def rxTimeoutCallback(self):
    self.lwb.rxTimeoutCallback()

################################################################################


if __name__ == '__main__':
  pass
