#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""

import pickle
import numpy   as np
import pandas  as pd
import seaborn as sns
import matplotlib.pyplot as plt
from copy        import copy, deepcopy
from datetime    import datetime, timezone
from collections import Counter, OrderedDict
from pprint      import pprint

import sx1262
import scenarios
import scenario_flocklab

from sim       import Sim
from node      import *
from glossy    import *
from lsr       import *
from scenarios import scenarioData
from results_output import ResultsOutput

################################################################################
# General Config
################################################################################
OUTPUT_DIR      = './output/output_lsr'
PICKLE_FILENAME = 'app_lsr.pkl'
CONFIG_FILENAME = 'config.txt'

usePickle = False

################################################################################
# Sim Configs
################################################################################

configs = OrderedDict()

# Config of simulation runs
configs['debug'] = OrderedDict([
  # ('scenarioList', ['sr', 'ls', 'lm', 'cr', 'rc', 'li', 'fo']),
  # ('scenarioList', ['sr', 'ls', 'lm']),
  # ('scenarioList', ['sr', 'ls']),
  # ('scenarioList', ['ls', 'lm']),  # long-range
  # ('scenarioList', ['sr']),  # short range
  # ('scenarioList', ['ls']),  # long-range single-hop
  # ('scenarioList', ['lm']),  # long-range multi-hop
  ('scenarioList', ['cr']),  # cluster with remote nodes
  # ('scenarioList', ['rc']),  # remote cluster
  # ('scenarioList', ['li']),  # line
  # ('scenarioList', ['fo']),  # fanout
  # ('scenarioList', ['sr', 'ls', 'lm', 'cr', 'rc', 'fl', 'li', 'fo']),
  # ('scenarioList', ['db']),  # debug
  # ('scenarioList', ['fl2all']),  # FL2 all nodes
  # ('scenarioList', ['fl2s12']),  # FL2 nodes 1 - 12
  # ('scenarioList', ['fl2s1']),   # FL2 scenario for paper 1

  # ('protocolList', ['lf', 'smw', 'smf']),
  # ('protocolList', ['lh', 'lf', 'smh', 'smf']),
  # ('protocolList', ['lw', 'lh', 'lf', 'sh', 'smw', 'smh', 'smf']),
  # ('protocolList', ['lh', 'lf', 'sh', 'smh', 'smf']),
  # ('protocolList', ['smw', 'smh', 'smf']),
  # ('protocolList', ['lw', 'lh', 'lf', 'sh']),
  # ('protocolList', ['lw', 'lh', 'lf']),
  # ('protocolList', ['lh', 'sh']),
  # ('protocolList', ['lw']),
  # ('protocolList', ['lh']),
  ('protocolList', ['lf']),
  # ('protocolList', ['smw']),
  # ('protocolList', ['smh']),
  # ('protocolList', ['smf']),
  # ('protocolList', ['sh']),

  ('hostNodeId', 1),                             # ID of the host node

  ('duration', 100*60+2),                        # duration of the simulation (in seconds)
  # ('duration', 60*60+2),                         # duration of the simulation (in seconds)
  ('period', 60),
  ('txPwr', 4),                                  # transmit power of the nodes (in dB)
  ('seed', 0),                                   # seed for random number generator
  ('raiseException', True),

  # ('modulations', [7, 8]),                     # flora modulation idx, must be ordered by increasing idx
  # ('numHops', [4, 5]),                         # number of hops (corresponds to modulations)
  # ('slotLength', 1.0),                         # LSR slot lengths

  # ('modulations', [4, 10]),
  ('modulations', [5, 10]),
  ('numHops', [5, 5]),
  ('slotLength', 1.2),

  # ('modulations', [7]),
  # ('numHops', [3]),
  # ('slotLength', 1.0),

  # ('modulations', [8]),
  # ('numHops', [5]),
  # ('slotLength', 1.0),

  ('nTx', 2),
  ('statsDepth', 3),
  ('selRcIdxPercentile', 40),
  ('deregisteringThreshold', 5*60),              # in seconds
  ('bpsRoundThreshold', 100),                    # in rounds
  ('pathlossMatrixScaling', 1.05),               # only applied for the flocklab scenario
  ('sensitivitySrcDatasheet', False),            # True: use sensitivity values based on datasheet; False: use sensitivity values based on measurements
  ('roundIdxOffsetMode', 'relative'),            # allowed values: 'relative', 'absolute'
  ('roundIdxOffset', 20),                        # offset in number of rounds for the evaluation of the results;
                                                 #   roundIdxOffsetMode='absolute': directly provide the roundIdxOffset relative to the first round (roundIdx=0), only warning is produced if a node is never registered
                                                 #   roundIdxOffsetMode='relative': roundIdxOffset relative to the first round where all nodes are registered , error is produced if a node is never registered

  # config for output (useful when running using results saved in pickle file)
  ('metricsOutput', ['reliability', 'energy', 'energyNormalized']),  # NOTE: reliability cannot be omitted since otherwise error display does no longer work correctly
  ('skipProtocolsOutput', []),
])
################################################################################

# config for paper: overview of scenarios
configs['overview'] = OrderedDict([
  ('scenarioList', ['sr', 'ls', 'lm', 'cr', 'rc', 'li', 'fo']),
  # ('scenarioList', ['cr']),
  # ('protocolList', ['lf', 'smw', 'smf']),
  ('protocolList', ['lf', 'smw']),

  ('hostNodeId', 1),                             # ID of the host node

  # ('duration', 60*60+2),                         # duration of the simulation (in seconds)
  # ('duration', 100*60+2),                        # duration of the simulation (in seconds)
  # ('duration', 200*60+2),                        # duration of the simulation (in seconds)
  ('duration', 500*60+2),                        # duration of the simulation (in seconds)
  ('period', 60),
  ('txPwr', 4),                                  # transmit power of the nodes (in dB)
  ('seed', 1),                                   # seed for random number generator
  ('raiseException', True),

  ('modulations', [5, 10]),
  ('numHops', [5, 5]),
  ('slotLength', 1.2),

  ('nTx', 2),
  ('statsDepth', 3),
  ('selRcIdxPercentile', 40),
  ('deregisteringThreshold', 5*60),              # in seconds
  ('bpsRoundThreshold', 100),                    # in rounds
  ('pathlossMatrixScaling', 1.05),               # only applied for the flocklab scenario
  ('sensitivitySrcDatasheet', False),            # True: use sensitivity values based on datasheet; False: use sensitivity values based on measurements
  # ('roundIdxOffsetMode', 'relative'),            # allowed values: 'relative', 'absolute'
  # ('roundIdxOffset', 20),                        # offset in number of rounds for the evaluation of the results;
  ('roundIdxOffsetMode', 'absolute'),            # allowed values: 'relative', 'absolute'
  ('roundIdxOffset', 30),                        # offset in number of rounds for the evaluation of the results; TODO: use to remove 30 rounds in the beginning
                                                 #   roundIdxOffsetMode='absolute': directly provide the roundIdxOffset relative to the first round (roundIdx=0), only warning is produced if a node is never registered
                                                 #   roundIdxOffsetMode='relative': roundIdxOffset relative to the first round where all nodes are registered , error is produced if a node is never registered

  # config for output (useful when running using results saved in pickle file)
  ('metricsOutput', ['reliability', 'energy', 'energyNormalized']),  # NOTE: reliability cannot be omitted since otherwise error display does no longer work correctly
  ('skipProtocolsOutput', []),
])
################################################################################

# config for paper: closer look
configs['closerlook'] = OrderedDict([
  ('scenarioList', ['cr']),
  # ('protocolList', ['lf']), # DEBUG
  ('protocolList', ['lf', 'smw', 'smf']),

  ('hostNodeId', 1),                             # ID of the host node

  # ('duration', 100*60+2),                        # duration of the simulation (in seconds)
  # ('duration', 120*60+2),                        # duration of the simulation (in seconds)
  # ('duration', 200*60+2),                        # duration of the simulation (in seconds)
  ('duration', 1000*60+2),                       # duration of the simulation (in seconds)
  ('period', 60),
  ('txPwr', 4),                                  # transmit power of the nodes (in dB)
  # ('seed', 5),                                   # seed for random number generator
  # ('seed', 24),                                  # seed for random number generator (thinning/re-register after bootstrap within first 100 rounds, node4 fails many times)
  # ('seed', 25),                                  # seed for random number generator (thinning/re-register after bootstrap  within first 120 rounds, node4 has 2 drops)
  # ('seed', 43),                                  # seed for random number generator (thinning/re-register after bootstrap within first 100 rounds, node4 has drops occasionally)
  # ('seed', 54),                                  # seed for random number generator (thinning/re-register after bootstrap within first 100 rounds, node4 has drops often)
  ('seed', 67),                                  # seed for random number generator (thinning/re-register after bootstrap within first 100 rounds, no drops)
  # ('seed', 74),                                  # seed for random number generator (thinning/re-register after bootstrap within first 100 rounds, node4 has 3 drops)
  # ('seed', 80),                                  # seed for random number generator (?)

  ('raiseException', True),

  ('modulations', [5, 10]),
  ('numHops', [5, 5]),
  ('slotLength', 1.2),

  ('nTx', 2),
  ('statsDepth', 3),
  ('selRcIdxPercentile', 40),
  ('deregisteringThreshold', 5*60),              # in seconds
  # ('bpsRoundThreshold', 10),                     # in rounds # DEBUG
  # ('bpsRoundThreshold', 100),                    # in rounds
  ('bpsRoundThreshold', 95),                     # in rounds
  ('pathlossMatrixScaling', 1.05),               # only applied for the flocklab scenario
  ('sensitivitySrcDatasheet', False),            # True: use sensitivity values based on datasheet; False: use sensitivity values based on measurements
  ('roundIdxOffsetMode', 'relative'),            # allowed values: 'relative', 'absolute'
  ('roundIdxOffset', 19),                        # offset in number of rounds for the evaluation of the results;
                                                 #   roundIdxOffsetMode='absolute': directly provide the roundIdxOffset relative to the first round (roundIdx=0), only warning is produced if a node is never registered
                                                 #   roundIdxOffsetMode='relative': roundIdxOffset relative to the first round where all nodes are registered , error is produced if a node is never registered

  # config for output (useful when running using results saved in pickle file)
  ('metricsOutput', ['reliability', 'energy', 'energyNormalized']),  # NOTE: reliability cannot be omitted since otherwise error display does no longer work correctly
  ('skipProtocolsOutput', []),
])
################################################################################

# config for paper: dynamical behavior
configs['dynamic'] = OrderedDict([
  ('scenarioList', ['rc']),
  ('protocolList', ['lf']),

  ('hostNodeId', 1),                             # ID of the host node

  # ('duration', 160*60+2),                        # duration of the simulation (in seconds)
  ('duration', 100*60+2),                        # duration of the simulation (in seconds)
  ('period', 60),
  ('txPwr', 4),                                  # transmit power of the nodes (in dB)
  # ('seed', 0),                                   # seed for random number generator (no packets lost, SR ok)
  # ('seed', 15),                                  # seed for random number generator (packet loss, SR balanced)
  # ('seed', 19),                                  # seed for random number generator (no packet loss, SR ok)
  ('seed', 25),                                  # seed for random number generator (no packets lost, SR nice)
  ('raiseException', True),

  ('modulations', [5, 10]),
  ('numHops', [5, 5]),
  ('slotLength', 1.2),

  ('nTx', 2),
  ('statsDepth', 8),
  ('selRcIdxPercentile', 40),
  ('deregisteringThreshold', 5*60),              # in seconds
  ('bpsRoundThreshold', 100),                    # in rounds
  ('pathlossMatrixScaling', 1.05),               # only applied for the flocklab scenario
  ('sensitivitySrcDatasheet', False),            # True: use sensitivity values based on datasheet; False: use sensitivity values based on measurements
  ('roundIdxOffsetMode', 'relative'),            # allowed values: 'relative', 'absolute'
  ('roundIdxOffset', 20),                        # offset in number of rounds for the evaluation of the results;
                                                 #   roundIdxOffsetMode='absolute': directly provide the roundIdxOffset relative to the first round (roundIdx=0), only warning is produced if a node is never registered
                                                 #   roundIdxOffsetMode='relative': roundIdxOffset relative to the first round where all nodes are registered , error is produced if a node is never registered

  # config for output (useful when running using results saved in pickle file)
  ('metricsOutput', ['reliability', 'energy', 'energyNormalized']),  # NOTE: reliability cannot be omitted since otherwise error display does no longer work correctly
  ('skipProtocolsOutput', []),
])
################################################################################

# configs for paper: compare sim and testbed

configs['fl2_s1'] = OrderedDict([
  ('scenarioList', ['fl2_s1']),
  # ('protocolList', ['lh']),
  ('protocolList', ['lf']),

  ('hostNodeId', 7),                             # ID of the host node

  ('duration', 180*60+2),                        # duration of the simulation (in seconds)
  # ('duration', 60*60+2),                        # duration of the simulation (in seconds) # DEBUG
  ('period', 60),
  ('txPwr', 4),                                  # transmit power of the nodes (in dB)
  ('seed', 0),                                   # seed for random number generator
  ('raiseException', True),

  ('modulations', [5, 10]),
  ('numHops', [2, 4]),
  ('slotLength', 1.),

  ('nTx', 2),
  ('statsDepth', 3),
  # ('statsDepth', 5),
  ('selRcIdxPercentile', 20),
  ('deregisteringThreshold', 5*60),              # in seconds
  ('bpsRoundThreshold', 100),                    # in rounds
  ('pathlossMatrixScaling', 1.03),               # only applied for the flocklab scenario
  # ('pathlossMatrixScaling', 1.05),               # only applied for the flocklab scenario
  ('sensitivitySrcDatasheet', False),            # True: use sensitivity values based on datasheet; False: use sensitivity values based on measurements
  ('roundIdxOffsetMode', 'absolute'),            # allowed values: 'relative', 'absolute'
  ('roundIdxOffset', 30),                        # offset in number of rounds for the evaluation of the results;
                                                 #   roundIdxOffsetMode='absolute': directly provide the roundIdxOffset relative to the first round (roundIdx=0), only warning is produced if a node is never registered
                                                 #   roundIdxOffsetMode='relative': roundIdxOffset relative to the first round where all nodes are registered , error is produced if a node is never registered

  # config for output (useful when running using results saved in pickle file)
  ('metricsOutput', ['reliability', 'energy', 'energyNormalized']),  # NOTE: reliability cannot be omitted since otherwise error display does no longer work correctly
  ('skipProtocolsOutput', []),
])

configs['fl2_s2'] = deepcopy(configs['fl2_s1'])
configs['fl2_s2']['scenarioList'] = ['fl2_s2']
configs['fl2_s2']['protocolList'] = ['lf']
configs['fl2_s2']['hostNodeId'] = 1

configs['fl2_s3'] = deepcopy(configs['fl2_s1'])
configs['fl2_s3']['scenarioList'] = ['fl2_s3']
configs['fl2_s3']['protocolList'] = ['lf']
configs['fl2_s3']['hostNodeId'] = 15
configs['fl2_s3']['seed'] = 0
# configs['fl2_s3']['seed'] = 2

configs['fl2_s4'] = deepcopy(configs['fl2_s1'])
configs['fl2_s4']['scenarioList'] = ['fl2_s4']
configs['fl2_s4']['protocolList'] = ['lf']
configs['fl2_s4']['hostNodeId'] = 24

################################################################################
# config for paper: compare LWB single-modulation (short-range) to LSR (2 modulations, full thinning)
configs['overviewLwbSr'] = deepcopy(configs['overview'])
configs['overviewLwbSr']['scenarioList'] = ['sr']
configs['overviewLwbSr']['protocolList'] = ['lf', 'smw1']
# configs['overviewLwbSr']['duration'] = 300*60+2 # commented out to use same duration as in 'overview' config

################################################################################


def protocolToText(protocol=None):
  m = {
    'lw':   'LSR without thinning',
    'lh':   'LSR with hop distance thinning',
    'lf':   'LSR with full thinning',
    'sh':   'Single-hop',
    'smw':  'Single modulation (Mod0) without thinning',
    'smh':  'Single modulation (Mod0) with hop distance thinning',
    'smf':  'Single modulation (Mod0) with full thinning',
    'smw1': 'Single modulation (Mod1) without thinning',
  }
  if protocol is None:
    return m
  elif protocol in m:
    return m[protocol]
  else:
    raise Exception('Unknown protocol \'{}\''.format(protocol))


def scenarioToName(scenario):
  if scenario in scenarioData:
    return scenarioData[scenario]['name']
  elif scenario[:3] == 'fl2':
    return 'FlockLab 2 ({})'.format(scenario[3:])
  else:
    raise Exception('Unknown scenario \'{}\''.format(scenario))


def scenarioToAbbrev(scenario):
  if scenario in scenarioData:
    return 'S{}'.format(list(scenarioData.keys()).index(scenario)+1)
  elif scenario[:3] == 'fl2':
    return 'FL2 ({})'.format(scenario[3:])
  else:
    raise Exception('Unknown scenario \'{}\''.format(scenario))


def runSim(config, outputDir=OUTPUT_DIR):
  """Runs the simulation for multiple scenarios and protocols variants
  Args:
    config:    configuration for running the simulations
    outputDir: path to directory for output
  Returns:
    results:   dict containing all eval results of all scenario/protocol combinations
    lastSim:   simulation object of the last scenario/protocol combination
  """
  config['simStartTime'] = datetime.now(timezone.utc).astimezone().isoformat()

  # sanity checks
  for scenario in config['scenarioList']:
    if ('fl2' not in scenario) and (config['hostNodeId'] != 1):
      raise Exception('hostNodeId for non-flocklab scenario ({}) is not 1!'.format(scenario))

  results = OrderedDict()
  for scenario in config['scenarioList']:
    print('===== scenario={} ====='.format(scenario))
    results[scenario] = OrderedDict()

    if scenario[:3] != 'fl2':
      np.random.seed(config['seed'])
      # get path loss matrix for current scenario
      edges = deepcopy(scenarios.scenarioData[scenario]['edges'])
      pathlossMatrix, nodeIdListPlm = scenarios.edgesToPathlossMatrix(
        edges,
        modIdxLongrange=config['modulations'][0],
        modIdxShortrange=config['modulations'][1],
        txPwr=config['txPwr'],
        sensitivitySrcDatasheet=config['sensitivitySrcDatasheet'],
      )

    for protocol in config['protocolList']:
      print('==== protocol={} ===='.format(protocol))

      # determine config based on protocol
      if protocol == 'lw':
        thinningHopDistance       = False
        thinningConnectivityGraph = False
        numHops                   = config['numHops']
        radioConfigsSlice         = slice(None)
      elif protocol == 'lh':
        thinningHopDistance       = True
        thinningConnectivityGraph = False
        numHops                   = config['numHops']
        radioConfigsSlice         = slice(None)
      elif protocol == 'lf':
        thinningHopDistance       = True
        thinningConnectivityGraph = True
        numHops                   = config['numHops']
        radioConfigsSlice         = slice(None)
      elif protocol == 'sh':
        thinningHopDistance       = False
        thinningConnectivityGraph = False
        numHops                   = [1]*len(config['modulations'])
        radioConfigsSlice         = slice(None)
      elif protocol == 'smw':
        thinningHopDistance       = False
        thinningConnectivityGraph = False
        numHops                   = config['numHops']
        radioConfigsSlice         = slice(1)  # stop=1
      elif protocol == 'smh':
        thinningHopDistance       = True
        thinningConnectivityGraph = False
        numHops                   = config['numHops']
        radioConfigsSlice         = slice(1)  # stop=1
      elif protocol == 'smf':
        thinningHopDistance       = True
        thinningConnectivityGraph = True
        numHops                   = config['numHops']
        radioConfigsSlice         = slice(1)  # stop=1
      elif protocol == 'smw1':
        thinningHopDistance       = False
        thinningConnectivityGraph = False
        numHops                   = config['numHops']
        radioConfigsSlice         = slice(1, 2)  # start=1, stop=2
      else:
        raise Exception('Unknown protocol {}!'.format(protocol))

      radioConfigs = []
      for modIdx in config['modulations']:
        rc = sx1262.getFloraConfig(modIdx=modIdx)
        # additional fields required for the simulation
        rc.frequency = 868100000  # in Hz
        rc.txPower   = config['txPwr']
        rc.sensitivitySrcDatasheet = config['sensitivitySrcDatasheet']
        radioConfigs.append(rc)

      sim = Sim(
        logFileName='simLog_{}_{}.txt'.format(scenario, protocol),
        outputDir=outputDir,
      )
      sim.setRadioConfigs(radioConfigs)
      sim.propagation     = True
      sim.timeDrift       = True
      sim.probabilisticRf = True
      sim.verbose         = True
      sim.debug           = False
      sim.setRandSeed(config['seed'])

      ##############################################################################
      # Scenarios
      ##############################################################################

      def genNode(nodeId, nodeIdList, pos):
        return LsrNode(
          radioConfigs=radioConfigs[radioConfigsSlice],
          hostNodeId=config['hostNodeId'],
          nodeId=nodeId,
          position=pos,
          nTx=config['nTx'],
          numHops=numHops,
          slotLength=config['slotLength'],
          period=config['period'],
          streamPeriod=config['period'],
          startDelay=10 if nodeId == config['hostNodeId'] else 0,
          minNodeId=min(nodeIdList),
          maxNodeId=max(nodeIdList),
          thinningHopDistance=thinningHopDistance,
          thinningConnectivityGraph=thinningConnectivityGraph,
          statsDepth=config['statsDepth'],
          selRcIdxPercentile=config['selRcIdxPercentile'],
          deregisteringThreshold=config['deregisteringThreshold'],
          bpsRoundThreshold=config['bpsRoundThreshold'],
        )

      if scenario.startswith('fl2_'):
        # get list of all nodes
        with open(scenario_flocklab.LINKTEST_PKL_FILE, 'rb' ) as f:
            linktest = pickle.load(f)
        nodeIdListAll = sorted(set(linktest['nodeList']))

        # select subset of flocklab nodes
        flScenario = scenario.replace('fl2_', '')
        if   flScenario == 'all':
          nodeIdList = nodeIdListAll
        elif flScenario == 's12':
          nodeIdList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        elif flScenario in ['s1', 's2', 's3', 's4']:
          nodeIdList = sorted([1, 4, 6, 7, 12, 13, 24, 26, 27, 15, 17, 25, 30])
        elif flScenario == 'd1':  # for debugging
          nodeIdList = sorted([1, 4, 6, 7, 12, 13, 24, 26, 27, 32, 15, 17, 25, 30])
        else:
          raise Exception('Unknown FlockLab scenario \'{}\''.format(flScenario))

        scenario_flocklab.addNodes(
          sim=sim,
          genNode=genNode,
          pathlossMatrixScaling=config['pathlossMatrixScaling'],
          subset=nodeIdList,
          linktestPklFile='flocklab/linktest_data_6298.pkl',
        )

        # ## adapt path loss matrix (dynamic behavior)
        # # get unmodified path loss matrix
        # pathlossMatrix, nodeIdList = scenario_flocklab.getPathlossMatrix(
        #   pathlossMatrixScaling=config['pathlossMatrixScaling'],
        # )

        # # determine modified path loss matrix
        # pathlossMatrixCh = deepcopy(pathlossMatrix)
        # nodeIdListCh = deepcopy(nodeIdList)
        # # remove node 7 for long-range
        # nodeId = 7
        # for nodeIdIter in nodeIdList:
        #   if nodeIdIter == nodeId:
        #     continue
        #   nodeIdIdx = nodeIdList.index(nodeId)
        #   nodeIdIterIdx = nodeIdList.index(nodeIdIter)
        #   pathlossMatrixCh[nodeIdIdx][nodeIdIterIdx] = np.nan
        #   pathlossMatrixCh[nodeIdIterIdx][nodeIdIdx] = np.nan

        # sim.schedulePathlossMatrix(
        #   ts=40*60,
        #   pathlossMatrix=pathlossMatrixCh,
        #   nodeIdList=nodeIdListCh,
        # )
        # sim.schedulePathlossMatrix(
        #   ts=60*60,
        #   pathlossMatrix=pathlossMatrix,
        #   nodeIdList=nodeIdList,
        # )

      else:  # scenario != 'fl2'
        scenarios.addNodes(
          sim=sim,
          scenario=scenario,
          genNode=genNode,
          nodeIdListPlm=nodeIdListPlm,
        )
        # set initial pathloss matrix (use path loss matrix generated above to ensure that we use same path loss matrix for all protocols within the same scenario)
        sim.schedulePathlossMatrix(
          pathlossMatrix=deepcopy(pathlossMatrix)*1.0,
          nodeIdList=deepcopy(nodeIdListPlm),
        )

        # ## adapt path loss matrix (dynamic behavior)
        if config['evalSel'] == 'dynamic':
          # get modified path loss matrix
          edgesCh = deepcopy(scenarios.scenarioData[scenario]['edges'])
          # change edges
          edgesCh[(1, 5)] = 1  # set edge to support short-range modulation
          # edgesCh[(1, 7)] = 1  # set edge to support short-range modulation
          pathlossMatrixCh, nodeIdListPlmCh = scenarios.edgesToPathlossMatrix(
            edgesCh,
            modIdxLongrange         = config['modulations'][0],
            modIdxShortrange        = config['modulations'][1],
            txPwr                   = config['txPwr'],
            sensitivitySrcDatasheet = config['sensitivitySrcDatasheet'],
          )
          # # remove node 3 for long-range
          # for e, m in edgesCh.items():
          #   if (3 in e) and (m == 0):
          #     n0 = nodeIdListPlmCh.index(e[0])
          #     n1 = nodeIdListPlmCh.index(e[1])
          #     pathlossMatrixCh[n0][n1] = np.nan
          #     pathlossMatrixCh[n1][n0] = np.nan
          #
          sim.schedulePathlossMatrix(
            ts=50*60,
            pathlossMatrix=pathlossMatrixCh,
            nodeIdList=nodeIdListPlmCh,
          )
          # sim.schedulePathlossMatrix(
          #   ts=60*60,
          #   pathlossMatrix=pathlossMatrix,
          #   nodeIdList=nodeIdList,
          # )

      ##############################################################################
      # Pre-register nodes on LSR host
      ##############################################################################
      # NOTE: nodeId of host is ignored by registerNode()

      # hostNodeIdSim = sim.nodeId2nodeIdSim(config['hostNodeId'])
      # for nodeId in sim.getNodeIdList():
      #   sim.nodes[hostNodeIdSim].lsr.preRegisterNode(nodeId=nodeId, rcIdx=0)
      # sim.nodes[hostNodeIdSim].lsr.preRegisterNode(nodeId=1, rcIdx=0)
      # sim.nodes[hostNodeIdSim].lsr.preRegisterNode(nodeId=3, rcIdx=1)

      ##############################################################################
      # Run simulation
      ##############################################################################
      try:
        sim.plotNodePositions()
        sim.runSimulation(until=config['duration'])

        print('=== GENERATING PLOTS ===')
        # only plot for last combination
        if ( (config['scenarioList'].index(scenario) + 1 == len(config['scenarioList']) and config['protocolList'].index(protocol) + 1 == len(config['protocolList'])) or
             (config['evalSel'] == 'closerlook') ):
          sim.plotTransmissions(outFile='transmissions_{}_{}.html'.format(scenario, protocol))
          sim.plotEnergy(outFile='energy_accumulated_{}_{}.html'.format(scenario, protocol))

        # the following plot is required for every config as it is used to collect stats for the evaluation
        Lsr.plotEnergyPerRound(sim=sim, radioConfigs=radioConfigs[radioConfigsSlice], outFile='energyPerRound_{}_{}.html'.format(scenario, protocol))
        print('=== GENERATING PLOTS FINISHED ===')

        print('=== STARTING EVALUATION ===')
        res = OrderedDict()
        s   = sim.stats
        res['registrationHost']      = s.evalRegistrationHost()
        res['registrationClients']   = s.evalRegistrationClients()
        if thinningConnectivityGraph:
          res['graphInfo']           = s.evalGraphInfo()
          res['thinningStatus']      = s.evalThinningStatus()
          res['selectedPaths']       = s.evalSelectedPaths()
        res['reliability']           = s.evalReliability()
        res['energy']                = s.evalEnergy()
        # res['energyNormalized']      = s.evalEnergyNormalized() # disabled as not used in paper and overhead is not negligible
        results[scenario][protocol] = res
        print('=== EVALUATION FINISHED ===')
      except Exception as e:
        if config['raiseException']:
          raise
        print('Sim lead to an error: {}'.format(e))
        results[scenario][protocol] = e

  config['simEndTime'] = datetime.now(timezone.utc).astimezone().isoformat()

  return results, sim


def getSimResults(config, outputDir=OUTPUT_DIR, storeLastSim=True):
  os.makedirs(outputDir, exist_ok=True)

  # try to load from pickle file
  pklHasSameConfig = False
  if usePickle and os.path.isfile(os.path.join(outputDir, PICKLE_FILENAME)):
    with open(os.path.join(outputDir, PICKLE_FILENAME), 'rb') as f:
      pklDict = pickle.load(f)

    configPkl          = deepcopy(pklDict['config'])
    configTmp          = deepcopy(config)
    doNotCompareFields = ['metricsOutput', 'skipProtocolsOutput', 'simStartTime', 'simEndTime', 'raiseException']
    for field in doNotCompareFields:
      if field in configPkl:
        del configPkl[field]
      if field in configTmp:
        del configTmp[field]

    if configPkl == configTmp:
      pklHasSameConfig = True
    else:
      print('Difference in config:')
      for k in list(set(list(configPkl.keys())).union(set(list(configTmp.keys())))):
        if configPkl[k] != configTmp[k]:
          print('{}: {} != {}'.format(k, configPkl[k], configTmp[k]))

  lastSim = None
  if pklHasSameConfig:
    # load from pickle
    print('Loading results from pickle file...')
    configFinal = pklDict['config']

    for field in doNotCompareFields:
      if field not in configFinal:
        configFinal[field] = config[field]

    results = pklDict['results']
    if 'lastSim' in pklDict:
      lastSim = pklDict['lastSim']

  else:
    # run sim
    print('Running simulation...')
    results, lastSim = runSim(config, outputDir)
    configFinal = config

    # store config and results in pickle file
    if storeLastSim:
      del lastSim._logFile  # circumvent pickle limitation: cannot pickle '_io.TextIOWrapper' object
      pickleDict = {'config': configFinal, 'results': results, 'lastSim': lastSim}
    else:
      pickleDict = {'config': configFinal, 'results': results}

    with open(os.path.join(outputDir, PICKLE_FILENAME), 'wb') as f:
      pickle.dump(pickleDict, f)

    with open(os.path.join(outputDir, CONFIG_FILENAME), 'w') as f:
      pprint(configFinal, stream=f)
    # f.write(repr(configFinal))

  return results, configFinal, lastSim

################################################################################


if __name__ == '__main__':
  # evalSel = 'debug'
  # evalSel = 'overview'
  # evalSel = 'closerlook'
  # evalSel = 'dynamic'
  # evalSel = 'fl2_s1'
  # evalSel = 'fl2_s2'
  evalSel = 'fl2_s3'
  # evalSel = 'fl2_s4'
  # evalSel = 'overviewLwbSr'

  # sanity check
  if evalSel not in configs:
    raise Exception('Eval selection \'{}\' is not defined!'.format(evalSel))

  outputDir = os.path.join(OUTPUT_DIR, evalSel)

  # Get sim results (by running sim or loading results from pickle file)
  config = configs[evalSel]
  config['evalSel'] = evalSel
  config.move_to_end('evalSel', last=False)  # make evalSel element the first element
  # NOTE: config is extended with start and end time
  results, config, lastSim = getSimResults(
    config=config,
    outputDir=outputDir,
  )

  plt.close('all')

  # output results

  # prepare
  # palette = [sns.color_palette('muted')[i] for i in [8, 0, 9, 1, 2, 3, 4, 5, 6, 7]]
  protocolColorMap = {
    'smw':  sns.color_palette('muted')[8],
    'smw1': sns.color_palette('muted')[8],
    'smf':  sns.color_palette('muted')[9],
    'lf':   sns.color_palette('muted')[0],
    'lw':   sns.color_palette('muted')[1],
  }
  ro = ResultsOutput(
    results=results,
    config=config,
    protocolToText=protocolToText,
    # scenarioToName=scenarioToAbbrev,
    scenarioToName=scenarioToName,
    outputDir=os.path.join(outputDir, 'results'),
    protocolColorMap=protocolColorMap,
    plotTitle=True,
  )

  # output for all scenarios and all protocols
  ro.printStats()
  ro.generateHtmlTable()
  ro.generateViolinHtml()

  if evalSel == 'overview':
    ro.generateAggPlots(subsetProtocols=OrderedDict([
      ('smw', 'LWB'),
      ('lf', 'LSR'),
    ]))
  elif evalSel == 'overviewLwbSr':
    ro.generateAggPlots(subsetProtocols=OrderedDict([
      ('smw1', 'LWB SR'),
      ('lf', 'LSR'),
    ]))
  elif evalSel == 'debug':
    p = config['protocolList'][0]
    ro.generateAggPlots(subsetProtocols=OrderedDict([
      (p, p),
    ]))

  # output for single scenario
  if evalSel == 'closerlook':
    ro.plotEnergySingleScenario(scenario='cr', subsetProtocols=OrderedDict([
      ('smw', 'LWB'),
      ('smf', 'LSR (1 modulation)'),
      ('lf',  'LSR (2 modulations)'),
    ]))
  elif evalSel == 'debug':
    s = config['scenarioList'][0]
    p = config['protocolList'][0]
    ro.plotEnergySingleScenario(scenario=s, subsetProtocols=OrderedDict([
      (p, p),
    ]))

  # output for single scenario and single protocol
  if evalSel == 'debug':
    s = config['scenarioList'][0]
    p = config['protocolList'][0]
    ro.plotEnergy(scenario=s, protocol=p)
    # ro.plotEnergyNormalized(scenario=s, protocol=p)
    ro.plotActiveModulation(scenario=s, protocol=p)
    ro.plotReliability(scenario=s, protocol=p)
    # ro.generateGraphPlots(scenario=s, protocol=p, highlightUsedEdges=True)
  elif evalSel == 'closerlook':
    ro.plotActiveModulation(scenario='cr', protocol='lf')
    ro.plotEnergy(scenario='cr', protocol='lf', roundIdxLimit=99)
    # ro.plotEnergy(scenario='cr', protocol='lf', roundIdxLimit=119)
    # ro.plotEnergy(scenario='cr', protocol='lf')
    # ro.plotEnergyNormalized(scenario='cr', protocol='lf')
  elif evalSel == 'dynamic':
    ro.plotActiveModulation(scenario='rc', protocol='lf')
    ro.plotReliability(scenario='rc', protocol='lf')
    ro.generateGraphPlots(scenario='rc', protocol='lf', highlightUsedEdges=True)
  elif evalSel.startswith('fl2_'):
    s = config['scenarioList'][0]
    p = config['protocolList'][0]
    variant = s.split('_')[1]

    # Load results from testbed execution (input = pkl file output from eval_results.py)
    if variant == 's1':
      pass
      # fileName = ''
    elif variant == 's2':
      pass
      # fileName = ''
    elif variant == 's3':
      fileName = '<filename>.pkl'
    elif variant == 's4':
      fileName = '<filename>.pkl'

    with open(os.path.join(outputDir, fileName), 'rb') as f:
      pklDict = pickle.load(f)
    # filter out first few rounds with bootstrapping phase
    roundIdxOffsetTestbed = 30
    relTestbedDf = pklDict['reliabilityDf']
    relTestbedDf = relTestbedDf[relTestbedDf.roundIdx >= roundIdxOffsetTestbed]
    # energyTestbedDf = pklDict['energyGpioDf']
    energyTestbedDf = pklDict['energyRlDf']
    energyTestbedDf = deepcopy(energyTestbedDf[energyTestbedDf.roundIdx >= roundIdxOffsetTestbed])  # deepcopy is not strictly necessary, added to suppress warning "value is trying to be set on a copy of a slice from a DataFrame" inside plotEnergy

    # # plot only sim data
    # ro.plotReliability(scenario=s, protocol=p, rx=False, tx=False, comb=True, testbedDf=None)
    # ro.plotEnergy(scenario=s, protocol=p, testbedDf=None)

    # plot sim and testbed data
    ro.plotReliability(scenario=s, protocol=p, rx=False, tx=False, comb=True, testbedDf=relTestbedDf)
    ro.plotEnergy(scenario=s, protocol=p, testbedDf=energyTestbedDf)
    ro.plotActiveModulation(scenario=s, protocol=p)
    # ro.plotEnergyNormalized(scenario=s, protocol=p)
