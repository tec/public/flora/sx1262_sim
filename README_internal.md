# Simulation for the SX126x Radio Platform (internal)
Details about the implementation of the simulator (details might be outdated in parts). 


## Goals
* Step through Glossy propagation steps
* Enter network connectivity as link quality matrix
* Add obstacles
* Graphical interface (for visualization and placement of obstacles) based on python-flask
* Testing of protocol implementation in an ideal environment (simplifying assumptions, e.g. no propagation delay)
* Performance metrics (Usage of frequency bands, success rates, data throughput, energy consumption)
* Implementation of an energy model

## Coverage & Limitations
### Modelled
* Processing delay
* Energy usage (simplified)
* Concurrent transmissions

### Not Modelled
* Propagation delay

## Components
* **Medium (air)**: Represents the air and environment, decisions as which transmission is successful in which time unit, which transmissions overlap.
* **Event queue**: Takes care of processing events in the order of time (e.g. TxStart, TxDone)
* **Sim**: Top element that connects other elements, list of available nodes.
* **Node**: Radio node which are independent (independent state, clock drift, etc. )
* **Events**: Available events for the nodes

## Interface
In: Function / callback function
Out: Events to be added to event queue in return of functions

* Node
  * In: Init
  * In: Radio callback functions
  * In: Timer callback functions
  * Out: Radio send
  * Out: Timer start

### Node

### Events

#### Event Properties
* eventId (index of dict)
* type (implicit, type of obj)
* ts: time when the event happens
* actionId: references the corresponding action obj in the action table
* txActionId: references the corresponding RadioTxAction obj (only existing and valid for RxDone events!)

#### Available Events
* See "Triggered Events" as listed for each action
*
NOTE: actionId of RxDone points to node which SENT the message since this node triggers the RxDone event.

### Actions
* Properties
  * actionId (index of dict)
  * type (implicit, type of obj)
  * nodeId
  * (additional action specific properties)

#### Timer Start Action
* Args
  * nodeId
  * startTime (as offset to callback function call)
  * timerId
  * timeout
* Effect: Triggered Events
  * TimerTimeout

#### Timer Stop Action
* Args
  * timerId
* Effect:
  * Deletes all future TimerTimeout events for the given nodeId and timerId

#### Radio Rx Action
* Args
  * nodeId
  * radioConfig
  * timeout
* Effect: Generates Events
  * RxStart
  * RxEnd
  * RxTimeout

#### Radio Tx Action
* Args
  * nodeId
  * radioConfig
  * payload
* Effect: Generates Events
  * TxStart
  * TxDone
  * (RxDone) (only if transmission is successful)
  * TxTimeout

#### Radio Reset Action
* Args:
  * (none)
* Effect:
  * Ends ongoing radio action (either Rx, Tx, or none) and deletes all future radio related events

### Sim State
#### Event Queue
[
  (ts:float, actionId:int)
]
#### Action Table
{
  actionId:int -> actionObj
}

#### Medium Table
{
  nodeId:int -> {tx:bool, actionId:int, overlap:list_of_actionIds}
}

### 3 Types of Floods
* **Send Flood**: send nTx times, no matter what is received
* **Participate Flood**: receive & send (send first transmission if initiator, otherwise send only after reception of message in preceding slot)
* **Receive Flood**: receive only, no Tx (nTx=0)


## TODO
* Power model (energy consumption in states, state transitions, overhead for decoding processing)
* Capture effect model
* Processing of event queue
  * State per input node action (e.g. RadioRxAction)
  * Tick off event queue events in node action Object
  * Remove node action object only if all event queue events are ticked off
* Visualization graph with arbitrary path loss matrix (labeled nodes, edge label = weight)
