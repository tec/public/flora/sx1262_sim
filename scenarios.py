#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""


import pickle
import numpy  as np
import pandas as pd
from copy import copy, deepcopy
import itertools

import sx1262
from sx1262.sx1262 import flora_radio_constants

from sim  import Sim
from node import *

################################################################################
# Scenario Data
################################################################################
# edges:     (nodeId1, nodeId2) -> rcIdx
# positions: nodeId -> (x, -y)
# rcIdx:     0: long-range; 1: short-range
# sink/host: node 1

scenarioData = {
  # short-range only (sr)
  'sr': {
    'name': 'Short-range',
    'edges': {
      (1, 2): 0,
      (1, 3): 1,
      (1, 4): 0,
      (1, 5): 0,
      (1, 6): 1,
      (1, 7): 1,
      (1, 8): 1,
      (1, 9): 1,
      (2, 3): 1,
      (2, 6): 1,
      (3, 6): 1,
      (4, 7): 1,
      (5, 6): 1,
      (5, 8): 1,
      (6, 8): 1,
      (7, 9): 1,
    },
    'positions': {
      1: (200, 155),
      2: (141, 90),
      3: (185, 110),
      4: (278, 122),
      5: (100, 185),
      6: (151, 157),
      7: (250, 155),
      8: (161, 199),
      9: (240, 199),
    }
  },
  # long-range single-hop (ls)
  'ls': {
    'name': 'Long-range single-hop',
    'edges': {
      (1, 2): 0,
      (1, 3): 0,
      (1, 4): 0,
      (1, 5): 0,
      (1, 6): 0,
      (1, 7): 0,
      (1, 8): 0,
      (1, 9): 0,
      (2, 4): 0,
      (2, 6): 0,
      (3, 5): 0,
      (4, 6): 0,
      (4, 8): 0,
      (5, 7): 0,
      (7, 9): 0,
    },
    'positions': {
      1: (600, 155),
      2: (550, 70),
      3: (770, 60),
      4: (500, 170),
      5: (750, 130),
      6: (450, 80),
      7: (690, 220),
      8: (470, 230),
      9: (770, 230),
    }
  },
  # long-range multi-hop (lm)
  'lm': {
    'name': 'Long-range multi-hop',
    'edges': {
      (1, 2): 0,
      (1, 3): 0,
      (1, 4): 0,
      (1, 5): 0,
      (1, 7): 0,
      (2, 4): 0,
      (2, 6): 0,
      (3, 5): 0,
      (4, 6): 0,
      (4, 8): 0,
      (5, 7): 0,
      (7, 9): 0,
    },
    'positions': {
      1: (600, 155),
      2: (550, 70),
      3: (770, 60),
      4: (500, 170),
      5: (750, 130),
      6: (450, 80),
      7: (690, 220),
      8: (470, 230),
      9: (770, 230),
    }
  },
  # cluster with remote nodes (cr)
  'cr': {
    'name': 'Cluster with remote nodes',
    'edges': {
      (1, 3): 1,
      (1, 5): 1,
      (1, 6): 1,
      (1, 7): 0,
      (1, 8): 1,
      (1, 9): 0,
      (2, 3): 0,
      (3, 4): 0,
      (3, 5): 1,
      (3, 6): 1,
      (4, 5): 0,
      (4, 6): 0,
      (5, 6): 1,
      (6, 7): 1,
      (6, 8): 1,
      (7, 8): 1,
      (8, 9): 0,
    },
    'positions': {
      1: (230, 419),
      2: (360, 317),
      3: (225, 376),
      4: (50, 400),
      5: (177, 400),
      6: (192, 449),
      7: (197, 489),
      8: (240, 476),
      9: (365, 436),
    }
  },
  # remote cluster (rc)
  'rc': {
    'name': 'Remote cluster',
    'edges': {
      (1, 2): 0,
      (1, 5): 0,
      (1, 7): 0,
      (1, 9): 0,
      (2, 3): 1,
      (2, 5): 1,
      (2, 6): 0,
      (3, 4): 1,
      (3, 5): 1,
      (3, 6): 1,
      (4, 5): 0,
      (4, 6): 1,
      (4, 8): 1,
      (4, 9): 0,
      (5, 6): 1,
      (6, 7): 1,
      (6, 8): 1,
      (6, 9): 1,
      (7, 8): 0,
      (7, 9): 1,
      (8, 9): 1,
    },
    'positions': {
      1: (540, 476),
      2: (650, 310),
      3: (700, 317),
      4: (760, 317),
      5: (670, 360),
      6: (730, 360),
      7: (709, 405),
      8: (770, 370),
      9: (750, 419),
    }
  },
  # line (li)
  'li': {
    'name': 'Line',
    'edges': {
      (1, 2): 0,
      (1, 3): 0,
      (2, 3): 0,
      (2, 4): 0,
      (3, 4): 0,
      (3, 5): 0,
      (4, 5): 0,
      (4, 6): 0,
      (5, 6): 0,
      (5, 7): 0,
      (6, 7): 0,
      (6, 8): 0,
      (7, 8): 0,
      (7, 9): 0,
      (8, 9): 0,
    },
    'positions': {
      1: (305, 692),
      2: (255, 692),
      3: (255, 642),
      4: (205, 642),
      5: (205, 692),
      6: (155, 692),
      7: (155, 642),
      8: (105, 642),
      9: (105, 692),
    }
  },
  # fanout (fo)
  'fo': {
    'name': 'Fanout',
    'edges': {
      (1, 8): 1,
      (1, 9): 1,
      (2, 8): 0,
      (3, 8): 0,
      (3, 9): 0,
      (4, 8): 0,
      (4, 9): 0,
      (5, 8): 0,
      (5, 9): 0,
      (6, 8): 0,
      (6, 9): 0,
      (7, 9): 0,
    },
    'positions': {
      1: (600, 736),
      2: (530, 600),
      3: (560, 600),
      4: (590, 600),
      5: (620, 600),
      6: (650, 600),
      7: (680, 600),
      8: (560, 700),
      9: (650, 700),
    }
  },
  # debug (db)
  'db': {
    'name': 'Debug',
    'edges': {
      (1, 2): 1,
      (1, 3): 1,
      (2, 4): 1,
      (3, 4): 1,
      (3, 5): 0,
      (4, 5): 1,
      (5, 6): 0,
    },
    'positions': {
      1: (1120, 655),
      2: (1070, 620),
      3: (1070, 660),
      4: (1020, 660),
      5: (970, 660),
      6: (880, 660),
    }
  },
}

############################

def genConvScenario(hops, cols):
  perHopIdxInc = 10

  # sanity checks
  if hops <= 0:
    raise Exception(f'Invalid number of hops: {hops}')

  ret = dict()
  ret['name'] = f'{hops} hops, {cols} columns'

  ret['edges'] = dict()
  for h in range(1, hops+1):
    for c in range(cols):
      if (c % 2 == 0) and (c < cols-1):
        # add horizontal edge
        ret['edges'].update( {(h*perHopIdxInc+c, h*perHopIdxInc+c+1): 0} )
      if h == 1:
        # add edge to host node
        ret['edges'].update( {(1, h*perHopIdxInc+c): 0} )
      else:
        # add edges to lower layer nodes
        ret['edges'].update( {((h-1)*perHopIdxInc+c, h*perHopIdxInc+c): 0} )
        if c < cols-1:
          ret['edges'].update( {((h-1)*perHopIdxInc+c+1, h*perHopIdxInc+c): 0} )

  # sort edges (for representation and manual verification, not strictly necessary)
  ret['edges'] = dict(sorted(ret['edges'].items(), key=lambda e: e[0][0]))

  # add dummy positions (for compatibility with sim)
  ret['positions'] = {n: (0, 0) for n in set(itertools.chain.from_iterable(ret['edges'].keys()))}

  return ret

scenarioData['co_2h_4c'] = genConvScenario(hops=2, cols=4)
scenarioData['co_3h_4c'] = genConvScenario(hops=3, cols=4)
scenarioData['co_4h_4c'] = genConvScenario(hops=4, cols=4)
scenarioData['co_2h_6c'] = genConvScenario(hops=2, cols=6)
scenarioData['co_3h_6c'] = genConvScenario(hops=3, cols=6)
scenarioData['co_4h_6c'] = genConvScenario(hops=4, cols=6)
scenarioData['co_2h_8c'] = genConvScenario(hops=2, cols=8)
scenarioData['co_3h_8c'] = genConvScenario(hops=3, cols=8)
scenarioData['co_4h_8c'] = genConvScenario(hops=4, cols=8)

scenarioData['co_3h_2c'] = genConvScenario(hops=3, cols=2)
scenarioData['co_3h_3c'] = genConvScenario(hops=3, cols=3)


################################################################################
# Functions
################################################################################

def edgesToPathlossMatrix(edges, modIdxLongrange=7, modIdxShortrange=8, txPwr=14, sensitivitySrcDatasheet=True):
  """Takes list of edges as input and converts it to a path loss matrix (symmetric path loss for edges with only one direction)
     (rcIdx: 0: long-range, 1: short-range)
  Args:
    edges:                   edge data as dict (node1, node2) -> rcIdx
    modIdxLongrange:         flora modulation index for long-range edges
    modIdxShortrange:        flora modulation index for short-range edges
    txPwr:                   transmit power
    sensitivitySrcDatasheet:
  Returns:
    pathlossMatrix:          path loss matrix based on the edges data
    nodeIdList:              list of nodeIds derived from the provided edges (sorted)
  """
  nodeIdList            = sorted(set([k[0] for k in edges.keys()]).union(set([k[1] for k in edges.keys()])))
  numNodes              = len(nodeIdList)
  sensitivityLongrange  = modIdxToSensitivity(modIdx=modIdxLongrange,  sensitivitySrcDatasheet=sensitivitySrcDatasheet)
  sensitivityShortrange = modIdxToSensitivity(modIdx=modIdxShortrange, sensitivitySrcDatasheet=sensitivitySrcDatasheet)
  minSensitivityDiff    = getMinSensitivityDiff(sensitivitySrcDatasheet=sensitivitySrcDatasheet)
  maxPathLossLongrange  = txPwr - sensitivityLongrange
  maxPathLossShortrange = txPwr - sensitivityShortrange
  pathlossSafetyMargin  = minSensitivityDiff/2
  pathlossRange         = 15  # in dB

  shiftFromThreshold     = 5  # shift range of used path losses away from threshold (i.e. where path loss is equal to sensitivity) to make scenario less pessimistic
  pathLossLongrangeLow   = max(maxPathLossLongrange - pathlossRange - shiftFromThreshold, maxPathLossShortrange + pathlossSafetyMargin)
  pathLossLongrangeHigh  = maxPathLossLongrange - pathlossSafetyMargin - shiftFromThreshold
  pathLossShortrangeLow  = max(maxPathLossShortrange - pathlossRange - shiftFromThreshold, 0)  # max(): ensure that path loss is always positive
  pathLossShortrangeHigh = maxPathLossShortrange - pathlossSafetyMargin - shiftFromThreshold

  print('== Scenarios ==')
  print('[pathLossLongrangeLow,  pathLossLongrangeHigh]  = [{:0.3f}, {:0.3f}], diff={:0.3f}'.format(pathLossLongrangeLow, pathLossLongrangeHigh, pathLossLongrangeHigh - pathLossLongrangeLow))
  print('[pathLossShortrangeLow, pathLossShortrangeHigh] = [{:0.3f}, {:0.3f}], diff={:0.3f}'.format(pathLossShortrangeLow, pathLossShortrangeHigh, pathLossShortrangeHigh - pathLossShortrangeLow))

  pathlossMatrix = np.empty( (numNodes, numNodes,) ) * np.nan
  for txIdx, nodeIdTx in enumerate(nodeIdList):
    for rxIdx, nodeIdRx in enumerate(nodeIdList):
      rcIdx = None
      if   (nodeIdTx, nodeIdRx) in edges:
        rcIdx = edges[(nodeIdTx, nodeIdRx)]
      elif (nodeIdRx, nodeIdTx) in edges:
        rcIdx = edges[(nodeIdRx, nodeIdTx)]

      if rcIdx is None:
        pathlossMatrix[txIdx][rxIdx] = np.nan
      elif not np.isnan(pathlossMatrix[rxIdx][txIdx]):  # ensure that matrix is symmetric
        pathlossMatrix[txIdx][rxIdx] = pathlossMatrix[rxIdx][txIdx]
      else:
        if   rcIdx == 0:
          pathlossMatrix[txIdx][rxIdx] = np.random.uniform(low=pathLossLongrangeLow, high=pathLossLongrangeHigh)
        elif rcIdx == 1:
          pathlossMatrix[txIdx][rxIdx] = np.random.uniform(low=pathLossShortrangeLow, high=pathLossShortrangeHigh)
        else:
          raise Exception('Unknown rcIdx {}'.format(rcIdx))

  return pathlossMatrix, nodeIdList


def modIdxToSensitivity(modIdx, sensitivitySrcDatasheet=True):
  """Convert flora modulation index to sensitivity value of the sx1262 radio
  """
  rcConfig = sx1262.getFloraConfig(modIdx)
  rcConfig.sensitivitySrcDatasheet = sensitivitySrcDatasheet
  return rcConfig.sensitivity


def getMinSensitivityDiff(sensitivitySrcDatasheet=True):
  sensitivityMap = dict()
  for floraModIdx in range(len(flora_radio_constants)):
    sensitivityMap[floraModIdx] = modIdxToSensitivity(floraModIdx, sensitivitySrcDatasheet=sensitivitySrcDatasheet)
    # print('floraModIdx={}, sensitivity={:0.3f}dB'.format(floraModIdx, modIdxToSensitivity(floraModIdx, sensitivitySrcDatasheet=sensitivitySrcDatasheet)))
  minSensitivityDiff = np.min(np.diff(list(sensitivityMap.values())))
  # print('sensitivity min diff: {:0.3f}dB'.format(minSensitivityDiff))
  return minSensitivityDiff


def addNodes(sim, scenario, genNode, nodeIdListPlm):
  nodeIdListPositions = sorted(scenarioData[scenario]['positions'].keys())
  if nodeIdListPlm != nodeIdListPositions:
    raise Exception('nodeIdList from edges and from positions differ for scenario \'{}\'!'.format(scenario))

  # add nodes
  for nodeId in nodeIdListPlm:
    pos  = scenarioData[scenario]['positions'][nodeId]
    pos  = (pos[0], -pos[1])
    node = genNode(nodeId=nodeId, nodeIdList=nodeIdListPlm, pos=pos)
    assert isinstance(node, Node)
    sim.addNode(node)

################################################################################


if __name__ == '__main__':
  plm_sr = edgesToPathlossMatrix(scenarioData['sr']['edges'], modIdxLongrange=7, modIdxShortrange=8, sensitivitySrcDatasheet=False)
  plm_lr = edgesToPathlossMatrix(scenarioData['ls']['edges'], modIdxLongrange=7, modIdxShortrange=8, sensitivitySrcDatasheet=False)
  plm_cr = edgesToPathlossMatrix(scenarioData['cr']['edges'], modIdxLongrange=7, modIdxShortrange=8, sensitivitySrcDatasheet=False)
  plm_rc = edgesToPathlossMatrix(scenarioData['rc']['edges'], modIdxLongrange=7, modIdxShortrange=8, sensitivitySrcDatasheet=False)
