#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""

'''

TODOs
- [ ] The node now tries to connect to the highest rated neighbor and the gathered information about all other overheard potential parents is stored.


BeaconMsg
* distance_to_sink (hop_count)
* load (num_children)

On child: connect to selection:
* To minimize tree depth, distance_to_sink (hop_count) has a higher weight than load (num_children) in the computation
  * hop_count > num_children > nodeId
* request contention window by sending _busy tone_ aka _activation frame_; participate in contention after beacon using back-off mechanism
* send "connection request" (C) during contention window
receive ack (handshake (H))
* for having information about potential parents, a node periodically listens for their beacons; based on predicted beacons (to keep already detected parents) and infrequent random listening (to detect new potential parents)
* suspend mode: node only rarely listens for beacon in case no beacon has been received for some time (period is trade-off between low-power and fast reconnect)

On parent: connect child
* send beacon (B)
* parent opens short Rx window right after beacon (for nodes to activate contentention window); no msg reception, just measuring RSSI
* receive _connection request_; a node only accepts one new child per beacon interval
* send acknowledgement (aka handshake (H))

Schedule
* each node has 2 schedules (child schedule: dictated by parent, parent schedule: self-determined for children)
* take into account clock drift, no global time sync!!!
* no global schedule: collisions between the transmissions of neighboring nodes [cannot] be excluded. [...] Dozer thus refrains from handling them actively.
* Prevent alignment of two independent schedules: Dozer extends the length of a TDMA round by a randomly chosen time span—also referred to as jitter.
  * parent draws a new random number for each round of the schedule which is then added to the round’s length.
  * Dozer uses a bound of seven times the time needed to flush the local message buffer (c.f. in Section 4.3).
  * seed random for number generator used is included in each beacon.
* Each data gathering msg is acked
  * With acknowledgment,  parent takes over responsibility for [forwarding] the packet
  * with ack, parent notifies the child about how many more messages it is willing to accept.
  * If a message transfer fails to be acknowledged the child immediately stops its data upload for this round
  * if there are multiple consecutive transmission failers, the Data Manager instructs the Tree Maintenance module to switch to another parent.
* Buffering
  * default: only buffer one data message from each node
  * if more than one message from the same origin meet on a node the newer one is buffered and forwarded wheras the older one is discarded.
* Data flooding from host to all nodes
  * beacon message is used to forward data (e.g. command) to all or individual nodes

'''

import numpy as np
from collections import OrderedDict
import struct
from transitions import Machine
import enum
from copy import copy, deepcopy

from message import Message
from node import Node
from actions import *

from sx1262 import LoraConfig, FskConfig
import sx1262


################################################################################
# Message Definitions
################################################################################
class DozerMsgType(enum.IntEnum):
  BEACON             = 0
  CONNECTION_REQUEST = 1
  DATA               = 2
  ACK                = 3

class DozerMsg(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = True
    self._formats = OrderedDict([
      ('msgType', 'B'),
      ('src', 'H'),
      ('dst', 'H'),
      ('payload', 'c'),
    ])
    super().__init__(msg, raw)

class DozerBeacon(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = True
    self._formats = OrderedDict([
      ('period', 'H'),
      ('distanceToSink', 'H'),
      ('numChildren', 'H'),
      ('slots', 'H'),
    ])
    super().__init__(msg, raw)

class DozerConnectionRequest(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = False
    self._formats = OrderedDict([
      # empty (currently no fields are used)
    ])
    super().__init__(msg, raw)

class DozerAck(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = False
    self._formats = OrderedDict([
      ('nodeId', 'H'),
      ('streamId', 'B'),
    ])
    super().__init__(msg, raw)

# DozerData
class ApplicationMsg(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = False
    self._formats = OrderedDict([
      ('seqNum', 'B'),
      ('text', '12s'),
    ])
    super().__init__(msg, raw)


################################################################################
# State Machine
################################################################################
states=[
  'Init',
  # side towards parent(s)
  'ParentBeacon',      # listen for beacon from parent
  'ParentContention',
  'ParentData',
  'ParentAck',
  'ParentPost',
  # side towards children
  'ChildBeacon',       # send beacon for child
  'ChildContention',
  'ChildData',
  'ChildAck',
  'ChildPost',
]
dozerTransitions = [
  { 'trigger': 'startSource', 'source': 'Init', 'dest': 'ParentBeacon', 'after': ['printState'] },
  { 'trigger': 'startSink', 'source': 'Init', 'dest': 'ChildBeacon', 'after': ['printState'] },
  { 'trigger': 'parentBeaconDone', 'source': 'ParentBeacon', 'dest': 'ParentContention', 'after': ['printState'] },
  { 'trigger': 'parentContentionDone', 'source': 'ParentContention', 'dest': 'ParentData', 'after': ['printState'] },
  { 'trigger': 'parentDataDone', 'source': 'ParentData', 'dest': 'ParentAck', 'after': ['printState'] },
  { 'trigger': 'parentAskDone', 'source': 'ParentAck', 'dest': 'ParentPost', 'after': ['printState'] },
  { 'trigger': 'parentPostDone', 'source': 'ParentPost', 'dest': 'ChildBeacon', 'after': ['printState'] },
  { 'trigger': 'childBeaconDone', 'source': 'ChildBeacon', 'dest': 'ChildContention', 'after': ['printState'] },
  { 'trigger': 'childContentionDone', 'source': 'ChildContention', 'dest': 'ChildData', 'after': ['printState'] },
  { 'trigger': 'childDateDone', 'source': 'ChildData', 'dest': 'ChildAck', 'after': ['printState'] },
  { 'trigger': 'childAckDone', 'source': 'ChildAck', 'dest': 'ChildPost', 'after': ['printState'] },
  { 'trigger': 'childPostDoneSource', 'source': 'ChildPost', 'dest': 'ParentBeacon', 'after': ['printState'] },
  { 'trigger': 'childPostDoneSink', 'source': 'ChildPost', 'dest': 'ChildBeacon', 'after': ['printState'] },
]


################################################################################
# Dozer
################################################################################
class Dozer():
  def __init__(
                self,
                timerId,
                node,
                dataslotLength=1.0,
                period=10,
                queueSize=10,
                maxPotParents=10
              ):

    # initialize
    self.timerId = timerId
    self.node = node
    self.dataSlotLength = dataslotLength # seconds
    self.period = period                 # seconds
    self.queueSize = queueSize           # size of the Rx and Tx queue

    # internal state
    self.sm = Machine(model=self, states=dozerStates, transitions=transitions, initial='Init')


  def _resetStart(self):
    self.rxQueue = []
    self.txQueue = []
    self.isHost = None
    self.postCallback = None
    self.to_Init() # FSM
    self.lastSyncPointLt = None
    self.lastSyncPointNt = None
    self.parent = None # nodeId of parent node
    self.potParents = deque([], maxlen=maxPotParents) # potential parents
    self.children = [] # list of nodeIds of child nodes


  def _print(self, text):
    self.node.print('[Dozer] {}'.format(text))


  ##############################################################################
  # Interface
  ##############################################################################

  def start(self, isHost, postCallback=None):
    self._resetStart()
    self.isHost = isHost
    if self.isHost:
      self.hostNodeId = self.node.nodeId
      self.currHostNodeId = int(self.node.nodeId)
    self._postCallback = postCallback

    # use timer action to trigger callback which includes timestamp
    if self.isHost:
      self.hostStart() # FSM
      self.node.timerStart(timerId=self.timerId, timeout=0)
    else:
      self.clientStart() # FSM
      self.node.timerStart(timerId=self.timerId, timeout=0)

  def stop(self):
    # Stop ongoing radio and timer actions
    self.node.timerStop(timerId=self.timerId)
    self.node.radioReset()

  def sendMsg(self, msg):
    '''Puts a message into the outgoing queue
    Args:
      msg: DozerMsg object to be sent
    Returns:
      True if successful, False otherwise
    '''
    self.node.print('sendMsg')
    if len(self.txQueue) < self.queueSize:
      self.txQueue.append(msg)
      return True
    else:
      self._print('WARNING: txQueue is full!')
      return False

  @property
  def rxQueuePktsWaiting(self):
    return len(self.rxQueue)

  def receiveMsg(self):
    '''Returns the oldest received message in the input queue
    Returns:
      DozerMsg object which has been received & nodeId of sending node
    '''
    if len(self.rxQueue) == 0:
      return None, None
    return self.rxQueue.pop(0)

  @property
  def txQueuePktsWaiting(self):
    return len(self.txQueue)

#   def getTime(self, localTime=None):
#     if self.tsGlobal is None:
#       return None
#     if localTime is None:
#       localTime = self.node.getTs()
#     return self.tsGlobal + (localTime - self.tsLocal)

  ##############################################################################
  # Callbacks
  ##############################################################################

  def rxDoneCallback(self, payload, crc, rssi=None, snr=None):
    pass

  def txDoneCallback(self):
    pass

  def rxTimeoutCallback(self):
    pass

  def timerCallback(self, timerId):
    if timerId == self.timerId:
      self._updateFsm()

  ##############################################################################
  # Helper Functions
  ##############################################################################

  def _updateFsm(self):
    self._print('Dozer updateFsm')

    if self.state == 'Init':
      if self.isHost:
        self.startSink()
      else:
        self.startSource()
    elif self.state == 'ParentBeacon':
      # TODO: listen for beacon, differentiate whether node is connected or not
      self.parentBeaconDone()
    elif self.state == 'ParentContention':
      self.parentContentionDone()
    elif self.state == 'ParentData':
      self.parentDataDone()
    elif self.state == 'ParentAck':
      self.parentAskDone()
    elif self.state == 'ParentPost':
      self.parentPostDone()
    elif self.state == 'ChildBeacon':
      self.childBeaconDone()
    elif self.state == 'ChildContention':
      self.childContentionDone()
    elif self.state == 'ChildData':
      self.childDateDone()
    elif self.state == 'ChildAck':
      self.childAckDone()
    elif self.state == 'ChildPost':
      if self.isHost:
        self.childPostDoneSink()
      else:
        self.childPostDoneSource()
      # TODO: wait for next round start
    else:
      self._print('FSM: Invalid current state: {}'.format(self.state))

#   def sendScheduleFlood(self):
#     # store current time
#     ts = self.node.getTs()
#     self.lastSyncPointLt = ts
#     self.lastSyncPointNt = ts # same since we are the host
#     self._print('sendScheduleFlood: lastSyncPointLt: {}'.format(self.lastSyncPointLt))
#     self._print('registeredNodes: {}'.format(self.registeredNodes))
#
#     slots = copy(self.registeredNodes)
#     if len(self.txQueue):
#       slots.insert(0, self.node.nodeId)
#
#     # prepare msg
#     schedule = LwbSchedule(msg={
#       'hostNodeId': self.node.nodeId,
#       'time': type(self).ts2us(ts),
#       'period': self.period,
#       'nContSlots': 1,
#       'nDataSlots': len(slots),
#       'slots': slots,
#     })
#     self.lastSchedule = schedule
#
#     scheduleMsg = LwbMsg(msg={
#       'msgType': int(LwbMsgType.SCHEDULE),
#       'src': self.node.nodeId,
#       'dst': 0,
#       'payload': schedule.raw,
#     })
#
#     # Send schedule
#     self.primitive.start(
#       initiator = True,
#       payload = scheduleMsg.raw,
#       nTx = self.primitiveConfig.nTx,
#       syncFlood = True,
#     )
#
#     # Setup timer for stopping primitive flood
#     self.node.timerStart(timerId=self.timerId, timeout=self.floodLength)
#
#   def sendContentionFlood(self):
#     self._print('sendContentionFlood')
#
#     # prepare msg
#     contention = LwbStreamReq(msg=OrderedDict([
#       ('nodeId', self.node.nodeId),
#       ('streamId', copy(self.nextStreamId)),
#       ('msgSize', 30),
#       ('nMsgs', 1),
#       ('period', 30),
#     ]))
#     self.nextStreamId += 1
#     contMsg = LwbMsg(msg=OrderedDict([
#       ('msgType', int(LwbMsgType.STREAM_REQ)),
#       ('src', self.node.nodeId),
#       ('dst', self.currHostNodeId),
#       ('payload', contention.raw),
#     ]))
#
#     # Send schedule
#     self.primitive.start(
#       initiator = True,
#       payload = contMsg.raw,
#       nTx = self.primitiveConfig.nTx,
#       syncFlood = False,
#     )
#
#     # Setup timer for stopping primitive flood
#     self.node.timerStart(timerId=self.timerId, timeout=self.floodLength)
#
#   def startListeningFlood(self, nTx=None, duration=None, syncFlood=False):
#     self._print('startListeningFlood')
#     duration = self.floodLength if duration is None else duration
#     ret = self.primitive.start(
#       initiator = False,
#       payload = None,
#       nTx = self.primitiveConfig.nTx if nTx is None else nTx,
#       syncFlood = syncFlood,
#     )
#     self.node.timerStart(timerId=self.timerId, timeout=duration)
#     return ret
#
#   def sendData(self):
#     self._print('sendData')
#
#     if len(self.txQueue):
#       self._print('Data in txQueue available -> sending 1 element')
#       appMsg = self.txQueue.pop(0)
#       if not isinstance(appMsg, ApplicationMsg):
#         raise Exception('Element in txQueue is not of type ApplicationMsg!')
#
#       lwbMsg = LwbMsg(msg=OrderedDict([
#         ('msgType', int(LwbMsgType.DATA)),
#         ('src', self.node.nodeId),
#         ('dst', self.currHostNodeId),
#         ('payload', appMsg.raw),
#       ]))
#
#       self.primitive.start(
#         initiator = True,
#         payload = lwbMsg.raw,
#         nTx = self.primitiveConfig.nTx,
#         syncFlood = False,
#       )
#
#       # Setup timer for stopping primitive flood
#       self.node.timerStart(timerId=self.timerId, timeout=self.floodLength)
#     else: # no msg in txQueue
#       self._print('No data in txQueue available -> skipping data slot')
#       self.finishDataSlot()
#
#   def processScheduleFlood(self):
#     if self.primitive.getRxCnt():
#       # self._print('getRxIndex: {}'.format(self.primitive.getRxIndex()))
#       lwbMsg = LwbMsg(raw=self.primitive.getPayload())
#       # self._print('lwbMsg: {}'.format(lwbMsg))
#       if lwbMsg['msgType'] == int(LwbMsgType.SCHEDULE):
#         self._print('Schedule received!')
#         lwbSchedule = LwbSchedule(raw=lwbMsg['payload'])
#         self._print('lwbSchedule: {}'.format(lwbSchedule))
#         self.hostNodeId = lwbSchedule['hostNodeId']
#         if self.primitive.isTRefUpdated():
#           self.lastSyncPointLt = self.primitive.getTRef()
#           self._print('lastSyncPointLt: {}'.format(self.lastSyncPointLt))
#           self._print('current Ts: {}'.format(self.node.getTs()))
#           self.lastSyncPointNt = type(self).us2ts(lwbSchedule['time'])
#         self.lastSchedule = lwbSchedule
#         self.currHostNodeId = lwbMsg['src']
#         if self.node.nodeId in lwbSchedule['slots']:
#           self._print('Stream registered!')
#           self.streamRegistered = True
#
#         return True
#     return False
#
#   def processContentionFlood(self):
#     if self.primitive.getRxCnt():
#       # self._print('getRxIndex: {}'.format(self.primitive.getRxIndex()))
#       lwbMsg = LwbMsg(raw=self.primitive.getPayload())
#       # self._print('lwbMsg: {}'.format(lwbMsg))
#       if lwbMsg['msgType'] == int(LwbMsgType.STREAM_REQ):
#         self._print('Stream Request received!')
#         lwbStreamreq = LwbStreamReq(raw=lwbMsg['payload'])
#         lwbStreamreqMsg = lwbStreamreq # FIXME: this is a property and is therefore not directly subscriptable
#         self._print('lwbStreamreq: {}'.format(lwbStreamreqMsg))
#         # check if node is not already registered
#         if lwbStreamreqMsg['nodeId'] in self.registeredNodes:
#           self._print('Node {} NOT registered! Node is already registered!'.format(lwbStreamreqMsg['nodeId']))
#           return False
#         # check if there are still data slots available
#         if self.period / self.slotLength - 3 - len(self.registeredNodes) >= 1: # -3: 1 slot for schedule, 1 slot for conention, 1 slot reserved for potential tx from host
#           self._print('Node {} registered!'.format(lwbStreamreqMsg['nodeId']))
#           self.registeredNodes = sorted(list(set(self.registeredNodes).union(set([lwbStreamreqMsg['nodeId']]))))
#         else:
#           self._print('Node {} NOT registered! No more LWB data slots available!'.format(lwbStreamreqMsg['nodeId']))
#         return True
#     return False
#
#   def processDataFlood(self, nodeId):
#     if self.primitive.getRxCnt():
#       # self._print('getRxIndex: {}'.format(self.primitive.getRxIndex()))
#       lwbMsg = LwbMsg(raw=self.primitive.getPayload())
#       # self._print('lwbMsg: {}'.format(lwbMsg))
#       if lwbMsg['msgType'] == int(LwbMsgType.DATA):
#         self._print('Data from node {} received!'.format(lwbMsg['src']))
#         appMsg = ApplicationMsg(raw=lwbMsg['payload'])
#         if len(self.rxQueue) < self.queueSize:
#           self.rxQueue.append((appMsg, nodeId))
#           return True
#         else:
#           self._print('WARNING: rxQueue is full! Received msg discarded!')
#           return False
#     return False
#
#   def finishDataSlot(self):
#     slots = self.lastSchedule['slots']
#     if self.currDataSlotIdx < len(slots) - 1:
#       self.currDataSlotIdx += 1
#       timeout = self.lastSyncPointLt + (self.currDataSlotIdx + 2)*self.slotLength
#       if slots and slots[self.currDataSlotIdx] != self.node.nodeId:
#         timeout -= self.safetyMargin # start earlier if listening
#       self.node.timerStart(timerId=self.timerId, timeout=max(self.node.getTs(), timeout), isRelative=False)
#     else:
#       self.dataDone()
#       self.currDataSlotIdx = 0
#       self.node.timerStart(timerId=self.timerId, timeout=0)
#
#   def bootstrap(self):
#     self.clientBootstrap()
#     self.node.timerStart(timerId=self.timerId,timeout=0)
#
#   def dataSlotRequired(self):
#     # FIXME: reduce probability of collision at startup, by exponential backoff and initial backoff counter value
#     return (not self.streamRegistered)
#
#   @staticmethod
#   def ts2us(ts):
#     return int(ts*1e6)
#
#   @staticmethod
#   def us2ts(us):
#     return float(us)/1e6
#
#   def printState(self):
#     self._print('new state: {}'.format(self.state))
#
################################################################################

class DozerNode(Node):
  def __init__(self, radioConfig, hostNodeId, nodeId, position=(0, 0), startDelay=0):
    super().__init__(radioConfig=radioConfig, position=position, nodeId=nodeId)

    self.dozer = Dozer(timerId=1, node=self)

    self.hostNodeId = hostNodeId
    self.startDelay = startDelay
    self.timerId = 2
    self.dataSeqNum = 0

  def postCallback(self):
    self.print('postCallback')
    # generate app msgs
    appMsg = ApplicationMsg(msg={
      'seqNum': copy(self.dataSeqNum),
      'text':   b'testtesttest',
    })
    self.dozer.sendMsg(appMsg) # add msg to out queue
    self.dataSeqNum += 1

    self.print('rxQueuePktsWaiting: {}'.format(self.dozer.rxQueuePktsWaiting))
    while self.dozer.rxQueuePktsWaiting > 0:
      appMsg, nodeId = self.dozer.receiveMsg()
      self.print('appMsg from node {}: {}'.format(nodeId, appMsg))

  def init(self):
    # delay start of dozer
    self.timerStart(timerId=self.timerId, timeout=self.startDelay)

  def timerCallback(self, timerId):
    self.dozer.timerCallback(timerId)

    if timerId == self.timerId:
      # start dozer
      self.dozer.start(
        isHost=(self.nodeId==self.hostNodeId),
        postCallback=self.postCallback,
      )

  def rxDoneCallback(self, payload, crc, rssi=None, snr=None):
    self.dozer.rxDoneCallback(payload, crc, rssi, snr)

  def txDoneCallback(self):
    self.dozer.txDoneCallback()

  def rxTimeoutCallback(self):
    self.dozer.rxTimeoutCallback()



################################################################################

if __name__ == '__main__':
  pass
