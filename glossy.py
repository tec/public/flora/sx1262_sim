#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""


import numpy as np
from collections import OrderedDict
from struct      import *

import sx1262

from message import Message
from actions import *
from node import Node

################################################################################
# Glossy Message Definitions
################################################################################


class GlossyMsg(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = True
    self._formats          = OrderedDict([('slot', 'B'), ('payload', 'c')])
    super().__init__(msg, raw)

################################################################################
# Glossy
################################################################################


class Glossy():
  def __init__(self, timerId, node):
    # config
    self.timerId     = timerId
    self.node        = node
    self.postRxDelay = 0.02

    self.tRef = -1  # should be initialized only once since it is valid even if tRef has not been updated
    self._initState()

  def _initState(self):
    """Init glossy state (should be called at the beginning of each flood, i.e. in glossy_start)
    """
    # glossy_start args
    self.nTx       = 0
    self.syncFlood = False
    self.initiator = None
    self.payload   = None

    # state
    self.lastRxMsg = None
    self.txMsg     = None

    self.tRefUpdated  = False
    self.txCount      = 0
    self.rxCount      = 0
    self.currSlot     = -1
    self.firstRxIndex = None
    self.slotLen      = 0
    self.toa          = None
    self.running      = False

  ##############################################################################
  # Interface
  ##############################################################################

  def start(self, initiator, payload, nTx, syncFlood):
    if self.running:
      raise Exception('ERROR: Glossy is already running!')

    # init state
    self._initState()
    self.running = True

    # store arguments of current flood
    self.nTx       = nTx
    self.syncFlood = syncFlood
    self.initiator = initiator
    self.payload   = payload if payload is not None else b''

    if self.initiator:
      self.currSlot = 0

      # prepare msg
      self.txMsg = GlossyMsg(msg=OrderedDict([
        ('slot', self.currSlot),
        ('payload', self.payload),
      ]))

      # determine toa (for non-initiator, this is done in rxDone callback)
      self.toa = self.node.getToa(self.txMsg.size)

      # initiate tx via timer action
      self.node.timerStart(timerId=self.timerId, timeout=0)

    else:  # non-initiator
      # listen for rist msg
      self.node.radioRx(timeout=60)

  def stop(self):
    if not self.running:
      raise Exception('ERROR: Glossy is already stopped!')

    self.node.timerStop(timerId=self.timerId)
    self.node.radioReset()
    self.running = False

  def getPayload(self):
    """Return payload of last flood
    """
    if self.lastRxMsg is None:
      return None
    else:
      return b''.join(self.lastRxMsg['payload'])

  def isTRefUpdated(self):
    return self.tRefUpdated

  def getTRef(self):
    return self.tRef

  def getRxCnt(self):
    return self.rxCount

  def getRxIndex(self):
    return self.firstRxIndex

  ##############################################################################
  # Callbacks
  ##############################################################################

  def rxDoneCallback(self, payload, crc, rssi=None, snr=None):
    tmpMsg   = GlossyMsg(raw=payload)
    msgSlot  = tmpMsg['slot']
    self.toa = self.node.getToa(tmpMsg.size)

    if msgSlot > self.currSlot:
      self.rxCount  += 1
      self.lastRxMsg = tmpMsg
      self.currSlot  = msgSlot

      # set tRef for non-initiator
      if self.syncFlood and self.rxCount == 1:
        self.tRefUpdated = True
        self.tRef        = self.node.getTs() - (self.currSlot+1)*self.toa - self.currSlot*self.postRxDelay
      if self.firstRxIndex is None:
        self.firstRxIndex = self.currSlot
      self.node.print('RxDone, payload="{}"'.format(self.lastRxMsg))
      if self.txCount < self.nTx:
        self.node.timerStart(timerId=self.timerId, timeout=self.postRxDelay)
    else:
      # node received a message of the next flood -> stop participation
      pass

  def txDoneCallback(self):
    self.txCount += 1
    if self.txCount < self.nTx:
      self.node.radioRx(timeout=2*self.postRxDelay + self.toa)

  def rxTimeoutCallback(self):
    if self.rxCount > 0 or self.txCount > 0:
      self.currSlot += 1

      # trigger next tx by producing a timer callback
      self.node.timerStart(timerId=self.timerId, timeout=0)
    else:  # nothing received/transmitted yet -> continue listening
      self.node.radioRx(timeout=60)

  def timerCallback(self, timerId):
    if timerId == self.timerId:
      if self.initiator and self.txCount == 0:
        self.currSlot = 0

        # set tRef for the initiator
        if self.syncFlood:
          self.tRefUpdated = True
          self.tRef        = self.node.getTs()
      else:
        self.currSlot += 1

      if self.initiator:
        self.txMsg['slot'] = self.currSlot
        self.node.radioTx(payload=self.txMsg.raw)
      elif self.rxCount:
        self.lastRxMsg['slot'] = self.currSlot
        self.node.radioTx(payload=self.lastRxMsg.raw,)
      else:
        # should never happen
        raise Exception('ERROR: wrong state, this condition should never be reached!')


################################################################################

class SingleGlossyFloodNode(Node):
  def __init__(self, radioConfig, position=(0, 0), initiator=False, nTx=3, floodDuration=0.5, nodeId=None):
    super().__init__(radioConfig=radioConfig, position=position, nodeId=nodeId)

    self.glossy        = Glossy(timerId=0, node=self)
    self.initiator     = initiator
    self.floodDuration = floodDuration
    self.nTx           = nTx

  def init(self):
    # prepare msg
    msg = GlossyMsg(msg=OrderedDict([
      ('slot', 0),
      ('payload', b'hello')
    ]))

    # start glossy flood
    self.glossy.start(
      initiator=self.initiator,
      payload=msg.raw,
      nTx=self.nTx,
      syncFlood=True,
    )

    # prepare stopping of flood
    self.timerStart(timerId=1, timeout=self.floodDuration,)

  def timerCallback(self, timerId):
    self.glossy.timerCallback(timerId)

    if timerId == 1:
      self.glossy.stop()

      if self.glossy.getRxCnt():
        rawPayload = self.glossy.getPayload()
        m          = GlossyMsg(raw=rawPayload)
        self.print('payload: {}'.format(m))
        self.print('isTRefUpdated: {}'.format(self.glossy.isTRefUpdated()))
        self.print('getTRef: {}'.format(self.glossy.getTRef()))
        self.print('getRxCnt: {}'.format(self.glossy.getRxCnt()))
        self.print('getRxIndex: {}'.format(self.glossy.getRxIndex()))
      else:
        self.print('No msg received during glossy flood!')

  def rxDoneCallback(self, payload, crc, rssi=None, snr=None):
    self.glossy.rxDoneCallback(payload, crc, rssi, snr)

  def txDoneCallback(self):
    self.glossy.txDoneCallback()

  def rxTimeoutCallback(self):
    self.glossy.rxTimeoutCallback()

################################################################################


if __name__ == '__main__':
  pass
