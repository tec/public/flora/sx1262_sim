#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""


import os
import itertools
import numpy  as np
import pandas as pd
from collections import OrderedDict

from bokeh.models   import WheelZoomTool, PanTool, BoxZoomTool, ResetTool, HoverTool, CustomJS, SaveTool, ColumnDataSource, DataRange1d
from bokeh.plotting import figure
from bokeh.io       import curdoc, show, output_file, save
from bokeh.events   import DoubleTap
from bokeh.palettes import Dark2_5 as palette

import sx1262

from actions import *
from events  import *

################################################################################


################################################################################

class EnergyPerLsrRoundPlot():
  def __init__(self):
    self.roundBoundaries = list()  # List of time instants when a new round starts

  def loadFromSim(self, sim, radioConfigs, isBoundaryMsg):
    self.sim     = sim
    floraModList = [rc.floraModIdx for rc in radioConfigs]
    minFloraMod  = floraModList[0]
    radioOpList  = ['rx', 'tx']

    # get set of nodes
    self.nodeIdSimList = sorted(list(set([a.nodeIdSim for a in sim.at.values()])))
    self.nodeIdList    = [sim.nodeIdSim2nodeId(nodeIdSim=nodeIdSim) for nodeIdSim in self.nodeIdSimList]

    # init
    energyEvents = OrderedDict()
    for nodeId in self.nodeIdList:
      energyEvents[nodeId] = list()

    # go through all actions with non-zero energy consumption & obtain round boundaries
    for actionId, a in sim.at.items():
      if isinstance(a, RadioTxAction) or isinstance(a, RadioRxAction):
        # round boundary
        if isinstance(a, RadioTxAction) and a.radioConfig.floraModIdx == minFloraMod and isBoundaryMsg(a.payload):
          self.roundBoundaries.append(a.startTime)

        # add energy event
        radioOp = None
        if isinstance(a, RadioRxAction):
          radioOp = 'rx'
          pwr     = sx1262.getRxPower()
        elif isinstance(a, RadioTxAction):
          radioOp = 'tx'
        else:
          raise Exception('Unexpected radio operation!')
        nodeId = self.sim.nodeIdSim2nodeId(a.nodeIdSim)
        energyEvents[nodeId].append((a.startTime, a.endTime, radioOp, a.radioConfig))

    # initialize data structure to collect energy per round values
    energyData = OrderedDict()
    for nodeId in self.nodeIdList:
      for roundIdx in range(len(self.roundBoundaries)+1):
        for rcIdx in range(len(floraModList)):
          for radioOp in radioOpList:
            energyData[nodeId, roundIdx, rcIdx, radioOp] = 0

    # go through energy events and collect energy per round
    for nodeId, eventList in energyEvents.items():
      for startTime, endTime, radioOp, radioConfig in eventList:
        rcIdx = floraModList.index(radioConfig.floraModIdx)
        if radioOp == 'rx':
          pwr = sx1262.getRxPower()
        elif radioOp == 'tx':
          pwr = sx1262.getTxPower(radioConfig.txPower)

        startIdx = np.searchsorted(self.roundBoundaries, startTime, side='right')
        endIdx   = np.searchsorted(self.roundBoundaries, endTime, side='right')
        if startIdx == endIdx:
          energyData[nodeId, startIdx, rcIdx, radioOp] += pwr * (endTime - startTime)
        else:
          # first part
          energyData[nodeId, startIdx, rcIdx, radioOp] += pwr * (self.roundBoundaries[startIdx] - startTime)
          # last part
          energyData[nodeId, endIdx, rcIdx, radioOp] += pwr * (endTime - self.roundBoundaries[endIdx-1])
          # middle parts (if existing)
          for middleIdx in range(startIdx+1, endIdx):
            energyData[nodeId, middleIdx, rcIdx, radioOp] += pwr * (self.roundBoundaries[middleIdx] - self.roundBoundaries[middleIdx-1])

    # create df from collected data
    energyList = []
    for nodeId in self.nodeIdList:
      for roundIdx in range(len(self.roundBoundaries)+1):
        for rcIdx in range(len(floraModList)):
          for radioOp in radioOpList:
            energyList.append({
              'nodeId':   nodeId,
              'roundIdx': roundIdx,
              'rcIdx':    rcIdx,
              'radioOp':  radioOp,
              'energy':   energyData[nodeId, roundIdx, rcIdx, radioOp],
            })
    self.energyDf = pd.DataFrame(energyList)

    self.sim.stats._roundBoundaries     = self.roundBoundaries
    self.sim.stats._energyPerLsrRoundDf = self.energyDf

  def generatePlot(self, outFile):
    p = figure(
        title=None,
        plot_width=300,
        plot_height=300,
        min_border=0,
        # toolbar_location=None
        sizing_mode = 'stretch_both',
        y_range=DataRange1d(start=0),
    )
    # p.add_tools(WheelZoomTool())
    hoverTool = HoverTool(
      tooltips=[
        ("Node", "$name"),
        ("Consumed Energy", "@y{0.000} J"),
        ("Round No.", "@x"),
        ("Time of round start", "@t s"),
      ],
    )
    p.add_tools(hoverTool)

    # add functionality to reset by double-click
    p.js_on_event(DoubleTap, CustomJS(args=dict(p=p), code='p.reset.emit()'))

    # create a color iterator
    colors = itertools.cycle(palette)

    # plot line for all nodes
    self.energyDf.sort_values(by=['nodeId', 'roundIdx'], inplace=True)  # to ensure roundBoundaries match
    for nodeId, gdf in self.energyDf.groupby('nodeId'):
      color = next(colors)
      legend_label = 'Node {}'.format(nodeId)  # make sure to use the same legend name; this will enable the showing/hiding of both glyphs (line and circles) happens together
      name = str(nodeId)
      # NOTE: use of global ColumnDataSource for all lines is not beneficial (except from potentially reducing memory footprint):
      #  1. column labels need to be converted to str because ColumnDataSource only works with str column labels
      #  2. No simple way to use _data values_ in hover tool since every column has a different name (it's possible to display the value corresponding to the current cursor location but that's not what we want)
      xVals, yVals = zip(*[(roundIdx, ggdf.energy.sum()*1e3 )for roundIdx, ggdf in gdf.groupby('roundIdx')])
      source = ColumnDataSource({
        'x': xVals,
        'y': yVals,
        't': [np.nan] + self.roundBoundaries
      })
      p.line(
        x='x',
        y='y',
        line_width=2,
        color=color,
        alpha=0.8,
        legend_label=legend_label,
        name=name,
        source=source,
      )
      p.circle(
        x='x',
        y='y',
        color=color,
        alpha=0.8,
        legend_label=legend_label,
        name=name,
        source=source,
      )

    p.xaxis.axis_label                 = "Round No. [1]"
    p.xaxis.axis_label_text_font_size  = "20pt"
    p.xaxis.major_label_text_font_size = "18pt"

    p.yaxis.axis_label                 = "Consumed Energy [mJ]"
    p.yaxis.major_label_text_font_size = "18pt"
    p.yaxis.axis_label_text_font_size  = "20pt"

    p.legend.location     = "top_left"
    p.legend.click_policy = "hide"

    curdoc().add_root(p)

    # show(p)

    os.makedirs(os.path.split(outFile)[0], exist_ok=True)
    output_file(outFile, title="Energy per LSR Round")
    save(p)

################################################################################


if __name__ == '__main__':
  ep = EnergyPerLsrRoundPlot()
  ep.generatePlot('output/out.html')
