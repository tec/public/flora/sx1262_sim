#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""

import numpy as np
from copy import copy, deepcopy

import sx1262
import scenario_flocklab

from sim    import Sim
from node   import *
from glossy import *
from lwb    import *

################################################################################


################################################################################

if __name__ == '__main__':
  sim = Sim(
    logFileName='sim_log.txt',
    outputDir='./output/output_lwb',
  )
  sim.propagation     = True
  sim.timeDrift       = True
  sim.probabilisticRf = True
  sim.verbose         = True
  sim.debug           = False
  sim.setRandSeed(0)

  radioConfig = sx1262.getFloraConfig(modIdx=6)
  # additional fields required for the simulation
  radioConfig.frequency = 868100000  # in Hz
  radioConfig.txPower   = 8          # in dB

  ##############################################################################
  # FlockLab
  ##############################################################################

  hostNodeFlNodeId = 2

  def genNode(nodeId, nodeIdList, pos):
    return LwbNode(
      position=pos,
      nodeId=nodeId,
      radioConfig=deepcopy(radioConfig),
      hostNodeId=hostNodeFlNodeId,
      nTx=2,
      startDelay=5 if nodeId == hostNodeFlNodeId else 0,
    )
  scenario_flocklab.addNodes(sim, genNode, pathlossMatrixScaling=1.2)
  # scenario_flocklab.addNodes(sim, genNode, pathlossMatrixScaling=1.2, subset=[1, 2, 3, 4, 5])

  ##############################################################################
  sim.plotNodePositions()
  sim.runSimulation(until=2*60 + 2)
  sim.plotTransmissions(outFile='transmissions.html')
  sim.plotEnergy(outFile='energy.html')

  # sim.printEventQueue()
