#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""

import pickle
import numpy   as np
import pandas  as pd
import seaborn as sns
import matplotlib.pyplot as plt
from copy        import copy, deepcopy
from datetime    import datetime, timezone
from collections import Counter, OrderedDict
from pprint      import pprint
import networkx as nx

import sx1262
import scenarios
import scenario_flocklab

from sim       import Sim
from node      import *
from glossy    import *
from lsr       import *
from scenarios import scenarioData
from results_output import ResultsOutput

################################################################################
# General Config
################################################################################
OUTPUT_DIR      = './output/output_lsr'
PICKLE_FILENAME = 'app_lsr.pkl'
CONFIG_FILENAME = 'config.txt'

usePickle = False

################################################################################
# Sim Configs
################################################################################

configs = OrderedDict()

################################################################################

# config for paper: closer look
configs['convergence'] = OrderedDict([
  ('scenarioList', [None]),
  # ('scenarioList', ['rc']),
  ('protocolList', ['lf']),

  ('hostNodeId', 1),                             # ID of the host node

  ('duration', 60*60+2),                        # duration of the simulation (in seconds)
  # ('duration', 100*60+2),                        # duration of the simulation (in seconds)
  ('period', 60),
  ('txPwr', 4),                                  # transmit power of the nodes (in dB)
  ('seed', 0),                                   # seed for random number generator

  ('raiseException', True),

  ('modulations', [5, 10]),
  ('numHops', [5, 5]),
  ('slotLength', 1.2),

  ('nTx', 2),
  ('statsDepth', 5),
  ('selRcIdxPercentile', 40),
  ('deregisteringThreshold', 5*60),              # in seconds
  ('bpsRoundThreshold', 200),                     # in rounds
  ('pathlossMatrixScaling', 1.05),               # only applied for the flocklab scenario
  ('sensitivitySrcDatasheet', False),            # True: use sensitivity values based on datasheet; False: use sensitivity values based on measurements
  ('roundIdxOffsetMode', 'absolute'),            # allowed values: 'relative', 'absolute'
  ('roundIdxOffset', 15),                        # offset in number of rounds for the evaluation of the results;
                                                 #   roundIdxOffsetMode='absolute': directly provide the roundIdxOffset relative to the first round (roundIdx=0), only warning is produced if a node is never registered
                                                 #   roundIdxOffsetMode='relative': roundIdxOffset relative to the first round where all nodes are registered , error is produced if a node is never registered

  # config for output (useful when running using results saved in pickle file)
  ('metricsOutput', ['reliability', 'energy', 'energyNormalized']),  # NOTE: reliability cannot be omitted since otherwise error display does no longer work correctly
  ('skipProtocolsOutput', []),
])
################################################################################

protocolColorMap = {
  'smw':  sns.color_palette('muted')[8],
  'smw1': sns.color_palette('muted')[8],
  'smf':  sns.color_palette('muted')[9],
  'lf':   sns.color_palette('muted')[0],
  'lw':   sns.color_palette('muted')[1],
}

def protocolToText(protocol=None):
  m = {
    'lw':   'LSR without thinning',
    'lh':   'LSR with hop distance thinning',
    'lf':   'LSR with full thinning',
    'sh':   'Single-hop',
    'smw':  'Single modulation (Mod0) without thinning',
    'smh':  'Single modulation (Mod0) with hop distance thinning',
    'smf':  'Single modulation (Mod0) with full thinning',
    'smw1': 'Single modulation (Mod1) without thinning',
  }
  if protocol is None:
    return m
  elif protocol in m:
    return m[protocol]
  else:
    raise Exception('Unknown protocol \'{}\''.format(protocol))


def scenarioToName(scenario):
  if scenario in scenarioData:
    return scenarioData[scenario]['name']
  elif scenario[:3] == 'fl2':
    return 'FlockLab 2 ({})'.format(scenario[3:])
  else:
    raise Exception('Unknown scenario \'{}\''.format(scenario))


def scenarioToAbbrev(scenario):
  if scenario in scenarioData:
    return 'S{}'.format(list(scenarioData.keys()).index(scenario)+1)
  elif scenario[:3] == 'fl2':
    return 'FL2 ({})'.format(scenario[3:])
  else:
    raise Exception('Unknown scenario \'{}\''.format(scenario))


def runSim(config, outputDir=OUTPUT_DIR):
  """Runs the simulation for multiple scenarios and protocols variants
  Args:
    config:    configuration for running the simulations
    outputDir: path to directory for output
  Returns:
    results:   dict containing all eval results of all scenario/protocol combinations
    lastSim:   simulation object of the last scenario/protocol combination
  """
  config['simStartTime'] = datetime.now(timezone.utc).astimezone().isoformat()

  # sanity checks
  for scenario in config['scenarioList']:
    if ('fl2' not in scenario) and (config['hostNodeId'] != 1):
      raise Exception('hostNodeId for non-flocklab scenario ({}) is not 1!'.format(scenario))

  results = OrderedDict()
  for scenario in config['scenarioList']:
    print('===== scenario={} ====='.format(scenario))
    results[scenario] = OrderedDict()

    if scenario[:3] != 'fl2':
      np.random.seed(config['seed'])
      # get path loss matrix for current scenario
      edges = deepcopy(scenarios.scenarioData[scenario]['edges'])
      pathlossMatrix, nodeIdListPlm = scenarios.edgesToPathlossMatrix(
        edges,
        modIdxLongrange=config['modulations'][0],
        modIdxShortrange=config['modulations'][1],
        txPwr=config['txPwr'],
        sensitivitySrcDatasheet=config['sensitivitySrcDatasheet'],
      )

    for protocol in config['protocolList']:
      print('==== protocol={} ===='.format(protocol))

      # determine config based on protocol
      if protocol == 'lw':
        thinningHopDistance       = False
        thinningConnectivityGraph = False
        numHops                   = config['numHops']
        radioConfigsSlice         = slice(None)
      elif protocol == 'lh':
        thinningHopDistance       = True
        thinningConnectivityGraph = False
        numHops                   = config['numHops']
        radioConfigsSlice         = slice(None)
      elif protocol == 'lf':
        thinningHopDistance       = True
        thinningConnectivityGraph = True
        numHops                   = config['numHops']
        radioConfigsSlice         = slice(None)
      elif protocol == 'sh':
        thinningHopDistance       = False
        thinningConnectivityGraph = False
        numHops                   = [1]*len(config['modulations'])
        radioConfigsSlice         = slice(None)
      elif protocol == 'smw':
        thinningHopDistance       = False
        thinningConnectivityGraph = False
        numHops                   = config['numHops']
        radioConfigsSlice         = slice(1)  # stop=1
      elif protocol == 'smh':
        thinningHopDistance       = True
        thinningConnectivityGraph = False
        numHops                   = config['numHops']
        radioConfigsSlice         = slice(1)  # stop=1
      elif protocol == 'smf':
        thinningHopDistance       = True
        thinningConnectivityGraph = True
        numHops                   = config['numHops']
        radioConfigsSlice         = slice(1)  # stop=1
      elif protocol == 'smw1':
        thinningHopDistance       = False
        thinningConnectivityGraph = False
        numHops                   = config['numHops']
        radioConfigsSlice         = slice(1, 2)  # start=1, stop=2
      else:
        raise Exception('Unknown protocol {}!'.format(protocol))

      radioConfigs = []
      for modIdx in config['modulations']:
        rc = sx1262.getFloraConfig(modIdx=modIdx)
        # additional fields required for the simulation
        rc.frequency = 868100000  # in Hz
        rc.txPower   = config['txPwr']
        rc.sensitivitySrcDatasheet = config['sensitivitySrcDatasheet']
        radioConfigs.append(rc)

      sim = Sim(
        logFileName='simLog_{}_{}.txt'.format(scenario, protocol),
        outputDir=outputDir,
      )
      sim.setRadioConfigs(radioConfigs)
      sim.propagation     = True
      sim.timeDrift       = True
      sim.probabilisticRf = True
      sim.verbose         = True
      sim.debug           = False
      sim.setRandSeed(config['seed'])

      ##############################################################################
      # Scenarios
      ##############################################################################

      def genNode(nodeId, nodeIdList, pos):
        return LsrNode(
          radioConfigs=radioConfigs[radioConfigsSlice],
          hostNodeId=config['hostNodeId'],
          nodeId=nodeId,
          position=pos,
          nTx=config['nTx'],
          numHops=numHops,
          slotLength=config['slotLength'],
          period=config['period'],
          streamPeriod=config['period'],
          startDelay=10 if nodeId == config['hostNodeId'] else 0,
          minNodeId=min(nodeIdList),
          maxNodeId=max(nodeIdList),
          thinningHopDistance=thinningHopDistance,
          thinningConnectivityGraph=thinningConnectivityGraph,
          statsDepth=config['statsDepth'],
          selRcIdxPercentile=config['selRcIdxPercentile'],
          deregisteringThreshold=config['deregisteringThreshold'],
          useBps=False, # disable best path selection (exhaustive search)
          bpsRoundThreshold=config['bpsRoundThreshold'],
        )

      if not scenario.startswith('fl2_'):
        scenarios.addNodes(
          sim=sim,
          scenario=scenario,
          genNode=genNode,
          nodeIdListPlm=nodeIdListPlm,
        )
        # set initial pathloss matrix (use path loss matrix generated above to ensure that we use same path loss matrix for all protocols within the same scenario)
        sim.schedulePathlossMatrix(
          pathlossMatrix=deepcopy(pathlossMatrix)*1.0,
          nodeIdList=deepcopy(nodeIdListPlm),
        )


      ##############################################################################
      # Pre-register nodes on LSR host
      ##############################################################################

      hostNodeIdSim = sim.nodeId2nodeIdSim(config['hostNodeId'])

      # register nodes on the host
      for nodeId in sim.getNodeIdList():
        # NOTE: nodeId of host is ignored by registerNode()
        sim.nodes[hostNodeIdSim].lsr.preRegisterNode(nodeId=nodeId, rcIdx=rcIdxSel)

      # activate registration on the nodes
      for nodeId in [n for n in sim.getNodeIdList() if n != config['hostNodeId']]:
        # artificial delay by one round (dataFirstRound=False) to make initial graph distance visible in plot
        sim.nodes[sim.nodeId2nodeIdSim(nodeId)].lsr.preActivateRegistration(rcIdxSel, dataFirstRound=False)

      ##############################################################################
      # Run simulation
      ##############################################################################
      try:
        sim.plotNodePositions()
        sim.runSimulation(until=config['duration'])

        print('=== GENERATING PLOTS ===')
        # only plot for last combination
        if ( (config['scenarioList'].index(scenario) + 1 == len(config['scenarioList']) and config['protocolList'].index(protocol) + 1 == len(config['protocolList'])) or
             (config['evalSel'] == 'closerlook') ):
          sim.plotTransmissions(outFile='transmissions_{}_{}.html'.format(scenario, protocol))
          sim.plotEnergy(outFile='energy_accumulated_{}_{}.html'.format(scenario, protocol))

        # the following plot is required for every config as it is used to collect stats for the evaluation
        Lsr.plotEnergyPerRound(sim=sim, radioConfigs=radioConfigs[radioConfigsSlice], outFile='energyPerRound_{}_{}.html'.format(scenario, protocol))
        print('=== GENERATING PLOTS FINISHED ===')

        print('=== STARTING EVALUATION ===')
        res = OrderedDict()
        s   = sim.stats
        res['registrationHost']      = s.evalRegistrationHost()
        res['registrationClients']   = s.evalRegistrationClients()
        if thinningConnectivityGraph:
          res['graphInfo']           = s.evalGraphInfo()
          res['thinningStatus']      = s.evalThinningStatus()
          res['selectedPaths']       = s.evalSelectedPaths()
        res['reliability']           = s.evalReliability()
        res['energy']                = s.evalEnergy()
        # res['energyNormalized']      = s.evalEnergyNormalized() # disabled as not used in paper and overhead is not negligible
        results[scenario][protocol] = res
        print('=== EVALUATION FINISHED ===')
      except Exception as e:
        if config['raiseException']:
          raise
        print('Sim lead to an error: {}'.format(e))
        results[scenario][protocol] = e

  config['simEndTime'] = datetime.now(timezone.utc).astimezone().isoformat()

  return results, sim


def getSimResults(config, outputDir=OUTPUT_DIR, storeLastSim=True):
  os.makedirs(outputDir, exist_ok=True)

  # try to load from pickle file
  pklHasSameConfig = False
  if usePickle and os.path.isfile(os.path.join(outputDir, PICKLE_FILENAME)):
    with open(os.path.join(outputDir, PICKLE_FILENAME), 'rb') as f:
      pklDict = pickle.load(f)

    configPkl          = deepcopy(pklDict['config'])
    configTmp          = deepcopy(config)
    doNotCompareFields = ['metricsOutput', 'skipProtocolsOutput', 'simStartTime', 'simEndTime', 'raiseException']
    for field in doNotCompareFields:
      if field in configPkl:
        del configPkl[field]
      if field in configTmp:
        del configTmp[field]

    if configPkl == configTmp:
      pklHasSameConfig = True
    else:
      print('Difference in config:')
      for k in list(set(list(configPkl.keys())).union(set(list(configTmp.keys())))):
        if configPkl[k] != configTmp[k]:
          print('{}: {} != {}'.format(k, configPkl[k], configTmp[k]))

  lastSim = None
  if pklHasSameConfig:
    # load from pickle
    print('Loading results from pickle file...')
    configFinal = pklDict['config']

    for field in doNotCompareFields:
      if field not in configFinal:
        configFinal[field] = config[field]

    results = pklDict['results']
    if 'lastSim' in pklDict:
      lastSim = pklDict['lastSim']

  else:
    # run sim
    print('Running simulation...')
    results, lastSim = runSim(config, outputDir)
    configFinal = config

    # store config and results in pickle file
    if storeLastSim:
      del lastSim._logFile  # circumvent pickle limitation: cannot pickle '_io.TextIOWrapper' object
      pickleDict = {'config': configFinal, 'results': results, 'lastSim': lastSim}
    else:
      pickleDict = {'config': configFinal, 'results': results}

    with open(os.path.join(outputDir, PICKLE_FILENAME), 'wb') as f:
      pickle.dump(pickleDict, f)

    with open(os.path.join(outputDir, CONFIG_FILENAME), 'w') as f:
      pprint(configFinal, stream=f)
    # f.write(repr(configFinal))

  return results, configFinal, lastSim

################################################################################


if __name__ == '__main__':
  evalSel = 'convergence'

  # sanity check
  if evalSel not in configs:
    raise Exception('Eval selection \'{}\' is not defined!'.format(evalSel))

  outputDir = os.path.join(OUTPUT_DIR, evalSel)

  rcIdxSel = 0

  seedList = list(range(10))
  # seedList = list(range(3))
  # seedList = [0]
  # seedList = [0, 1]
  allScenarios = [
    'co_2h_4c',
    'co_3h_4c',
    'co_4h_4c',
    'co_2h_8c',
    'co_3h_8c',
    'co_4h_8c',
  ]

  resList = []
  for (seed, scenario) in itertools.product(seedList, allScenarios):
    print(f'== seed={seed}, scenario={scenario} ==')
    config = deepcopy(configs[evalSel])
    config['evalSel'] = evalSel
    config.move_to_end('evalSel', last=False)  # make evalSel element the first element
    config['seed'] = seed
    config['scenarioList'] = [scenario]

    # NOTE: config is extended with start and end time
    results, config, lastSim = getSimResults(
      config=config,
      outputDir=outputDir,
    )
    plt.close('all')

    ro = ResultsOutput(
      results=results,
      config=config,
      protocolToText=protocolToText,
      # scenarioToName=scenarioToAbbrev,
      scenarioToName=scenarioToName,
      outputDir=os.path.join(outputDir, 'results'),
      protocolColorMap=protocolColorMap,
      plotTitle=True,
    )

    # output for all scenarios and all protocols
    # ro.printStats()
    # ro.generateHtmlTable()
    # ro.generateViolinHtml()

    # output for single scenario and single protocol
    s = config['scenarioList'][0]
    p = config['protocolList'][0]
    # ro.plotActiveModulation(scenario=s, protocol=p)
    # ro.plotReliability(scenario=s, protocol=p)
    # ro.plotEnergy(scenario=s, protocol=p)
    # ro.generateGraphPlots(scenario=s, protocol=p, highlightUsedEdges=True)
    refGraphEdges = scenarios.scenarioData[s]['edges']
    graphInfoDf = ro.getGraphDistance(scenario=s, protocol=p, refGraphEdges=refGraphEdges, config=config)
    resList.append({
      'config': config,
      'graphInfoDf': graphInfoDf,
    })

  # compute "lower bound" for convergence (NOTE: this does not actually represent a lower bound; convergence is possible before the calculated value in case not all measurements are necessary to determine the full shortest connectivity graph (-> "best case of the worst case"))
  bestCaseConvTimes = OrderedDict()
  for scenario in allScenarios:
    g = nx.Graph()
    g.add_edges_from(scenarios.scenarioData[scenario]['edges'])
    gMap = OrderedDict() # maps hop distance to list of nodes with corresponding hop distance
    for nodeId in g.nodes():
      hd = nx.shortest_path_length(g, source=config['hostNodeId'], target=nodeId)
      if hd not in gMap:
        gMap[hd] = []
      gMap[hd].append(nodeId)
    # sort gMap
    gMap = dict(sorted(gMap.items(), key=lambda e: e[0]))

    roundsHopDistance = config['statsDepth'] # NOTE: this assumes that there is no data packet transmission in the first round (i.e. preActivateRegistration() is called with dataFirstRound=False), otherwise value should be decremented by 1 round
    roundsConnectivity = np.sum([len(gMap[k]) for k in list(gMap.keys())[1:-1]]) if len(gMap) > 2 else 0
    bestCaseConvTimes[scenario] = roundsHopDistance + roundsConnectivity


  # plot results stored in resList
  fig, ax   = plt.subplots(figsize=(9, 4))
  # colors    = itertools.cycle(ro._colorsList)
  # markers   = itertools.cycle(ro._markerList)
  lineStyles = [
    ('dotted',                (0, (1, 1))),
    ('dashed',                (0, (5, 5))),
    ('dashdotted',            (0, (3, 5, 1, 5))),
    ('dashdotdotted',         (0, (3, 5, 1, 5, 1, 5))),
    ('densely dotted',        (0, (1, 1))),
    ('densely dashed',        (0, (5, 1))),
    ('densely dashdotted',    (0, (3, 1, 1, 1))),
    ('densely dashdotdotted', (0, (3, 1, 1, 1, 1, 1)))]
  for res in resList:
    df = res['graphInfoDf']
    df = df[df.rcIdx==rcIdxSel]
    scenario = res['config']['scenarioList'][0]
    idx = allScenarios.index(scenario)
    ax.plot(
      df.roundIdx,
      df.graphDistance,
      marker=ro._markerList[idx],
      markerfacecolor='None',
      linestyle=lineStyles[idx][1],
      label='{}'.format(scenarioToName(scenario)),
      c=ro._colorsList[idx],
    )
  ax.set_xlabel('Round No.')
  ax.set_ylabel('Graph distance [1]')
  ax.set_ylim(bottom=min(0, ax.get_ylim()[0]))
  # add line for computed "lower bound" for convergence
  for idx, scenario in enumerate(allScenarios):
    ax.axvline(
      x=bestCaseConvTimes[scenario],
      # marker=ro._markerList[idx],
      markerfacecolor='None',
      linestyle=lineStyles[idx][1],
      label='{}'.format(scenarioToName(scenario)),
      c=ro._colorsList[idx],
      linewidth = 2,
    )
  # only one legend element for each scenario
  handles, labels = ax.get_legend_handles_labels()
  newLabels, newHandles = [], []
  for handle, label in zip(handles, labels):
    if label not in newLabels:
      newLabels.append(label)
      newHandles.append(handle)
  ax.legend(newHandles, newLabels, loc='center left', bbox_to_anchor=(1, 0.5))
  ax.get_legend().get_frame().set_boxstyle('square')
  plt.savefig(os.path.join(os.path.join(outputDir, 'results'), 'graphDistance_hops_nodes_{}.pdf'.format(p)), bbox_inches="tight")
