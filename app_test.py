#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""

import numpy as np
from copy import copy, deepcopy

# sys.path.append('./sx1262')
import sx1262

from sim  import Sim
from node_examples import *

################################################################################


class TimerStopTestNode(Node):
  def __init__(self, radioConfig, position=(0, 0), nodeId=None):
    super().__init__(radioConfig=radioConfig, position=position, nodeId=nodeId)

  def init(self):
    self.timerStart(timerId=1, timeout=1.0)
    self.timerStart(timerId=2, timeout=250)

  def timerCallback(self, timerId):
    self.print('timerCallback (ts={:.3f}, timerId={})'.format(self.getTs(), timerId))
    if timerId == 1:
      self.timerStart(timerId=1, timeout=1.0)
    elif timerId == 2:
      self.timerStop(timerId=1)


class RadioResetTestNode(Node):
  def __init__(self, radioConfig, position=(0, 0), name=None):
    super().__init__(radioConfig=radioConfig, position=position, nodeId=name)

  def init(self):
    # self.radioTx(payload='Test from N{}'.format(self._nodeIdSim).encode('utf-8'))
    self.radioRx(timeout=2.0)
    self.timerStart(timerId=1, timeout=0.1)

  def timerCallback(self, timerId):
    if timerId == 1:
      self.radioReset()

################################################################################


if __name__ == '__main__':
  sim = Sim(
    logFileName='simLog.txt',
    outputDir='./output/output_test',
  )
  sim.propagation     = True
  sim.timeDrift       = True
  sim.probabilisticRf = True
  sim.verbose         = True
  sim.debug           = True
  sim.setRandSeed(0)

  radioConfig = sx1262.LoraConfig()
  radioConfig.bw            = 125000
  radioConfig.sf            = 9
  radioConfig.phyPl         = None
  radioConfig.cr            = 1
  radioConfig.ih            = False
  radioConfig.lowDataRate   = False
  radioConfig.crc           = True
  radioConfig.nPreambleSyms = 12
  # additional fields required for the simulation
  radioConfig.frequency = 868100000
  radioConfig.txPower   = 8  # in dB

  # rc = deepcopy(radioConfig)
  # sim.addNode( TimerStopTestNode(
  #     position=(0, 0),
  #     radioConfig=rc,
  # ))

  # rc = deepcopy(radioConfig)
  # sim.addNode( RadioResetTestNode(
  #     position=(0, 0),
  #     radioConfig=rc,
  # ))

  # rc = deepcopy(radioConfig)
  # # rc.frequency = 868300000
  # sim.addNode( SingleTxNode(
  #     position=(0, 0),
  #     radioConfig=rc,
  # ))
  #
  # rc = deepcopy(radioConfig)
  # # rc.frequency = 868300000
  # sim.addNode( SingleTxNode(
  #     position=(10, 0),
  #     radioConfig=rc,
  # ))

  rc = deepcopy(radioConfig)
  rc.frequency = 868300000
  sim.addNode(
    PeriodicTxNode(
      position=(0, 0),
      radioConfig=rc,
      period=2.0,
      startOffset=1.0,
    )
  )

  # rc = deepcopy(radioConfig)
  # rc.frequency = 868300000
  # sim.addNode(
  #   PeriodicTxNode(
  #     position=(0, 500),
  #     radioConfig=rc,
  #     period=2.0,
  #     startOffset=1.0,
  #   )
  # )

  # sim.addNode( SingleTxNode(
  #     position=(9, 0),
  #     radioConfig=radioConfig,
  # ))

  # sim.addNode( SingleRxNode(
  #   position=(5, 2),
  #   radioConfig=radioConfig,
  #   duration=2.0,
  # ))

  rc = deepcopy(radioConfig)
  rc.frequency = 868300000
  sim.addNode( ContinuousRxNode(
    position=(1500, 250),
    radioConfig=rc,
  ))

  # sim.addNode( ContinuousRxNode(
  #   position=(5, 1000),
  #   radioConfig=radioConfig,
  # ))

  sim.calcPathlossmatrixFromPositions()

  sim.plotNodePositions()
  sim.runSimulation(until=10.0)
  sim.plotTransmissions(outFile='transmissions.html')

  # sim.printEventQueue()
