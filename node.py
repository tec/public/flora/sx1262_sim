#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""


from collections import OrderedDict
from struct      import *

import sx1262

from actions import *

################################################################################
# Node
################################################################################


class Node():
  def __init__(self, radioConfig, position=(0, 0), nodeId=None):
    self.radioConfig = radioConfig
    self.x           = position[0]  # x-coordinate (in metres)
    self.y           = position[1]  # y-coordinate (in metres)
    self._nodeIdSim  = None
    self.nodeId      = nodeId
    self._sim        = None         # pointer to sim obj

  def init(self):
    """ TODO: Should be implemented
    """
    return []

  def setSim(self, sim):
    self._sim = sim

  def setNodeIdSim(self, nodeIdSim):
    self._nodeIdSim = nodeIdSim
    if self.nodeId is None:
      self.nodeId = nodeIdSim

  def print(self, text):
    """Print with node ID
    """
    nodeHeader = '[{:>3}]'.format(self.nodeId)
    self._sim.printInfo('{} {}'.format(nodeHeader, text))

  def setPosition(self, x, y):
    self.x = x
    self.y = y

  def getToa(self, phyPl):
    # FIXME: remove function from Node class -> directly get toa from radioConfig obj
    if self.radioConfig is None:
      return None

    tmpPhyPl               = self.radioConfig.phyPl
    self.radioConfig.phyPl = phyPl
    toa                    = self.radioConfig.timeOnAir
    self.radioConfig.phyPl = tmpPhyPl
    return toa

  def getTs(self):
    return self._sim.simTime2nodeTime(nodeIdSim=self._nodeIdSim)

  # Functions to add actions ###################################################

  def timerStart(self, timerId, timeout, isRelative=True):
    if not isRelative:
      timeout = timeout - self.getTs()
    # ensure timer event is not in the past
    if timeout < 0:
      raise Exception('Timeout must be in the future!')
    self._sim.addActions(
      actionList = [TimerStartAction(timerId=timerId, timeout=timeout)],
      nodeIdSim  = self._nodeIdSim,
    )

  def timerStop(self, timerId):
    self._sim.addActions(
      actionList = [TimerStopAction(timerId=timerId)],
      nodeIdSim  = self._nodeIdSim,
    )

  def radioRx(self, timeout, radioConfig=None):
    self._sim.addActions(
      actionList = [RadioRxAction(timeout=timeout, radioConfig=radioConfig)],
      nodeIdSim  = self._nodeIdSim,
    )

  def radioTx(self, payload, radioConfig=None):
    self._sim.addActions(
      actionList = [RadioTxAction(payload=payload, radioConfig=radioConfig)],
      nodeIdSim  = self._nodeIdSim,
    )

  def radioReset(self):
    self._sim.addActions(
      actionList = [RadioResetAction()],
      nodeIdSim  = self._nodeIdSim,
    )

  # Callback Functions #########################################################

  def timerCallback(self, timerId):
    """" TODO: Should be implemented
    """
    raise Exception('This is a stub callback function. It should not be called directly!')

  def txDoneCallback(self):
    """ TODO: Should be implemented
    """
    raise Exception('This is a stub callback function. It should not be called directly!')

  def txTimeoutCallback(self):
    """ TODO: Should be implemented
    """
    raise Exception('This is a stub callback function. It should not be called directly!')

  def rxDoneCallback(self, payload, crc, rssi=None, snr=None):
    """ TODO: Should be implemented
    """
    raise Exception('This is a stub callback function. It should not be called directly!')

  def rxTimeoutCallback(self):
    """ TODO: Should be implemented
    """
    raise Exception('This is a stub callback function. It should not be called directly!')


################################################################################


if __name__ == '__main__':
  pass
