#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""


import os
import itertools
import numpy    as np
import pandas   as pd
import networkx as nx
from copy        import copy, deepcopy
from collections import Counter, OrderedDict

# construct html with python
from dominate.tags import *
from dominate.util import raw

# plots
import matplotlib.pyplot  as plt
import matplotlib.patches as patches
import seaborn            as sns
from bokeh         import plotting
from bokeh.layouts import column, row, gridplot
from bokeh.models  import ColumnDataSource, LabelSet, Div, Range1d, DataRange1d
import holoviews as hv
hv.extension('bokeh')
from holoviews import opts

from lsr       import ThinningState
from graphplot import GraphPlot

################################################################################
PLOT_HEIGHT = 600


################################################################################
# ResultsOutput class
################################################################################

class ResultsOutput(object):
  def __init__(self, results, config, protocolToText, scenarioToName, outputDir, protocolColorMap, plotTitle=False, nodeColorsList=None, markerList=None):
    self._results          = results
    self._config           = config
    self._protocolToText   = protocolToText
    self._scenarioToName   = scenarioToName
    self._outputDir        = outputDir
    os.makedirs(self._outputDir, exist_ok=True)
    self._protocolColorMap = protocolColorMap
    self._plotTitle        = plotTitle
    if nodeColorsList is None:
      self._colorsList = plt.cm.gist_rainbow(np.linspace(0, 1, 8))
      self._colorsList = self._colorsList[[6, 2, 4, 0, 3, 7, 5, 1]]  # shuffle colors

      # self._colorsList = plt.cm.tab10.colors[:3]
    else:
      self._colorsList = nodeColorsList
    if markerList is None:
      self._markerList = ['.', 'x', '1', '+', 'v', '4', '<', 's']
    else:
      self._markerList = markerList

  ##############################################################################
  # Helper Functions
  ##############################################################################

  def getFirstRoundIdxRegistered(self, nodeId, scenario, protocol):
    """Determine first roundIdx where node is registered
    """
    df = self._results[scenario][protocol]['registrationHost']
    return df[(df.nodeId == nodeId) & (pd.notna(df.rcIdx))].roundIdx.min()

  def getRoundIdxAllNodesRegistered(self, scenario, protocol):
    """Determine smallest roundIdx such that all nodes could get registered at least once in previous or current round
    """
    df         = self._results[scenario][protocol]['registrationHost']
    nodeIdList = sorted(df.nodeId.unique())

    firstRoundIdxRegisteredList = []
    notRegisteredNodeList       = []

    for nodeId in nodeIdList:
      frir = self.getFirstRoundIdxRegistered(nodeId, scenario, protocol)
      firstRoundIdxRegisteredList.append(frir)
      if frir is None:
        notRegisteredNodeList.append(nodeId)

    ret = None
    if len(notRegisteredNodeList):
      outText = 'Nodes {} are never registered!'.format(notRegisteredNodeList)
      if self._config['roundIdxOffsetMode'] == 'relative':
        raise Exception(outText)
      else:
        print(outText)
    else:
      ret = np.max(firstRoundIdxRegisteredList)

    return ret

  def getRoundIdxOffset(self, scenario, protocol):
    roundIdxAllRegistered = self.getRoundIdxAllNodesRegistered(scenario, protocol)
    roundIdxOffset        = None
    if   self._config['roundIdxOffsetMode'] == 'absolute':
      roundIdxOffset = self._config['roundIdxOffset']
    elif self._config['roundIdxOffsetMode'] == 'relative':
      roundIdxOffset = roundIdxAllRegistered + self._config['roundIdxOffset']
    else:
      raise Exception('Unknown roundIdxOffsetMode \'{}\''.format(self._config['roundIdxOffsetMode']))

    return roundIdxOffset

  def getProtocolColor(self, protocol):
    if protocol not in self._protocolColorMap:
      raise Exception('Tried to lookup color for protocol \'{}\' which is not in protocolColorMap {}!'.format(protocol, self._protocolColorMap))
    return self._protocolColorMap[protocol]

  def sanityCheck(self, scenario, protocol=None):
    if scenario not in self._results:
      raise Exception('Requested scenario \'{}\' is not contained in the results!'.format(scenario))
    if (protocol is not None) and (protocol not in self._results[scenario]):
      raise Exception('Requested protocol \'{}\' is not contained in the results for scenario \'{}\'!'.format(protocol, scenario))

  ##############################################################################
  # Output Methods: Multiple Scenarios & Multiple Protocols
  ##############################################################################

  def printStats(self):
    """Print summary of all results (multiple scenarios/protocol combinations)
    """
    print('===== printStats =====')

    for scenario in self._config['scenarioList']:
      print('==== scenario={} ===='.format(scenario))
      for protocol in self._config['protocolList']:
        print('==== protocol={} ===='.format(protocol))
        res = self._results[scenario][protocol]

        # print stats
        if isinstance(res, Exception):
          print('Error: {}'.format(res))
        else:
          print('== steady-state (rcIdx) ==')  # limited to rcIdx, does not consider thinning
          roundIdxOffset = self.getRoundIdxOffset(scenario, protocol)
          print('roundIdxOffset: {}'.format(roundIdxOffset))
          regClientsDf = res['registrationClients']
          regClientsDf = regClientsDf[regClientsDf.roundIdx >= roundIdxOffset]

          for nodeId, gdf in regClientsDf.groupby('nodeId'):
            c      = Counter(gdf.rcIdx)
            steady = False
            if len(c) == 1 and pd.notna(list(c.keys())[0]):
              steady = True
            print('nodeId={}: {}'.format(nodeId, 'steady' if steady else 'NOT steady'))

          if 'thinningStatus' in res:
            thinningStatusDf = res['thinningStatus']
            thinningStatusDf = thinningStatusDf[thinningStatusDf.roundIdx >= roundIdxOffset]
            for rcIdx, gdf in thinningStatusDf.groupby('rcIdx'):
              print('thinningStatus [rcIdx={}]: {}'.format(rcIdx, Counter([ThinningState(e).name for e in gdf.status.to_list()])))

          print('== RegisteredNodes (host) ==')  # no filtering based on roundIdxOffset
          for nodeId, gdf in res['registrationHost'].groupby('nodeId'):
            print('nodeId={}: {}'.format(nodeId, Counter(gdf.rcIdx)))

          print('== RegisteredNodes (clients) ==')  # no filtering based on roundIdxOffset
          for nodeId, gdf in res['registrationClients'].groupby('nodeId'):
            print('nodeId={}: {}'.format(nodeId, Counter(gdf.rcIdx)))

          print('== Reliability ==')
          relDf = res['reliability']
          relDf = relDf[relDf.roundIdx >= roundIdxOffset]
          # reliabilityVals = np.asarray([gdf.rxPkts.sum()/gdf.txPkts.sum() for nodeId, gdf in relDf.groupby('nodeId')])*100
          reliabilityVals = np.asarray([gdf.rxPkts.sum()/len(gdf.txPkts) for nodeId, gdf in relDf.groupby('nodeId')])*100
          print('reliability (min/median/max): {:0.1f}%/{:0.1f}%/{:0.1f}%'.format(np.min(reliabilityVals), np.median(reliabilityVals), np.max(reliabilityVals)))
          # stats per node (aggregated over all rounds)

          print('== Reliability ==')
          for nodeId, gdf in relDf.groupby('nodeId'):
            idxFirstRoundTx     = gdf[gdf.txPkts != 0].roundIdx.min()  # should be equal to roundIdxRegistered = self.getFirstRoundIdxRegistered(nodeId)
            roundsWithoutDataTx = len(gdf[gdf.txPkts == 0])  # assumption: node always has messages in tx queue
            with np.errstate(divide='ignore', invalid='ignore'):
              reliabilityNode = np.divide(np.sum(gdf.rxPkts), np.sum(gdf.txPkts))  # use divide to allow for division by 0
            print('Node {}: {}/{} ({:0.1f}%) (minRoundIdx={}, roundsWithoutDataTx={})'.format(nodeId, np.sum(gdf.rxPkts), np.sum(gdf.txPkts), reliabilityNode*100, idxFirstRoundTx, roundsWithoutDataTx))

          print('== Energy ==')
          eDf   = res['energy']
          eDf   = eDf[eDf.roundIdx >= roundIdxOffset]
          dfAgg = eDf.groupby(['nodeId', 'roundIdx'], as_index=False).energy.agg('sum')
          # energyVals = np.asarray([gdf.energy.median() for nodeId, gdf in dfAgg.groupby('nodeId')])*1e3
          energyVals = np.concatenate([gdf.energy.to_numpy() for nodeId, gdf in dfAgg.groupby('nodeId')])*1e3
          print('energy (min/median/max): {:0.0f}mJ/{:0.0f}mJ/{:0.0f}mJ'.format(np.min(energyVals), np.median(energyVals), np.max(energyVals)))

          if 'energyNormalized' in res:
            print('== Energy normalized ==')
            eNormDf = res['energyNormalized']
            eNormDf = eNormDf[eNormDf.roundIdx >= roundIdxOffset]
            print('energyNormalized (per round): {:0.1f}'.format(eNormDf.energyNormalized.median()))

  def generateHtmlTable(self):
    print('===== generateHtmlTable =====')
    htmlStyleBlock = '''
      .tg  {border-collapse:collapse;border-spacing:0;}
      .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
        overflow:hidden;padding:10px 5px;word-break:normal;}
      .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
        font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
      .tg .tg-bgctb{background-color:#efefef;border-color:inherit;font-weight:bold;text-align:center;vertical-align:top}
      .tg .tg-bgct{background-color:#efefef;border-color:inherit;text-align:center;vertical-align:top}
      .tg .tg-bglmb{background-color:#efefef;border-color:inherit;font-weight:bold;text-align:left;vertical-align:middle}
      .tg .tg-cm{border-color:inherit;text-align:center;vertical-align:middle}
      p, ul, li  {font-family:Arial, sans-serif;font-size:20px;font-weight:normal;}
      @media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}
    '''
    config = self._config

    h = html()
    with h.add(head()):
        meta(charset='UTF-8')
        style(raw(htmlStyleBlock))
        title('Stats')
    with h.add(body()).add(div(cls="tg-wrap")):
        with table(cls="tg").add(tbody()):
            with tr():
                td(raw("&nbsp;"), cls="tg-cm", colspan="2", rowspan="2")
                for s in config['scenarioList']:
                  td(self._scenarioToName(s), cls="tg-bgctb", colspan="3")
            with tr():
                for s in config['scenarioList']:
                  td('min/10th', cls="tg-bgct")
                  td('med/avg',  cls="tg-bgct")
                  td('max/90th', cls="tg-bgct")
            for protocol in config['protocolList']:
              if protocol in config['skipProtocolsOutput']:
                continue
              with tr():
                td(raw('{} [{}]'.format(self._protocolToText(protocol), protocol)), cls="tg-bglmb", rowspan="{}".format(len(config['metricsOutput'])))
                if 'reliability' in config['metricsOutput']:
                  td(raw('Reliability [%]'), cls="tg-bglmb")
                  for scenario in config['scenarioList']:
                    res = self._results[scenario][protocol]
                    if isinstance(res, Exception):
                      td(raw('Error: <br>{}'.format(res)), cls="tg-cm", colspan="3", rowspan="{}".format(len(config['metricsOutput'])))
                    else:
                      roundIdxOffset  = self.getRoundIdxOffset(scenario, protocol)
                      relDf           = res['reliability']
                      relDf           = relDf[relDf.roundIdx >= roundIdxOffset]
                      reliabilityVals = np.asarray([gdf.rxPkts.sum()/len(gdf.txPkts) for nodeId, gdf in relDf.groupby('nodeId')])*100
                      td('{:0.1f}'.format(np.min(reliabilityVals)),    cls="tg-cm")
                      td('{:0.1f}'.format(np.median(reliabilityVals)), cls="tg-cm")
                      td('{:0.1f}'.format(np.max(reliabilityVals)),    cls="tg-cm")
              if 'energy' in config['metricsOutput']:
                with tr():
                    td('Energy per round (per node) [mJ]', cls="tg-bglmb")
                    for scenario in config['scenarioList']:
                      res = self._results[scenario][protocol]
                      if not isinstance(res, Exception):
                        roundIdxOffset = self.getRoundIdxOffset(scenario, protocol)
                        eDf            = res['energy']
                        eDf            = eDf[eDf.roundIdx >= roundIdxOffset]
                        dfAgg          = eDf.groupby(['nodeId', 'roundIdx'], as_index=False).energy.agg('sum')
                        energyVals     = np.concatenate([gdf.energy.to_numpy() for nodeId, gdf in dfAgg.groupby('nodeId')])*1e3
                        # td('{:0.0f}'.format(np.min(energyVals)), cls="tg-cm")
                        # td('{:0.0f}'.format(np.median(energyVals)), cls="tg-cm")
                        # td('{:0.0f}'.format(np.max(energyVals)), cls="tg-cm")
                        td('{:0.0f}'.format(np.percentile(energyVals, 10)), cls="tg-cm")
                        td('{:0.0f}'.format(np.mean(energyVals)),           cls="tg-cm")
                        td('{:0.0f}'.format(np.percentile(energyVals, 90)), cls="tg-cm")
              if ('energyNormalized' in config['metricsOutput']) and ('energyNormalized' in res):
                with tr():
                    td('Normalized energy per round (all nodes)', cls="tg-bglmb")
                    for scenario in config['scenarioList']:
                      res     = self._results[scenario][protocol]
                      eNormDf = res['energyNormalized']
                      eNormDf = eNormDf[eNormDf.roundIdx >= roundIdxOffset]
                      if not isinstance(res, Exception):
                        td('{:0.1f}'.format(eNormDf.energyNormalized.median()), cls="tg-cm", colspan="3")
              # if 'latency' in metrics:
              #   with tr(): # NOTE: don't forget to adapt rowspan of protocol td when re-enabling!
              #       td('Latency', cls="tg-bglmb")
              #       for scenario in config['scenarioList']:
              #         td('', cls="tg-cm") # min
              #         td('', cls="tg-cm") # median
              #         td('', cls="tg-cm") # max

        with p('Config:'):
          with ul():
            for k, v in config.items():
              li('{}={}'.format(k, v))
        for scenario in config['scenarioList']:
          for protocol in config['protocolList']:
            res = self._results[scenario][protocol]
            with p('rcCounts (scenario={}, protocol={})'.format(scenario, protocol)):
              with ul():
                if isinstance(res, Exception):
                  li('Error: {}'.format(res))
                else:
                  for nodeId, gdf in res['registrationHost'].groupby('nodeId'):
                    li('nodeId={}: {}'.format(nodeId, Counter(gdf.rcIdx)))

    htmlPath = os.path.join(self._outputDir, 'stats.html')
    with open(htmlPath, "w") as fp:
       fp.write(h.render())

  def generateViolinHtml(self):
    print('===== generateViolinHtml =====')
    np.seterr('warn')  # prevent "FloatingPointError: underflow encountered in exp" which occur in certain cases when plotting violin plots
    # dummyCat = []
    # dummyVals = []
    # for k in ['a', 'b', 'c']:
    #   for i in range(50):
    #     dummyCat.append(k)
    #     dummyVals.append(np.random.uniform())
    #     # dummyVals.append(np.random.randint(10))

    # try:
    plotList = []
    for scenario in self._config['scenarioList']:
      print('Plotting violin html plot for: {}'.format(self._scenarioToName(scenario)))
      plotList.append(Div(text='<b>{} ({})</b>'.format(self._scenarioToName(scenario), scenario), style={'font-size': '200%'}))
      reliabilityProtoList = []
      reliabilityValsList  = []
      energyProtoList      = []
      energyValsList       = []
      for protocol in self._config['protocolList']:
        res = self._results[scenario][protocol]
        if isinstance(res, Exception):
          reliabilityProtoList += [protocol]
          reliabilityValsList  += [np.nan]  # dummy entry to indicate that there is no data for this scenario/protocol combination
          energyProtoList      += [protocol]
          energyValsList       += [np.nan]  # dummy entry to indicate that there is no data for this scenario/protocol combination
        else:
          roundIdxOffset = self.getRoundIdxOffset(scenario, protocol)
          relDf          = res['reliability']
          relDf          = relDf[relDf.roundIdx >= roundIdxOffset]
          # reliabilityVals = np.asarray([gdf.rxPkts.sum()/gdf.txPkts.sum() for nodeId, gdf in relDf.groupby('nodeId')])*100
          reliabilityVals       = np.asarray([gdf.rxPkts.sum()/len(gdf.txPkts) for nodeId, gdf in relDf.groupby('nodeId')])*100
          reliabilityProtoList += [protocol]*len(reliabilityVals)
          reliabilityValsList  += list(reliabilityVals)

          eDf   = res['energy']
          eDf   = eDf[eDf.roundIdx >= roundIdxOffset]
          dfAgg = eDf.groupby(['nodeId', 'roundIdx'], as_index=False).energy.agg('sum')
          # energyVals = np.asarray([gdf.energy.median() for nodeId, gdf in dfAgg.groupby('nodeId')])*1e3
          energyVals       = np.concatenate([gdf.energy.to_numpy() for nodeId, gdf in dfAgg.groupby('nodeId')])*1e3
          energyProtoList += [protocol]*len(energyVals)
          energyValsList  += list(energyVals)

      # violin plots
      # violin plot requires list keys and values (list of list does not work out-of-the-box)
      hpReliability = hv.Violin((reliabilityProtoList, reliabilityValsList), kdims='category', vdims='values')
      hpReliability.opts(title='Reliability')
      hpReliability.opts(opts.Violin(cut=0.1, bandwidth=0.1))
      # hpReliability.opts(opts.Violin(inner='stick', cut=0.1, bandwidth=0.0)) # attention: if distribution consists of a single value (e.g. 100%) then the violin is not visible
      pReliability = hv.render(hpReliability)
      pReliability.plot_height      = PLOT_HEIGHT
      pReliability.plot_width       = PLOT_HEIGHT
      pReliability.xaxis.axis_label = 'Protocol'
      pReliability.yaxis.axis_label = 'Reliability [%]'
      pReliability.y_range          = Range1d(0, 100)
      hpEnergy = hv.Violin((energyProtoList, energyValsList), kdims='category', vdims='values')
      hpEnergy.opts(title='Energy Per Round')
      hpEnergy.opts(opts.Violin(cut=0.1, bandwidth=0.1))
      # hpEnergy.opts(opts.Violin(inner='stick', cut=0.1, bandwidth=0.1)) # attention: if distribution consists of a single value (e.g. 100%) then the violin is not visible
      pEnergy = hv.render(hpEnergy)
      pEnergy.plot_height      = PLOT_HEIGHT
      pEnergy.plot_width       = PLOT_HEIGHT
      pEnergy.xaxis.axis_label = 'Protocol'
      pEnergy.yaxis.axis_label = 'Energy per round (per node) [mJ]'
      pEnergy.y_range          = DataRange1d(start=0)
      # legend Div
      lines = ['{}: {}'.format(proto, self._protocolToText(proto)) for proto in self._protocolToText().keys()]
      legendDiv = Div(text='<div style="display: table-cell; vertical-align: bottom; height: {}px; width: {}px">'.format(PLOT_HEIGHT, PLOT_HEIGHT)+'<br>'.join(lines)+'</div>', style={'width': '{}px'.format(PLOT_HEIGHT)})
      plotList.append(row(pReliability, pEnergy, legendDiv))

    # plot all plots to a single HTML file
    htmlPath = os.path.join(self._outputDir, 'stats_violin.html')
    plotting.output_file(htmlPath, title='Stats Violin')
    # infoDiv = Div(text='DummyDivText')
    # plotting.save(column([infoDiv, gridplot(plotList, ncols=2)]))
    # plotting.save(column([gridplot(plotList, ncols=2)]))
    plotting.save(column(plotList))
    # except FloatingPointError as e: # This happens with certain constellation; problem is hv.render(); could not narrow down exact reason for it
    #   print('WARNING: Generating violin plots failed: {}'.format(e))
    np.seterr('raise')

  def generateAggPlots(self, subsetProtocols=None):
    """Plots of aggregated values (multiple scenarios and multiple protocols are allowed)
    Args:
      subsetProtocols: dict for mapping of protocol identifier to protocol labels which should be used in the plots; if not specified, all protocols with identifier as labels will be used.
    """
    print('===== generateAggPlots =====')

    # sanitation of arguments
    if subsetProtocols is None:
      subsetProtocols = OrderedDict(zip(self._config['protocolList'], self._config['protocolList']))
    if set(list(subsetProtocols.keys())).difference(set(self._config['protocolList'])):
      raise Exception('Provided subsetProtocols contains protocols which are not contained in results of simulation run!')

    np.seterr('warn')  # prevent "FloatingPointError: underflow encountered in exp" which occur in certain cases when plotting violin plots

    # collect values
    reliabilityList      = []
    energyList           = []
    energyNormalizedList = []
    for protocol in self._config['protocolList']:
      for scenario in self._config['scenarioList']:
        res = self._results[scenario][protocol]
        if isinstance(res, Exception):
          reliabilityList.append({'protocol': protocol, 'scenario': scenario, 'reliability': np.nan})  # dummy entry to indicate that there is no data for this scenario/protocol combination
          energyList.append({'protocol': protocol, 'scenario': scenario, 'energy': np.nan})  # dummy entry to indicate that there is no data for this scenario/protocol combination
          if 'energyNormalized' in res:
            energyNormalizedList.append({'protocol': protocol, 'scenario': scenario, 'energyNormalized': np.nan})  # dummy entry to indicate that there is no data for this scenario/protocol combination
        else:
          roundIdxOffset = self.getRoundIdxOffset(scenario, protocol)
          relDf          = res['reliability']
          relDf          = relDf[relDf.roundIdx >= roundIdxOffset]
          # reliabilityVals = np.asarray([gdf.rxPkts.sum()/gdf.txPkts.sum() for nodeId, gdf in relDf.groupby('nodeId')])*100
          reliabilityVals = np.asarray([gdf.rxPkts.sum()/len(gdf.txPkts) for nodeId, gdf in relDf.groupby('nodeId')])*100
          for relVal in reliabilityVals:
            reliabilityList.append({'protocol': protocol, 'scenario': scenario, 'reliability': relVal})

          eDf   = res['energy']
          eDf   = eDf[eDf.roundIdx >= roundIdxOffset]
          dfAgg = eDf.groupby(['nodeId', 'roundIdx'], as_index=False).energy.agg('sum')
          # energyVals = np.asarray([gdf.energy.median() for nodeId, gdf in dfAgg.groupby('nodeId')])*1e3
          energyVals = np.concatenate([gdf.energy.to_numpy() for nodeId, gdf in dfAgg.groupby('nodeId')])*1e3
          for eVal in energyVals:
            energyList.append({'protocol': protocol, 'scenario': scenario, 'energy': eVal})
          if 'energyNormalized' in res:
            enDf = res['energyNormalized']
            enDf = enDf[enDf.roundIdx >= roundIdxOffset]
            for enVal in enDf.energyNormalized.to_numpy():
              energyNormalizedList.append({'protocol': protocol, 'scenario': scenario, 'energyNormalized': enVal})

    with sns.axes_style("whitegrid"):  # use this context manager (with ..:) instead of sns.set_theme, as it messed up appearance of other plots
      medianprops = dict(linestyle='--', linewidth=1.5, color='firebrick')
      palette     = [self.getProtocolColor(p) for p in subsetProtocols.keys()]

      print('Plotting plot for reliability')
      reliabilityDf = pd.DataFrame(reliabilityList)
      df            = reliabilityDf[np.isin(reliabilityDf.protocol, list(subsetProtocols.keys()))]
      fig, ax       = plt.subplots(figsize=(12, 6))
      sns.violinplot(x="scenario", y="reliability", hue="protocol", hue_order=subsetProtocols, data=reliabilityDf, palette=palette, cut=0., saturation=1.0)
      ax.set_xlabel('Scenario')
      ax.set_ylabel('Reliability [%]')
      plt.savefig(os.path.join(self._outputDir, 'agg_violin_reliability.pdf'), bbox_inches="tight")

      # boxplot
      fig, ax = plt.subplots(figsize=(6., 4))
      sns.boxplot(x="scenario", y="reliability", hue="protocol", hue_order=subsetProtocols, data=df, palette=palette, saturation=1.0, whis=[0, 100], medianprops=medianprops)
      adjust_box_widths(fig, fac=0.9)
      ax.set_xlabel('Scenario')
      ax.set_ylabel('Reliability [%]')
      ax.get_legend().get_frame().set_boxstyle('square')
      ax.get_legend().set_title(None)
      # ax.get_legend().get_title().set_text('Protocol')
      for text in ax.get_legend().texts:
        if text.get_text()[0] == 'l':
          text.set_weight('bold')
        text.set_text(subsetProtocols[text.get_text()])
      xtickLabels = [e.get_text().upper() for e in ax.get_xticklabels()]
      # xtickLabels = [self._scenarioToName(e.get_text()) for e in ax.get_xticklabels()]
      ax.set_xticklabels(xtickLabels)
      # ax.set_xticklabels(xtickLabels, rotation=45)
      plt.savefig(os.path.join(self._outputDir, 'agg_boxplot_reliability.pdf'), bbox_inches="tight")

      print('Plotting plot for energy')
      energyDf = pd.DataFrame(energyList)
      df       = energyDf[np.isin(energyDf.protocol, list(subsetProtocols.keys()))]
      # violin plot
      fig, ax = plt.subplots(figsize=(12, 6))
      sns.violinplot(x="scenario", y="energy", hue="protocol", hue_order=subsetProtocols, data=df, palette=palette, cut=0., saturation=1.0)
      ax.set_ylim((0, 300))
      ax.set_xlabel('Scenario')
      ax.set_ylabel('Energy per round [mJ]')
      plt.savefig(os.path.join(self._outputDir, 'agg_violin_energy.pdf'), bbox_inches="tight")

      # boxplot
      fig, ax = plt.subplots(figsize=(6., 4))
      sns.boxplot(
        x="scenario",
        y="energy",
        hue="protocol",
        hue_order=subsetProtocols,
        data=df,
        palette=palette,
        saturation=1.0,
        whis=[10, 90],  # use percentiles instead of 1.5*IQR; latter would cover 99.3 percent of data if it would be distributed according to a normal distribution, but here we potentially have special distributions where all samples from 1 out of 8 nodes are significantly larger than the rest; with percentiles this is visible, with 1.5*IQR this is not visible
        showfliers=False,
        # showfliers=True,
        medianprops=medianprops
      )
      adjust_box_widths(fig, fac=0.9)
      # # manually set y-range (only needed if plotting of outliers is enabled)
      # dfAgg = energyDf.groupby(['scenario', 'protocol'], as_index=False).energy.agg(lambda x: np.percentile(x, q = 99.3))
      # ax.set_ylim(0, 1.05*dfAgg.energy.max())
      ax.set_ylim(bottom=0)
      ax.set_xlabel('Scenario')
      ax.set_ylabel('Energy per round [mJ]')
      ax.get_legend().get_frame().set_boxstyle('square')
      ax.get_legend().set_title(None)
      # ax.get_legend().get_title().set_text('Protocol')
      for text in ax.get_legend().texts:
        if text.get_text()[0] == 'l':
          text.set_weight('bold')
        text.set_text(subsetProtocols[text.get_text()])
      # xtickLabels = [self._scenarioToName(e.get_text()) for e in ax.get_xticklabels()]
      xtickLabels = [e.get_text().upper() for e in ax.get_xticklabels()]
      ax.set_xticklabels(xtickLabels)
      # ax.set_xticklabels(xtickLabels, rotation=45)
      plt.savefig(os.path.join(self._outputDir, 'agg_boxplot_energy.pdf'), bbox_inches="tight")

      if 'energyNormalized' in res:
        print('Plotting plot for energy normalized')
        energyNormalizedDf = pd.DataFrame(energyNormalizedList)
        df                 = energyNormalizedDf[np.isin(energyNormalizedDf.protocol, list(subsetProtocols.keys()))]
        fig, ax            = plt.subplots(figsize=(12, 6))
        sns.violinplot(x="scenario", y="energyNormalized", hue="protocol", hue_order=subsetProtocols, data=energyNormalizedDf, palette=palette, cut=0., saturation=1.0)
        ax.set_ylim((0, 30))
        ax.set_xlabel('Scenario')
        ax.set_ylabel('Energy normalized [1]')
        plt.savefig(os.path.join(self._outputDir, 'agg_violin_energy_normalized.pdf'), bbox_inches="tight")

    np.seterr('raise')

  ##############################################################################
  # Output Methods: Single Scenario & Multiple Protocols
  ##############################################################################

  def plotEnergySingleScenario(self, scenario, subsetProtocols=None):
    """Plots of values per node (single scenarios and multiple protocols are allowed)
    Args:
      scenario:
      subsetProtocols: dict for mapping of protocol identifier to protocol labels which should be used in the plots; if not specified, all protocols with identifier as labels will be used.
    """
    print('===== plotSingleScenario =====')

    # sanitation of arguments
    self.sanityCheck(scenario)
    if subsetProtocols is None:
      subsetProtocols = OrderedDict(zip(self._config['protocolList'], self._config['protocolList']))
    if set(list(subsetProtocols.keys())).difference(set(self._config['protocolList'])):
      raise Exception('Provided subsetProtocols contains protocols which are not contained in the results!')

    resScenario = self._results[scenario]

    np.seterr('warn')  # prevent "FloatingPointError: underflow encountered in exp" which occur in certain cases when plotting violin plots

    # collect values
    energyList = []
    for protocol in self._config['protocolList']:
      res            = deepcopy(resScenario[protocol])
      roundIdxOffset = self.getRoundIdxOffset(scenario, protocol)

      eDf    = res['energy']
      eDf    = eDf[eDf.roundIdx >= roundIdxOffset]
      eAggDf = eDf.groupby(['nodeId', 'roundIdx'], as_index=False).energy.agg('sum')
      eAggDf['protocol'] = protocol
      energyList.append(eAggDf)

    # generate plots
    with sns.axes_style("whitegrid"):  # use this context manager (with ..:) instead of sns.set_theme, as it messed up appearance of other plots
      medianprops = dict(linestyle='--', linewidth=1.5, color='firebrick')
      palette     = [self.getProtocolColor(p) for p in subsetProtocols.keys()]

      print('Plotting plot for energy')
      energyDf           = pd.concat(energyList)
      energyDf['energy'] = energyDf.energy * 1e3  # convert J to mJ
      df                 = energyDf[np.isin(energyDf.protocol, list(subsetProtocols.keys()))]
      # boxplot
      fig, ax = plt.subplots(figsize=(6.3, 3.5))
      sns.boxplot(x="nodeId", y="energy", hue="protocol", hue_order=subsetProtocols, data=df, palette=palette, saturation=1.0, showfliers=False, medianprops=medianprops)
      adjust_box_widths(fig, fac=0.9)

      # # manually set y-range (only needed if plotting of outliers is enabled)
      # dfAgg = energyDf.groupby(['protocol'], as_index=False).energy.agg(lambda x: np.percentile(x, q = 99.3))
      # ax.set_ylim(0, 1.05*dfAgg.energy.max())
      ax.set_xlabel('Node ID')
      ax.set_ylabel('Energy per round [mJ]')
      ax.get_legend().get_title().set_text('Protocol')
      ax.get_legend().get_frame().set_boxstyle('square')
      for text in ax.get_legend().texts:
        if text.get_text()[0] == 'l':
          text.set_weight('bold')
        text.set_text(subsetProtocols[text.get_text()])
      plt.savefig(os.path.join(self._outputDir, 'singleScenario_boxplot_energy_{}.pdf'.format(scenario)), bbox_inches="tight")

      # barplot
      fig, ax = plt.subplots(figsize=(6.3, 3.5))
      sns.barplot(x="nodeId", y="energy", hue="protocol", hue_order=subsetProtocols, data=df, palette=palette, saturation=1.0, edgecolor='black')
      # adjust_box_widths(fig, fac=0.9)
      ax.set_xlabel('Node ID')
      ax.set_ylabel('Energy per round [mJ]')
      # ax.set_yscale('log')
      ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
      ax.get_legend().get_frame().set_boxstyle('square')
      ax.get_legend().get_title().set_text('Protocol')
      ax.get_legend().get_frame().set_boxstyle('square')
      for text in ax.get_legend().texts:
        if text.get_text()[0] == 'l':
          text.set_weight('bold')
        text.set_text(subsetProtocols[text.get_text()])
      # hatching
      hatch = []
      for protocol in subsetProtocols:
        if protocol == 'smf':
          hatch.append('//')
        else:
          hatch.append('')
      hatchRep = np.repeat(hatch, len(ax.patches)/len(hatch))
      for p, h in zip(ax.patches, hatchRep):
        p.set_hatch(h)
      for p, h in zip(ax.get_legend().get_patches(), hatch):
        p.set_hatch(h)
      plt.savefig(os.path.join(self._outputDir, 'singleScenario_barplot_energy_{}.pdf'.format(scenario)), bbox_inches="tight")
      # IDEA: horizontal bars to indicate max value (max mean) of each protocol
      np.seterr('raise')

  ##############################################################################
  # Output Methods: Single Scenario & Single Protocol
  ##############################################################################

  def plotActiveModulation(self, scenario, protocol):
    print('===== plotActiveModulation =====')

    self.sanityCheck(scenario, protocol)
    res            = self._results[scenario][protocol]
    roundIdxOffset = self.getRoundIdxOffset(scenario, protocol)

    # active modulation (aka registration) (view from host & from clients)
    for label in ['host', 'clients']:
      regDf = deepcopy(res['registration{}'.format(label.capitalize())])
      regDf.replace({'rcIdx': pd.NaT}, -1, inplace=True)  # rcIdx==-1 means node is not registered
      maxOffset = 0.4
      nodeIds   = sorted(regDf.nodeId.unique())
      fig, ax   = plt.subplots(figsize=(9, 4))
      colors    = itertools.cycle(self._colorsList)
      markers   = itertools.cycle(self._markerList)

      for nodeId, gdf in regDf.groupby('nodeId'):
        corr    = 0.5 if (len(nodeIds) % 2 == 0) else 0  # for even number of nodes shift is too far, this corrects it
        yOffset = ((len(nodeIds)/2 - corr) - nodeIds.index(nodeId))/len(nodeIds)*maxOffset*0.85
        ax.plot(
          gdf.roundIdx.to_numpy(),
          gdf.rcIdx.to_numpy() + yOffset,
          marker='.',
          # marker=next(markers),
          # markerfacecolor='None',
          label='Node {}'.format(nodeId),
          c=next(colors),
        )

      for i in range(regDf.rcIdx.max() + 3):
        ax.add_patch(patches.Rectangle(
          (-1, maxOffset/2 + i - 2),  # position of left lower corner
          regDf.roundIdx.max() + 2,   # width
          1 - maxOffset,  # height
          edgecolor=None,
          facecolor='grey',
          alpha=0.1,
          fill=True
        ))

      ax.set_xlabel('Round No.')
      ax.set_ylabel('Registration')
      modList = sorted(regDf.rcIdx.unique())[1:]
      ax.set_yticks([-1] + modList)
      ax.set_yticklabels(['Not reg.'] + ['Mod{}'.format(e) for e in modList])
      ax.set_xlim(-1, regDf.roundIdx.max()+1)
      ax.set_ylim(-1 - 1.2*maxOffset/2, regDf.rcIdx.max() + 1.2*maxOffset/2)
      if self._plotTitle:
        ax.set_title('Active modulation ({})'.format(label.capitalize()))
      ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
      ax.get_legend().get_frame().set_boxstyle('square')
      plt.savefig(os.path.join(self._outputDir, 'activeModulation_{}_{}_{}.pdf'.format(label, scenario, protocol)), bbox_inches="tight")

  def plotReliability(self, scenario, protocol, rx=True, tx=True, comb=True, testbedDf=None):
    print('===== plotReliability =====')

    self.sanityCheck(scenario, protocol)
    res              = self._results[scenario][protocol]
    roundIdxOffset   = self.getRoundIdxOffset(scenario, protocol)
    subsetRelMetrics = [label for label, val in zip(['rx', 'tx', 'comb'], [rx, tx, comb]) if val]
    if testbedDf is not None and len(subsetRelMetrics) != 1:
      raise Exception('Plotting testbed data is only supported using a single reliability metric (subsetRelMetrics: {})'.format(subsetRelMetrics))

    # prepare reliability data
    reliabilityDf = res['reliability']
    # filter out rounds according to roundIdxOffset
    reliabilityDf = reliabilityDf[reliabilityDf.roundIdx >= roundIdxOffset]
    # ensure that we have the same number of rounds for all nodes
    if np.sum(np.diff([len(gdf.txPkts) for nodeId, gdf in reliabilityDf.groupby('nodeId')])) != 0:
      raise Exception('Number of rounds differs for different nodeIds (sim results)!')
    # determine reliability values and construct long-form dataset
    tmpList = []
    for nodeId, gdf in reliabilityDf.groupby('nodeId'):
      tmpList.append({'src': 'sim', 'nodeId': nodeId, 'relMetric': 'rx',   'reliability': gdf.rxPkts.sum()/gdf.txPkts.sum()})
      tmpList.append({'src': 'sim', 'nodeId': nodeId, 'relMetric': 'tx',   'reliability': gdf.txPkts.sum()/len(gdf.txPkts)})  # assumption: target is to have exactly 1 slot per round
      tmpList.append({'src': 'sim', 'nodeId': nodeId, 'relMetric': 'comb', 'reliability': gdf.rxPkts.sum()/len(gdf.txPkts)})  # assumption: target is to have exactly 1 slot per round
    if testbedDf is not None:
      # ensure that we have the same number of rounds for all nodes
      if np.sum(np.diff([len(gdf.txPkts) for nodeId, gdf in testbedDf.groupby('nodeId')])) != 0:
        raise Exception('Number of rounds differs for different nodeIds (testbedDf)!')
      for nodeId, gdf in testbedDf.groupby('nodeId'):
        tmpList.append({'src': 'testbed', 'nodeId': nodeId, 'relMetric': 'rx',   'reliability': gdf.rxPkts.sum()/gdf.txPkts.sum()})
        tmpList.append({'src': 'testbed', 'nodeId': nodeId, 'relMetric': 'tx',   'reliability': gdf.txPkts.sum()/len(gdf.txPkts)})  # assumption: target is to have exactly 1 slot per round
        tmpList.append({'src': 'testbed', 'nodeId': nodeId, 'relMetric': 'comb', 'reliability': gdf.rxPkts.sum()/len(gdf.txPkts)})  # assumption: target is to have exactly 1 slot per round
    combDf                = pd.DataFrame(tmpList)
    combDf['reliability'] = combDf.reliability * 100.  # convert to percent

    # reliability (bar plot, seaborn)
    with sns.axes_style("whitegrid"):  # use this context manager (with ..:) instead of sns.set_theme, as it messed up appearance of other plots
      fig, ax = plt.subplots(figsize=(6, 3))
      if testbedDf is None:  # plot different reliability metrics (sim only)
        sns.barplot(x='nodeId', y='reliability', hue='relMetric', hue_order=subsetRelMetrics, data=combDf, palette='muted', saturation=1.0, edgecolor='black')
        if len(subsetRelMetrics) == 1:
          ax.get_legend().remove()
        else:
          ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
          ax.get_legend().get_frame().set_boxstyle('square')
          ax.get_legend().set_title(None)
      else:  # plot different sources (sim and testbed; single reliability metric only)
        combDf = combDf[combDf.relMetric == subsetRelMetrics[0]]  # reduce dataframe to a single metric
        sns.barplot(x='nodeId', y='reliability', hue='src', hue_order=['sim', 'testbed'], data=combDf, palette='muted', saturation=1.0, edgecolor='black')
        # ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        ax.legend(loc='lower right')
        ax.get_legend().get_frame().set_boxstyle('square')
        ax.get_legend().set_title(None)
        for text in ax.get_legend().texts:
          if   text.get_text() == 'sim':
            text.set_text('Simulation')
          elif text.get_text() == 'testbed':
            text.set_text('Testbed')
      ax.set_xlabel('Node ID')
      ax.set_ylabel('Reliability [%]')
      ax.set_ylim(0, 100)
      plt.savefig(os.path.join(self._outputDir, 'reliability_{}_{}.pdf'.format(scenario, protocol)), bbox_inches="tight")

    # stats values for text in paper
    if testbedDf is not None:
      combSimDf = combDf[combDf.src == 'sim'].copy()
      combSimDf.sort_values(by=['nodeId'], inplace=True)
      combTestbedDf = combDf[combDf.src == 'testbed'].copy()
      combTestbedDf.sort_values(by=['nodeId'], inplace=True)
      if combSimDf.nodeId.to_list() != combTestbedDf.nodeId.to_list():
        raise Exception('Node lists differ! sim: {}, testbed: {}'.format(combSimDf.nodeId.to_list(), combTestbedDf.nodeId.to_list()))
      ratioList = combTestbedDf.reliability.to_numpy() / combSimDf.reliability.to_numpy()
      print('reliability testbed / reliability sim: {:0.3f}'.format(np.mean(ratioList)))

    # reliability over time (line plot; comb only)
    reliabilityDf = res['reliability']
    # # filter out rounds according to roundIdxOffset
    # reliabilityDf = reliabilityDf[reliabilityDf.roundIdx >= roundIdxOffset]
    nodeIds   = sorted(reliabilityDf.nodeId.unique())
    fig, ax   = plt.subplots(figsize=(9, 4))
    colors    = itertools.cycle(self._colorsList)
    markers   = itertools.cycle(self._markerList)
    maxOffset = 0.4
    for nodeId, gdf in reliabilityDf.groupby('nodeId'):
      yOffset = ((len(nodeIds)/2) - nodeIds.index(nodeId))/len(nodeIds)*maxOffset*0.85
      ax.plot(
        gdf.roundIdx,
        gdf.rxPkts + yOffset,
        marker=next(markers),
        markerfacecolor='None',
        label='Node {}'.format(nodeId),
        c=next(colors),
      )
    ax.set_xlabel('Round No.')
    ax.set_ylabel('Number of received pkts [1]')
    ax.set_yticks(sorted(reliabilityDf.rxPkts.unique()))
    if self._plotTitle:
      ax.set_title('Reliability over time')
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.savefig(os.path.join(self._outputDir, 'reliabilityOverTime_{}_{}.pdf'.format(scenario, protocol)), bbox_inches="tight")

    # reliability over time binary (patch plot; comb only)
    reliabilityDf = res['reliability']
    # # filter out rounds according to roundIdxOffset
    # reliabilityDf = reliabilityDf[reliabilityDf.roundIdx >= roundIdxOffset]
    nodeIds   = sorted(reliabilityDf.nodeId.unique())
    fig, ax   = plt.subplots(figsize=(9, 3))
    height    = 0.7
    vInterval = 1.0
    width     = 1.0
    grouped   = reliabilityDf.groupby('nodeId')

    for nodeId in nodeIds:
      gdf = grouped.get_group(nodeId)
      # edgecolor = next(colors)
      for roundIdx, rxPkts in zip(gdf.roundIdx, gdf.rxPkts):
        yOffset   = (len(nodeIds) - 1) - nodeIds.index(nodeId) - height/2.
        fillColor = 'grey' if rxPkts < 1 else 'white'
        ax.add_patch(patches.Rectangle(
          (roundIdx - width/2., yOffset),  # position of left lower corner
          width,   # width
          height,  # height
          edgecolor='black',
          facecolor=fillColor,
          alpha=1.,
          fill=True,
        ))
    ax.set_xlabel('Round No.')
    ax.set_ylabel('Node ID')
    ax.set_yticks(range(len(nodeIds)))
    ax.set_yticklabels(reversed(nodeIds))
    ax.set_xlim(0 - width/2., (reliabilityDf.roundIdx.max() + 1 + 0.5)*width)
    ax.set_ylim(0 - vInterval/2., (len(nodeIds) - 1 + 0.5)*vInterval)
    if self._plotTitle:
      ax.set_title('Reliability over time')
    plt.savefig(os.path.join(self._outputDir, 'reliabilityOverTimeBinary_{}_{}.pdf'.format(scenario, protocol)), bbox_inches="tight")

  def plotEnergy(self, scenario, protocol, roundIdxLimit=None, testbedDf=None):
    """Plot energy for single scenario and single protocol
    Args:
      scenario:
      protocol:
      roundIdxLimit: if not None, plot energy over time only in interval [0, roundIdxLimit]
      testbedDf:
    """
    print('===== plotEnergy =====')

    self.sanityCheck(scenario, protocol)
    res            = self._results[scenario][protocol]
    roundIdxOffset = self.getRoundIdxOffset(scenario, protocol)

    # energy over time
    eDf     = res['energy']
    nodeIds = sorted(eDf.nodeId.unique())
    eAggDf  = eDf.groupby(['nodeId', 'roundIdx'], as_index=False).energy.agg('sum')
    if roundIdxLimit is not None:
      eAggDf = eAggDf[eAggDf.roundIdx <= roundIdxLimit]
    with sns.axes_style("whitegrid"):
      fig, ax = plt.subplots(figsize=(6, 4))
      colors  = itertools.cycle(self._colorsList)
      markers = itertools.cycle(self._markerList)
      for nodeId, gdf in eAggDf.groupby('nodeId'):
        ax.plot(
          gdf.roundIdx.to_numpy(),
          gdf.energy.to_numpy()*1e3,
          marker=next(markers),
          markerfacecolor='None',
          label='Node {}'.format(nodeId),
          c=next(colors),
        )
      # ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
      ax.legend()
      ax.get_legend().get_frame().set_boxstyle('square')
      ax.set_xlabel('Round No.')
      ax.set_ylabel('Energy per round [mJ]')
      if roundIdxLimit is not None:
        ax.set_xlim(0, roundIdxLimit+1)
      ax.set_ylim(bottom=0)
      # ax.set_ylim(0, 250)
      plt.savefig(os.path.join(self._outputDir, 'energyOverTime_{}_{}.pdf'.format(scenario, protocol)), bbox_inches="tight")

    # energy per node (barplot)
    energyDf = res['energy']
    energyDf = deepcopy(energyDf[energyDf.roundIdx >= roundIdxOffset])  # filter out rounds according to roundIdxOffset
    dfAgg    = energyDf.groupby(['nodeId', 'roundIdx'], as_index=False).energy.agg('sum')
    dfAgg['src'] = 'sim'
    if testbedDf is None:
      combDf = dfAgg
    else:
      testbedDf['src'] = 'testbed'
      combDf = pd.concat( (dfAgg, testbedDf) )
    combDf['energy'] = combDf.energy * 1e3  # convert J to mJ
    np.seterr('warn')  # prevent "FloatingPointError: underflow encountered in exp" which occur in certain cases when plotting violin plots
    # try:
    with sns.axes_style("whitegrid"):  # use this context manager (with ..:) instead of sns.set_theme, as it messed up appearance of other plots
      fig, ax = plt.subplots(figsize=(6, 3))
      ax      = sns.barplot(x='nodeId', y='energy', hue='src', data=combDf, estimator=np.mean, palette='muted', saturation=1.0, edgecolor='black')
      ax.set_ylim(bottom=0.)
      ax.set_xlabel('Node ID')
      ax.set_ylabel('Energy per round [mJ]')
      ax.legend(loc='lower right')
      ax.get_legend().get_frame().set_boxstyle('square')
      ax.get_legend().set_title(None)
      for text in ax.get_legend().texts:
        if   text.get_text() == 'sim':
          text.set_text('Simulation')
        elif text.get_text() == 'testbed':
          text.set_text('Testbed')
      plt.savefig(os.path.join(self._outputDir, 'energyNodes_{}_{}.pdf'.format(scenario, protocol)), bbox_inches="tight")
      # except FloatingPointError as e:
      #   print('WARNING: Error when creating violin plot for energy ({})'.format(e))
    np.seterr('raise')

    # stats values for text in paper
    if testbedDf is not None:
      # factor testbed vs. sim
      combSimDf = combDf[combDf.src == 'sim']
      aggSimDf  = combSimDf.groupby(['nodeId'], as_index=False).energy.agg('mean')
      aggSimDf.sort_values(by=['nodeId'], inplace=True)
      combTestbedDf = combDf[combDf.src == 'testbed']
      aggTestbedDf  = combTestbedDf.groupby(['nodeId'], as_index=False).energy.agg('mean')
      aggTestbedDf.sort_values(by=['nodeId'], inplace=True)
      if aggSimDf.nodeId.to_list() != aggTestbedDf.nodeId.to_list():
        raise Exception('Node lists differ! sim: {}, testbed: {}'.format(aggSimDf.nodeId.to_list(), aggTestbedDf.nodeId.to_list()))

      ratioList = aggTestbedDf.energy.to_numpy() / aggSimDf.energy.to_numpy()
      print('energy testbed / energy sim: {:0.3f}'.format(np.mean(ratioList)))

      # diff roof vs indoor
      for src in combDf.src.unique():
        roofDf      = combDf[(np.isin(combDf.nodeId, [15, 17, 25, 30])) & (combDf.src == src)]
        indoorDf    = combDf[(np.invert(np.isin(combDf.nodeId, [15, 17, 25, 30])) & (combDf.src == src))]
        aggRoofDf   = roofDf.groupby(['nodeId'],   as_index=False).energy.agg('mean')
        aggIndoorDf = indoorDf.groupby(['nodeId'], as_index=False).energy.agg('mean')
        print('{}: energy roof - energy indoor = {:0.3f} - {:0.3f} = {:0.3f}'.format(src, aggRoofDf.energy.mean(), aggIndoorDf.energy.mean(), aggRoofDf.energy.mean() - aggIndoorDf.energy.mean()))

    # energy per node (box/violin plot)
    energyDf   = res['energy']
    energyDf   = energyDf[energyDf.roundIdx >= roundIdxOffset]  # filter out rounds according to roundIdxOffset
    dfAgg      = energyDf.groupby(['nodeId', 'roundIdx'], as_index=False).energy.agg('sum')
    keys, vals = zip(*[(nodeId, gdf.energy*1e3) for nodeId, gdf in dfAgg.groupby('nodeId')])
    np.seterr('warn')  # prevent "FloatingPointError: underflow encountered in exp" which occur in certain cases when plotting violin plots
    # try:
    fig, ax = plt.subplots(figsize=(6, 3))
    # ax.boxplot(vals)
    ax.violinplot(vals)
    ax.set_xticks(np.arange(1, len(keys) + 1))  # only needed for violin plot
    ax.set_xticklabels(keys)
    ax.set_xticks(ax.get_xticks()[::1])  # set tick interval
    ax.set_ylim(bottom=0.)
    ax.set_xlabel('Node ID')
    ax.set_ylabel('Energy per round [mJ]')
    plt.savefig(os.path.join(self._outputDir, 'energyNodesOld_{}_{}.pdf'.format(scenario, protocol)), bbox_inches="tight")
    # except FloatingPointError as e:
    #   print('WARNING: Error when creating violin plot for energy ({})'.format(e))
    np.seterr('raise')

    # energy per node split seaborn (split into rcIdx and/or radioOp)
    energyDf = res['energy']
    energyDf = deepcopy(energyDf[energyDf.roundIdx >= roundIdxOffset])  # filter out rounds according to roundIdxOffset
    # numRounds = len(energyDf.roundIdx.unique())
    energyDf.sort_values(by=['nodeId', 'radioOp', 'rcIdx'], inplace=True)  # ensure proper order (required since we assume same ordering of nodeIds further below)
    with sns.axes_style("whitegrid"):  # use this context manager (with ..:) instead of sns.set_theme, as it messed up appearance of other plots
      fig, ax         = plt.subplots(figsize=(6, 4))
      dfAgg           = energyDf.groupby(['nodeId', 'radioOp', 'rcIdx', 'roundIdx'], as_index=False).energy.agg('sum')
      dfAgg['energy'] = dfAgg.energy * 1e3  # convert J to mJ
      dfAgg['hue']    = [(rcIdx, radioOp) for radioOp, rcIdx in zip(dfAgg.radioOp, dfAgg.rcIdx)]
      hue_order       = sorted(dfAgg.hue.unique())

      # construct bar style mapping
      palette = []
      alpha   = []
      hatch   = []
      for rcIdx, radioOp in hue_order:
        # swap colors to be consistent with rest of paper
        if   rcIdx == 0:
          pIdx = 1
        elif rcIdx == 1:
          pIdx = 0
        else:
          pIdx = rcIdx
        c = plt.get_cmap("tab10")(pIdx)
        # c = (*c[:3], 1.0 if radioOp == 'tx' else 0.6) # apparently alpha value is ignored by seaborn
        palette.append(c)
        alpha.append(1.0 if radioOp == 'tx' else 0.6)
        if   rcIdx == 0 and radioOp == 'rx':
          hatch.append('//')
        elif rcIdx == 0 and radioOp == 'tx':
          hatch.append('///')
        elif rcIdx == 1 and radioOp == 'rx':
          hatch.append('..')
        else:
          hatch.append('')

      # plot
      ax = sns.barplot(x='nodeId', y='energy', hue='hue', hue_order=hue_order, data=dfAgg, estimator=np.mean, palette=palette, saturation=1.0, edgecolor='black')
      # set alpha & hatch
      alphaRep = np.repeat(alpha, len(ax.patches)/len(alpha))
      hatchRep = np.repeat(hatch, len(ax.patches)/len(hatch))
      for p, a, h in zip(ax.patches, alphaRep, hatchRep):
        c = (*p.get_facecolor()[:3], a)
        p.set_facecolor(c)
        p.set_hatch(h)
      for p, a, h in zip(ax.get_legend().get_patches(), alpha, hatch):
        c = (*p.get_facecolor()[:3], a)
        p.set_facecolor(c)
        p.set_hatch(h)

      # cleanup
      # ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
      ax.get_legend().get_frame().set_boxstyle('square')
      ax.get_legend().set_title(None)
      for text in ax.get_legend().texts:
        rcIdx, radioOp = eval(text.get_text())
        text.set_text('{} Mod{}'.format(radioOp.capitalize(), rcIdx))
      ax.set_ylim(bottom=-0.5)  # clearly display bars with size 0
      ax.set_xlabel('Node ID')
      ax.set_ylabel('Energy per round [mJ]')
      plt.savefig(os.path.join(self._outputDir, 'energySplit_{}_{}.pdf'.format(scenario, protocol)), bbox_inches="tight")

  def plotEnergyNormalized(self, scenario, protocol):
    print('===== plotEnergyNormalized =====')

    self.sanityCheck(scenario, protocol)
    res = self._results[scenario][protocol]

    # energy normalized over time
    enDf    = res['energyNormalized']
    fig, ax = plt.subplots(figsize=(6, 3))
    ax.plot(enDf.roundIdx, enDf.energyActual,     marker='.', label='Energy actual')
    ax.plot(enDf.roundIdx, enDf.energyOptimal,    marker='.', label='Energy optimal')
    ax.plot(enDf.roundIdx, enDf.energyNormalized, marker='.', label='Energy normalized')
    ax.set_xlabel('Round Index')
    ax.set_ylabel('Energy [J] / Energy normalized [1]')
    ax.legend()
    plt.savefig(os.path.join(self._outputDir, 'energyNormalized_{}_{}.pdf'.format(scenario, protocol)), bbox_inches="tight")

  def generateGraphPlots(self, scenario, protocol, highlightUsedEdges=False):
    """Plot connectivity graphs of all rounds for all modulations.
       NOTE: only works for a single scenario/protocol combination!
    Args:
      scenario:             selected scenario
      protocol:             selected protocol
      highlightUsedEdges:   highlight edges which are part of selected paths (NOTE: requires results to contain data for selectedPaths)
    """
    print('===== generateGraphPlots =====')

    self.sanityCheck(scenario, protocol)
    res = self._results[scenario][protocol]

    outputDir = os.path.join(self._outputDir, 'graphPlots')
    os.makedirs(outputDir, exist_ok=True)
    colorMap = ['#FFC847', '#E0EFFF']  # TODO: make more general, currently limited to 2 modulations max
    if 'graphInfo' in res:
      dfGraphInfo = res['graphInfo']
      for idx, row in dfGraphInfo.iterrows():
        # if row['roundIdx'] not in [30, 80]: continue # DEBUG: faster execution
        if highlightUsedEdges:
          # plot with drawing used paths thicker (edges with label >= 1 will be thick)
          g = nx.Graph()
          for e in row['edges']:
            g.add_edge(e[0], e[1], usage=0)
          df = res['selectedPaths']
          df = df[(df.roundIdx == row['roundIdx']) & (df.rcIdx == row['rcIdx'])]
          for idxSelPath, rowSelPath in df.iterrows():
            selPath = rowSelPath['selectedPath']
            if selPath is None:
              continue
            for i, n in enumerate(selPath):
              if i >= len(selPath)-1:
                continue
              if   selPath[i]   not in g.nodes:
                print('WARNING: Node {} is part of a selected path but is not contained in the corresponding connectivity graph (nodes: {}).'.format(selPath[i], g.nodes))
              elif selPath[i+1] not in g.nodes:
                print('WARNING: Node {} is part of a selected path but is not contained in the corresponding connectivity graph (nodes: {}).'.format(selPath[i+1], g.nodes))
              elif (selPath[i], selPath[i+1]) not in g.edges:
                print('WARNING: Edge {} is part of a selected path but is not contained in the corresponding connectivity graph (edges: {}).'.format((selPath[i], selPath[i+1]), g.edges))
              else:
                g.edges[(selPath[i], selPath[i+1])]['usage'] += 1
          edges = {(u, v): g.edges[u, v]['usage'] for u, v in g.edges}  # dict of edge -> label (usage)
        else:
          # plot without showing used paths
          edges = row['edges']  # list of edges
        GraphPlot.generatePlot(
          row['hopDistanceMap'],
          edges,
          filePaths=[
            # os.path.join(outputDir, 'rcIdx{}_roundIdx{}.png'.format(row['rcIdx'], row['roundIdx'])),
            os.path.join(outputDir, 'rcIdx{}_roundIdx{}.svg'.format(row['rcIdx'], row['roundIdx'])),
          ],
          nodeColor=colorMap[row['rcIdx']],
        )

  def getGraphDistance(self, scenario, protocol, refGraphEdges, config):
    """Determine the distance between the measured connectivity graph and the original graph (knownGraph) for every round.
    Args:
      scenario:      selected scenario
      protocol:      selected protocol
      refGraphEdges: reference graph (ground truth) as list of edges (complete graph is ok, does not need to be shortest connectivity graph)
    """
    print('===== getGraphDistance =====')

    self.sanityCheck(scenario, protocol)
    res = self._results[scenario][protocol]

    hostNodeId = config['hostNodeId']

    # determine shortest connectivity graph from refGraphEdges
    scGraph = nx.DiGraph()
    for edge in refGraphEdges:
      scGraph.add_edges_from( (edge, tuple(reversed(edge))) )
    # remove all edges from node with d_1 to node with d_2>=d_1
    edgesToRemove = []
    for edge in scGraph.edges():
      n1, n2 = edge
      d1 = nx.shortest_path_length(scGraph, source=n1, target=hostNodeId)
      d2 = nx.shortest_path_length(scGraph, source=n2, target=hostNodeId)
      if d2 >= d1:
        edgesToRemove.append(edge)
    scGraph.remove_edges_from(edgesToRemove)
    # get normalized edges
    scGraphEdgesNormalized = [tuple(sorted(e)) for e in scGraph.edges()]
    # remove duplicates (make non-directional)
    scGraphEdges = list(set(scGraphEdgesNormalized))

    if 'graphInfo' not in res:
      raise Exception('getGraphDistance: no graphInfo available in sim results!')
    graphInfoDf = res['graphInfo']
    graphDistanceList = []
    for idx, row in graphInfoDf.iterrows():
      # get normalized edges representation
      edgesRound = set([tuple(sorted(e)) for e in row['edges']])
      symDiff = edgesRound.symmetric_difference(scGraphEdges)
      graphDistanceList.append(len(symDiff))
    graphInfoDf['graphDistance'] = graphDistanceList

    return graphInfoDf



################################################################################


def adjust_box_widths(g, fac):
  """Adjust the withs of a seaborn-generated boxplot.
  https://stackoverflow.com/questions/56838187/how-to-create-spacing-between-same-subgroup-in-seaborn-boxplot
  """

  # iterating through Axes instances
  for ax in g.axes:
    # iterating through axes artists:
    for c in ax.get_children():
      # searching for PathPatches
      if isinstance(c, patches.PathPatch):
        # getting current width of box:
        p         = c.get_path()
        verts     = p.vertices
        verts_sub = verts[:-1]
        xmin      = np.min(verts_sub[:, 0])
        xmax      = np.max(verts_sub[:, 0])
        xmid      = 0.5 * (xmin+xmax)
        xhalf     = 0.5 * (xmax - xmin)

        # setting new width of box
        xmin_new = xmid-fac*xhalf
        xmax_new = xmid+fac*xhalf
        verts_sub[verts_sub[:, 0] == xmin, 0] = xmin_new
        verts_sub[verts_sub[:, 0] == xmax, 0] = xmax_new

        # setting new width of median line
        for l in ax.lines:
          if np.all(l.get_xdata() == [xmin, xmax]):
            l.set_xdata([xmin_new, xmax_new])

################################################################################


if __name__ == '__main__':
  pass
