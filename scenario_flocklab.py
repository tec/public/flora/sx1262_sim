#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""


import pickle
import json
import numpy  as np
import pandas as pd
from copy import copy, deepcopy
from collections import OrderedDict

import sx1262

from sim  import Sim
from node import *

################################################################################
LINKTEST_PKL_FILE = 'flocklab/linktest_data_6298.pkl'
################################################################################


def addNodes(sim, genNode, pathlossMatrixScaling=1.0, flPosJsonFile='flocklab/flocklab-observer-positions_fl2.json', linktestPklFile=LINKTEST_PKL_FILE, subset=None):
  sim.flocklabBackground = True

  # read FlockLab node positions
  with open(flPosJsonFile) as json_file:
    # NOTE: minus for y values due to different origin in plot (bokeh, vs flocklab webpage)
    data      = json.load(json_file)
    positions = OrderedDict([(e['node_id'], {'x': e['x'], 'y': -e['y']}) for e in data])

  # read FlockLab linktest data
  with open(linktestPklFile, 'rb' ) as f:
    linktest = pickle.load(f)

  # form list of nodes with available data
  positionSet = set(positions.keys())
  linktestSet = set(linktest['nodeList'])
  nodeSetAll  = positionSet & linktestSet
  assert len(positionSet ^ linktestSet) == 0
  if subset is None:
    nodeIdList = list(nodeSetAll)
  else:
    nodeIdList = sorted(nodeSetAll.intersection(set(subset)))
  print('nodeList: {}'.format(nodeIdList))

  # add nodes
  for nodeId in nodeIdList:
    pos  = ( positions[nodeId]['x'], positions[nodeId]['y'] )
    node = genNode(nodeId, nodeIdList, pos)
    assert isinstance(node, Node)
    sim.addNode(node)

  # get and add path loss matrix
  pathlossMatrix, nodeIdList = getPathlossMatrix(
    pathlossMatrixScaling = pathlossMatrixScaling,
    linktestPklFile       = linktestPklFile,
  )
  sim.schedulePathlossMatrix(
    pathlossMatrix = pathlossMatrix,
    nodeIdList     = nodeIdList,
  )


def getPathlossMatrix(pathlossMatrixScaling=1.0, linktestPklFile=LINKTEST_PKL_FILE):
  # read FlockLab linktest data
  with open(linktestPklFile, 'rb' ) as f:
    linktest = pickle.load(f)

  pathlossMatrix = linktest['pathlossMatrix']*pathlossMatrixScaling
  nodeIdList     = linktest['nodeList']

  return pathlossMatrix, nodeIdList

################################################################################


if __name__ == '__main__':
  pass
