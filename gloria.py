#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""

import numpy as np
from collections import OrderedDict
from struct      import *

import sx1262

from message import Message
from actions import *
from node    import Node

################################################################################
# Gloria Message Definitions
################################################################################


class GloriaMsg(Message):
  def __init__(self, msg=None, raw=None):
    self._hasVariableField = True
    self._formats          = OrderedDict([('slot', 'B'), ('payload', 'c')])
    super().__init__(msg, raw)

################################################################################
# Gloria
################################################################################


class Gloria():
  def __init__(self, timerId, node, postRxDelay=0.02, rxTimeoutDuration=5*60, logRxDone=False):
    # config
    self.timerId           = timerId
    self.node              = node
    self.postRxDelay       = postRxDelay
    self.rxTimeoutDuration = rxTimeoutDuration
    self.logRxDone         = logRxDone

    self.tRef           = -1  # should be initialized only once since it is valid even if tRef has not been updated
    self._delayedTx     = 0
    self._floodCallback = None
    self._initState()

  def _initState(self):
    """Init gloria state (should be called at the beginning of each flood, i.e. in gloria_start)
    """
    # gloria_start args
    self.nTx       = 0
    self.syncFlood = False
    self.initiator = None
    self.payload   = None

    # state
    self.lastRxMsg = None
    self.txMsg     = None

    self.tRefUpdated  = False
    self.txCount      = 0
    self.rxCount      = 0
    self.currSlot     = -1
    self.firstRxIndex = None
    self.slotLen      = 0
    self.toa          = None
    self.running      = False

  def _print(self, text):
    self.node.print('[Gloria] {}'.format(text))

  @property
  def _tSlot(self):
    return self.toa + self.postRxDelay

  def _callFloodCallback(self):
    if self._floodCallback is not None:
      self.stop()
      self._floodCallback()

  ##############################################################################
  # Interface
  ##############################################################################

  def start(self, initiator, payload, nTx, syncFlood, abortRetransmitFunc=None):
    if self.running:
      raise Exception('Error: Gloria is already running!')

    # init state
    self._initState()
    self.running = True

    # store arguments of current flood
    self.nTx                 = nTx
    self.syncFlood           = syncFlood
    self.initiator           = initiator
    self.payload             = payload if payload is not None else b''
    self.abortRetransmitFunc = abortRetransmitFunc

    if self.initiator:
      self.currSlot = 0

      # prepare msg
      self.txMsg = GloriaMsg(msg=OrderedDict([
        ('slot', self.currSlot),
        ('payload', self.payload),
      ]))

      # determine toa (for non-initiator, this is done in rxDone callback)
      self.toa = self.node.getToa(self.txMsg.size)

      # send msg
      self.node.radioTx(payload=self.txMsg.raw)

      # set tRef for the initiator
      if self.syncFlood:
        self.tRefUpdated = True
        self.tRef        = self.node.getTs()

    else:  # non-initiator
      # listen for first msg
      self.node.radioRx(timeout=self.rxTimeoutDuration)

  def stop(self):
    if not self.running:
      raise Exception('Error: Gloria is already stopped!')

    self.node.timerStop(timerId=self.timerId)
    self.node.radioReset()
    self._delayedTx = 0
    self.running    = False

  def getPayload(self):
    """Return payload of last flood
    """
    if self.lastRxMsg is None:
      return None
    else:
      return b''.join(self.lastRxMsg['payload'])

  def isTRefUpdated(self):
    return self.tRefUpdated

  def getTRef(self):
    return self.tRef

  def getRxCnt(self):
    return self.rxCount

  def getRxIndex(self):
    return self.firstRxIndex

  def setDelayedTx(self, numSlots=1):
    self._delayedTx = numSlots

  def getFloodDuration(self, nTx, numHops, radioConfig):
    """Calculates the duration of a flood
       NOTE: phyPl must be set in the radioConfig object
    """
    slotTime = radioConfig.timeOnAir + self.postRxDelay
    numSlots = nTx + numHops - 1
    return numSlots*slotTime

  def registerFloodCallback(self, cb):
    self._floodCallback = cb

  ##############################################################################
  # Callbacks
  ##############################################################################

  def rxDoneCallback(self, payload, crc, rssi=None, snr=None):
    self.lastRxMsg = GloriaMsg(raw=payload)
    self.currSlot  = self.lastRxMsg['slot']
    self.toa       = self.node.getToa(self.lastRxMsg.size)
    self.rxCount  += 1

    # set tRef for non-initiator
    if self.syncFlood and self.rxCount == 1:
      self.tRefUpdated = True
      self.tRef        = self.node.getTs() - (self.currSlot+1)*self.toa - self.currSlot*self.postRxDelay
    if self.firstRxIndex is None:
      self.firstRxIndex = self.currSlot
    if self.logRxDone:
      self._print('RxDone, payload="{}"'.format(self.lastRxMsg))
    if self.txCount < self.nTx:
      # check if retransmission should be aborted based on provided eval function
      if (self.abortRetransmitFunc is not None) and (self.abortRetransmitFunc(self.lastRxMsg['payload'])):
        # terminate flood without retransmission
        self._callFloodCallback()
      else:
        # retransmit
        self.node.timerStart(timerId=self.timerId, timeout=self.postRxDelay)
    else:
      # terminate flood
      self._callFloodCallback()

  def txDoneCallback(self):
    self.txCount += 1
    if self.txCount < self.nTx:
      self.node.timerStart(timerId=self.timerId, timeout=self.postRxDelay)
    else:
      self._callFloodCallback()

  def rxTimeoutCallback(self):
    # nothing received yet -> continue listening
    self.node.radioRx(timeout=self.rxTimeoutDuration)

  def timerCallback(self, timerId):
    if timerId == self.timerId:
      # timer is only used to wait for start of following slot -> update current slot index
      self.currSlot += 1
      if self.currSlot > 255:
        self._print('WARNING: More than 255 Gloria slots used. Current Gloria implementation only supports floods with <=255 slots! Flood has automatically been terminated!')
        self._callFloodCallback()
        return

      if self.initiator:
        self.txMsg['slot'] = self.currSlot
        self.node.radioTx(payload=self.txMsg.raw)
      elif self.txCount < self.nTx:
        if self._delayedTx > 0:
          self._delayedTx -= 1
          self.node.timerStart(timerId=self.timerId, timeout=self._tSlot)
        else:
          self.lastRxMsg['slot'] = self.currSlot
          self.node.radioTx(payload=self.lastRxMsg.raw)
      else:
        # should never happen
        raise Exception('Wrong state! This condition should never be reached!')


################################################################################

class SingleGloriaFloodNode(Node):
  def __init__(self, radioConfig, position=(0, 0), initiator=False, nTx=3, floodDuration=0.5, nodeId=None, startDelay=0, delayedTx=0):
    super().__init__(radioConfig=radioConfig, position=position, nodeId=nodeId)

    self.gloria        = Gloria(timerId=0, node=self)
    self.initiator     = initiator
    self.floodDuration = floodDuration
    self.nTx           = nTx
    self.startDelay    = startDelay
    self.delayedTx     = delayedTx

  def init(self):
    # prepare start of flood
    self.timerStart(timerId=1, timeout=self.startDelay)

  def timerCallback(self, timerId):
    self.gloria.timerCallback(timerId)
    if timerId == 1:
      if not self.gloria.running:
        # prepare msg
        msg = GloriaMsg(msg=OrderedDict([
          ('slot', 0),
          ('payload', b'hello')
        ]))

        # start gloria flood
        self.gloria.start(
          initiator=self.initiator,
          payload=msg.raw,
          nTx=self.nTx,
          syncFlood=True,
        )
        self.gloria.setDelayedTx(numSlots=self.delayedTx)

        # prepare stop of flood
        self.timerStart(timerId=1, timeout=self.startDelay + self.floodDuration)
      else:
        self.gloria.stop()

        if self.gloria.getRxCnt():
          rawPayload = self.gloria.getPayload()
          m          = GloriaMsg(raw=rawPayload)
          self.print('payload: {}'.format(m))
          self.print('isTRefUpdated: {}'.format(self.gloria.isTRefUpdated()))
          self.print('getTRef: {}'.format(self.gloria.getTRef()))
          self.print('getRxCnt: {}'.format(self.gloria.getRxCnt()))
          self.print('getRxIndex: {}'.format(self.gloria.getRxIndex()))
        else:
          self.print('No msg received during gloria flood!')

  def rxDoneCallback(self, payload, crc, rssi=None, snr=None):
    self.gloria.rxDoneCallback(payload, crc, rssi, snr)

  def txDoneCallback(self):
    self.gloria.txDoneCallback()

  def rxTimeoutCallback(self):
    self.gloria.rxTimeoutCallback()

################################################################################


if __name__ == '__main__':
  pass
