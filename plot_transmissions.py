#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""


import os
import numpy as np
from munch import Munch
from collections         import OrderedDict

from bokeh.models        import ColumnDataSource, Plot, LinearAxis, Grid, WheelZoomTool, PanTool, BoxZoomTool, ResetTool, HoverTool, CustomJS, SaveTool
from bokeh.models.glyphs import Rect
from bokeh.io            import curdoc, show, output_file, save
from bokeh.events        import DoubleTap

from actions import *
from events  import *

################################################################################
HEIGHT = 0.8
Y_SEP  = 1.0

################################################################################


class TransmissionPlot():
  def __init__(self, sim):
    self._rects = Munch([
      ('x', []),
      ('y', []),
      ('w', []),
      ('h', []),
      ('fc', []),
      ('lc', []),
      ('lw', []),
      ('nodeId', []),
      ('typeLabel', []),
      ('radioConfigLabel', []),
      ('floraModIdx', []),
    ])
    self._sim = sim

    self.loadFromSim()

  @staticmethod
  def type2Color(typeLabel):
    # using named bokeh CSS colors
    if   typeLabel == 'tx':
      return 'royalblue'         # colors.named.royalblue.to_rgb(): rgb(65, 105, 225)
    elif typeLabel == 'listen':
      return 'lightsalmon'       # colors.named.lightsalmon.to_rgb(): rgb(255, 160, 122)
    elif typeLabel == 'rx':
      return 'orangered'         # colors.named.orangered.to_rgb(): rgb(255, 69, 0)

  def addRect(self, xl, xr, y, fc='blue', lc='black', lw=1, nodeId=0, typeLabel=None, radioConfigLabel=None, floraModIdx=None):
    self._rects.x.append((xl+xr)/2)
    self._rects.y.append((y+y+HEIGHT)/2)
    self._rects.w.append(xr-xl)
    self._rects.h.append(HEIGHT)
    self._rects.fc.append(fc)
    self._rects.lc.append(lc)
    self._rects.lw.append(lw)
    self._rects.nodeId.append(nodeId)
    self._rects.typeLabel.append(typeLabel)
    self._rects.radioConfigLabel.append(radioConfigLabel)
    self._rects.floraModIdx.append(floraModIdx)

  def addRectByNodeId(self, xl, xr, nodeIdSim, typeLabel, radioConfigLabel=None, floraModIdx=None, lc='black', lw=1):
    """Add a rectangle without providing y-axis values, instead providing node number.
       NOTE: self._sim needs to be available!
    Args:
      xl:               x-axis value on the left of the rectangular box
      xr:               x-axis value on the right of the rectangular box
      nodeIdSim:        ID of the node (internal sim ID)
      typeLabel:
      radioConfigLabel:
      floraModIdx:
      lc:               color
      lw:
    """
    if self._sim is None:
      raise Exception('self._sim is not available but required by addRectByNodeId()!')

    nodeIdSimList = sorted(self._sim.nodes.keys())
    nodeIdSimIdx  = nodeIdSimList.index(nodeIdSim)

    # convert NodeId to y-axis value
    y = len(nodeIdSimList) - (nodeIdSimIdx+1)*Y_SEP

    self.addRect(
      xl=xl,
      xr=xr,
      y=y,
      fc=type(self).type2Color(typeLabel),
      lc=lc,
      lw=lw,
      nodeId=self._sim.nodeIdSim2nodeId(nodeIdSim),
      typeLabel=typeLabel,
      radioConfigLabel=radioConfigLabel,
      floraModIdx=floraModIdx,
    )

  def loadFromSim(self):
    # create list of all RxDone events with actionId-index (do it only once here instead of in for loop below for performance reasons)
    rxDoneEvents = OrderedDict()
    for event in self._sim.eq.q:
      if isinstance(event, RxDone):
        assert event.actionId not in rxDoneEvents
        rxDoneEvents[event.actionId] = event

    for actionId, a in self._sim.at.items():
      if isinstance(a, RadioTxAction):
        # add tx action
        self.addRectByNodeId(a.startTime, a.endTime, a.nodeIdSim, typeLabel='tx', radioConfigLabel=str(a.radioConfig), floraModIdx=a.radioConfig.floraModIdx, lc='black', lw=1)
      elif isinstance(a, RadioRxAction):
        listenEndTime = a.endTime
        # get corresponding RxDone event (if it exists)
        if actionId in rxDoneEvents:
          rxDoneEvent = rxDoneEvents[actionId]
          txAction    = self._sim.at[rxDoneEvent.txActionId]

          # add rx action
          self.addRectByNodeId(txAction.startTime, txAction.endTime, a.nodeIdSim, typeLabel='rx', radioConfigLabel=str(a.radioConfig), floraModIdx=a.radioConfig.floraModIdx, lc='black', lw=1)
          # shorten listening time
          assert txAction.startTime <= listenEndTime
          listenEndTime = txAction.startTime

        # add listening action
        self.addRectByNodeId(a.startTime, listenEndTime, a.nodeIdSim, typeLabel='listen', radioConfigLabel=str(a.radioConfig), floraModIdx=a.radioConfig.floraModIdx, lc='black', lw=1)

  def generatePlot(self, outFile):
    rectSource = ColumnDataSource(dict(
      x=self._rects.x,
      y=self._rects.y,
      w=self._rects.w,
      h=self._rects.h,
      fc=self._rects.fc,
      lc=self._rects.lc,
      lw=self._rects.lw,
      nodeId=self._rects.nodeId,
      typeLabel=self._rects.typeLabel,
      radioConfigLabel=self._rects.radioConfigLabel,
      floraModIdx=self._rects.floraModIdx,
    ))

    plot = Plot(
        title=None,
        plot_width=300,
        plot_height=300,
        min_border=0,
        # toolbar_location=None
        sizing_mode='stretch_both'
    )
    # plot.add_tools(WheelZoomTool())
    panTool       = PanTool(dimensions='width')
    boxZoomTool   = BoxZoomTool(dimensions='width')
    wheelZoomTool = WheelZoomTool(dimensions='width')
    hoverTool     = HoverTool(
      tooltips=[
        ("Node",                   "@nodeId"),
        ("Type",                   "@typeLabel"),
        ("Radio Config",           "@radioConfigLabel"),
        ("Flora Modulation Index", "@floraModIdx"),
        ("Time",                   "$x{0.000000} s"),
      ],
    )
    plot.add_tools(panTool)
    plot.add_tools(boxZoomTool)
    plot.add_tools(wheelZoomTool)
    plot.add_tools(hoverTool)
    plot.add_tools(SaveTool())
    plot.add_tools(ResetTool())
    plot.toolbar.active_drag   = boxZoomTool
    plot.toolbar.active_scroll = wheelZoomTool
    # add functionality to reset by double-click
    plot.js_on_event(DoubleTap, CustomJS(args=dict(p=plot), code='p.reset.emit()'))

    rectGlyph = Rect(
      x="x",
      y="y",
      width="w",
      height="h",
      fill_color="fc",
      line_color="lc",
      line_width="lw",
    )
    plot.add_glyph(rectSource, rectGlyph)

    xaxis = LinearAxis()
    xaxis.axis_label                 = "Time [s]"
    xaxis.axis_label_text_font_size  = "20pt"
    xaxis.major_label_text_font_size = "18pt"
    plot.add_layout(xaxis, 'below')

    yaxis = LinearAxis()
    yaxis.major_label_text_font_size  = "18pt"
    yaxis.major_label_text_font_style = "bold"
    plot.add_layout(yaxis, 'left')

    # Add y-axis labels according to node IDs
    nodeIdList        = [self._sim.nodeIdSim2nodeId(nodeIdSim=nodeIdSim) for nodeIdSim in self._sim.nodes.keys()]
    y_loc             = np.arange(len(nodeIdList))
    ytick_loc         = y_loc + HEIGHT / 2
    plot.yaxis.ticker = ytick_loc
    tick_labels       = [str(nodeId) for nodeId in reversed(nodeIdList)]
    plot.yaxis.major_label_overrides = dict(zip(ytick_loc, tick_labels))

    plot.add_layout(Grid(dimension=0, ticker=xaxis.ticker))
    plot.add_layout(Grid(dimension=1, ticker=y_loc))

    curdoc().add_root(plot)

    # show(plot)

    os.makedirs(os.path.split(outFile)[0], exist_ok=True)
    output_file(outFile, title="Transmission Plot")
    save(plot)


################################################################################

if __name__ == '__main__':
  # Test TransmissionPlot class
  from sim import Sim   # ATTENTION: do not move to beginning of file! (not required if imported and would lead to issues with circular imports)
  from node import Node # ATTENTION: do not move to beginning of file! (not required if imported and would lead to issues with circular imports)

  sim = Sim()
  sim.addNode(Node(radioConfig=None, nodeId=1))  # nodeIdSim=0
  sim.addNode(Node(radioConfig=None, nodeId=2))  # nodeIdSim=1
  tp = TransmissionPlot(sim=sim)

  tp.addRectByNodeId(1, 3,   nodeIdSim=0, typeLabel='tx',     lc='black', lw=1)  # Tx
  tp.addRectByNodeId(2, 4.5, nodeIdSim=1, typeLabel='listen', lc='black', lw=1)  # Rx listening
  tp.addRectByNodeId(3, 4,   nodeIdSim=1, typeLabel='rx',     lc='black', lw=1)  # Rx receiving

  tp.generatePlot('output/out.html')
