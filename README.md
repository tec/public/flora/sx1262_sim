# Simulation for the SX126x Radio Platform
This README describes how to make use of the simulator. Please see [README_internal.md](README_internal.md) for more details about the implementation of the simulator.

## Coverage & Limitations
### Modelled
* Processing delay
* Energy usage (simplified)
* Concurrent transmissions

### Not Modelled
* Propagation delay

## Components
* **Sim**: Top element that connects other elements, list of available nodes.
* **Medium (air)**: Represents the air and environment, decisions as which transmission is successful in which time unit, which transmissions overlap.
* **Event queue**: Takes care of processing events in the order of time (e.g. TxStart, TxDone)
* **Node**: Radio node which are independent (independent state, clock drift, etc. )
* **Events**: Available events for the nodes

## Interactions of Nodes
In: Functions / callback functions
* Init: `init()`
* Radio callback functions: `rxDoneCallback()`, `rxTimeoutCallback()`, `txDoneCallback()`
* Timer callback function: `timerCallback()`
Out: Actions (which trigger the insertin of one or more events in the event queue)
* TimerStartAction
* TimerStopAction
* RadioRxAction
* RadioTxAction
* RadioResetAction

### Events
* TimerTimeout
* RxStart
* RxDone
* RxHeaderValidSync
* RxTimeout
* TxStart
* TxDone
* SetPathlossMatrix

## HowTo
This quick introduction explains the required steps by using the `app_test.py` as a simple example.

1. Create your nodes with its behavior by inheriting from the `Node` class and implementing the init and callback functions.
   Some generic examples, such as the `PeriodicTxNode()` and `ContinuousRxNode()` are provided in `ǹode_example.py`.
   ```
   def init(self):
     self.timerStart(timerId=0, timeout=self.startOffset)

   def timerCallback(self, timerId):
     self.print('timerCallback (count={})'.format(self.counter))
     self.counter += 1
     self.radioTx(
       payload = 'test'.encode('utf-8')
     )
     self.timerStart(
       timerId = 0,
       timeout = self.period,
     )
   ```
2. Construct the simulation application, e.g. similar to app_test.py.
   1. Create an instance of the simulation class:
      ```
      sim = Sim(
        # logFileName='simLog.txt',
        outputDir='./output/output_test',
      )
      sim.propagation     = True
      sim.timeDrift       = True
      sim.probabilisticRf = True
      sim.verbose         = True
      sim.debug           = True
      sim.setRandSeed(0)
      ```
   2. Add nodes as desired, e.g.:
      ```
      sim.addNode(
        PeriodicTxNode(
         position=(0, 0),
         radioConfig=rc,
         period=2.0,
         startOffset=1.0,
        )
      )
      ```
   3. Add steps for simulation execution:
      ```
      sim.calcPathlossmatrixFromPositions()
      sim.plotNodePositions()
      sim.runSimulation(until=10.0)
      sim.plotTransmissions(outFile='transmissions.html')
      ```
3. Run the simulation application and inspect the results:
   ```
   python app_test.py
   ```
   Results:
   * **Text Log**: The simulation log is printed to the console (default) or to a file in the `outputDir` (if `logFileName` is specified). The log contains the simulation time, the node ID (if applicable) and a text string.
   ```
   [   0.000000] === SIMULATION STARTED ===
   [   0.000000] Initializing nodeIdSim=0 (nodeId=0)
   [   0.000000] Initializing nodeIdSim=1 (nodeId=1)
   [   1.000004] [  0] timerCallback (count=0)
   [   1.140292] [  0] TxDone!
   [   3.000000] [  1] RxTimeout callback
   [   3.000012] [  0] timerCallback (count=1)
   [   3.140300] [  0] TxDone!
   [   3.140300] [  1] RxDone, payload="test"
   [   5.000020] [  0] timerCallback (count=2)
   [   5.140308] [  0] TxDone!
   [   6.140300] [  1] RxTimeout callback
   [   7.000027] [  0] timerCallback (count=3)
   [   7.140315] [  0] TxDone!
   [   7.140315] [  1] RxDone, payload="test"
   [   9.000035] [  0] timerCallback (count=4)
   [   9.140323] [  0] TxDone!
   [   9.140323] [  1] RxDone, payload="test"
   [  11.000043] === SIMULATION ENDED ===
   ```
   * **Graphical results**: Graphical output is generated in the directory specified with `outputDir`. For example, this includes `transmissions.html`, an interactive plot as an HTML file of all radio operations of all nodes over time similar as shown below (blue: transmissions, light red: listening, red: reception).
   <img width="100%" src="./figures/screenshot_transmissions_app_test.png">
