#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2022, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


@author: rtrueb
"""


import os
import itertools
import numpy as np
from collections    import OrderedDict

from bokeh.models   import WheelZoomTool, PanTool, BoxZoomTool, ResetTool, HoverTool, CustomJS, SaveTool, DataRange1d
from bokeh.plotting import figure
from bokeh.io       import curdoc, show, output_file, save
from bokeh.events   import DoubleTap
from bokeh.palettes import Dark2_5 as palette

import sx1262

from actions import *
from events  import *

################################################################################


################################################################################

class AccumulatedEnergyPlot():
  def __init__(self):
    self.data = OrderedDict()

  def loadFromSim(self, sim):
    self.sim = sim

    # get set of nodes
    self.nodeIdSimList = sorted(list(set([a.nodeIdSim for a in sim.at.values()])))
    self.nodeIdList    = [sim.nodeIdSim2nodeId(nodeIdSim=nodeIdSim) for nodeIdSim in self.nodeIdSimList]

    # init self.data
    for nodeIdSim in self.nodeIdSimList:
      self.data[nodeIdSim] = {'t': [0], 'consumedEnergy': [0]}

    for actionId, a in sim.at.items():
      if isinstance(a, RadioTxAction) or isinstance(a, RadioRxAction):
        # add point at start of action
        prevEnergyLevel = self.data[a.nodeIdSim]['consumedEnergy'][-1]
        self.data[a.nodeIdSim]['t'].append(a.startTime)
        self.data[a.nodeIdSim]['consumedEnergy'].append(prevEnergyLevel)  # assumption: only radio actions contribute to energy consumption

        # add point at end of action
        duration = a.endTime - a.startTime
        pwr      = None
        if isinstance(a, RadioRxAction):
          pwr = sx1262.getRxPower()
        elif isinstance(a, RadioTxAction):
          pwr = sx1262.getTxPower(a.radioConfig.txPower)

        newEnergyLevel = prevEnergyLevel + duration*pwr
        self.data[a.nodeIdSim]['t'].append(a.endTime)
        self.data[a.nodeIdSim]['consumedEnergy'].append(newEnergyLevel)

  def generatePlot(self, outFile):
    p = figure(
        title=None,
        plot_width=300,
        plot_height=300,
        min_border=0,
        # toolbar_location=None,
        sizing_mode = 'stretch_both',
        y_range=DataRange1d(start=0),
    )
    # p.add_tools(WheelZoomTool())
    hoverTool = HoverTool(
      tooltips=[
        ("Node", "$name"),
        ("Consumed Energy", "@y{0.000} J"),
        ("Time", "@x{0.000000} s"),
      ],
    )
    p.add_tools(hoverTool)

    # add functionality to reset by double-click
    p.js_on_event(DoubleTap, CustomJS(args=dict(p=p), code='p.reset.emit()'))

    # create a color iterator
    colors = itertools.cycle(palette)

    # plot line for all nodes
    for nodeIdSim in self.nodeIdSimList:
      p.line(
        x=self.data[nodeIdSim]['t'],
        y=self.data[nodeIdSim]['consumedEnergy'],
        line_width=2,
        color=next(colors),
        alpha=0.8,
        legend_label='Node {}'.format(self.sim.nodeIdSim2nodeId(nodeIdSim)),
        name=str(self.sim.nodeIdSim2nodeId(nodeIdSim))
      )

    p.xaxis.axis_label                 = "Time [s]"
    p.xaxis.axis_label_text_font_size  = "20pt"
    p.xaxis.major_label_text_font_size = "18pt"

    p.yaxis.axis_label                 = "Consumed Energy [J]"
    p.yaxis.major_label_text_font_size = "18pt"
    p.yaxis.axis_label_text_font_size  = "20pt"

    p.legend.location     = "top_left"
    p.legend.click_policy = "hide"

    curdoc().add_root(p)

    # show(p)

    os.makedirs(os.path.split(outFile)[0], exist_ok=True)
    output_file(outFile, title="Accumulated Energy")
    save(p)


################################################################################

if __name__ == '__main__':
  ep = AccumulatedEnergyPlot()
  ep.generatePlot('output/out.html')
